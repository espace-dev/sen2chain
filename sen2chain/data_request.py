# coding: utf-8

"""
Module for obtaining data from the scihub API.
"""

import logging
import pickle
import re
import sys
import fiona
from pathlib import Path
from datetime import datetime
from collections import OrderedDict
from sentinelsat import SentinelAPI
from fiona import collection
from osgeo import ogr
from shapely.geometry import shape, Point, Polygon, box, MultiPolygon
from shapely.wkt import loads
from shapely.ops import cascaded_union
from pprint import pprint
import itertools

# type annotations
from typing import List, Set, Dict, Tuple, Union

from .config import Config, SHARED_DATA

from .utils import grouper, str_to_datetime
from .geo_utils import (
    get_tiles_from_point,
    get_tiles_from_bbox,
    get_tiles_from_file,
    serialise_tiles_index,
)

# présentation problème #
# il n'est pas possible avec l'API du scihub de faire une recherche
# par tuile des images antérieures à 20161205
# pour contourner ce problème, on utilisera la géométrie de la footprint
# de la tuile d'intérêt


s2_tiles_index = SHARED_DATA.get("tiles_index")
s2_tiles_index_dict = SHARED_DATA.get("tiles_index_dict")

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class DataRequest:
    """Class for getting scihub API and retrieving products identifiers.

    :param start_date: query's start date (YYYY-MM-DD).
    :param end_date: query's end date(YYYY-MM-DD) or datetime objet.
    :param land_only: keep only tiles that contain land or datetime object.
    :param cloud_cover_percentage: cloud cover percentage range (min, max)
        from 0 to 100.

    Usage:
        >>> data_request.DataRequest(
                start_date="2018-01-10",
                end_date="2018-01-31,
                land_only=True).from_tiles["40KCB", "40KEC"]
    """

    # Proxy settings
    proxy_http_url = Config().get("proxy_http_url").strip()
    proxy_https_url = Config().get("proxy_https_url").strip()

    def __init__(
        self,
        start_date: Union[str, datetime] = None,
        end_date: Union[str, datetime] = None,
        land_only: bool = False,
        cloud_cover_percentage: Tuple[int, int] = None,
    ) -> None:

        if start_date is None:
            # default start_date : first sentinel2 acquisition
            self.start_date = str_to_datetime("2015-06-29", "ymd")
            logger.info(
                "Start date not provided, using {}.".format(self.start_date)
            )
        else:
            if not isinstance(start_date, datetime):
                start_date = str_to_datetime(start_date, "ymd")
            self.start_date = start_date

        if end_date is None:
            self.end_date = datetime.now()
            logger.info("No end date provided, using today's date.")
        else:
            if not isinstance(end_date, datetime):
                end_date = str_to_datetime(end_date, "ymd")
            self.end_date = end_date

        self.land_only = land_only
        self.tiles_to_keep = None
        self.tiles_to_keep_geom = dict()
        self.products_list = {}
        self.cloudcoverpercentage = (
            cloud_cover_percentage if cloud_cover_percentage else (0, 100)
        )
        self.api = SentinelAPI(
            Config().get("scihub_id"),
            Config().get("scihub_pwd"),
            "https://apihub.copernicus.eu/apihub/",
        )

        # Set proxy settings to the Requests session
        if self.proxy_http_url or self.proxy_https_url:
            proxies = {
                "http": self.proxy_http_url,
                "https": self.proxy_https_url,
            }
            self.api.session.proxies = proxies

    def _get_tiles_geom(self) -> None:
        """Returns the geometries of S2 tiles."""
        try:
            assert s2_tiles_index_dict.exists()
        except AssertionError:
            serialise_tiles_index()

        with open(str(s2_tiles_index_dict), "rb") as pfile:
            tiles_dict = pickle.load(pfile)

        for tile_kept in self.tiles_to_keep:
            try:
                self.tiles_to_keep_geom[tile_kept] = tiles_dict[tile_kept]
            except KeyError:
                logger.error("Incorrect tile name: {}".format(tile_kept))
                raise

    def from_tiles(self, tiles: List[str]) -> Dict[str, Dict]:
        """Makes request from a list of tiles.

        :param : tiles: list of valid tiles names.
        """
        self.tiles_to_keep = [
            re.sub("^T", "", tile.upper()) for tile in set(tiles)
        ]
        self._get_tiles_geom()
        self._make_request()
        return self.products_list

    def from_point(self, lon: float, lat: float) -> Dict[str, Dict]:
        """Makes request from longitude, latitude coordinates.

        :param lon: longitude.
        :param lat: latitude.
        """
        self.tiles_to_keep = get_tiles_from_point(
            lon, lat, land_only=self.land_only
        )
        self._get_tiles_geom()
        self.make_request()
        return self.products_list

    def from_bbox(
        self, lon_min: float, lat_min: float, lon_max: float, lat_max: float
    ) -> Dict[str, Dict]:
        """akes request from a bbox.

        :param lon_min: longitude.
        :param lat_min: latitude.
        :param lon_max: longitude.
        :param lat_max: latitude.
        """
        self.tiles_to_keep = get_tiles_from_bbox(
            lon_min, lat_min, lon_max, lat_max, land_only=self.land_only
        )
        self._get_tiles_geom()
        self._make_request()
        return self.products_list

    def from_file(self, vectors_file) -> Dict[str, Dict]:
        """Makes request from a vectors file.

        :param : tiles: list of valid tiles names.
        """
        geom_tiles = get_tiles_from_file(
            vectors_file, land_only=self.land_only
        )
        self.tiles_to_keep = list(
            set(itertools.chain.from_iterable(geom_tiles.values()))
        )
        self._get_tiles_geom()
        self._make_request()
        return self.products_list

    def _make_request(self) -> None:
        """Scihub API request using sentinelsat."""
        logger.debug("_make_request")
        logger.info(
            "Requesting images ranging from {} to {}".format(self.start_date, self.end_date)
        )

        if self.tiles_to_keep is None:
            raise ValueError("Query tiles not provided")

        print("Sentinel2 tiles:\n", self.tiles_to_keep)

        products = dict()

        # remove water-only tiles when land-only parameter is enabled
        if self.land_only:
            with open(str(s2_tiles_index_dict), "rb") as pfile:
                tiles_land = pickle.load(pfile)
                water_tiles = []
                for tile in self.tiles_to_keep.copy():
                    if tile not in tiles_land:
                        self.tiles_to_keep.remove(tile)
                        water_tiles.append(tile)
                        self.tiles_to_keep_geom.pop(tile)
            print("Ignoring water-only tiles:", water_tiles)

        query_kwargs = {
            "platformname": "Sentinel-2",
            "producttype": "S2MSI1C",
            "cloudcoverpercentage": self.cloudcoverpercentage,
            "date": (self.start_date, self.end_date),
        }

        products = OrderedDict()
        for tile in self.tiles_to_keep:
            kw = query_kwargs.copy()
            kw["filename"] = "*_T{}_*".format(tile)
            pp = self.api.query(**kw)
            products.update(pp)

        # save products list as a pandas dataframe
        products_df = self.api.to_dataframe(products)

        if products_df.empty:
            return
            # fill each dictionnary depending on the acquisition date
        for index, row in products_df[["title", "beginposition"]].iterrows():
            img_title = row[0]
            img_date = row[1].to_pydatetime()
            self.products_list[img_title] = {
                "date": img_date,
                "tile": re.findall("_T([0-9]{2}[A-Z]{3})_", img_title)[0],
            }

        # pprint dicts in chronological order
        pprint(
            list(
                OrderedDict(
                    sorted(
                        self.products_list.items(),
                        key=lambda t: t[1]["date"],
                    )
                )
            )
        )
