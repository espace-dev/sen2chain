# coding: utf-8

# FIXME: docs

import os
import shutil
import logging
import pathlib

from pathlib import Path

# type annotations
from typing import List, Dict
from itertools import chain

from .config import Config, SHARED_DATA
from .products import L1cProduct
from .tiles import Tile
from .utils import human_size, human_size_decimal
from .multi_processing import (
    l2a_multiprocessing,
    cld_version_probability_iterations_reprocessing_multiprocessing,
    idx_multiprocessing,
)

s2_tiles_index = SHARED_DATA.get("tiles_index")

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class TileSet:
    """
    Module to process l2a, cloud masks and indices from a tile list, using multiprocessing.
    """

    def __init__(
        self,
        tile_list: list = [],
    ):
        self.tile_list = tile_list
        if not self.tile_list:
            logger.info(
                "TileSet list cannot be empty, please provide a tile list while initializing class"
            )

    def __repr__(self) -> str:
        return "TileSet: {}".format(self.tile_list)

    def compute_l2a(
        self, date_min: str = None, date_max: str = None, nb_proc: int = 8
    ):
        """
        Compute all missing l2a for l1c products
        """
        l1c_process_list = []
        for tile in self.tile_list:
            t = Tile(tile)
            l1c_process_list.append(
                list(
                    p.identifier
                    for p in t.l2a_missings.filter_dates(
                        date_min=date_min, date_max=date_max
                    )
                )
            )
        l1c_process_list = list(chain.from_iterable(l1c_process_list))
        if l1c_process_list:
            logger.info(
                "{} L1C products to process:".format(len(l1c_process_list))
            )
            logger.info("{}".format(l1c_process_list))
            l2a_multiprocessing(l1c_process_list, nb_proc=nb_proc)
        else:
            logger.info("All L2A already computed")

    def compute_cloudmasks(
        self, date_min: str = None, date_max: str = None, nb_proc: int = 8
    ):
        """
        Compute all missing cloud masks for l2a products
        """
        cld_l2a_process_list = []
        for tile in self.tile_list:
            t = Tile(tile)
            cld_l2a_process_list.append(
                list(
                    p.identifier
                    for p in t.cloudmasks_missings.filter_dates(
                        date_min=date_min, date_max=date_max
                    )
                )
            )
        cld_l2a_process_list = list(chain.from_iterable(cld_l2a_process_list))
        if cld_l2a_process_list:
            logger.info(
                "{} L2A products to process:".format(len(cld_l2a_process_list))
            )
            logger.info("{}".format(cld_l2a_process_list))
            # ~ cld_multiprocessing(cld_l2a_process_list, nb_proc=nb_proc)
        else:
            logger.info("All cloud masks already computed")

    def compute_indices(
        self,
        indices: list = [],
        date_min: str = None,
        date_max: str = None,
        nb_proc: int = 8,
    ):
        """
        Compute all missing indices for l2a products
        - indices are given as a list
        - if indices not provided, will compute missing dates of already existing indices for each tile (no new indice computed)
        - ! indices won't be masked if no cloud masks are present, you have to compute cloudmasks first
        """
        indices_l2a_process_list = []
        for tile in self.tile_list:
            t = Tile(tile)
            if not indices:
                indices = list(t._paths["indices"].keys())
            else:
                indices = [indice.upper() for indice in indices]

            for i in indices:
                l2a_list = [
                    p.identifier
                    for p in t.missing_indices(i).filter_dates(
                        date_min=date_min, date_max=date_max
                    )
                ]
                for j in l2a_list:
                    indices_l2a_process_list.append([j, i])
        if indices_l2a_process_list:
            logger.info(
                "{} l2a products to process:".format(
                    len(indices_l2a_process_list)
                )
            )
            logger.info("{}".format(indices_l2a_process_list))
            idx_multiprocessing(indices_l2a_process_list, nb_proc=nb_proc)
        else:
            logger.info("All indices already computed")

    @property
    def info(self):
        for t in self.tile_list:
            logger.info(t)
            Tile(t).info
