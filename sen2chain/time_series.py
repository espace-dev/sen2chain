# coding: utf-8

"""
Module for computing time series.
"""

import logging
import pathlib
from pathlib import Path
from collections import OrderedDict
import fiona
import rasterio
from rasterio.plot import show
from rasterio.plot import plotting_extent
from rasterio.mask import mask
from rasterio.enums import Resampling
from fiona import transform
from shapely.geometry import shape
from shapely.geometry import mapping
from rasterstats import zonal_stats
import pandas as pd
import geopandas as gpd

# type annotations
from typing import Sequence, List, Dict, Union
import matplotlib.pyplot as plt
import numpy as np
import math
from datetime import timedelta
import time
import multiprocessing
import itertools
from functools import partial
import os.path

from .config import Config
from .tiles import Tile, ProductsList
from .indices import IndicesCollection
from .geo_utils import get_tiles_from_file
from .products import L2aProduct, IndiceProduct


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
# logging.basicConfig(format='%(process)s %(asctime)s %(levelname)s:%(module)s:%(message)s', level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')
logging.getLogger().handlers[0].setFormatter(
    logging.Formatter(
        "%(process)s:%(asctime)s:%(levelname)s:%(name)s:%(message)s",
        "%Y-%m-%d %H:%M:%S",
    )
)


class TimeSeries:
    """Class for time series extraction.

    :param vectors_file: path to the vector file.
    :param indices: list of valid indices names.
    :param date_min: starting date of the time series (YYYY-MM-DD format).
    :param date_max: ending date of the time series (YYYY-MM-DD format).
    :param cover_min: minimum cloud cover value. Default: 0.
    :param cover_max: minimum cloud cover value. Default: 100.

    Usage:
        >>> ts = TimeSeries(
                "polygons.geojson",
                indices=["NDVI"],
                date_min="2017-01-01",
                date_max="2018-01-01"
            )
    """

    def __init__(
        self,
        vectors_file: Union[str, pathlib.PosixPath],
        indices: Sequence[str] = None,
        date_min: str = None,
        date_max: str = None,
        cover_min: int = 0,
        cover_max: int = 100,
        field_names: str = None,
        multiproc: int = 0,
        getstat: bool = True,
        cm_version: str = "CM001",
        probability: int = 1,
        iterations: int = 5,
        cld_shad: bool = True,
        cld_med_prob: bool = True,
        cld_hi_prob: bool = True,
        thin_cir: bool = True,
        reproject: bool = False
    ) -> None:
        
        if reproject is True:
            data = gpd.read_file(vectors_file)
            new_vectorsfile = vectors_file[:-4]+"_wgs84.shp"
            data.to_crs(epsg=4326).to_file(new_vectorsfile)
            vectors_file = new_vectorsfile

        self._vectors_file = Path(vectors_file)
        self._date_min = date_min
        self._date_max = date_max
        self._cover_min = cover_min
        self._cover_max = cover_max
        self._field_names = field_names
        self._multiproc = multiproc
        self._out_path = Config().get("extraction_path")
        self.cm_version = cm_version
        self.probability = probability
        self.iterations = iterations
        self.cld_shad = cld_shad
        self.cld_med_prob = cld_med_prob
        self.cld_hi_prob = cld_hi_prob
        self.thin_cir = thin_cir

        if indices is None:
            self._indices_list = IndicesCollection.list
        else:
            for indice in indices:
                if indice not in IndicesCollection:
                    raise ValueError("Invald indice name: {}".format(indice))
            self._indices_list = indices

        if self._field_names:
            self._key = self._field_names
        else:
            self._key = "fid"

        self._tiles_geoms = self._get_tiles_geom_dict()
        self._df_dicts = dict()
        if getstat is True:
            if self._multiproc:
                self._get_stats_multiproc()
            else:
                self._get_stats()

    @property
    def data(self) -> Dict[str, pd.DataFrame]:
        """Returns a dictionnary with indices as keys and pandas.DataFrame as values."""
        return self._df_dicts

    @staticmethod
    def _is_indice_in_tile(indice: str, tile: Tile) -> bool:
        """Does the tile has a folder for this indice.

        :param indice: indice name.
        :param tile: Tile object.
        """
        if indice.lower() in tile.paths["indices"]:
            return True
        return False

    def _get_tiles_geom_dict(self) -> Dict[str, List[str]]:
        """{Feature id: list of intersected tiles} to {tile: list of intersected
        features ids}."""
        intersected_tiles_dict = get_tiles_from_file(
            self._vectors_file, land_only=False
        )
        tiles_geom = dict()
        for feat_id, tiles_list in intersected_tiles_dict.items():
            for tile in tiles_list:
                if tile not in tiles_geom:
                    tiles_geom[tile] = []
                tiles_geom[tile].append(feat_id)
        return tiles_geom

    @staticmethod
    def _filter_products(
        tile: Tile,
        indice: str,
        date_min: str,
        date_max: str,
        cover_min: int,
        cover_max: int,
        cm_version: str = "CM001",
        probability: int = 1,
        iterations: int = 5,
        cld_shad: bool = True,
        cld_med_prob: bool = True,
        cld_hi_prob: bool = True,
        thin_cir: bool = True,
    ) -> ProductsList:
        """Filters tile's indices products.

        :param tile: tile.
        :param indice: indice.
        :param date_min: oldest date.
        :param date_max: most recent date.
        :param cover_min: minimum cloud coverage.
        :param cover_max: maximum cloud coverage.
        """
        #test filtering with cm
        products = getattr(vars(tile)[
            indice.lower()
        ].masks,cm_version).params(
            probability = probability,
            iterations = iterations,
            cld_shad = cld_shad,
            cld_med_prob = cld_med_prob,
            cld_hi_prob = cld_hi_prob,
            thin_cir = thin_cir
        )
        filt = products.filter_dates(date_min, date_max).filter_clouds(
            cover_min, cover_max
        )
        return filt

    @staticmethod
    def _get_raster_stats_in_geom(
        feature: Dict,
        raster_path: Union[str, pathlib.PosixPath],
        cloud_path: str = None,
        cloud_proba_path: str = None,
    ) -> Dict:
        """Extracts statistics from a raster in a geometry.

        :param feature: GeoJSON like object.
        :param raster_path: path to the raster.
        """
        geom = shape(
            transform.transform_geom(
                "EPSG:4326",
                rasterio.open(str(raster_path)).crs["init"],
                feature["geometry"],
            )
        )

        # with rasterio.open(str(raster_path)) as raster_src:
        # raster_profile = raster_src.profile
        # logger.info(raster_profile)

        stats = zonal_stats(
            geom,
            str(raster_path),
            stats=[
                "count",
                "nodata",
                "min",
                "max",
                "median",
                "mean",
                "std",
                "percentile_25",
                "percentile_75",
            ],
        )[0]
        # logger.info(stats)

        if cloud_path:
            # def mycount(x):
            # return np.count_nonzero(x == 1)
            # with rasterio.open(str(cloud_path)) as cld_src:
            # cld_profile = cld_src.profile
            # cld_array = cld_src.read(1, out_shape = (10980, 10980), resampling=Resampling.nearest)
            # cld_array = cld_src.read(1)
            # logger.info("shape: {}".format(cld_array.shape))

            # stats_cld = zonal_stats(geom, cld_array, affine=raster_profile["transform"] , stats=["count", "nodata"], add_stats={'cldcount':mycount})
            # stats_cld = zonal_stats(geom, cld_array, affine=raster_profile["transform"], categorical=True)[0]
            # stats_cld = zonal_stats(geom, str(cloud_path), nodata = 16383.0, categorical=True)[0]
            # stats_cld = zonal_stats(geom, str(cloud_path), stats=["count", "nodata"], add_stats={'cldcount':mycount})
            # stats_cld = zonal_stats(geom, str(cloud_path), categorical=True, nodata = 16383.0, category_map= {-999: 'tttttttttttt', 1.0: 'clds', 0.0:'noclds', 16383.0: 'nnnnnnn'})[0]
            stats_cld = zonal_stats(
                geom, str(cloud_path), stats=["count", "nodata"]
            )[0]

            # logger.info(stats_cld)
            try:
                nbcld = stats["nodata"] - stats_cld["nodata"]
            except Exception:
                nbcld = 0

            # logger.info(stats_cld)
            # logger.info(cld_pct)
            stats["nbcld"] = nbcld

        if cloud_proba_path:
            stats_cld_prb = zonal_stats(
                geom, str(cloud_proba_path), stats=["mean"]
            )[0]
            stats["cldprb"] = stats_cld_prb["mean"]
        logger.info(stats)

        # mettre ici le cloud mask !

        return stats

    def _get_stats(self) -> None:
        start = time.time()
        """Compute stats in polygons."""
        with fiona.open(str(self._vectors_file), "r") as vectors:
            features = {feat["id"]: feat for feat in vectors}
            for index2, indice in enumerate(self._indices_list):
                logger.info(
                    "computing {}/{}: {}".format(
                        index2 + 1, len(self._indices_list), indice
                    )
                )
                rows_list = []

                # for tile, fid_list in self._tiles_geoms.items():
                for index3, tile in enumerate(self._tiles_geoms):
                    fid_list = self._tiles_geoms[tile]
                    tile_obj = Tile(tile)

                    logger.info(
                        "Tile {}/{}: {}".format(
                            index3 + 1, len(self._tiles_geoms), tile
                        )
                    )

                    if TimeSeries._is_indice_in_tile(indice, tile_obj):
                        tile_indice_path = tile_obj.paths["indices"][
                            indice.lower()
                        ]
                        products = TimeSeries._filter_products(
                            tile_obj,
                            indice,
                            self._date_min,
                            self._date_max,
                            self._cover_min,
                            self._cover_max,
                            self.cm_version,
                            self.probability,
                            self.iterations,
                            self.cld_shad,
                            self.cld_med_prob,
                            self.cld_hi_prob,
                            self.thin_cir,
                        )

                        for index1, prod in enumerate(products):
                            indice_product = IndiceProduct(prod.identifier)
                            # prod_path = str(indice_product.path)
                            # prod_path_unmasked = prod_path.replace(
                                # "_CM001", ""
                            # )
                            # l2a = L2aProduct(indice_product.l2a)
                            # if l2a.path.exists():
                                # prod_path_cloud_proba = l2a.msk_cldprb_20m
                            # else:
                                # prod_path_cloud_proba = None
                            # prod_path = tile_indice_path / prod.identifier[:(-12 - len(indice))] / prod.identifier
                            # cloud_path = tile_obj.paths["l2a"] / (prod.identifier[:(-12 - len(indice))] + "_CLOUD_MASK.jp2")
                            # prod_path_unmasked = tile_indice_path / prod.identifier[:(-12 - len(indice))] / (prod.identifier[:-11] + '.jp2')
                            # prod_path_cloud_proba = L2aProduct(prod.identifier[:(-12 - len(indice))]).msk_cldprb_20m
                            # logger.info(prod_path_cloud_proba)

                            logger.info(
                                "Product {}/{}: {}".format(
                                    index1 + 1, len(products), prod.identifier
                                )
                            )
                            logger.info("{} features".format(len(fid_list)))

                            for index, fid in enumerate(fid_list):
                                df_dict = OrderedDict()
                                df_dict["fid"] = fid

                                # feat_properties = features[fid]["properties"]
                                # if feat_properties:
                                # df_dict.update(feat_properties)
                                df_dict.update(
                                    TimeSeries._get_raster_stats_in_geom(
                                        features[fid],
                                        str(indice_product.path),
                                        str(indice_product.unmasked_path),
                                        indice_product.msk_cldprb_20m,
                                    )
                                )

                                # df_properties = features[fid]["properties"]
                                df_dict["date"] = prod.date
                                df_dict["tile"] = tile
                                df_dict["filename"] = prod.identifier

                                for prop in features[fid]["properties"]:
                                    df_dict[prop] = features[fid][
                                        "properties"
                                    ][prop]

                                rows_list.append(df_dict)
                if rows_list:
                    self._df_dicts[indice] = TimeSeries._list_to_df(rows_list)
        end = time.time()
        logger.info(
            "Execution time: {}".format(timedelta(seconds=end - start))
        )

    def _raster_stats_multi(self, features, shared_list, proc_item):
        indice_product = IndiceProduct(proc_item[0].identifier)
        # logger.info(
            # "masque nuages path: {} - {}".format(
                # indice_product.identifier,
                # str(indice_product.msk_cldprb_20m)
            # )
        # )
        # prod_path = str(indice_product.path)
        # prod_path_unmasked = prod_path.replace("_CM001", "")
        # l2a = L2aProduct(indice_product.l2a)
        # if l2a.path.exists():
            # prod_path_cloud_proba = l2a.msk_cldprb_20m
        # else:
            # prod_path_cloud_proba = None
        # prod_path = proc_item[2] / proc_item[0].identifier[:(-12 - len(proc_item[3]))] / proc_item[0].identifier
        # prod_path_unmasked = proc_item[2] / proc_item[0].identifier[:(-12 - len(proc_item[3]))] / (proc_item[0].identifier[:-11] + '.jp2')
        # prod_path_cloud_proba = L2aProduct(proc_item[0].identifier[:(-12 - len(proc_item[3]))]).msk_cldprb_20m
        # logger.info(prod_path_cloud_proba)

        fid = proc_item[1]
        result_dict = OrderedDict()
        result_dict["fid"] = fid
        result_dict.update(
            TimeSeries._get_raster_stats_in_geom(
                features[fid],
                str(indice_product.path),
                str(indice_product.unmasked_path),
                indice_product.msk_cldprb_20m,
            )
        )
        result_dict["date"] = proc_item[0].date
        result_dict["tile"] = proc_item[4]
        result_dict["filename"] = proc_item[0].identifier
        for prop in features[fid]["properties"]:
            # if type(features[fid]["properties"][prop]) == float:
            # result_dict[prop] = "{:.6f}".format(features[fid]["properties"][prop])
            # logger.info("toto {}".format(result_dict[prop]))
            # else:
            # result_dict[prop] = features[fid]["properties"][prop]
            # logger.info("tata {}".format(result_dict[prop]))
            result_dict[prop] = features[fid]["properties"][prop]
        shared_list.append(result_dict)

    def _get_stats_multiproc(self,) -> None:
        start = time.time()
        """Compute stats in polygons."""
        with fiona.open(str(self._vectors_file), "r") as vectors:
            features = {feat["id"]: feat for feat in vectors}
            for index2, indice in enumerate(self._indices_list):
                logger.info(
                    "computing {}/{}: {}".format(
                        index2 + 1, len(self._indices_list), indice
                    )
                )
                # rows_list = []
                manager = multiprocessing.Manager()
                shared_list = manager.list()

                # for tile, fid_list in self._tiles_geoms.items():
                for index3, tile in enumerate(self._tiles_geoms):
                    fid_list = self._tiles_geoms[tile]
                    tile_obj = Tile(tile)

                    logger.info(
                        "Tile {}/{}: {}".format(
                            index3 + 1, len(self._tiles_geoms), tile
                        )
                    )

                    if TimeSeries._is_indice_in_tile(indice, tile_obj):
                        tile_indice_path = tile_obj.paths["indices"][
                            indice.lower()
                        ]
                        products = TimeSeries._filter_products(
                            tile_obj,
                            indice,
                            self._date_min,
                            self._date_max,
                            self._cover_min,
                            self._cover_max,
                            self.cm_version,
                            self.probability,
                            self.iterations,
                            self.cld_shad,
                            self.cld_med_prob,
                            self.cld_hi_prob,
                            self.thin_cir,
                        )

                        proc_list = list(
                            itertools.product(
                                products,
                                fid_list,
                                [tile_indice_path],
                                [indice],
                                [tile],
                            )
                        )
                        logger.info("{} extractions".format(len(proc_list)))

                        pool = multiprocessing.Pool(self._multiproc)
                        results = [
                            pool.map(
                                partial(
                                    self._raster_stats_multi,
                                    features,
                                    shared_list,
                                ),
                                proc_list,
                            )
                        ]
                        pool.close()
                        pool.join()

                        # logger.info("{}".format(shared_list))
                        # for index1, prod in enumerate(products):
                        rows_list = list(shared_list)
                        # logger.info("rows_list {}".format(rows_list))
                        if rows_list:
                            self._df_dicts[indice] = TimeSeries._list_to_df(
                                rows_list
                            )

                #             prod_path = tile_indice_path / prod.identifier[:(-12 - len(indice))] / prod.identifier
                #             logger.info("Product {}/{}: {}".format(index1 + 1, len(products), prod.identifier))
                #             logger.info("{} features".format(len(fid_list)))

                #             for index, fid in enumerate(fid_list):
                #                 df_dict = OrderedDict()
                #                 df_dict["fid"] = fid

                #                 # feat_properties = features[fid]["properties"]
                #                 # if feat_properties:
                #                     # df_dict.update(feat_properties)
                #                 df_dict.update(TimeSeries._get_raster_stats_in_geom(features[fid], prod_path))

                #                 # df_properties = features[fid]["properties"]
                #                 df_dict["date"] = prod.date
                #                 df_dict["tile"] = tile
                #                 df_dict["filename"] = prod.identifier

                #                 for prop in features[fid]["properties"]:
                #                     df_dict[prop] = features[fid]["properties"][prop]

                #                 rows_list.append(df_dict)
                # if rows_list:
                #     self._df_dicts[indice] = TimeSeries._list_to_df(rows_list)
        end = time.time()
        logger.info(
            "Execution time: {}".format(timedelta(seconds=end - start))
        )

    @staticmethod
    def _list_to_df(rows_list: List[Dict]) -> pd.DataFrame:
        """Converts a list of dictionaries as a pandas dataframe.

        :param rows_list: list of dictionaries.
        """
        df = pd.DataFrame.from_dict(rows_list)
        df = df.sort_values(by=["date"])
        df.set_index("date", inplace=True, drop=False)
        del df["date"]
        df = df.dropna(subset=["mean"])
        return df

    def to_csv(self, out_path: Union[str, pathlib.PosixPath] = None) -> None:
        """Exports the time series to CSV format.

        :param out_path: output folder. Default is DATA/EXTRACTION.
        """
        if out_path is None:
            out_path = self._out_path
        out_path_folder = Path(out_path) / self._vectors_file.stem
        out_path_folder.mkdir(parents=True, exist_ok=True)

        list_order = [
            "fid",
            "tile",
            "filename",
            "count",
            "nodata",
            "nbcld",
            "cldprb",
            "min",
            "max",
            "mean",
            "std",
            "median",
            "percentile_25",
            "percentile_75",
        ]
        # b=[a for a in df.columns if a not in liste]

        for df_name, df in self._df_dicts.items():
            csv_path = out_path_folder / "{0}_{1}_{2}_{3}.csv".format(
                self._vectors_file.stem,
                df_name,
                self._date_min,
                self._date_max,
            )
            # df.sort_values(by=['fid', 'date', 'count', 'tile']).reindex(columns=(list_order + [a for a in df.columns if a not in list_order]), copy=False).to_csv(str(csv_path))
            df["fid"] = pd.to_numeric(df["fid"])
            dg = (
                df.sort_values(
                    by=["fid", "date", "count", "std"],
                    ascending=[True, True, True, False],
                )
                .reindex(
                    columns=(
                        list_order
                        + [a for a in df.columns if a not in list_order]
                    ),
                    copy=False,
                )
                .reset_index()
                .drop_duplicates(["date", "fid"], keep="last")
            )

            dg.set_index("date", inplace=True, drop=True)
            dg.replace(
                to_replace=[r"\\t|\\n|\\r", "\t|\n|\r"],
                value=["", ""],
                regex=True,
                inplace=True,
            )
            dg.to_csv(str(csv_path))
            logger.info("exported to csv: {}".format(csv_path))

    def plot_global(
        self, out_path: Union[str, pathlib.PosixPath] = None
    ) -> None:
        """Exports the time series to CSV format.

        :param out_path: output folder. Default is DATA/EXTRACTION.
        """
        if out_path is None:
            out_path = self._out_path
        out_path_folder = Path(out_path) / self._vectors_file.stem
        out_path_folder.mkdir(parents=True, exist_ok=True)

        for df_name, df in self._df_dicts.items():
            png_path = (
                out_path_folder
                / "{0}_plot-global_{1}_{2}_{3}.png".format(
                    self._vectors_file.stem,
                    df_name,
                    self._date_min,
                    self._date_max,
                )
            )
            df = self.data[df_name]
            plt.figure(figsize=(19.2, 10.8))
            if df_name in ["NDVI", "NDWIGAO", "NDWIMCF", "MNDWI"]:
                plt.ylim((-10000, 10000))
            else:
                plt.ylim((0, 10000))
            df.dropna(subset=["mean"]).groupby(self._key)["mean"].plot(
                legend=True
            )
            plt.title(df_name)
            plt.savefig(str(png_path))
            plt.close()
            logger.info("Plot saved to png: {}".format(png_path))

    def plot_details(
        self, out_path: Union[str, pathlib.PosixPath] = None
    ) -> None:
        """Exports the time series to CSV format.

        :param out_path: output folder. Default is DATA/EXTRACTION.
        """
        if out_path is None:
            out_path = self._out_path
        out_path_folder = Path(out_path) / self._vectors_file.stem
        out_path_folder.mkdir(parents=True, exist_ok=True)
        for df_name, df in self._df_dicts.items():
            png_path = (
                out_path_folder
                / "{0}_plot-details_{1}_{2}_{3}.png".format(
                    self._vectors_file.stem,
                    df_name,
                    self._date_min,
                    self._date_max,
                )
            )
            png_path_nonan = (
                out_path_folder
                / "{0}_plot-details_{1}-nonan_{2}_{3}.png".format(
                    self._vectors_file.stem,
                    df_name,
                    self._date_min,
                    self._date_max,
                )
            )

            if df_name in ["NDVI", "NDWIGAO", "NDWIMCF", "MNDWI"]:
                ylim = [-10000, 10000]
            else:
                ylim = [0, 10000]

            df = self.data[df_name]
            grouped = df.groupby(self._key)
            ncols = int(math.ceil(len(grouped) ** 0.5))
            nrows = int(math.ceil(len(grouped) / ncols))
            fig, axs = plt.subplots(nrows, ncols, figsize=(19.2, 10.8))
            for (name, dfe), ax in zip(grouped, axs.flat):
                ax.set_ylim(ylim)
                ax.set_title(name)
                dfe.dropna(subset=["mean"]).plot(
                    y=["mean"],
                    ax=ax,
                    yerr="std",
                    color="black",
                    elinewidth=0.2,
                    legend=False,
                )
                dfe.dropna(subset=["mean"]).plot(
                    y=["min", "max"],
                    ax=ax,
                    linewidth=0.25,
                    color="black",
                    legend=False,
                )
                ax2 = ax.twinx()
                ax2.set_ylim([0, 1])
                dfe["na_ratio"] = dfe["nodata"] / (
                    dfe["count"] + dfe["nodata"]
                )
                dfe.dropna(subset=["mean"]).plot(
                    y=["na_ratio"],
                    marker="o",
                    ax=ax2,
                    color="red",
                    linewidth=0.25,
                    linestyle="",
                    legend=False,
                )
                # dfe.dropna().plot.bar(y=['na_ratio'], ax=ax2, color='red',legend=False)
                ax.plot(np.nan, "-r", label="NaN ratio")
                # ax.legend(loc=0, prop={'size': 6})
                ax.legend(loc=0, labelspacing=0.05)
            fig.tight_layout()
            fig.suptitle(df_name, fontsize=16)
            plt.savefig(str(png_path))
            plt.close()
            logger.info("Plot saved to png: {}".format(png_path))

            try:
                fig, axs = plt.subplots(nrows, ncols, figsize=(19.2, 10.8))
                for (name, dfe), ax in zip(grouped, axs.flat):
                    ax.set_ylim(ylim)
                    ax.set_title(name)
                    dfe = dfe.dropna(subset=["mean"])
                    dfe = dfe[(dfe[["nodata"]] == 0).all(axis=1)]
                    dfe.plot(
                        y=["mean"],
                        ax=ax,
                        yerr="std",
                        color="black",
                        elinewidth=0.2,
                        legend=False,
                    )
                    dfe.plot(
                        y=["min", "max"],
                        ax=ax,
                        linewidth=0.25,
                        color="black",
                        legend=False,
                    )
                    ax.legend(loc=0, labelspacing=0.05)
                fig.tight_layout()
                fig.suptitle(df_name, fontsize=16)
                plt.savefig(str(png_path_nonan))
                plt.close()
                logger.info("Plot saved to png: {}".format(png_path_nonan))
            except Exception:
                logger.info("No data to plot (no date with 0 nodata)")
                pass

    def extract_ql(
        self, out_path: Union[str, pathlib.PosixPath] = None
    ) -> None:
        """Extract ql images around vectors."""
        if out_path is None:
            out_path = Config().get("extraction_path")
        out_path_folder = Path(out_path) / self._vectors_file.stem
        out_path_folder.mkdir(parents=True, exist_ok=True)

        for df_name, df in self.data.items():
            fid_list = dict(zip(df.fid, df[self._key]))
            if not self._field_names:
                fid_list = fid_list.fromkeys(fid_list, "")

            cmap_dict = {
                "NDVI": "RdYlGn",
                "NDWIGAO": "RdYlBu",
                "NDWIMCF": "RdYlBu",
                "BIGR": "pink",
                "BIRNIR": "afmhot",
                "BIBG": "bone",
                "MNDWI": "BrBG",
            }
            if df_name in ["NDVI", "NDWIGAO", "NDWIMCF", "MNDWI"]:
                vmin = -10000
                vmax = 10000
            else:
                vmin = 0
                vmax = 10000

            for fid, name in fid_list.items():
                if name:
                    fidname = name
                else:
                    fidname = "FID" + fid
                out_path_fid_folder = out_path_folder / str("QL_" + fidname)
                out_path_fid_folder.mkdir(parents=True, exist_ok=True)
                indice_png_path = (
                    out_path_fid_folder
                    / "{0}_MOZ-QL_{1}_{2}_{3}_{4}.jpg".format(
                        self._vectors_file.stem,
                        fidname,
                        df_name,
                        self._date_min,
                        self._date_max,
                    )
                )
                l2a_png_path = (
                    out_path_fid_folder
                    / "{0}_MOZ-QL_{1}_{2}_{3}_{4}.jpg".format(
                        self._vectors_file.stem,
                        fidname,
                        "L2A",
                        self._date_min,
                        self._date_max,
                    )
                )
                logger.info("fid/name:{}".format(fidname))
                nb_prod = len(df.loc[df["fid"] == fid])
                ncols = int(math.ceil(nb_prod ** 0.5))
                nrows = int(math.ceil(nb_prod / ncols))
                figa, axs = plt.subplots(
                    nrows,
                    ncols,
                    figsize=(5 * ncols, 5 * nrows),
                    sharey=True,
                    sharex=True,
                )
                figb, bxs = plt.subplots(
                    nrows,
                    ncols,
                    figsize=(5 * ncols, 5 * nrows),
                    sharey=True,
                    sharex=True,
                )
                for (index, row), ax, bx in zip(
                    (df.loc[df["fid"] == fid])
                    .sort_values(by=["date"])
                    .iterrows(),
                    np.array(axs).flatten(),
                    np.array(bxs).flatten(),
                ):
                    # logger.info("row[filename]: {}".format(row['filename']))
                    prod_id = IndiceProduct(row["filename"]).l2a
                    # prod_id = row['filename'][:(-12 - len(df_name))]
                    indice_png_tile_path = (
                        out_path_fid_folder
                        / "{0}_QL_{1}_{2}_{3}_{4}_{5}.jpg".format(
                            self._vectors_file.stem,
                            fidname,
                            df_name,
                            prod_id[11:19],
                            prod_id[39:44],
                            prod_id[0:2],
                        )
                    )
                    l2a_png_tile_path = (
                        out_path_fid_folder
                        / "{0}_QL_{1}_{2}_{3}_{4}_{5}.jpg".format(
                            self._vectors_file.stem,
                            fidname,
                            "L2A",
                            prod_id[11:19],
                            prod_id[39:44],
                            prod_id[0:2],
                        )
                    )
                    tile_obj = Tile(row["tile"])
                    tile_indice_path = tile_obj.paths["indices"][
                        df_name.lower()
                    ]
                    tile_l2a_path = tile_obj.paths["l2a"]
                    prod_path = tile_indice_path / prod_id / row["filename"]
                    tci_path = L2aProduct(prod_id).tci_10m
                    # tci_path = list(Path(str(tile_l2a_path / row['filename'][:(-12 - len(df_name))])+ '.SAFE/')\
                    # .glob('GRANULE/*/IMG_DATA/R10m/*_TCI_10m.jp2'))

                    crop_extent = gpd.read_file(str(self._vectors_file))
                    logger.info(tci_path)
                    raster_tci = rasterio.open(tci_path)
                    crop_extent_new_proj = crop_extent.to_crs(raster_tci.crs)
                    extent_geojson = mapping(
                        crop_extent_new_proj["geometry"][int(fid)].buffer(1000)
                    )

                    with rasterio.open(tci_path) as tci_data:
                        tci_data_crop, tci_data_crop_affine = mask(
                            tci_data, [extent_geojson], crop=True
                        )
                    tci_crop = np.dstack(
                        (tci_data_crop[0], tci_data_crop[1], tci_data_crop[2])
                    )
                    tci_data_extent = plotting_extent(
                        tci_data_crop[0], tci_data_crop_affine
                    )
                    ax.imshow(tci_crop, extent=tci_data_extent)
                    crop_extent_new_proj.loc[[int(fid)], "geometry"].plot(
                        ax=ax, facecolor="none", edgecolor="black", linewidth=3
                    )
                    ax.set_title(
                        "{} - {}".format(prod_id[11:19], prod_id[39:44]),
                        fontsize=10,
                    )

                    fig2, ax2 = plt.subplots(1, 1, figsize=(4, 4))
                    plt.subplots_adjust(
                        left=0.1, right=0.9, top=0.9, bottom=0.1
                    )
                    ax2.imshow(tci_crop, extent=tci_data_extent)
                    crop_extent_new_proj.loc[[int(fid)], "geometry"].plot(
                        ax=ax2,
                        facecolor="none",
                        edgecolor="black",
                        linewidth=3,
                    )
                    ax2.set_axis_off()
                    fig2.suptitle(name)
                    ax2.set_title(prod_id[39:44] + " " + prod_id[11:19])
                    fig2.tight_layout()
                    fig2.savefig(str(l2a_png_tile_path), bbox_inches="tight")
                    plt.close(fig=fig2)

                    raster = rasterio.open(prod_path)
                    nrg = raster.read(1)
                    with rasterio.open(prod_path) as img_data:
                        img_data_crop, img_data_crop_affine = mask(
                            img_data, [extent_geojson], crop=True
                        )
                    img_data_extent = plotting_extent(
                        img_data_crop[0], img_data_crop_affine
                    )
                    bx.imshow(
                        img_data_crop[0],
                        extent=img_data_extent,
                        cmap=cmap_dict[df_name],
                        vmin=vmin,
                        vmax=vmax,
                    )
                    crop_extent_new_proj.loc[[int(fid)], "geometry"].plot(
                        ax=bx, facecolor="none", edgecolor="black", linewidth=3
                    )
                    bx.set_title(
                        "{} - {}".format(prod_id[11:19], prod_id[39:44]),
                        fontsize=10,
                    )

                    fig_t_ind, ax_t_ind = plt.subplots(1, 1, figsize=(4, 4))
                    plt.subplots_adjust(
                        left=0.1, right=0.9, top=0.9, bottom=0.1
                    )
                    ax_t_ind.imshow(
                        img_data_crop[0],
                        extent=img_data_extent,
                        cmap=cmap_dict[df_name],
                        vmin=vmin,
                        vmax=vmax,
                    )
                    crop_extent_new_proj.loc[[int(fid)], "geometry"].plot(
                        ax=ax_t_ind,
                        facecolor="none",
                        edgecolor="black",
                        linewidth=3,
                    )
                    ax_t_ind.set_axis_off()
                    fig_t_ind.suptitle(name)
                    ax_t_ind.set_title(prod_id[39:44] + " " + prod_id[11:19])
                    fig_t_ind.tight_layout()
                    fig_t_ind.savefig(
                        str(indice_png_tile_path), bbox_inches="tight"
                    )
                    plt.close(fig=fig_t_ind)

                figa.suptitle(fidname + " - L2A")
                figa.savefig(str(l2a_png_path), bbox_inches="tight")
                plt.close(fig=figa)

                figb.suptitle(fidname + " - " + df_name)
                figb.savefig(str(indice_png_path), bbox_inches="tight")
                plt.close(fig=figb)
