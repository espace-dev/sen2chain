# -*- coding:utf-8 -*-

# FIXME: delete file ?

"""
    This python script should be run from terminal for multiprocessing l1c-> l2a
"""

import sys
import setuptools
from sen2chain import L1cProduct

identifier = sys.argv[1]
copy_l2a_sideproducts = bool(setuptools.distutils.util.strtobool(str(sys.argv[2])))

l1c = L1cProduct(identifier)
l1c.process_l2a(copy_l2a_sideproducts = copy_l2a_sideproducts)
