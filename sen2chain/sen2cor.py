# coding: utf-8

"""
Module for sen2cor processings.
"""

import logging
import pathlib
import subprocess

from typing import Union

from .config import Config
from .utils import (
    get_Sen2Cor_version, 
    get_latest_Sen2Cor_version_from_id,
    get_Sen2Cor_path_from_version,
)


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def process_sen2cor(
    l1c_product_path: Union[str, pathlib.PosixPath],
    l2a_product_path: Union[str, pathlib.PosixPath],
    s2c_path: Union[str, pathlib.PosixPath] = None,
    pb: str = "99.99",
    resolution: int = 10,
) -> None:
    """Process a L1C SAFE folder using sen2cor thanks to a subprocess call.
    The sen2cor bashrc file must be specified in the sen2chain configuration.

    :param l1c_product_path: path to the L1C SAFE folder.
    :param resolution: resolution to process.
    """
    # workaround for sen2cor 2.5.5 crash
    # 20 m resolution bands must be processed before 10 m
    # see: https://forum.step.esa.int/t/problem-with-sen2cor-2-5-5-version/10324/20

    # TODO: Add 60m resolution.

    s2c_path = s2c_path or get_Sen2Cor_path_from_version(
        get_latest_Sen2Cor_version_from_id(
            pathlib.Path(
                l1c_product_path
            ).stem
        )
    )
    if s2c_path:
        s2c_v = get_Sen2Cor_version(s2c_path)
        l2a_product_path_tmp = l2a_product_path.parent / (l2a_product_path.stem + '.tmp')
        if s2c_v == '02.05.05':
            logger.info("sen2cor {} processing: {}".format(s2c_v, l1c_product_path))
            if resolution == 10:
                logger.info("sen2cor processing 20 m: {}".format(l1c_product_path))
                command1 = "/bin/bash, -c, source {sen2cor_bashrc} && L2A_Process --resolution {res} {l1c_folder}".format(
                    sen2cor_bashrc=str(s2c_path),
                    res=20,
                    l1c_folder=str(l1c_product_path)
                    )
                process1 = subprocess.run(command1.split(", "))
                logger.info("sen2cor processing 10 m: {}".format(l1c_product_path))
                command2 = "/bin/bash, -c, source {sen2cor_bashrc} && L2A_Process --resolution {res} {l1c_folder}".format(
                    sen2cor_bashrc=str(s2c_path),
                    res=10,
                    l1c_folder=str(l1c_product_path)
                )
                process2 = subprocess.run(command2.split(", "))
            
            else:
                logger.debug("sen2cor processing: {}".format(l1c_product_path))
                command = "/bin/bash, -c, source {sen2cor_bashrc} && L2A_Process --resolution {resolution} {l1c_folder}".format(
                    sen2cor_bashrc=str(s2c_path),
                    resolution=resolution,
                    l1c_folder=str(l1c_product_path)
                )
                process = subprocess.run(command.split(", "))
                
        elif s2c_v in ['02.08.00','02.09.00', '02.10.01', '02.11.00', "02.12.03"]:
            logger.info("sen2cor {} processing: {}".format(s2c_v, l1c_product_path))
            command = "/bin/bash, -c, source {sen2cor_bashrc} && L2A_Process --processing_baseline {processing_baseline} --output_dir {out_dir} {l1c_folder}".format(
                sen2cor_bashrc = str(s2c_path),
                processing_baseline = pb,
                out_dir = l2a_product_path_tmp,
                l1c_folder = str(l1c_product_path)
            )
            process = subprocess.run(command.split(", "))
            sorted(l2a_product_path_tmp.glob("*.SAFE"))[0].rename(l2a_product_path.parent / (l2a_product_path.stem + '.SAFE'))
            l2a_product_path_tmp.rmdir()
        elif s2c_v is not None:
            logger.info('The provided Sen2Cor version {} is not compatible with Sen2Chain yet'.format(s2c_v))
        
        else:
            logger.info('Could not determine Sen2Cor version from provided path, please check pattern "Sen2Cor-**.**.**" is in path')

    else:
        logger.info('Could not determine a compatible installed sen2cor version to process this product')
