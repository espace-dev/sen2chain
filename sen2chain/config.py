# coding: utf-8

"""
Module for collecting configuration data from 
``~/sen2chain_data/config/config.cfg``
"""

import os
import logging
from pathlib import Path
from configparser import ConfigParser


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

ROOT = Path(os.path.realpath(__file__)).parent.parent

SHARED_DATA = dict(
    tiles_index=ROOT / "sen2chain" / "data" / "tiles_index.gpkg",
    tiles_index_dict=ROOT / "sen2chain" / "data" / "tiles_index_dict.p",
    peps_download=ROOT / "sen2chain" / "peps_download3.py",
    sen2chain_meta=ROOT / "sen2chain" / "data" / "sen2chain_info.xml",
    raw_job_cfg=ROOT / "sen2chain" / "data" / "job_ini.cfg",
    raw_job_extr_cfg=ROOT / "sen2chain" / "data" / "job_ini_extraction.cfg",
)


class Config:
    """Class for loading, checking, and retrieving configuration parameters.
    The class reads during its first initialization the configurable settings
    from the configuration file.

    Usage::
        >>> Config().get("l1c_path")
    """

    _USER_DIR = Path.home() / "sen2chain_data"
    _CONFIG_DIR = _USER_DIR / "config"
    _DEFAULT_DATA_DIR = _USER_DIR / "data"
    _CONFIG_FILE = _CONFIG_DIR / "config.cfg"
    _TILES_TO_WATCH = _CONFIG_DIR / "tiles_to_watch.csv"
    _JOBS_DIR = _CONFIG_DIR / "jobs"
    _JOBS_EXTR_DIR = _CONFIG_DIR /"jobs_extr"

    # TODO: Implement the Config class as a singleton.
    # Note: Below breaks for some reason (e.g. downloads), needs more testing.
    # def __new__(self):
    #     if not hasattr(self, 'instance'):
    #         self.instance = super(Config, self).__new__(self)
    #     return self.instance

    def __init__(self) -> None:

        self._config_params = ConfigParser()
        self._config_params["DATA PATHS"] = {
            "temp_path": "",
            "l1c_path": "",
            "l1c_archive_path": "",
            "l2a_path": "",
            "l2a_archive_path": "",
            "indices_path": "",
            "time_series_path": "",
            "temporal_summaries_path": "",
            "cloudmasks_path": "",
            "extraction_path": "",
        }
        self._config_params["SEN2COR PATH"] = {
            "sen2cor_bashrc_paths": "",
        }
        self._config_params["HUBS LOGINS"] = {
            "scihub_id": "",
            "scihub_pwd": "",
            "peps_config_path": "",
        }
        self._config_params["PROXY SETTINGS"] = {
            "proxy_http_url": "",
            "proxy_https_url": "",
        }
        self._config_params["SEN2CHAIN VERSIONS"] = {
            "sen2chain_processing_version": "xx.xx"
        }

        self._config_params["LOG PATH"] = {
            "log_path": str(self._USER_DIR / "logs")
        }

        if self._CONFIG_FILE.exists():
            self._config_params.read(str(self._CONFIG_FILE))
            self._config_params_disk = ConfigParser()
            self._config_params_disk.read(str(self._CONFIG_FILE))
            if self._config_params_disk != self._config_params:
                self._create_config()
        else:
            self._create_config()

        # Override the Sen2cor Bashrc path with an environment variable if it exists
        env_sen2cor_bashrc_paths = os.environ.get("SEN2CHAIN_SEN2COR_BASHRC_PATHS","")
        if env_sen2cor_bashrc_paths:
            self._config_params["SEN2COR PATH"]["sen2cor_bashrc_paths"] = env_sen2cor_bashrc_paths

        self.config_dict = dict()

        for section in self._config_params.sections():
            for key in self._config_params[section].keys():
                self.config_dict[key] = self._config_params[section][key]

        self._check_data_paths()

    def _create_config(self) -> None:
        """Create a new config file in ``~/senchain_data/config/config.cfg``."""
        self._USER_DIR.mkdir(exist_ok=True)
        self._CONFIG_DIR.mkdir(exist_ok=True)
        self._DEFAULT_DATA_DIR.mkdir(exist_ok=True)
        self._JOBS_DIR.mkdir(exist_ok=True)
        Path(self._config_params["LOG PATH"]["log_path"]).mkdir(exist_ok=True)
        # ~ (self.__JOBS_DIR / "logs").mkdir(exist_ok=True)

        with open(str(self._CONFIG_FILE), "w") as cfg_file:
            self._config_params.write(cfg_file)

    def _check_data_paths(self) -> None:
        """
        Checks if data paths are provided and valids. If not, create default
        folders in sen2chain_data/DATA and update the configuration file.
        """

        def update_config(section, key, val):
            """
            Update a setting in config.ini
            """
            self._config_params.set(section, key, val)
            with open(str(self._CONFIG_FILE), "w") as cfg_file:
                self._config_params.write(cfg_file)

        data_paths = [option for option in self._config_params["DATA PATHS"]]

        for path in data_paths:
            value = self.config_dict[path]
            if value.rstrip() == "" or not Path(value).exists():
                default_value = (
                    self._DEFAULT_DATA_DIR / path.replace("_path", "").upper()
                )
                default_value.mkdir(parents=True, exist_ok=True)
                update_config("DATA PATHS", path, str(default_value))
                logger.info(
                    "{} not set or absent, using default path {}".format(path, str(default_value))
                )

        sen2cor_bashrc_path_values = [item for item in self.config_dict["sen2cor_bashrc_paths"].strip().split('\n')]
        self.config_dict["sen2cor_bashrc_paths"] = sen2cor_bashrc_path_values
        
        for item in sen2cor_bashrc_path_values:
            if (
                item == ""
                or not Path(item).exists()
            ):
                logging.error(
                    "Make sure the path to the sen2cor Bashrc file is valid: {}".format(item)
                )
                raise ValueError("Invalid sen2cor Bashrc")
        
    def get(self, param: str) -> str:
        """Returns a parameter value.

        :param param: parameter.
        """
        if param not in self.config_dict:
            raise ValueError("Invalid parameter", param)

        return self.config_dict.get(param)

    @property
    def tiles_to_watch(self):
        return self._TILES_TO_WATCH
