import os
import pathlib
from datetime import datetime, timedelta
import time
from pathlib import Path
import pandas as pd
import numpy as np
from scipy.interpolate import UnivariateSpline
import matplotlib.pyplot as plt

#"traduction" des codes R : 
def divide10k(x):
    return x / 10000

def spline_interpolation(group):
    output_freq = 'W'
    x_numeric = (group['date'] - group['date'].min()).dt.total_seconds().values
    spline = UnivariateSpline(x_numeric, group['median'],s=0)
    x_interp = pd.date_range(start=group['date'].min(), end=group['date'].max(), freq=output_freq)
    x_interp_numeric = (x_interp - group['date'].min()).total_seconds().values
    y_interp = spline(x_interp_numeric)
    result = pd.DataFrame({'date': x_interp, 'spline': y_interp, 'fid' : group['fid'].iloc[0]})
    return result

def create_spline(this_data,inspect=False):

    ind_spline = this_data.groupby('fid').apply(spline_interpolation).reset_index(drop=True)
    if inspect:
        for fid, group in this_data.groupby('fid'):
            plt.figure(figsize=(10, 6))

            date_array = group['date'].to_numpy()
            median_array = group['median'].to_numpy()

            plt.plot(date_array, median_array, 'o', label='Original')

            if fid in ind_spline['fid'].unique():
                plt.plot(ind_spline[ind_spline['fid'] == fid]['date'].to_numpy(),
                         ind_spline[ind_spline['fid'] == fid]['spline'].to_numpy(),
                         label='Spline')
            else:
                print(f"No spline data found for fid {fid}")
            
            plt.title(f'Interpolation pour le plgn {fid}')
            plt.xlabel('Date')
            plt.ylabel('Median')
            plt.legend()
            #plt.show()
            plt.ioff()
            plt.savefig(f'/home/juliette/LISSAGE/plgn')
    return ind_spline


def compile_data():

    """
    ici c'est à mettre dans le compile_data de TimeSeries
    """
    """
    bad_dates = pd.to_datetime(['2023-03-26', '2023-03-31', '2023-04-10'])
    this_data = (
        this_data[['comm_fkt','fid','date', 'count2', 'nodata2', 'median2']]
        .assign(date=lambda x: pd.to_datetime(x['date'].dt.strftime('%Y-%m-%d')))
        .assign(perc_cloud=lambda x: x['nodata2'] / (x['nodata2'] + x['count2']))
        .query('perc_cloud < 0.25')
        .loc[~this_data['date'].isin(bad_dates)]
        .assign(median = lambda x: divide10k(x['median2']))
    )
    """
    read = '/home/juliette/Downloads/raw_sen2_subset.csv' #changer le chemin si necessaire
    
    this_data = pd.read_csv(read)
    print(this_data)
    this_data["count"] = pd.to_numeric(this_data["count"])
    this_data["nodata"] = pd.to_numeric(this_data["nodata"])
    this_data["median"] = pd.to_numeric(this_data["median"])
    this_data["perc_cloud"] = pd.to_numeric(this_data["perc_cloud"])
    this_data["fid"] = pd.to_numeric(this_data["id"])
    this_data["date"] = pd.to_datetime(this_data["date"])
    test = create_spline(this_data, inspect = True)
    test.to_csv(str('/home/juliette/LISSAGE/result.csv')) #le chemin /home/juliette/LISSAGE doit exister (il faut avoir un dossier lISSAGE)


start = time.time()
compile_data()
end = time.time()
print(timedelta(seconds=end - start))
