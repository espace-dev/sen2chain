# coding: utf-8

"""
Module for computing time series.
"""
import os
import csv
import hashlib 
import zipfile
import logging

import pathlib
from pathlib import Path
from collections import OrderedDict
import fiona
import rasterio
from rasterio.plot import show
from rasterio.plot import plotting_extent
from rasterio.mask import mask
from rasterio.enums import Resampling
from fiona import transform
from shapely.geometry import shape
from shapely.geometry import mapping
from rasterstats import zonal_stats
import pandas as pd
import geopandas as gpd

# type annotations
from typing import Sequence, List, Dict, Union
import matplotlib.pyplot as plt
import numpy as np
import math
from datetime import datetime, timedelta
import time
import multiprocessing
import shutil
import itertools
from functools import partial
import os.path

from .config import Config
from .tiles import Tile, ProductsList
from .indices import IndicesCollection
from .geo_utils import get_tiles_from_file
from .products import L2aProduct, IndiceProduct

logpath=f"{Config().get('log_path')}/extraction_{datetime.now().strftime('%Y-%m-%d')}.log"
Log_Format = "%(asctime)s:%(levelname)s:%(name)s:%(message)s"
dateformat='%Y-%m-%d %H:%M:%S'

handler = logging.FileHandler(logpath)
formatter = logging.Formatter(Log_Format, datefmt=dateformat)
handler.setFormatter(formatter)

logger = logging.getLogger("_extraction_")
logger.addHandler(handler)
logger.setLevel(logging.INFO)

console_logger = logging.getLogger("_")

class TimeSeriesCompil:
    """Class for time series extraction.
    :param vectors_file: path to the vector file.
    :param indices: list of valid indices names.
    :param date_min: starting date of the time series (YYYY-MM-DD format).
    :param date_max: ending date of the time series (YYYY-MM-DD format).
    :param cover_min: minimum cloud cover value. Default: 0.
    :param cover_max: minimum cloud cover value. Default: 100.
    Usage:
        >>> ts = TimeSeriesCompil(
                "polygons.geojson",
                indices=["NDVI"],
                date_min="2017-01-01",
                date_max="2018-01-01"
            )
    """

    def __init__(
        self,
        vectors_file: Union[str, pathlib.PosixPath],
        indices: Sequence[str] = None,
        date_min: str = None,
        date_max: str = None,
        cover_min: int = 0,
        cover_max: int = 100,
        field_names: str = None,
        multiproc: int = 0,
        cm_version: str = "CM004",
        probability: int = 1,
        iterations: int = 1,
        cld_shad: bool = True,
        cld_med_prob: bool = True,
        cld_hi_prob: bool = True,
        thin_cir: bool = True,
        reproject: bool = False
    ) -> None:
        
        if reproject is True:
            data = gpd.read_file(vectors_file)
            new_vectorsfile = vectors_file[:-4]+"_wgs84.shp"
            data.to_crs(epsg=4326).to_file(new_vectorsfile)
            vectors_file = new_vectorsfile

        self._vectors_file = Path(vectors_file)
        self._checksum = self.calculate_checksum()
        self._date_min = date_min
        self._date_max = date_max
        self._cover_min = cover_min
        self._cover_max = cover_max
        self._field_names = field_names
        self._multiproc = multiproc
        self._out_path = Config().get("extraction_path")
        self.cm_version = cm_version
        self.probability = probability
        self.iterations = iterations
        self.cld_shad = cld_shad
        self.cld_med_prob = cld_med_prob
        self.cld_hi_prob = cld_hi_prob
        self.thin_cir = thin_cir
        
        if(date_min is None and date_max is not None) or (date_min is not None and date_max is None):
            raise ValueError("If you specify datemin or datemax, you must specify both")
        if indices is None:
            self._indices_list = IndicesCollection.list
        else:
            for indice in indices:
                if indice not in IndicesCollection:
                    raise ValueError("Invald indice name: {}".format(indice))
            self._indices_list = indices

        if self._field_names:
            self._key = self._field_names
        else:
            self._key = "fid"
        self._tiles_geoms = self._get_tiles_geom_dict()
        self._df_dicts = dict()
        path = Path(self._vectors_file)
        logger.info("Extraction info : \nshp : {}; \ndatemin = {}; \ndatemax = {}; \ncm_version = {}; \nprb =  {}; \nitr = {}".format(self._vectors_file, self._date_min, self._date_max, self.cm_version, self.probability, self.iterations))
        existing_folder_name = None
        for d in Path(self._out_path).glob('*'):
            if d.is_dir() and self._checksum in d.name:
                existing_folder_name = d.name
                break  
        if existing_folder_name:
            self._data_name = '_'.join(existing_folder_name.split("_")[:-1])
            logger.info("You already executed this shp")
        else: 
            self._data_name = self._vectors_file.stem
        folder = self._data_name  + "_" + self._checksum
        logger.info("You will find your data in the folder {}".format(Path(self._out_path) / folder))
        folder_cp = Path(self._out_path) / folder / "shp_files"
        folder_cp.mkdir(parents=True, exist_ok=True)
        for ext in ['.shp', '.cpg', '.dbf', '.prj', '.shx']:
            file_to_cp = path.with_suffix(ext)
            shutil.copy2(file_to_cp, folder_cp)
        
    def calculate_checksum(self):
        """
        Input shapefile checksum calculation: facilitates the creation of separate tree structures for each shapefile
        """
        #getting all the files :
        temp_path = Config().get("temp_path")
        path = Path(self._vectors_file)
        #checksum : 
        sha256_hash = hashlib.sha256()
        for ext in ['.shp', '.cpg', '.dbf', '.prj', '.shx']:
            with open(path.with_suffix(ext), "rb") as f:
                while True:
                    data = f.read(65536)
                    if not data:
                        break
                    sha256_hash.update(data)
        checksum = sha256_hash.hexdigest()[:16]
        return checksum

    def extract_data(self,getstat: bool = True, force: bool=False): 
        start = time.time()
       
        if getstat:
            if self._multiproc:
                self._get_stats_multiproc(force)
            else:
                self._get_stats(force)
        end = time.time()
        logger.info(
            "Execution time: {}".format(timedelta(seconds=end - start))
        )
        """
        except:
            logger.info("Error : Extraction problem")
        else:
            logger.info("\nEnd")
        """
    @property
    def data(self) -> Dict[str, pd.DataFrame]:
        """Returns a dictionnary with indices as keys and pandas.DataFrame as values."""
        df = dict()
        if self.cm_version == "CM001":
            cm = "CM001"
        elif self.cm_version == "CM002":
            cm = "CM002-B11"
        elif self.cm_version == "CM003":
            cm = str("CM003" +
                "-PRB" + str(self.probability) +
                "-ITER" + str(self.iterations))
        elif self.cm_version == "CM004":
            cm = str("CM004" +
                "-CSH" + str(1 * self.cld_shad) +
                "-CMP" + str(1 * self.cld_med_prob) +  
                "-CHP" + str(1 * self.cld_hi_prob) +
                "-TCI" + str(1 * self.thin_cir) +
                "-ITER" + str(1 * self.iterations))
        name= self._data_name + "_" + self._checksum
        compiled_data = Path(self._out_path) / name  / "compiled_data"
        self.compile_csv(alldata=True)
        for indice in os.listdir(compiled_data):
            path_file = Path(compiled_data)/indice / cm / "all_data" /f'{self._data_name}_CC{self._cover_max}_all_data.csv'
            if os.path.isfile(path_file):
                df[indice] = pd.read_csv(str(path_file), dtype=str, index_col = 0)
                df[indice][['fid','count','nodata','nbcld','cldprb','min','max','mean','std','median','percentile_25','percentile_75','id']] = df[indice][['fid','count','nodata','nbcld','cldprb','min','max','mean','std','median','percentile_25','percentile_75','id']].apply(pd.to_numeric,errors='coerce')
            else:
                logger.info("All data file doesn't exist for {},{},{}, please check info or try extract_data".format(indice, cm, self._cover_max))
        return df

    @staticmethod
    def _is_indice_in_tile(indice: str, tile: Tile) -> bool:
        """Does the tile has a folder for this indice.
        :param indice: indice name.
        :param tile: Tile object.
        """
        if indice.lower() in tile.paths["indices"]:
            return True
        return False

    def _get_tiles_geom_dict(self) -> Dict[str, List[str]]:
        """{Feature id: list of intersected tiles} to {tile: list of intersected
        features ids}."""
        intersected_tiles_dict = get_tiles_from_file(
            self._vectors_file, land_only=False
        )
        tiles_geom = dict()
        for feat_id, tiles_list in intersected_tiles_dict.items():
            for tile in tiles_list:
                if tile not in tiles_geom:
                    tiles_geom[tile] = []
                tiles_geom[tile].append(feat_id)
        return tiles_geom

    @staticmethod
    def _filter_products(
        tile: Tile,
        indice: str,
        date_min: str,
        date_max: str,
        cover_min: int,
        cover_max: int,
        cm_version: str = "CM001",
        probability: int = 1,
        iterations: int = 5,
        cld_shad: bool = True,
        cld_med_prob: bool = True,
        cld_hi_prob: bool = True,
        thin_cir: bool = True,
    ) -> ProductsList:
        """Filters tile's indices products.

        :param tile: tile.
        :param indice: indice.
        :param date_min: oldest date.
        :param date_max: most recent date.
        :param cover_min: minimum cloud coverage.
        :param cover_max: maximum cloud coverage.
        """
        products = getattr(vars(tile)[
            indice.lower()
        ].masks,cm_version).params(
            probability = probability,
            iterations = iterations,
            cld_shad = cld_shad,
            cld_med_prob = cld_med_prob,
            cld_hi_prob = cld_hi_prob,
            thin_cir = thin_cir
        )
        filt = products.filter_dates(date_min, date_max).filter_clouds(
            cover_min, cover_max
        )
        return filt

    @staticmethod
    def _get_raster_stats_in_geom(
        feature: Dict,
        raster_path: Union[str, pathlib.PosixPath],
        cloud_path: str = None,
        cloud_proba_path: str = None,
    ) -> Dict:
        """Extracts statistics from a raster in a geometry.
        :param feature: GeoJSON like object.
        :param raster_path: path to the raster.
        """
        geom = shape(
            transform.transform_geom(
                "EPSG:4326",
                rasterio.open(str(raster_path)).crs["init"],
                feature["geometry"],
            )
        )
        stats = zonal_stats(
            geom,
            str(raster_path),
            stats=[
                "count",
                "nodata",
                "min",
                "max",
                "median",
                "mean",
                "std",
                "percentile_25",
                "percentile_75",
            ],
        )[0]
        if cloud_path:
            stats_cld = zonal_stats(
                geom, str(cloud_path), stats=["count", "nodata"]
            )[0]
            try:
                nbcld = stats["nodata"] - stats_cld["nodata"]
            except Exception:
                nbcld = 0
            stats["nbcld"] = nbcld


        if cloud_proba_path:
           
            stats_cld_prb = zonal_stats(
                geom, str(cloud_proba_path), stats=["mean"]
            )[0]
            stats["cldprb"] = stats_cld_prb["mean"]

        console_logger.info(stats)
        return stats

    def _get_stats(self, force : bool) -> None:
        start = time.time()
        """Compute stats in polygons."""
        folder= self._data_name  + "_" + self._checksum
        if self.cm_version == "CM001":
            cm = "CM001"
        elif self.cm_version == "CM002":
            cm = "CM002-B11"
        elif self.cm_version == "CM003":
            cm = str("CM003" +
                "-PRB" + str(self.probability) +
                "-ITER" + str(self.iterations))
        elif self.cm_version == "CM004":
            cm = str("CM004" +
                "-CSH" + str(1 * self.cld_shad) +
                "-CMP" + str(1 * self.cld_med_prob) +  
                "-CHP" + str(1 * self.cld_hi_prob) +
                "-TCI" + str(1 * self.thin_cir) +
                "-ITER" + str(1 * self.iterations))
        base_path_folder= Path(self._out_path) / folder / "raw_data"
        base_path_folder.mkdir(parents=True, exist_ok=True)
        with fiona.open(str(self._vectors_file), "r") as vectors:
            features = {feat["id"]: feat for feat in vectors}
            for index2, indice in enumerate(self._indices_list):
                logger.info(
                    "computing {}/{}: {}".format(
                        index2 + 1, len(self._indices_list), indice
                    )
                )
                rows_list = []
                all_products = []
                products_data = set()
                if force == False:
                    products_data = set(['_'.join(f.stem.split('_')[-9:])+".jp2" for f in (Path(self._out_path) / folder / "raw_data" / indice / cm).glob('*') if f.is_file()])
                product_folder = Path(base_path_folder) / indice / cm / "product_data"
                product_folder.mkdir(parents=True, exist_ok=True)
                csv = Path(product_folder) / f'products_no_data.csv'
                if os.path.exists(csv):
                    df = pd.read_csv(csv, dtype=object)
                    data = set(df["filename"])
                    if force == False:
                        products_data.update(data)
                for index3, tile in enumerate(self._tiles_geoms):
                    fid_list = self._tiles_geoms[tile]
                    tile_obj = Tile(tile)

                    logger.info(
                        "Tile {}/{}: {}".format(
                            index3 + 1, len(self._tiles_geoms), tile
                        )
                    )
                    if TimeSeriesCompil._is_indice_in_tile(indice, tile_obj):
                        tile_indice_path = tile_obj.paths["indices"][
                            indice.lower()
                        ]
                        products = TimeSeriesCompil._filter_products(
                            tile_obj,
                            indice,
                            self._date_min,
                            self._date_max,
                            self._cover_min,
                            self._cover_max,
                            self.cm_version,
                            self.probability,
                            self.iterations,
                            self.cld_shad,
                            self.cld_med_prob,
                            self.cld_hi_prob,
                            self.thin_cir,
                        )
                        products._dict = {cle: valeur for cle, valeur in products._dict.items() if cle not in products_data}
                        filenames = [filename for filename in products._dict.keys()]
                        all_products.extend(filenames)
                        for index1, prod in enumerate(products):
                            indice_product = IndiceProduct(prod.identifier)
                            logger.info(
                                "Product {}/{}: {}".format(
                                    index1 + 1, len(products), prod.identifier
                                )
                            )
                            logger.info("{} features".format(len(fid_list)))

                            for index, fid in enumerate(fid_list):
                                df_dict = OrderedDict()
                                df_dict["fid"] = fid
                                df_dict.update(
                                    TimeSeriesCompil._get_raster_stats_in_geom(
                                        features[fid],
                                        str(indice_product.path),
                                        str(indice_product.unmasked_path),
                                        indice_product.msk_cldprb_20m,
                                    )
                                )
                                df_dict["date"] = prod.date
                                df_dict["tile"] = tile
                                df_dict["filename"] = prod.identifier

                                for prop in features[fid]["properties"]:
                                    df_dict[prop] = features[fid][
                                        "properties"
                                    ][prop]
                                rows_list.append(df_dict)
                                if rows_list:
                                    self._df_dicts[indice] = TimeSeriesCompil._list_to_df(rows_list)
                            self.to_csv()
                            
                            rows_list.clear()
                            self._df_dicts.clear()
                            products_raw_data = set(['_'.join(f.stem.split('_')[-9:])+".jp2" for f in (Path(self._out_path) / folder / "raw_data" / indice / cm).glob('*') if f.is_file()])
                            no_data_products = set(all_products) - products_raw_data
                            dg = pd.DataFrame(list(no_data_products),columns=["filename"])
                            if os.path.exists(csv):
                                dg.to_csv(csv, index=False,mode='a',header=False)
                            else:
                                dg.to_csv(csv, index=False,mode='a')
            end = time.time()
        logger.info(
            "Execution time: {}".format(timedelta(seconds=end - start))
        )

    def _raster_stats_multi(self, features, shared_list, proc_item):
        indice_product = IndiceProduct(proc_item[0].identifier)
        fid = proc_item[1]
        result_dict = OrderedDict()
        result_dict["fid"] = fid
        result_dict.update(
            TimeSeriesCompil._get_raster_stats_in_geom(
                features[fid],
                str(indice_product.path),
                str(indice_product.unmasked_path),
                indice_product.msk_cldprb_20m,
            )
        )
        result_dict["date"] = proc_item[0].date
        result_dict["tile"] = proc_item[4]
        result_dict["filename"] = proc_item[0].identifier
        for prop in features[fid]["properties"]:
            result_dict[prop] = features[fid]["properties"][prop]
        shared_list.append(result_dict)


    def _get_stats_multiproc(self,force: bool) -> None:
        """Compute stats in polygons."""
        start = time.time()
        folder= self._data_name  + "_" + self._checksum
        if self.cm_version == "CM001":
            cm = "CM001"
        elif self.cm_version == "CM002":
            cm = "CM002-B11"
        elif self.cm_version == "CM003":
            cm = str("CM003" +
                "-PRB" + str(self.probability) +
                "-ITER" + str(self.iterations))
        elif self.cm_version == "CM004":
            cm = str("CM004" +
                "-CSH" + str(1 * self.cld_shad) +
                "-CMP" + str(1 * self.cld_med_prob) +  
                "-CHP" + str(1 * self.cld_hi_prob) +
                "-TCI" + str(1 * self.thin_cir) +
                "-ITER" + str(1 * self.iterations))
        base_path_folder= Path(self._out_path) / folder / "raw_data"
        base_path_folder.mkdir(parents=True, exist_ok=True)
        
        with fiona.open(str(self._vectors_file), "r") as vectors:
            features = {feat["id"]: feat for feat in vectors}
            for index2, indice in enumerate(self._indices_list):
                logger.info(
                    "computing {}/{}: {}".format(
                        index2 + 1, len(self._indices_list), indice
                    )
                )
                manager = multiprocessing.Manager()
                #retrieve product names from raw_data
                products_data = set()
                data = set()
                if force == False: 
                    products_data = set(['_'.join(f.stem.split('_')[-9:])+".jp2" for f in (Path(self._out_path) / folder / "raw_data" / indice / cm).glob('*') if f.is_file()])
                df = pd.DataFrame()
                product_folder = Path(base_path_folder) / indice / cm / "product_data"
                product_folder.mkdir(parents=True, exist_ok=True)
                #creation of a csv to manage no data
                csv = Path(product_folder) / f'products_no_data.csv'
                if os.path.exists(csv):
                    df = pd.read_csv(csv, dtype=object)
                    data = set(df["filename"])
                    if force == False:
                        products_data.update(data)
                all_products = []
                for index3, tile in enumerate(self._tiles_geoms):
                    shared_list = manager.list()
                    fid_list = self._tiles_geoms[tile]
                    tile_obj = Tile(tile)
                    logger.info(
                        "Tile {}/{}: {}".format(
                            index3 + 1, len(self._tiles_geoms), tile
                        )
                    )
                    if TimeSeriesCompil._is_indice_in_tile(indice, tile_obj):
                        tile_indice_path = tile_obj.paths["indices"][
                            indice.lower()
                        ]
                        products= TimeSeriesCompil._filter_products(
                            tile_obj,
                            indice,
                            self._date_min,
                            self._date_max,
                            self._cover_min,
                            self._cover_max,
                            self.cm_version,
                            self.probability,
                            self.iterations,
                            self.cld_shad,
                            self.cld_med_prob,
                            self.cld_hi_prob,
                            self.thin_cir,
                        )
                        #products already present in the raw data are removed from the list
                        products._dict = {cle: valeur for cle, valeur in products._dict.items() if cle not in products_data}
                        filenames = [filename for filename in products._dict.keys()]
                        all_products.extend(filenames)
                        proc_list = list(
                            itertools.product(
                                products,
                                fid_list,
                                [tile_indice_path],
                                [indice],
                                [tile],
                            )
                        )
                        logger.info("{} extractions".format(len(proc_list)))
                        pool = multiprocessing.Pool(self._multiproc)
                        results = [
                            pool.map(
                                partial(
                                    self._raster_stats_multi,
                                    features,
                                    shared_list,
                                ),
                                proc_list,
                            )
                        ]
                        pool.close()
                        pool.join()
                        rows_list = list(shared_list)
                        if rows_list:
                            self._df_dicts[indice] = TimeSeriesCompil._list_to_df(
                                rows_list
                            )    
                    self.to_csv()
                    rows_list.clear()
                    self._df_dicts.clear()
                products_raw_data = set(['_'.join(f.stem.split('_')[-9:])+".jp2" for f in (Path(self._out_path) / folder / "raw_data" / indice / cm).glob('*') if f.is_file()])
                no_data_products = (set(all_products) - products_raw_data)
                no_data_products = no_data_products - data
                dg = pd.DataFrame(list(no_data_products),columns=["filename"])
                if os.path.exists(csv):
                    dg.to_csv(csv, index=False,mode='a',header=False)
                else:
                    dg.to_csv(csv, index=False,mode='a')

        end = time.time()
        logger.info(
            "Execution time: {}".format(timedelta(seconds=end - start))
        )

    @staticmethod
    def _list_to_df(rows_list: List[Dict]) -> pd.DataFrame:
        """Converts a list of dictionaries as a pandas dataframe.
        :param rows_list: list of dictionaries.
        """
        df = pd.DataFrame.from_dict(rows_list)
        df = df.sort_values(by=["date"])
        df.set_index("date", inplace=True, drop=False)
        del df["date"]
        df = df.dropna(subset=["mean"])
        return df

    def to_csv(self, out_path: Union[str, pathlib.PosixPath] = None) -> None:
        """Exports the time series to CSV format.
        :param out_path: output folder. Default is DATA/EXTRACTION.
        """
        if out_path is None:
            out_path = self._out_path
        folder= self._data_name + "_" + self._checksum
        base_path_folder= Path(out_path) / folder / "raw_data"
        base_path_folder.mkdir(parents=True, exist_ok=True)

        list_order = [
            "fid",
            "tile",
            "filename",
            "count",
            "nodata",
            "nbcld",
            "cldprb",
            "min",
            "max",
            "mean",
            "std",
            "median",
            "percentile_25",
            "percentile_75",
        ]
        for df_name, df in self._df_dicts.items():
            if len(df["filename"]) > 0:
                df = pd.DataFrame(df)
                parts = os.path.splitext(df["filename"][0])[0].split("_")
                indice = parts[8]
                out_path_folder = Path(base_path_folder) / df_name / indice
                out_path_folder.mkdir(parents=True, exist_ok=True)
                df["fid"] = pd.to_numeric(df["fid"])
                df["mean"] = pd.to_numeric(df["mean"]).astype(float)
                df["std"] = pd.to_numeric(df["std"])
                df["count"] = pd.to_numeric(df["count"])
                df["nodata"] = pd.to_numeric(df["nodata"])
                df["nbcld"] = pd.to_numeric(df["nbcld"])
                df["cldprb"] = pd.to_numeric(df["cldprb"])
                df["min"] = pd.to_numeric(df["min"])
                df["max"] = pd.to_numeric(df["max"])
                df["percentile_25"] = pd.to_numeric(df["percentile_25"])
                df["percentile_75"] = pd.to_numeric(df["percentile_75"])
                dates = df["filename"].unique()
                for date in dates:
                    df_filtered = df[df["filename"] == date]
                    csv_path = out_path_folder / "{0}_{1}.csv".format(
                        self._data_name,
                        os.path.splitext(df_filtered["filename"][0])[0]
                    )
                    dg = (
                        df_filtered.sort_values(
                            by=["fid", "date", "count", "std"],
                            ascending=[True, True, True, False],
                        )
                        .reindex(
                            columns=(
                                list_order
                                + [a for a in df.columns if a not in list_order]
                            ),
                            copy=False,
                        )
                        .reset_index()
                        .drop_duplicates(["date", "fid"], keep="last")
                    )
                    dg.set_index("date", inplace=True, drop=True)
                    dg.replace(
                        to_replace=[r"\\t|\\n|\\r", "\t|\n|\r"],
                        value=["", ""],
                        regex=True,
                        inplace=True,
                    )
                    dg.to_csv(str(csv_path))
                    logger.info("exported to csv: ({})".format(csv_path))
                    
    def plot_global(
        self, out_path: Union[str, pathlib.PosixPath] = None
    ) -> None:
        """Exports the time series to CSV format.
        :param out_path: output folder. Default is DATA/EXTRACTION.
        """
        if out_path is None:
            out_path = self._out_path
        folder = self._data_name  + "_" + self._checksum
        out_path_folder = Path(out_path) / folder
        out_path_folder.mkdir(parents=True, exist_ok=True)
        for df_name, df in self.data.items():
            png_path = (
                out_path_folder
                / "{0}_plot-global_{1}_{2}_{3}.png".format(
                    self._data_name,
                    df_name,
                    self._date_min,
                    self._date_max,
                )
            )
            df = self.data[df_name]
            plt.figure(figsize=(19.2, 10.8))
            if df_name in ["NDVI", "NDWIGAO", "NDWIMCF", "MNDWI"]:
                plt.ylim((-10000, 10000))
            else:
                plt.ylim((0, 10000))
            df.dropna(subset=["mean"]).groupby(self._key)["mean"].plot(
                legend=True
            )
            plt.title(df_name)
            plt.savefig(str(png_path))
            plt.close()
            logger.info("Plot saved to png: {}".format(png_path))

    def plot_details(
        self, out_path: Union[str, pathlib.PosixPath] = None
    ) -> None:
        """Exports the time series to CSV format.

        :param out_path: output folder. Default is DATA/EXTRACTION.
        """
        if out_path is None:
            out_path = self._out_path
        folder= self._data_name  + "_" + self._checksum
        out_path_folder = Path(out_path) / folder
        out_path_folder.mkdir(parents=True, exist_ok=True)
        for df_name, df in self.data.items():
            png_path = (
                out_path_folder
                / "{0}_plot-details_{1}_{2}_{3}.png".format(
                    self._data_name,
                    df_name,
                    self._date_min,
                    self._date_max,
                )
            )
            png_path_nonan = (
                out_path_folder
                / "{0}_plot-details_{1}-nonan_{2}_{3}.png".format(
                    self._data_name,
                    df_name,
                    self._date_min,
                    self._date_max,
                )
            )

            if df_name in ["NDVI", "NDWIGAO", "NDWIMCF", "MNDWI"]:
                ylim = [-10000, 10000]
            else:
                ylim = [0, 10000]

            df = self.data[df_name]
            grouped = df.groupby(self._key)
            ncols = int(math.ceil(len(grouped) ** 0.5))
            nrows = int(math.ceil(len(grouped) / ncols))
            fig, axs = plt.subplots(nrows, ncols, figsize=(19.2, 10.8))
            for (name, dfe), ax in zip(grouped, axs.flat):
                ax.set_ylim(ylim)
                ax.set_title(name)
                dfe.dropna(subset=["mean"]).plot(
                    y=["mean"],
                    ax=ax,
                    yerr="std",
                    color="black",
                    elinewidth=0.2,
                    legend=False,
                )
                dfe.dropna(subset=["mean"]).plot(
                    y=["min", "max"],
                    ax=ax,
                    linewidth=0.25,
                    color="black",
                    legend=False,
                )
                ax2 = ax.twinx()
                ax2.set_ylim([0, 1])
                dfe["na_ratio"] = dfe["nodata"] / (
                    dfe["count"] + dfe["nodata"]
                )
                dfe.dropna(subset=["mean"]).plot(
                    y=["na_ratio"],
                    marker="o",
                    ax=ax2,
                    color="red",
                    linewidth=0.25,
                    linestyle="",
                    legend=False,
                )
                ax.plot(np.nan, "-r", label="NaN ratio")
                ax.legend(loc=0, labelspacing=0.05)
            fig.tight_layout()
            fig.suptitle(df_name, fontsize=16)
            plt.savefig(str(png_path))
            plt.close()
            logger.info("Plot saved to png: {}".format(png_path))

            try:
                fig, axs = plt.subplots(nrows, ncols, figsize=(19.2, 10.8))
                for (name, dfe), ax in zip(grouped, axs.flat):
                    ax.set_ylim(ylim)
                    ax.set_title(name)
                    dfe = dfe.dropna(subset=["mean"])
                    dfe = dfe[(dfe[["nodata"]] == 0).all(axis=1)]
                    dfe.plot(
                        y=["mean"],
                        ax=ax,
                        yerr="std",
                        color="black",
                        elinewidth=0.2,
                        legend=False,
                    )
                    dfe.plot(
                        y=["min", "max"],
                        ax=ax,
                        linewidth=0.25,
                        color="black",
                        legend=False,
                    )
                    ax.legend(loc=0, labelspacing=0.05)
                fig.tight_layout()
                fig.suptitle(df_name, fontsize=16)
                plt.savefig(str(png_path_nonan))
                plt.close()
                logger.info("Plot saved to png: {}".format(png_path_nonan))
            except Exception:
                logger.info("No data to plot (no date with 0 nodata)")
                pass

    def extract_ql(
        self, out_path: Union[str, pathlib.PosixPath] = None
    ) -> None:
        """Extract ql images around vectors."""
        if out_path is None:
            out_path = Config().get("extraction_path")
        folder= self._data_name  + "_" + self._checksum
        out_path_folder = Path(out_path) / folder
        out_path_folder.mkdir(parents=True, exist_ok=True)

        for df_name, df in self.data.items():
            fid_list = dict(zip(df.fid, df[self._key]))
            if not self._field_names:
                fid_list = fid_list.fromkeys(fid_list, "")

            cmap_dict = {
                "NDVI": "RdYlGn",
                "NDWIGAO": "RdYlBu",
                "NDWIMCF": "RdYlBu",
                "BIGR": "pink",
                "BIRNIR": "afmhot",
                "BIBG": "bone",
                "MNDWI": "BrBG",
            }
            if df_name in ["NDVI", "NDWIGAO", "NDWIMCF", "MNDWI"]:
                vmin = -10000
                vmax = 10000
            else:
                vmin = 0
                vmax = 10000

            for fid, name in fid_list.items():
                if name:
                    fidname = name
                else:
                    fidname = "FID" + fid
                out_path_fid_folder = out_path_folder / str("QL_" + fidname)
                out_path_fid_folder.mkdir(parents=True, exist_ok=True)
                indice_png_path = (
                    out_path_fid_folder
                    / "{0}_MOZ-QL_{1}_{2}_{3}_{4}.jpg".format(
                        self._data_name,
                        fidname,
                        df_name,
                        self._date_min,
                        self._date_max,
                    )
                )
                l2a_png_path = (
                    out_path_fid_folder
                    / "{0}_MOZ-QL_{1}_{2}_{3}_{4}.jpg".format(
                        self._data_name,
                        fidname,
                        "L2A",
                        self._date_min,
                        self._date_max,
                    )
                )
                logger.info("fid/name:{}".format(fidname))
                nb_prod = len(df.loc[df["fid"] == fid])
                ncols = int(math.ceil(nb_prod ** 0.5))
                nrows = int(math.ceil(nb_prod / ncols))
                figa, axs = plt.subplots(
                    nrows,
                    ncols,
                    figsize=(5 * ncols, 5 * nrows),
                    sharey=True,
                    sharex=True,
                )
                figb, bxs = plt.subplots(
                    nrows,
                    ncols,
                    figsize=(5 * ncols, 5 * nrows),
                    sharey=True,
                    sharex=True,
                )
                for (index, row), ax, bx in zip(
                    (df.loc[df["fid"] == fid])
                    .sort_values(by=["date"])
                    .iterrows(),
                    np.array(axs).flatten(),
                    np.array(bxs).flatten(),
                ):
                    prod_id = IndiceProduct(row["filename"]).l2a
                    indice_png_tile_path = (
                        out_path_fid_folder
                        / "{0}_QL_{1}_{2}_{3}_{4}_{5}.jpg".format(
                            self._data_name,
                            fidname,
                            df_name,
                            prod_id[11:19],
                            prod_id[39:44],
                            prod_id[0:2],
                        )
                    )
                    l2a_png_tile_path = (
                        out_path_fid_folder
                        / "{0}_QL_{1}_{2}_{3}_{4}_{5}.jpg".format(
                            self._data_name,
                            fidname,
                            "L2A",
                            prod_id[11:19],
                            prod_id[39:44],
                            prod_id[0:2],
                        )
                    )
                    tile_obj = Tile(row["tile"])
                    tile_indice_path = tile_obj.paths["indices"][
                        df_name.lower()
                    ]
                    tile_l2a_path = tile_obj.paths["l2a"]
                    prod_path = tile_indice_path / prod_id / row["filename"]
                    tci_path = L2aProduct(prod_id).tci_10m
                    crop_extent = gpd.read_file(str(self._vectors_file))
                    logger.info(tci_path)
                    raster_tci = rasterio.open(tci_path)
                    crop_extent_new_proj = crop_extent.to_crs(raster_tci.crs)
                    extent_geojson = mapping(
                        crop_extent_new_proj["geometry"][int(fid)].buffer(1000)
                    )

                    with rasterio.open(tci_path) as tci_data:
                        tci_data_crop, tci_data_crop_affine = mask(
                            tci_data, [extent_geojson], crop=True
                        )
                    tci_crop = np.dstack(
                        (tci_data_crop[0], tci_data_crop[1], tci_data_crop[2])
                    )
                    tci_data_extent = plotting_extent(
                        tci_data_crop[0], tci_data_crop_affine
                    )
                    ax.imshow(tci_crop, extent=tci_data_extent)
                    crop_extent_new_proj.loc[[int(fid)], "geometry"].plot(
                        ax=ax, facecolor="none", edgecolor="black", linewidth=3
                    )
                    ax.set_title(
                        "{} - {}".format(prod_id[11:19], prod_id[39:44]),
                        fontsize=10,
                    )

                    fig2, ax2 = plt.subplots(1, 1, figsize=(4, 4))
                    plt.subplots_adjust(
                        left=0.1, right=0.9, top=0.9, bottom=0.1
                    )
                    ax2.imshow(tci_crop, extent=tci_data_extent)
                    crop_extent_new_proj.loc[[int(fid)], "geometry"].plot(
                        ax=ax2,
                        facecolor="none",
                        edgecolor="black",
                        linewidth=3,
                    )
                    ax2.set_axis_off()
                    fig2.suptitle(name)
                    ax2.set_title(prod_id[39:44] + " " + prod_id[11:19])
                    fig2.tight_layout()
                    fig2.savefig(str(l2a_png_tile_path), bbox_inches="tight")
                    plt.close(fig=fig2)

                    raster = rasterio.open(prod_path)
                    nrg = raster.read(1)
                    with rasterio.open(prod_path) as img_data:
                        img_data_crop, img_data_crop_affine = mask(
                            img_data, [extent_geojson], crop=True
                        )
                    img_data_extent = plotting_extent(
                        img_data_crop[0], img_data_crop_affine
                    )
                    bx.imshow(
                        img_data_crop[0],
                        extent=img_data_extent,
                        cmap=cmap_dict[df_name],
                        vmin=vmin,
                        vmax=vmax,
                    )
                    crop_extent_new_proj.loc[[int(fid)], "geometry"].plot(
                        ax=bx, facecolor="none", edgecolor="black", linewidth=3
                    )
                    bx.set_title(
                        "{} - {}".format(prod_id[11:19], prod_id[39:44]),
                        fontsize=10,
                    )

                    fig_t_ind, ax_t_ind = plt.subplots(1, 1, figsize=(4, 4))
                    plt.subplots_adjust(
                        left=0.1, right=0.9, top=0.9, bottom=0.1
                    )
                    ax_t_ind.imshow(
                        img_data_crop[0],
                        extent=img_data_extent,
                        cmap=cmap_dict[df_name],
                        vmin=vmin,
                        vmax=vmax,
                    )
                    crop_extent_new_proj.loc[[int(fid)], "geometry"].plot(
                        ax=ax_t_ind,
                        facecolor="none",
                        edgecolor="black",
                        linewidth=3,
                    )
                    ax_t_ind.set_axis_off()
                    fig_t_ind.suptitle(name)
                    ax_t_ind.set_title(prod_id[39:44] + " " + prod_id[11:19])
                    fig_t_ind.tight_layout()
                    fig_t_ind.savefig(
                        str(indice_png_tile_path), bbox_inches="tight"
                    )
                    plt.close(fig=fig_t_ind)

                figa.suptitle(fidname + " - L2A")
                figa.savefig(str(l2a_png_path), bbox_inches="tight")
                plt.close(fig=figa)

                figb.suptitle(fidname + " - " + df_name)
                figb.savefig(str(indice_png_path), bbox_inches="tight")
                plt.close(fig=figb)




    def compile_data(
    self,
    alldata : bool = False,
    daily : bool = False,
    weekly : bool = False,
    monthly : bool = False,
    yearly : bool = False,
    datemin : str = None, 
    datemax : str = None,
    covermax : int = 100 ):
        """
        Creation of csv files grouping raw data by day, week, month, year, all data and/or between two selected dates
        :param alldata: boolean, true if you want to compile alla the data. Default is FALSE.
        :param daily: boolean, true if you want to compile data by day. Default is FALSE.
        :param weekly: boolean, true if you want to compile data by week. Default is FALSE.
        :param monthly: boolean, true if you want to compile data by month. Default is FALSE.
        :param yearly: boolean, true if you want to compile data by year. Default is FALSE.
        :param date_min: compilation start day (YYYY-MM-DD format).
        :param date_max: compilation end date (YYYY-MM-DD format).
        :param cover_max: maximum cloud cover value. Default: 100.
        """
        if(datemin is None and datemax is not None) or (datemin is not None and datemax is None):
            raise ValueError("If you specify datemin or datemax, you must specify both")
        if self.cm_version == "CM001":
            cm = "CM001"
        elif self.cm_version == "CM002":
            cm = "CM002-B11"
        elif self.cm_version == "CM003":
            cm = str("CM003" +
                "-PRB" + str(self.probability) +
                "-ITER" + str(self.iterations))
        elif self.cm_version == "CM004":
            cm = str("CM004" +
                "-CSH" + str(1 * self.cld_shad) +
                "-CMP" + str(1 * self.cld_med_prob) +  
                "-CHP" + str(1 * self.cld_hi_prob) +
                "-TCI" + str(1 * self.thin_cir) +
                "-ITER" + str(1 * self.iterations))
        name= self._data_name + "_" + self._checksum
        raw_data= Path(self._out_path) / name / "raw_data"
        compiled_data = Path(self._out_path) / name  / "compiled_data"
        list_order = [
            "fid",
            "tile",
            "filename",
            "count",
            "nodata",
            "nbcld",
            "cldprb",
            "min",
            "max",
            "mean",
            "std",
            "median",
            "percentile_25",
            "percentile_75",
            "completeness"
        ]
        
        if os.path.isdir(raw_data):
            compiled_data.mkdir(parents=True, exist_ok=True)
            for indice in os.listdir(raw_data):
                logger.info("Indice {} :".format(indice))
                ind = Path(compiled_data) / indice
                ind.mkdir(parents=True, exist_ok=True)
                if cm in os.listdir(Path(raw_data)/indice):
                    cm_path = Path(ind) / cm
                    cm_path.mkdir(parents=True, exist_ok=True)
                    #create folders
                    new_data = pd.DataFrame()
                    if alldata:
                        all_data_folder = Path(cm_path)/ "all_data"
                        all_data_folder.mkdir(parents=True, exist_ok=True)
                        
                    if daily:
                        daily_folder =  Path(ind) / cm / "daily"
                        daily_folder.mkdir(parents=True, exist_ok=True)
                        #daily_new_data = pd.DataFrame()
                    if weekly:
                        weekly_folder =  Path(ind) / cm / "weekly"
                        weekly_folder.mkdir(parents=True, exist_ok=True)
                        #weekly_new_data = pd.DataFrame()
                    if monthly: 
                        monthly_folder = Path(ind) / cm / "monthly"
                        monthly_folder.mkdir(parents=True, exist_ok=True)
                        #monthly_new_data = pd.DataFrame()
                    if yearly:
                        yearly_folder = Path(ind) / cm / "yearly"
                        yearly_new_data = pd.DataFrame()
                        #yearly_folder.mkdir(parents=True, exist_ok=True)
                    
                    if datemin is not None and datemax is not None:
                        custom_folder = Path(ind) / cm / "custom"
                        custom_data = pd.DataFrame()
                        custom_folder.mkdir(parents=True, exist_ok=True)

                    raw  = Path(raw_data) / indice / cm
                    
                    #getting data in raw_data
                    products_data = set([ ('_'.join(f.stem.split('_')[-9:])+".jp2", f) for f in (Path(raw)).glob('*') if f.is_file()])

                    for name, path in products_data:
                        df = pd.read_csv(path,dtype=str)
                        new_data = pd.concat([new_data,df])

                if not new_data.empty: 
                    #sorting and removing duplicate:
                    new_data["count2"] = pd.to_numeric(new_data["count"])
                    new_data["nbcld2"] = pd.to_numeric(new_data["nbcld"])
                    new_data["nodata2"] = pd.to_numeric(new_data["nodata"])
                    new_data["std2"] = pd.to_numeric(new_data["std"])
                    new_data["fid"] = pd.to_numeric(new_data["fid"])
                    new_data["date"] = pd.to_datetime(new_data["date"])
                    new_data["product_version"] = new_data["filename"].str.split('_').str[3]
                    # (nodata - nbcld) / (count + nodata) < 10 %
                    new_data["completeness"] = ( (new_data["nodata2"] - new_data["nbcld2"]) / (new_data["count2"] + new_data["nodata2"]))
                    new_data["complete"] = ( (new_data["completeness"])<0.10)
                    if yearly:
                        new_data["year"] = new_data["date"].dt.strftime('%Y')
                    if monthly:
                        new_data["month"] = new_data["date"].dt.strftime('%Y-%m')
                    if weekly:
                        new_data["week"] = new_data["date"].dt.strftime('%Y-%U')
                    if daily:
                        new_data["day"] = pd.to_datetime(new_data["date"]).dt.strftime('%Y-%m-%d')
                    
                    dg = (
                        new_data.sort_values(
                            by=["fid","date", "complete", "product_version", "count2", "std2"],
                            ascending=[True, True, True, True, True, False],
                        )
                        .reindex(
                            columns=(
                                list_order
                                + [a for a in new_data.columns if a not in list_order]),
                            copy=False,
                        )
                        .reset_index(drop=True)
                        .drop_duplicates(["date", "fid"], keep="last")
                    )
                    dg.drop(['count2', 'std2','nodata2','nbcld2','complete','product_version'], axis =1, inplace=True)
                    dg.set_index("date", inplace=True, drop=True)
                    dg.replace(
                        to_replace=[r"\\t|\\n|\\r", "\t|\n|\r"],
                        value=["", ""],
                        regex=True,
                        inplace=True,
                    )
                    dg["completeness"] = 1 - dg["completeness"]

                    
                    if covermax < 100:
                        dg = dg[dg["cldprb"] <= covermax]

                    #LANCER LE LISSAGE ICI, AVEC LE DATAFRAME DG
                    
                    if alldata:
                        csv = Path(all_data_folder) / f'{self._data_name}_CC{covermax}_all_data.csv'
                        dg.to_csv(str(csv))
                        logger.info(f"CSV created : ({csv})")
                    if yearly:
                        groups_by_year = dg.groupby(dg["year"])
                        for year, group in groups_by_year:
                            csv = yearly_folder / f'{self._data_name}_CC{covermax}_yearly_{year}.csv'
                            group.drop(['year'], axis =1, inplace=True)
                            group.to_csv(csv)
                            logger.info(f"CSV created : ({csv})")
                    if monthly:
                        groups_by_month = dg.groupby(dg["month"])
                        for month, group in groups_by_month:
                            csv = monthly_folder / f'{self._data_name}_CC{covermax}_monthly_{month}.csv'
                            group.drop(['month'], axis =1, inplace=True)
                            group.to_csv(csv)
                            logger.info(f"CSV created : ({csv})")
                    if weekly:
                        groups_by_week = dg.groupby(dg["week"])
                        #separation of data by week :
                        for week, group in groups_by_week:
                            csv = weekly_folder / f'{self._data_name}_CC{covermax}_weekly_{week}.csv'
                            group.drop(['week'], axis =1, inplace=True)
                            group.to_csv(csv)
                            logger.info(f"CSV created : ({csv})")
                    if daily:
                        groups_by_day = dg.groupby(dg["day"])
                        #separation of data by day :
                        for day, group in groups_by_day:
                            csv = Path(Path(daily_folder) / f'{self._data_name}_CC{covermax}_daily_{day}.csv')
                            group.drop(['day'], axis =1, inplace=True)
                            group.to_csv(csv)
                            logger.info(f"CSV created : ({csv})")
                    if datemin is not None and datemax is not None:
                        datemin = pd.to_datetime(datemin).date()
                        datemax = pd.to_datetime(datemax).date()
                        print(dg.index.date)
                        custom = dg[(dg.index.date <= datemax) & (dg.index.date>= datemin)]
                        real_date_min= custom.index.min().date()
                        real_date_max= custom.index.max().date()
                        csv = Path(custom_folder) / f'{self._data_name}_CC{covermax}_{real_date_min}_{real_date_max}.csv'
                        custom.to_csv(str(csv))
                        logger.info(f"CSV created : ({csv})")
                        
                else:
                    logger.info(f"No data in raw_data")

