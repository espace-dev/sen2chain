# coding: utf-8

# Copyright (C) 2018  Jeremy Commins <jebins@laposte.net>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
This module lists all externally useful classes and functions.
"""

from .config import Config
from .tiles import Tile
from .products import (
    L1cProduct,
    L2aProduct,
    OldCloudMaskProduct,
    NewCloudMaskProduct,
    IndiceProduct,
    FamilyProduct,
)
from .library import Library
from .data_request import DataRequest
from .indices import IndicesCollection
from .download_and_process import DownloadAndProcess
from .download_eodag import S2cEodag
from .time_series import TimeSeries
from .automatization import Automatization
from .time_series_compil import TimeSeriesCompil
from .jobs_extraction import JobExtraction

from .utils import (
    format_word,
    grouper,
    datetime_to_str,
    str_to_datetime,
    human_size_decimal,
    human_size,
    get_Sen2Cor_version, 
    get_Sen2Cor_versions,
    get_latest_Sen2Cor_version,
    get_latest_Sen2Cor_version_from_id,
    set_permissions,
    get_cm_dict, get_cm_string_from_dict, get_indice_from_identifier
)
from .geo_utils import (
    serialise_tiles_index,
    full_db_2_shp,
    crop_product_by_shp,
)
from .multi_processing import (
    l2a_multiprocessing,
    cld_version_probability_iterations_reprocessing_multiprocessing,
    idx_multiprocessing,
)
from .tileset import TileSet
from .jobs import Jobs, Job

__version__ = "v1.02.00"
__author__ = (
    "Jérémy Commins <jebins@laposte.net> & Impact <pascal.mouquet@ird.fr>"
)
