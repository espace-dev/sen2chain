# coding: utf-8

"""
Module for parsing L1C and L2A products metadatada.
"""

import logging
import pathlib
import re
import xml.etree.ElementTree as et

import sen2chain
from .utils import get_Sen2Cor_version
from .config import SHARED_DATA, Config


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class MetadataParser:
    """Class for getting metadata values from a L1C or L2A product's XML metadata file.

    :param metadata_path: XML metadata file path.
    :param tile: product's tile name.
    """

    def __init__(self, metadata_path: pathlib.PosixPath, tile: str) -> None:

        if metadata_path is None or not metadata_path.exists():
            raise ValueError(
                "metadata file not found: {}".format(metadata_path)
            )

        self._metadata_path = metadata_path
        self.tile = tile
        self._safe_path = self._metadata_path.parent.absolute()
        self._root = et.parse(str(self._metadata_path)).getroot()
        self._product_level = None
        self._psd = None
        self._granule_string = None
        self._granule = None
        self._image_string = None

        self._get_product_level()
        self._get_psd()
        self._get_granule()
        self._get_bands_image_string()

    def _get_product_level(self) -> None:
        """Description."""
        if "L2A" in self._safe_path.name:
            self._product_level = "L2A"
        else:
            self._product_level = "L1C"

    def _get_psd(self) -> None:
        """Returns the version of the metadata file."""
        schema_location = self._root.attrib[
            "{http://www.w3.org/2001/XMLSchema-instance}schemaLocation"
        ]
        psd_version = re.findall(
            "https://psd-([0-9]{2}).sentinel2.eo.esa.int", schema_location
        )[0]
        self._psd = int(psd_version)

    def _get_granule(self) -> None:
        """Load granule identifier."""
        self._granule_string = "Granule" if self._psd > 13 else "Granules"
        try:
            self._granule = [
                g.attrib["granuleIdentifier"]
                for g in self._root.findall(
                    ".//{}".format(self._granule_string)
                )
                if self.tile in g.attrib["granuleIdentifier"]
            ][0]
        except IndexError:
            logger.error(
                "{}: could not find granule for tile {}".format(
                    self._metadata_path.name, self.tile
                )
            )
            raise

    def _get_bands_image_string(self) -> None:
        """Returns XML key string for granule's bands paths."""
        image_string = {
            t.tag
            for t in self._root.findall(".//{}/*".format(self._granule_string))
        }
        self._image_string = list(image_string)[0]

    def get_metadata_value(self, key: str = None) -> str:
        """
        Returns metadata.

        :param key: metadata tag name.
        """
        try:
            return [v.text for v in self._root.findall(".//{0}".format(key))][
                0
            ]
        except IndexError:
            logger.error("Metadata value not found: {}".format(key))

    def get_band_path(self, key: str, res: str = None) -> str:
        """
        Returns full band path.

        :param key: band's tag name.
        :param res: band's resolution. None for L1C.
        """
        # folder's name for each resolution.
        res_strings_dict = {"10m": "R10m", "20m": "R20m", "60m": "R60m"}

        pattern = (
            key.upper() if res is None else key.upper() + "_" + res.lower()
        )

        if key.upper() in ("CLD", "SNW"):
            # try:
            #     path = [v.text
            #             for v in self._root.findall(".//{0}".format(self._image_string))
            #             if pattern in v.text][0].split("/")
            #     if self._psd > 13:
            #         full_path = self._safe_path / "{}/{}/{}/{}.jp2".format(path[0], path[1], path[2], path[3])
            #     else:
            #         full_path = self._safe_path / "GRANULE/{}/QI_DATA/{}.jp2".format(self._granule, path[-1], pattern)
            #     return str(full_path)
            # except Exception as e:
            #     logger.debug("{}".format(e))
            #     granule_path = self._safe_path / "GRANULE"
            #     return list(granule_path.rglob("*{}*".format(key.upper())))[0]
            path = self._safe_path.glob(
                "**/QI_DATA/*{}*{}.jp2".format(key.upper(), res.lower())
            )
            return next(path, None)

        try:
            if self._product_level == "L2A":
                full_path = (
                    self._safe_path
                    if self._psd > 13
                    else self._safe_path
                    / "GRANULE"
                    / self._granule
                    / "IMG_DATA"
                    / res_strings_dict[res]
                )
            else:
                full_path = (
                    self._safe_path
                    if self._psd > 13
                    else self._safe_path
                    / "GRANULE"
                    / self._granule
                    / "IMG_DATA"
                )
            return [
                str(full_path / f.text) + ".jp2"
                for f in self._root.findall(
                    ".//{}[@granuleIdentifier='{}']/{}".format(
                        self._granule_string, self._granule, self._image_string
                    )
                )
                if pattern in f.text
            ][0]
        except IndexError:
            logger.error("Band not found: {}".format(pattern))
            raise


class Sen2ChainMetadataParser:
    """ """

    def __init__(
        self,
        xml_path,
    ):
        SEN2CHAIN_META = SHARED_DATA.get("sen2chain_meta")
        self.xml_path = xml_path

        if xml_path.exists():
            self._tree = et.parse(str(xml_path))

        else:
            self._tree = et.parse(str(SEN2CHAIN_META))

        self._root = self._tree.getroot()

    def get_default_values(self):
        keys = [
            "SEN2CHAIN_VERSION",
            "SEN2CHAIN_PROCESSING_VERSION",
            "SEN2COR_VERSION",
        ]
        self.default_values = []
        for key in keys:
            self.default_values.append(
                [v.text for v in self._root.findall(".//{0}".format(key))][0]
            )

    def get_metadata_value(self, key: str = None) -> str:
        """
        Returns metadata.

        :param key: metadata tag name.
        """
        try:
            return [v.text for v in self._root.findall(".//{0}".format(key))][
                0
            ]
        except IndexError:
            logger.error("Metadata value not found: {}".format(key))

    def init_metadata(self):
        self._root.find("SEN2CHAIN_VERSION").text = sen2chain.__version__
        self._root.find("SEN2CHAIN_PROCESSING_VERSION").text = Config().get(
            "sen2chain_processing_version"
        )
        self._root.find("SEN2COR_VERSION").text = get_Sen2Cor_version()
        self._tree.write(
            str(self.xml_path), 
            encoding="UTF-8", 
            xml_declaration=True
        )

    def set_metadata(
        self,
        sen2chain_version: str = None,
        sen2chain_processing_version: str = None,
        sen2cor_version: str = None,
    ):
        self._root.find("SEN2CHAIN_VERSION").text = (
            sen2chain_version or sen2chain.__version__
        )
        self._root.find(
            "SEN2CHAIN_PROCESSING_VERSION"
        ).text = sen2chain_processing_version or Config().get(
            "sen2chain_processing_version"
        )
        self._root.find("SEN2COR_VERSION").text = (
            sen2cor_version or get_Sen2Cor_version()
        )
        self._tree.write(
            str(self.xml_path), 
            encoding="UTF-8", 
            xml_declaration=True
        )
