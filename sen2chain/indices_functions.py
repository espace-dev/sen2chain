# coding: utf-8

"""
This module contains functions to compute radiometric indices.
"""

import logging
import pathlib
from pathlib import Path

#  import otbApplication
import rasterio
from rasterio.warp import reproject, Resampling
import numpy as np
from typing import Union
from osgeo import gdal
from packaging import version

# import gdal

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

#  def create_raw_ndvi(nir_path: Union[str, pathlib.PosixPath],
#                      vir_path: Union[str, pathlib.PosixPath],
#                      out_path: Union[str, pathlib.PosixPath]="./raw_ndvi.tif"
#                      ) -> pathlib.PosixPath:
#      """
#      Creates a NDVI raster from NIR and VIR rasters.

#      :param nir_path: path to the NIR raster.
#      :param vir_path: path to the VIR raster.
#      :param out_path: path to the output raster.
#      """
#      logger.info("creating raw NDVI (tiff - int16)")

#      with rasterio.open(str(nir_path)) as nir_src, \
#              rasterio.open(str(vir_path)) as vir_src:
#          nir_profile = nir_src.profile
#          np.seterr(divide='ignore', invalid='ignore')    # ignore warnings when dividing by zero
#          nir = nir_src.read(1).astype(np.float32)
#          vir = vir_src.read(1).astype(np.float32)
#          ndvi = ((nir - vir) / (nir + vir)*10000).astype(np.int16)
#          ndvi_masked = np.where(nir != 0, ndvi, 32767)

#      nir_profile.update(driver="Gtiff",
#                         compress="DEFLATE",
#                         tiled=False,
#                         dtype=np.int16,
#                         nodata=32767,
#                         transform=nir_src.transform)
#      nir_profile.pop('tiled', None)
#      with rasterio.Env(GDAL_CACHEMAX=512) as env:
#          with rasterio.open(str(out_path), "w", **nir_profile) as dst:
#              dst.write(ndvi_masked, 1)
#      return Path(str(out_path)).absolute

#  def create_raw_ndwimcf(nir_path: Union[str, pathlib.PosixPath],
#                         green_path: Union[str, pathlib.PosixPath],
#                         out_path: Union[str, pathlib.PosixPath]="./raw_ndwimcf.tif") -> pathlib.PosixPath:
#      """
#      Creates a NDWI (McFeeters) raster from GREEN and NIR rasters.

#      :param nir_path: path to the NIR raster.
#      :param green_path: path to the GREEN raster.
#      :param out_path: path to the output raster.
#      """
#      logger.info("creating raw NDWIMCF (tiff - int16)")

#      with rasterio.open(str(nir_path)) as nir_src, \
#              rasterio.open(str(green_path)) as green_src:

#          nir_profile = nir_src.profile

#          nir = nir_src.read(1).astype(np.float32)
#          green = green_src.read(1).astype(np.float32)

#          np.seterr(divide='ignore', invalid='ignore')    # ignore warnings when dividing by zero
#          ndwimcf = ((green - nir) / (green + nir)*10000).astype(np.int16)

#          ndwimcf_masked = np.where(nir != 0, ndwimcf, 32767)

#      nir_profile.update(driver="Gtiff",
#                         compress="DEFLATE",
#                         tiled=False,
#                         dtype=np.int16,
#                         nodata=32767,
#                         transform=nir_src.transform)
#      nir_profile.pop('tiled', None)
#      with rasterio.Env(GDAL_CACHEMAX=512) as env:
#          with rasterio.open(str(out_path), "w", **nir_profile) as dst:
#              dst.write(ndwimcf_masked, 1)
#      return Path(str(out_path)).absolute


def create_raw_ndr(
    baseline,
    b1_path: Union[str, pathlib.PosixPath],
    b2_path: Union[str, pathlib.PosixPath],
    out_path: Union[str, pathlib.PosixPath] = "./raw_ndr.tif",
) -> pathlib.PosixPath:
    """
    Creates a generic normalized difference ratio raster from B1 and B2 rasters.
    NDR = (B1 - B2) / (B1 + B2)
    :param b1_path: path to the B1 raster.
    :param b2_path: path to the B2 raster.
    :param out_path: path to the output raster.
    """
    logger.info(
        "creating raw generic NDR ({}, {})".format(
            Path(b1_path).name, Path(b2_path).name
        )
    )

    with rasterio.open(str(b1_path)) as b1_src, rasterio.open(
        str(b2_path)
    ) as b2_src:
        b1_profile = b1_src.profile
        np.seterr(
            divide="ignore", invalid="ignore"
        )  # ignore warnings when dividing by zero
        b1 = b1_src.read(
            1,
            out_shape=(
                1,
                max(b1_src.height, b2_src.height),
                max(b1_src.width, b2_src.width),
            ),
            resampling=Resampling.bilinear,
        ).astype(np.float32)
        b2 = b2_src.read(
            1,
            out_shape=(
                1,
                max(b1_src.height, b2_src.height),
                max(b1_src.width, b2_src.width),
            ),
            resampling=Resampling.bilinear,
        ).astype(np.float32)
        if version.parse(baseline) >= version.parse("4.0"):
            logger.info('Baseline >= 4.0 detected, changing DN calculation')
            ndr = (((b1-1000) - (b2-1000)) / ((b1-1000) + (b2-1000)) * 10000).astype(np.int16)
            ndr_masked = np.where(b1 != 0, ndr, 32767)
        else:
            logger.info('Baseline <= 4.0 detected, sticking with original DN calculation')
            ndr = ((b1 - b2) / (b1 + b2) * 10000).astype(np.int16)
            ndr_masked = np.where(b1 != 0, ndr, 32767)

    b1_profile.update(
        driver="Gtiff",
        compress="DEFLATE",
        tiled=False,
        dtype=np.int16,
        nodata=32767,
        transform=b1_src.transform,
    )
    b1_profile.pop("tiled", None)
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path), "w", **b1_profile) as dst:
            dst.write(ndr_masked, 1)
    return Path(str(out_path)).absolute


#  def create_raw_ndwigao(nir_path: Union[str, pathlib.PosixPath],
#          swir_path: Union[str, pathlib.PosixPath],
#          out_path: Union[str, pathlib.PosixPath]="./raw_ndwigao.tif") -> pathlib.PosixPath:
#      """
#      Creates a NDWI raster from NIR and SWIR rasters.

#      :param nir_path: path to the NIR raster.
#      :param swir_path: path to the SWIR raster.
#      :param out_path: path to the output raster.
#      """
#      logger.info("creating raw NDWIGAO (tiff - int16)")

#      with rasterio.open(str(nir_path)) as nir_src, \
#              rasterio.open(str(swir_path)) as swir_src:

#          nir_profile = nir_src.profile

#          nir = nir_src.read(1).astype(np.float32)
#          swir = swir_src.read(1).astype(np.float32)

#          swir_reproj = np.empty(nir.shape, dtype=np.float32)
#          reproject(source=swir,
#                    destination=swir_reproj,
#                    src_transform=swir_src.transform,
#                    src_crs=swir_src.crs,
#                    dst_transform=nir_src.transform,
#                    dst_crs=nir_src.crs,
#                    resampling=Resampling.bilinear)

#          np.seterr(divide='ignore', invalid='ignore')    # ignore warnings when dividing by zero
#          ndwi = ((nir - swir_reproj) / (nir + swir_reproj)*10000).astype(np.int16)

#          ndwi_masked = np.where(nir != 0, ndwi, 32767)

#      nir_profile.update(driver="Gtiff",
#                         compress="DEFLATE",
#                         tiled=False,
#                         dtype=np.int16,
#                         nodata=32767,
#                         transform=nir_src.transform)
#      nir_profile.pop('tiled', None)
#      with rasterio.Env(GDAL_CACHEMAX=512) as env:
#          with rasterio.open(str(out_path), "w", **nir_profile) as dst:
#              dst.write(ndwi_masked, 1)
#      return Path(str(out_path)).absolute

#  def create_raw_mndwi(green_path: Union[str, pathlib.PosixPath],
#          swir_path: Union[str, pathlib.PosixPath],
#          out_path: Union[str, pathlib.PosixPath]="./raw_mndwi.tif") -> pathlib.PosixPath:
#      """
#      Creates a MNDWI raster from GREEN and SWIR rasters.

#      :param green_path: path to the GREEN raster.
#      :param swir_path: path to the SWIR raster.
#      :param out_path: path to the output raster.
#      """
#      logger.info("creating raw MNDWI (tiff - int16)")

#      with rasterio.open(str(green_path)) as green_src, \
#              rasterio.open(str(swir_path)) as swir_src:

#          green_profile = green_src.profile
#          # swir_profile = swir_src.profile

#          green = green_src.read(1).astype(np.float32)
#          swir = swir_src.read(1).astype(np.float32)

#          # reproject swir band (20m) to nir band resolution (10m)
#          swir_reproj = np.empty(green.shape, dtype=np.float32)
#          reproject(source=swir,
#                    destination=swir_reproj,
#                    src_transform=swir_src.transform,
#                    src_crs=swir_src.crs,
#                    dst_transform=green_src.transform,
#                    dst_crs=green_src.crs,
#                    resampling=Resampling.bilinear)

#          np.seterr(divide='ignore', invalid='ignore')    # ignore warnings when dividing by zero
#          ndwi = ((green - swir_reproj) / (green + swir_reproj)*10000).astype(np.int16)

#          ndwi_masked = np.where(green != 0, ndwi, 32767)

#      green_profile.update(driver="Gtiff",
#                         compress="DEFLATE",
#                         tiled=False,
#                         dtype=np.int16,
#                         nodata=32767,
#                         transform=green_src.transform)
#      green_profile.pop('tiled', None)
#      with rasterio.Env(GDAL_CACHEMAX=512) as env:
#          with rasterio.open(str(out_path), "w", **green_profile) as dst:
#              dst.write(ndwi_masked, 1)
#      return Path(str(out_path)).absolute

#  def create_raw_ndre(nir_path: Union[str, pathlib.PosixPath],
#          redge_path: Union[str, pathlib.PosixPath],
#          out_path: Union[str, pathlib.PosixPath]="./raw_ndre.tif") -> pathlib.PosixPath:
#      """
#      Creates a NDRE raster from NIR and RED EDGE rasters.

#      :param nir_path: path to the NIR raster.
#      :param redge_path: path to the RED EDGE raster.
#      :param out_path: path to the output raster.
#      """
#      logger.info("creating raw NDRE (tiff - int16)")

#      with rasterio.open(str(nir_path)) as nir_src, \
#              rasterio.open(str(redge_path)) as redge_src:

#          nir_profile = nir_src.profile

#          nir = nir_src.read(1).astype(np.float32)
#          redge = redge_src.read(1).astype(np.float32)

#          # reproject redge band (20m) to nir band resolution (10m)
#          redge_reproj = np.empty(nir.shape, dtype=np.float32)
#          reproject(source=redge,
#                    destination=redge_reproj,
#                    src_transform=redge_src.transform,
#                    src_crs=redge_src.crs,
#                    dst_transform=nir_src.transform,
#                    dst_crs=nir_src.crs,
#                    resampling=Resampling.bilinear)

#          np.seterr(divide='ignore', invalid='ignore')    # ignore warnings when dividing by zero
#          ndre = ((nir - redge_reproj) / (nir + redge_reproj)*10000).astype(np.int16)

#          ndre_masked = np.where(nir != 0, ndre, 32767)

#      nir_profile.update(driver="Gtiff",
#                         compress="DEFLATE",
#                         tiled=False,
#                         dtype=np.int16,
#                         nodata=32767,
#                         transform=nir_src.transform)
#      nir_profile.pop('tiled', None)
#      with rasterio.Env(GDAL_CACHEMAX=512) as env:
#          with rasterio.open(str(out_path), "w", **nir_profile) as dst:
#              dst.write(ndre_masked, 1)
#      return Path(str(out_path)).absolute


def create_raw_ireci(
    baseline,
    b1_path: Union[str, pathlib.PosixPath],
    b2_path: Union[str, pathlib.PosixPath],
    b3_path: Union[str, pathlib.PosixPath],
    b4_path: Union[str, pathlib.PosixPath],
    out_path: Union[str, pathlib.PosixPath] = "./raw_ireci.tif",
) -> pathlib.PosixPath:
    """
    Creates an IRECI raster from NIR, RED and RED EDGE rasters.

    :param b1_path: path to the NIR raster.
    :param b2_path: path to the RED raster.
    :param b3_path: path to the first RED EDGE raster.
    :param b4_path: path to the second RED EDGE raster.
    :param out_path: path to the output raster.
    """
    logger.info("creating raw IRECI (tiff - int16)")

    with rasterio.open(str(b1_path)) as b1_src, rasterio.open(
        str(b2_path)
    ) as b2_src, rasterio.open(str(b3_path)) as b3_src, rasterio.open(
        str(b4_path)
    ) as b4_src:
        b2_profile = b2_src.profile
        np.seterr(
            divide="ignore", invalid="ignore"
        )  # ignore warnings when dividing by zero
        b1 = b1_src.read(
            1,
            out_shape=(
                1,
                max(
                    b1_src.height, b2_src.height, b3_src.height, b4_src.height
                ),
                max(b1_src.width, b2_src.width, b3_src.width, b4_src.width),
            ),
            resampling=Resampling.bilinear,
        ).astype(np.float32)
        b2 = b2_src.read(
            1,
            out_shape=(
                1,
                max(
                    b1_src.height, b2_src.height, b3_src.height, b4_src.height
                ),
                max(b1_src.width, b2_src.width, b3_src.width, b4_src.width),
            ),
            resampling=Resampling.bilinear,
        ).astype(np.float32)
        b3 = b3_src.read(
            1,
            out_shape=(
                1,
                max(
                    b1_src.height, b2_src.height, b3_src.height, b4_src.height
                ),
                max(b1_src.width, b2_src.width, b3_src.width, b4_src.width),
            ),
            resampling=Resampling.bilinear,
        ).astype(np.float32)
        b4 = b4_src.read(
            1,
            out_shape=(
                1,
                max(
                    b1_src.height, b2_src.height, b3_src.height, b4_src.height
                ),
                max(b1_src.width, b2_src.width, b3_src.width, b4_src.width),
            ),
            resampling=Resampling.bilinear,
        ).astype(np.float32)
        if version.parse(baseline) >= version.parse("4.0"):
            logger.info('Baseline >= 4.0 detected, changing DN calculation')
            ireci = ((b4-1000) * ((b1-1000) - (b2-1000)) / (b3-1000)).astype(np.int16)
            ireci_masked = np.where(b1 != 0, ireci, 32767)
        else:
            logger.info('Baseline <= 4.0 detected, sticking with original DN calculation')
            ireci = (b4 * (b1 - b2) / b3).astype(np.int16)
            ireci_masked = np.where(b1 != 0, ireci, 32767)

    b2_profile.update(
        driver="Gtiff",
        compress="DEFLATE",
        tiled=False,
        dtype=np.int16,
        nodata=32767,
        transform=b2_src.transform,
    )
    b2_profile.pop("tiled", None)
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path), "w", **b2_profile) as dst:
            dst.write(ireci_masked, 1)
    return Path(str(out_path)).absolute


def create_raw_bigr(
	baseline,
    red_path: Union[str, pathlib.PosixPath],
    green_path: Union[str, pathlib.PosixPath],
    out_path: Union[str, pathlib.PosixPath] = "./raw_bigr.tif",
) -> pathlib.PosixPath:
    """
    Creates a BI (Green, Red) raster from GREEN and RED rasters.

    :param red_path: path to the RED raster.
    :param green_path: path to the GREEN raster.
    :param out_path: path to the output raster.
    """
    logger.info("creating raw BIGR (tiff - int16)")

    with rasterio.open(str(red_path)) as red_src, rasterio.open(
        str(green_path)
    ) as green_src:
        red_profile = red_src.profile
        red = red_src.read(1).astype(np.float32)
        green = green_src.read(1).astype(np.float32)
        np.seterr(
            divide="ignore", invalid="ignore"
        )  # ignore warnings when dividing by zero
        if version.parse(baseline) >= version.parse("4.0"):
            logger.info('Baseline >= 4.0 detected, changing DN calculation')
            bigr = ((((green-1000) ** 2 + (red-1000) ** 2) / 2) ** 0.5).astype(np.int16)
            bigr_masked = np.where(red != 0, bigr, 32767)
        else:
            logger.info('Baseline <= 4.0 detected, sticking with original DN calculation')
            bigr = ((((green) ** 2 + (red) ** 2) / 2) ** 0.5).astype(np.int16)
            bigr_masked = np.where(red != 0, bigr, 32767)


    red_profile.update(
        driver="Gtiff",
        compress="DEFLATE",
        tiled=False,
        dtype=np.int16,
        nodata=32767,
        transform=red_src.transform,
    )
    red_profile.pop("tiled", None)
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path), "w", **red_profile) as dst:
            dst.write(bigr_masked, 1)
    return Path(str(out_path)).absolute


def create_raw_birnir(
    baseline,
    red_path: Union[str, pathlib.PosixPath],
    nir_path: Union[str, pathlib.PosixPath],
    out_path: Union[str, pathlib.PosixPath] = "./raw_birnir.tif",
) -> pathlib.PosixPath:
    """
    Creates a BI (Red, NIR) raster from RED and NIR rasters.

    :param red_path: path to the RED raster.
    :param nir_path: path to the NIR raster.
    :param out_path: path to the output raster.
    """
    logger.info("creating raw BIRNIR (tiff - int16)")

    with rasterio.open(str(red_path)) as red_src, rasterio.open(
        str(nir_path)
    ) as nir_src:
        red_profile = red_src.profile
        red = red_src.read(1).astype(np.float32)
        nir = nir_src.read(1).astype(np.float32)
        np.seterr(
            divide="ignore", invalid="ignore"
        )  # ignore warnings when dividing by zero
        if version.parse(baseline) >= version.parse("4.0"):
            logger.info('Baseline >= 4.0 detected, changing DN calculation')
            birnir = ((((nir-1000) ** 2 + (red-1000) ** 2) / 2) ** 0.5).astype(np.int16)
            birnir_masked = np.where(red != 0, birnir, 32767)
        else:
            logger.info('Baseline <= 4.0 detected, sticking with original DN calculation')
            birnir = ((((nir) ** 2 + (red) ** 2) / 2) ** 0.5).astype(np.int16)
            birnir_masked = np.where(red != 0, birnir, 32767)


    red_profile.update(
        driver="Gtiff",
        compress="DEFLATE",
        tiled=False,
        dtype=np.int16,
        nodata=32767,
        transform=red_src.transform,
    )
    red_profile.pop("tiled", None)
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path), "w", **red_profile) as dst:
            dst.write(birnir_masked, 1)
    return Path(str(out_path)).absolute


def create_raw_bibg(
    baseline,
    blue_path: Union[str, pathlib.PosixPath],
    green_path: Union[str, pathlib.PosixPath],
    out_path: Union[str, pathlib.PosixPath] = "./raw_bibg.tif",
) -> pathlib.PosixPath:
    """
    Creates a BI (Blue, Green) raster from BLUE and GREEN rasters.

    :param blue_path: path to the BLUE raster.
    :param green_path: path to the GREEN raster.
    :param out_path: path to the output raster.
    """
    logger.info("creating raw BIBG (tiff - int16)")

    with rasterio.open(str(blue_path)) as blue_src, rasterio.open(
        str(green_path)
    ) as green_src:
        blue_profile = blue_src.profile
        blue = blue_src.read(1).astype(np.float32)
        green = green_src.read(1).astype(np.float32)
        np.seterr(
            divide="ignore", invalid="ignore"
        )  # ignore warnings when dividing by zero
        if version.parse(baseline) >= version.parse("4.0"):
            logger.info('Baseline >= 4.0 detected, changing DN calculation')
            bibg = ((((green-1000) ** 2 + (blue-1000) ** 2) / 2) ** 0.5).astype(np.int16)
            bibg_masked = np.where(blue != 0, bibg, 32767)
        else:
            logger.info('Baseline <= 4.0 detected, sticking with original DN calculation')
            bibg = ((((green) ** 2 + (blue) ** 2) / 2) ** 0.5).astype(np.int16)
            bibg_masked = np.where(blue != 0, bibg, 32767)


    blue_profile.update(
        driver="Gtiff",
        compress="DEFLATE",
        tiled=False,
        dtype=np.int16,
        nodata=32767,
        transform=blue_src.transform,
    )
    blue_profile.pop("tiled", None)
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path), "w", **blue_profile) as dst:
            dst.write(bibg_masked, 1)
    return Path(str(out_path)).absolute


# A faire
def create_raw_bi(
    baseline,
    b1_path: Union[str, pathlib.PosixPath],
    b2_path: Union[str, pathlib.PosixPath],
    out_path: Union[str, pathlib.PosixPath] = "./raw_bi.tif",
) -> pathlib.PosixPath:
    """
    Creates a BI (Green, Red) raster from GREEN and RED rasters.

    :param red_path: path to the RED raster.
    :param green_path: path to the GREEN raster.
    :param out_path: path to the output raster.
    """
    logger.info("creating raw BIGR (tiff - int16)")

    with rasterio.open(str(red_path)) as red_src, rasterio.open(
        str(green_path)
    ) as green_src:
        red_profile = red_src.profile
        red = red_src.read(1).astype(np.float32)
        green = green_src.read(1).astype(np.float32)
        np.seterr(
            divide="ignore", invalid="ignore"
        )  # ignore warnings when dividing by zero
        if version.parse(baseline) >= version.parse("4.0"):
            logger.info('Baseline >= 4.0 detected, changing DN calculation')
            bigr = ((((green-1000) ** 2 + (red-1000) ** 2) / 2) ** 0.5).astype(np.int16)
            bigr_masked = np.where(red != 0, bigr, 32767)
        else:
            logger.info('Baseline <= 4.0 detected, sticking with original DN calculation')
            bigr = ((((green) ** 2 + (red) ** 2) / 2) ** 0.5).astype(np.int16)
            bigr_masked = np.where(red != 0, bigr, 32767)


    red_profile.update(
        driver="Gtiff",
        compress="DEFLATE",
        tiled=False,
        dtype=np.int16,
        nodata=32767,
        transform=red_src.transform,
    )
    red_profile.pop("tiled", None)
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path), "w", **red_profile) as dst:
            dst.write(bigr_masked, 1)
    return Path(str(out_path)).absolute

def create_raw_evi(
    baseline,
    blue_path: Union[str, pathlib.PosixPath],
    red_path: Union[str, pathlib.PosixPath],
    nir_path: Union[str, pathlib.PosixPath],
    out_path: Union[str, pathlib.PosixPath] = "./raw_evi.tif",
) -> pathlib.PosixPath:
    """
    Creates en EVI raster from BLUE, RED, NIR rasters.

    :param blue_path: path to the BLUE raster
    :param red_path: path to the RED raster.
    :param nir_path: path to the NIR raster.
    :param out_path: path to the output raster.
    """
    logger.info("creating raw EVI (tiff - int16)")

    with rasterio.open(str(red_path)) as red_src, rasterio.open(str(blue_path)) as blue_src, rasterio.open(str(nir_path)) as nir_src:
        red_profile = red_src.profile
        red = red_src.read(1).astype(np.float32)
        blue = blue_src.read(1).astype(np.float32)
        nir = nir_src.read(1).astype(np.float32)
        np.seterr(
            divide="ignore", invalid="ignore"
        )  # ignore warnings when dividing by zero
        if version.parse(baseline) >= version.parse("4.0"):
            logger.info('Baseline >= 4.0 detected, changing DN calculation')
            evi = (2.5 * (((nir-1000)/10000) - ((red-1000)/10000)) / (((nir-1000)/10000) + 6.0 * ((red-1000)/10000) - 7.5 * ((blue-1000)/10000) + 1)*10000).astype(np.int32)
            evi_masked = np.where(red != 0, evi, 32767)
        else:
            logger.info('Baseline <= 4.0 detected, sticking with original DN calculation')
            evi = (2.5 * ((nir/10000 - red/10000)) / ((nir/10000 + 6.0 * red/10000 - 7.5 * blue/10000) + 1)*10000).astype(np.int32)
            evi_masked = np.where(red != 0, evi, 32767)


    red_profile.update(
        driver="Gtiff",
        compress="DEFLATE",
        tiled=False,
        dtype=np.int16,
        nodata=32767,
        transform=red_src.transform,
    )
    red_profile.pop("tiled", None)
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path), "w", **red_profile) as dst:
            dst.write(evi_masked, 1)
    return Path(str(out_path)).absolute

def create_masked_indice(
    indice_path: Union[str, pathlib.PosixPath],
    cloud_mask_path: Union[str, pathlib.PosixPath],
    out_path: Union[str, pathlib.PosixPath] = "./masked_indice.tif",
) -> pathlib.PosixPath:
    """
    Masks an indice raster with a cloud mask.

    :param indice_path: path to the NDVI raster.
    :param cloud_mask_path: path to the cloud mask raster.
    :param out_path: path to the output raster.
    """
    # TODO: Improve the function in order to mask any raster.

    logger.info("Cloud-masking indice (int16)")

    with rasterio.open(str(indice_path)) as indice_src, rasterio.open(
        str(cloud_mask_path)
    ) as cld_src:

        profile = indice_src.profile
        raw_indice = indice_src.read(1)
        cld = cld_src.read(1)
        # repoject cloud_mask to ndvi resolution
        cld_reproj = np.empty(raw_indice.shape, dtype=np.uint8)
        reproject(
            source=cld,
            destination=cld_reproj,
            src_transform=cld_src.transform,
            src_crs=cld_src.crs,
            dst_transform=indice_src.transform,
            dst_crs=indice_src.crs,
            resampling=Resampling.nearest,
        )
        #        indice_borders_mask = np.where(raw_indice > 0, raw_indice, 32767)
        indice_cloud_mask = np.where(cld_reproj == 0, raw_indice, 32767)
        if indice_path.suffix == ".jp2":
            indice_cloud_mask = np.where(
                indice_cloud_mask == 16383, 32767, indice_cloud_mask
            )
        profile.update(
            driver="Gtiff",
            compress="DEFLATE",
            tiled=False,
            dtype=np.int16,
            nodata=32767,
            transform=indice_src.transform,
        )
    #        profile.pop('tiled', None)

    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_path), "w", **profile) as dst:
            dst.write(indice_cloud_mask, 1)

    return str(Path(str(out_path)).absolute)


def index_tiff_2_jp2(
    img_path: Union[str, pathlib.PosixPath],
    out_path: Union[str, pathlib.PosixPath] = "./indice_2_jp2.jp2",
    quality: int = 20,
) -> pathlib.PosixPath:
    """
    Convert a indice file from TIF to JP2.
    :param out_path: path to the output raster.
    """
    logger.info("converting raw indice to JPEG2000")
    src_ds = gdal.Open(str(img_path))
    driver = gdal.GetDriverByName("JP2OpenJPEG")
    for i in range(src_ds.RasterCount):
        src_ds.GetRasterBand(i + 1).SetNoDataValue(float(16383))
    dst_ds = driver.CreateCopy(
        str(out_path),
        src_ds,
        options=["NBITS=15", "CODEC=JP2", "QUALITY=" + str(quality)],
    )
    dst_ds = None
    src_ds = None
    return str(Path(str(out_path)).absolute)
