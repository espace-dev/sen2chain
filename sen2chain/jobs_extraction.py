
"""
Module for managing sen2chain processing jobs
"""

import logging
import pathlib
from pathlib import Path
import pandas as pd
import datetime, time
from itertools import chain, groupby
import re
import setuptools
from crontab import CronTab
from collections import OrderedDict
from configparser import ConfigParser
import copy
import numpy as np
import subprocess
from typing import Sequence, List, Dict, Union

from .time_series_compil import TimeSeriesCompil
from .config import SHARED_DATA, Config
from .indices import IndicesCollection
from .library import Library
from .tiles import Tile
from .utils import datetime_to_str
from .multi_processing import (
    l2a_multiprocessing,
    idx_multiprocessing,
    cld_version_probability_iterations_reprocessing_multiprocessing,
)
from .download_eodag import (
    S2cEodag_download, 
    S2cEodag_download_uniq,
)


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class JobExtraction:

    def __init__(self, jid: str):
        self._config_path = Config()._JOBS_EXTR_DIR / ("job_" + jid + ".cfg")
        self._python_script_path = Config()._JOBS_EXTR_DIR / ("jobextraction_" + jid + ".py")
        self.jid = jid
        if self._config_path.exists():
            logger.info("Reading existing config...")
            self.read(self._config_path)
        else:
            logger.info("Creating new job...")
            self.init()
        self._cron = CronTab(user=True)
        self.cron_status = self.get_cron_status()
        self._log_folder_path = Path(Config().get("log_path")) / ("job_" + jid)
        

    def __repr__(self):
        return (
            "logs: {}"
            "\ntiming: {}"
            "\ncron_status: {}"
            "\nnb_proc: {}"
            "\nprocess_by_line: {}"
            "\n\ntasks:\n{}".format(
                self.logs, 
                self.timing,
                self.cron_status,
                self.nb_proc,
                self.process_by_line,   
                self.tasks,
            )
        )
    def init(self):
        raw_job_cfg = SHARED_DATA.get("raw_job_extr_cfg")
        self.read(raw_job_cfg)
        first_row = OrderedDict(
            [
                ("shapefile", "False"),
                ("date_min", "2015-01-01"),
                ("date_max", datetime_to_str(datetime.datetime.now(), date_format = 'ymd')),
                ("max_clouds", [100]),
                ("cloudmasks", "False"),
                ("indices", ["False"]),
                ("force_extraction", "False"),
                ("compiled_data", ["False"]),
                ("copy_server", "False"),
            ]
        )
        self.tasks = pd.DataFrame(first_row)

    def read(self, path):
        parser = ConfigParser(allow_no_value=True, strict = False)
        with open(str(path)) as stream:
            parser.read_string(
                "[top]\n" + stream.read(),
            )  # This line does the trick.
        self.logs = bool(
            setuptools.distutils.util.strtobool(
                parser["top"]["logs"]
            )
        )
        self.timing = parser["top"]["timing"]

        try:
            self.nb_proc = int(parser["top"]["nb_proc"])
        except:
            self.nb_proc = 8

        try:
            self.process_by_line = int(parser["top"]["process_by_line"])
        except:
            self.process_by_line = 0         
            
        for i in range(1, 11):
            try:
                self.tasks = pd.read_csv(
                    path,
                    sep=";",
                    na_values="",
                    na_filter=False,
                    comment="#",
                    dtype=str,
                    header = i,
                    skip_blank_lines = True,
                )
                if 'cloudmask' in self.tasks:
                    self.tasks.rename(
                        columns={"cloudmask": "cloudmasks"}, 
                        inplace=True,
                    )
                self.format_cm_indices()      
                break
            except:
                pass

    def task_add(self, row: dict = None):
        """
        Add a task to the job.

        :param row: Dict of specific parameters for task actions. If no dict provided, will provide a default task format.
        :type row: Dict
        """
        if not row:
            logger.info("No row provided, using default")
            row = pd.DataFrame(
                {
                    "shapefile": "False",
                    "date_min": "2015-01-01",
                    "date_max": datetime_to_str(datetime.datetime.now(), date_format = 'ymd'),
                    "max_clouds": [100],
                    "cloudmasks": "False",
                    "indices": ["False"],
                    "force_extraction": "False",
                    "compiled_data": ["False"],
                    "copy_server": "False",
                }
            )

        self.tasks = pd.concat([self.tasks, row], ignore_index=True)[
            self.tasks.columns
        ]
        self.format_cm_indices()
        logger.info("\n{}".format(self.tasks))

    def task_remove(self, task_id: int = None):
        """
        Remove a task from job. The task line is deleted from the config file.

        :param task_id: Task identifier (row number).
        :type task_id: int

        """
        if task_id is None:
            logger.info("Please provide task_id to remove")
        else:
            if task_id in self.tasks.index:
                self.tasks.drop(labels=task_id, axis=0, inplace=True)
                self.tasks.reset_index(inplace=True, drop=True)
                logger.info("\n{}".format(self.tasks))
            else:
                logger.info("task_id not found")

    def task_edit(self, task_id: int = None, **kwargs):
        """
        Edit an existing task, identified by task_id (row number)

        :param task_id: Task identifier, or row number in config file.
        :type task_id: int
        :param kwargs: Any keys/items pairs from task actions options.

        """
        if task_id is None:
            logger.info(
                "Please provide task_id to edit, if no task in job, create_task first"
            )
        else:
            if task_id in self.tasks.index:
                for arg, val in kwargs.items():
                    if arg in self.tasks.columns:
                        self.tasks.loc[task_id, arg] = val
                        logger.info(
                            "Line {}: {} updated to {}".format(
                                task_id, arg, val
                            )
                        )
                    else:
                        logger.info("{} not found".format(arg))
                self.format_cm_indices()
                logger.info("\n{}".format(self.tasks))
            else:
                logger.info("task_id not found")
                
    def format_cm_indices(self):
        for index, row in self.tasks.iterrows():
            if not row.indices == "False":
                if row.indices == "All":
                    self.tasks.at[
                        index, "indices"
                    ] = IndicesCollection.available_indices
                else:
                    if not isinstance(self.tasks.at[index, "indices"] , list):
                        self.tasks.at[index, "indices"] = str(row.indices).split(
                            "/"
                        )
            if not row.cloudmasks == "False":
                if not isinstance(self.tasks.at[index, "cloudmasks"] , list):
                    self.tasks.at[index, "cloudmasks"] = str(row.cloudmasks).split(
                        "/"
                    )
            if not row.compiled_data == "False":
                if not isinstance(self.tasks.at[index, "compiled_data"] , list):
                    self.tasks.at[index, "compiled_data"] = str(row.compiled_data).split(
                        "/"
                    )
    def unformat_cm_indices(self):
        for index, row in self.tasks.iterrows():
            if not((row.indices == "False") or (row.indices == "All")):
                # logger.info(row.indices)
                self.tasks.at[index, "indices"] = '/'.join(self.tasks.at[index, "indices"])   
            if not (row.cloudmasks == "False"):
                self.tasks.at[index, "cloudmasks"] = '/'.join(self.tasks.at[index, "cloudmasks"])
            if not row.compiled_data == "False":
                self.tasks.at[index, "compiled_data"] = '/'.join(self.tasks.at[index, "compiled_data"])

    @staticmethod
    def get_cm_version(identifier) -> str:
        """
        Returns cloudmask version from a cloud mask identifier string.
        :param identifier: string pattern from which the cloud mask version is extracted.
        :type identifier: str
        """
        try:
            pat = re.compile(r"(?P<cm_version>CM00[1-2])")
            returned_val = pat.match(identifier).groupdict()
        except Exception:
            try:
                pat = re.compile(
                    r"(?P<cm_version>CM003)"
                    + "-PRB(?P<probability>.*)"
                    + "-ITER(?P<iterations>.*)"
                )
                returned_val = pat.match(identifier).groupdict()
            except Exception:
                try:
                    pat = re.compile(
                        r"(?P<cm_version>CM004)"
                        + "-CSH(?P<cld_shad>.*)"
                        + "-CMP(?P<cld_med_prob>.*)"
                        + "-CHP(?P<cld_hi_prob>.*)"
                        + "-TCI(?P<thin_cir>.*)"
                        + "-ITER(?P<iterations>.*)"
                    )
                    returned_val = pat.match(identifier).groupdict()
                except Exception:
                    returned_val = {
                        "cm_version": "CM001",
                        "probability": 1,
                        "iterations": 1,
                        "cld_shad": True,
                        "cld_med_prob": True,
                        "cld_hi_prob": True, 
                        "thin_cir": True, 
                    }        
        

        if "probability" not in returned_val:
            returned_val["probability"] = 1
        if "iterations" not in returned_val:
            returned_val["iterations"] = 1
        if "cld_shad" not in returned_val:
            returned_val["cld_shad"] = True
        if "cld_med_prob" not in returned_val:
            returned_val["cld_med_prob"] = True
        if "cld_hi_prob" not in returned_val:
            returned_val["cld_hi_prob"] = True
        if "thin_cir" not in returned_val:
            returned_val["thin_cir"] = True
        
        returned_val["probability"] = int(returned_val["probability"])
        returned_val["iterations"] = int(returned_val["iterations"])
        returned_val["cld_shad"] = returned_val["cld_shad"] in ["1", 1, True, "True"] 
        returned_val["cld_med_prob"] = returned_val["cld_med_prob"] in ["1", 1, True, "True"]
        returned_val["cld_hi_prob"] = returned_val["cld_hi_prob"] in ["1", 1, True, "True"]
        returned_val["thin_cir"] = returned_val["thin_cir"] in ["1", 1, True, "True"]

        return returned_val
    def fill_dates_clouds(
        self,
        tasks, 
    ):
        """
        Fill in dates and cloud cover values with correct values.
        """
        for index, row in tasks.iterrows():
            if not row.max_clouds:
                tasks.at[index, "max_clouds"] = 100
            else:
                tasks.at[index, "max_clouds"] = int(tasks.at[index, "max_clouds"])
            if not row.date_min:
                # self.tasks.at[index, "start_time"] = (datetime.datetime.now()-datetime.timedelta(days=delta_t)).strftime('%Y-%m-%d')
                tasks.at[index, "date_min"] = datetime.datetime.strptime(
                    "2015-01-01", "%Y-%m-%d"
                ).strftime("%Y-%m-%d")
            elif "today" in row.date_min:
                tasks.at[index, "date_min"] = (
                    datetime.datetime.now() - datetime.timedelta(
                        days = int(row.date_min.split("-")[1])
                    )
                ).strftime("%Y-%m-%d") 
            if not row.date_max:
                tasks.at[index, "date_max"] = datetime.datetime.strptime(
                    "9999-12-31", "%Y-%m-%d"
                ).strftime("%Y-%m-%d")
            elif row.date_max == "today":
                tasks.at[index, "date_max"] = (
                    datetime.datetime.now() + datetime.timedelta(days=1)
                ).strftime("%Y-%m-%d")   
        return tasks
        

    def save(self):
        """
        Save Job config.cfg file to disk.
        """
        with open(str(self._config_path), "w") as ict:
            comments_header ="\n".join(
                [
                    "### Parameters", 
                    "# logs: True | False", 
                    "# timing: in cron format", 
                    "# nb_proc: the number of cpu cores to use for this job, default 8", 
                    "### Table of task entries", 
                    '# Each record line starting with "!" can be commented (and skipped)',
                    "# shapefile: path to your file identifier, comment line using ! before shapefile", 
                    "# date_min the start date for this task, possible values:", 
                    "#    empty (2015-01-01 will be used) | any date | today-xx (xx nb of days before today to consider)", 
                    "# date_max the last date for this task, possible values:", 
                    "#    empty (9999-12-31 will be used) | any date | today", 
                    "# cloudmasks: the cloudmask(s) to compute and use to mask indice(s). Possible values range from none (False) to multiple cloudmasks:", 
                    "#    False | CM001/CM002/CM003-PRB1-ITER5/CM004-CSH1-CMP1-CHP1-TCI1-ITER0/etc.", 
                    "# indices: False | All | NDVI/NDWIGAO/etc.", 
                    "#force_extraction :  do not consider what's already been extracted : True | False"
                    "#compiled_data : compile data by year, month, week. Possible values :"
                    "#      yearly | monthly | weekly | daily | yearly/monthly | ..."
                    "# max_clouds: max cloud cover to consider for compiled_data",
                    "#copy_server: copy on remote server. Possible value :link to remote server",

                    "",
                    "",
                ]
            )
            header = "\n".join(
                [
                    "logs = " + str(self.logs),
                    "timing = " + self.timing,
                    "nb_proc = " + str(self.nb_proc),
                    "process_by_line = " + str(self.process_by_line),
                    "",
                    "",
                ]
            )
            
            for line in comments_header:
                ict.write(line)
            
            for line in header:
                ict.write(line)
            # self.tasks.to_csv(ict)
            self.unformat_cm_indices()
            self.tasks.to_csv(ict, index=False, sep=";")
            self.format_cm_indices()
    
    def get_cron_status(self):
        """
        Returns job cron status.
        """
        iter = list(self._cron.find_comment("sen2chain_job_" + self.jid))
        if iter:
            for job in iter:
                if job.is_enabled():
                    status = "enabled"
                else:
                    status = "disabled"
                self.cron_job = job
                self.cron_timing = str(self.cron_job.slices)
        else:
            status = "absent"
            self.cron_job = None
            self.cron_timing = None
        return status

    def create_python_script(self):
        """
        Creates python script for cron.
        """
        lines = ["# -*- coding:utf-8 -*-\n"]
        lines.append("\n")
        lines.append('"""\n')
        lines.append("Module for managing sen2chain processing jobs\n")
        lines.append('"""\n')
        lines.append("\n")
        lines.append("from sen2chain import JobExtraction\n")
        lines.append(
            'JobExtraction("'
            + self.jid
            + '").run()\n'
        )
        with open(str(self._python_script_path), "w") as f:
            f.writelines(lines)

    def cron_enable(
        self,
    ):
        """
        Enables job in cron.
        """
        self.save()
        self.create_python_script()
        iter = list(self._cron.find_comment("sen2chain_job_" + self.jid))
        if iter:
            for job in iter:
                logger.info("Enabling job")
                if self.timing:
                    job.setall(self.timing)
                job.enable()
        else:
            job = self._cron.new(
                command="/usr/bin/python3 " + str(self._python_script_path),
                comment="sen2chain_job_" + self.jid,
            )
            if self.timing:
                job.setall(self.timing)
            else:
                job.setall("0 0 * * *")
            job.enable()
            logger.info("Enabling job...")

        # logger.info("Time: {}".format(job.time))
        self._cron.write()
        # new.enable(False)
        self.get_cron_status()

    def cron_disable(self):
        """
        Disable job in cron. The corresponding line is commented in cron.
        """
        iter = list(self._cron.find_comment("sen2chain_job_" + self.jid))
        if iter:
            for job in iter:
                logger.info("Disabling job...")
                job.enable(False)
            self._cron.write()
            self.get_cron_status()


    def cron_remove(self):
        """
        Remove job from cron. The corresponding line is deleted
        """
        iter = list(self._cron.find_comment("sen2chain_job_" + self.jid))
        if iter:
            for job in iter:
                logger.info("Removing job from cron...")
                self._cron.remove(job)
            self._cron.write()
            self.get_cron_status()

    def run(
        self,
    ):
        """
        Runs job immediately. Job needs to be saved first.
        Tasks are performed line by line.
        """

        tasks = copy.deepcopy(self.tasks)
        tasks = self.fill_dates_clouds(tasks)
        
        if self.logs:
            self._log_folder_path.mkdir(exist_ok=True)
            self._log_file = self._log_folder_path / (
                "job_"
                + self.jid
                + "_run_"
                + datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
                + ".log"
            )
            f = open(str(self._log_file), "w")
            f.write("{}\nStart\n\n".format(datetime.datetime.now()))
            f.write(repr(self) + "\n\n")
            f.flush()
        
        # tasks = self.tasks
        tasks = tasks[tasks["shapefile"].str.contains("!") == False]
        logger.info("Running task(s):\n{}".format(tasks))
        logger.info("Processing by lines: {}".format(self.process_by_line))
        if not tasks.empty:
            if self.process_by_line > 0:
                split_tasks = np.array_split(
                    tasks, 
                    tasks.shape[0] // self.process_by_line + min(1, tasks.shape[0] % self.process_by_line)
                )
            else:
                split_tasks = [tasks]
            for chunk in split_tasks:
                logger.info("Extracting data")
                if self.logs:
                    f.write("{}\nExtracting data\n".format(datetime.datetime.now()))
                    f.flush()
                for index, row in chunk.iterrows():
                    if not (row.shapefile == "False" or not row.shapefile):
                        shp = Path(row.shapefile)
                        if not (row.cloudmasks == "False" or not row.cloudmasks):
                            for cm in row.cloudmasks:
                                cloudmask = self.get_cm_version(cm) 
                                cmversion = cloudmask["cm_version"].upper()
                                prb = cloudmask["probability"]
                                itr = cloudmask["iterations"]
                                cld_shad = cloudmask["cld_shad"]
                                cld_med_prob = cloudmask["cld_med_prob"]
                                cld_hi_prob = cloudmask["cld_hi_prob"]
                                thin_cir = cloudmask["thin_cir"]
                                if not (row.indices == "False" or not row.indices):
                                    if (row.force_extraction == "True" or row.force_extraction=="False"):
                                        ts = TimeSeriesCompil(vectors_file=shp,date_min=row.date_min, date_max=row.date_max, cm_version=cmversion,indices=row.indices,iterations=itr,multiproc=self.nb_proc)
                                        ts.extract_data()
                                    if not (row.compiled_data == "False" or not row.compiled_data):
                                        for comp in row.compiled_data:
                                            if comp=="yearly":
                                                ts.compile_data(yearly=True,)
                                            if comp=="monthly":
                                                ts.compile_data(monthly=True)
                                            if comp=="weekly":
                                                ts.compile_data(weekly=True)
                                            if comp=="daily":
                                                ts.compile_data(daily=True)
                                            if comp=="all":
                                                ts.compile_data(datemin=ts._date_min,datemax=ts._date_max)
                        print("ok")
                        #SCP pour copier les fichiers locaux vers le serveur distant
                        print(f"copy : {row.copy_server}")
                        if not (row.copy_server == "False" or not row.copy_server):
                            print("here")
                            folder = ts._data_name  + "_" + ts._checksum
                            path = ts._out_path+"/"+ folder +"/"+ "compiled_data/"
                            commande_scp = ["scp", "-r", f"{path}", row.copy_server]
                            print(commande_scp)
                            # Commande SSH pour créer un répertoire distant
                            info = row.copy_server.split(':')
                            commande_ssh_mkdir = ["ssh", info[0], "mkdir", "-p", info[1]]
                            print(commande_ssh_mkdir)
                            # Exécuter la commande SSH
                            resultat_ssh = subprocess.run(commande_ssh_mkdir, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                            # Exécuter la commande SCP
                            try:
                                resultat_scp = subprocess.run(commande_scp, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                                # Afficher le résultat de la commande SCP
                                print("Sortie standard de la commande SCP :")
                                print(resultat_scp.stdout.decode())
                            except subprocess.CalledProcessError as e:
                                print(f"Erreur de la commande SCP : {e.returncode}. Erreur : {e.stderr.decode()}")
                                                  
