# coding: utf-8

"""
This module contains usefull functions commons to every modules.
"""

import os, stat
import shutil
import logging
from itertools import zip_longest
from datetime import datetime
from typing import Iterable, Union
import re
import pathlib
from pathlib import Path
from packaging import version
from operator import itemgetter

from .config import Config
# from .products import L1cProduct

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


# useful dates formats encountered when working with Sentinel-2 data
DATES_FORMATS = dict(
    ymd="%Y-%m-%d",
    Ymd = "%Y%m%d",
    filename="%Y%m%dT%H%M%S",
    metadata="%Y-%m-%dT%H:%M:%S.%fZ"
)

def format_word(word: str) -> str:
    """
    Formats a word to lower case and removes heading and trailing spaces.

    :param word: a string to format.
    """
    if not isinstance(word, str):
        raise TypeError("word has to be a string")
    return word.lower().strip()


def grouper(iterable, n: int, fillvalue: bool = None) -> Iterable:
    """
    Collects data into fixed-length chunks or blocks.

    :param iterable: an iterable.
    :param n: number of items per chunk/block.
    :param fillvalue: replacing value for missing items in the chunk.
    """
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def str_to_datetime(date_string: str, date_format: str) -> datetime:
    """
    Returns a datetime object from a string according to a format.

    :param date_string: string to format.
    :param date_format: date format.
    """
    try:
        date_format = DATES_FORMATS.get(date_format)
    except KeyError:
        raise ValueError("Invalid date format")
    return datetime.strptime(date_string, date_format)


def datetime_to_str(date: datetime, date_format: str) -> str:
    """
    Returns a string from a datetime object according to a format.

    :param date: string to format.
    :param date_format: date format.
    """
    try:
        date_format = DATES_FORMATS.get(date_format)
    except KeyError:
        raise ValueError("Invalid date format")
    return datetime.strftime(date, date_format)


def human_size(bytes, units=["B", "KB", "MB", "GB", "TB", "PB", "EB"]):
    """Returns a human readable string reprentation of bytes"""
    return (
        str(bytes) + units[0]
        if bytes < 1024.0
        else human_size(bytes >> 10, units[1:])
    )


def human_size_decimal(size, decimal_places=2):
    for unit in ["B", "KB", "MB", "GB", "TB", "PB", "EB"]:
        if size < 1024.0:
            break
        size /= 1024.0
    return "{}{}".format(format(size, "." + str(decimal_places) + "f"), unit)
    # return f"{size:.{decimal_places}f}{unit}"


def getFolderSize(folder, follow_symlinks=False):
    total_size = os.path.getsize(folder)
    try:
        for item in os.listdir(folder):
            itempath = os.path.join(folder, item)
            if os.path.isfile(itempath) and (
                follow_symlinks or not os.path.islink(itempath)
            ):
                total_size += os.path.getsize(itempath)
            elif os.path.isdir(itempath) and (
                follow_symlinks or not os.path.islink(itempath)
            ):
                total_size += getFolderSize(itempath)
    except Exception:
        pass
    return total_size

def set_permissions(path):
    # adds write permissions to owner & group of safe folder
    os.chmod(
        str(path), 
        os.stat(str(path)).st_mode | stat.S_IWUSR | stat.S_IWGRP
    )
    # removes write permissions to other of safe folder
    os.chmod(
        str(path), 
        os.stat(str(path)).st_mode & ~stat.S_IWOTH
    )
    # if parent setgit set, set setgid and gid for safe folder
    if os.stat(path.parent).st_mode & stat.S_ISGID:
        os.chmod(
            path, 
            os.stat(path).st_mode | stat.S_ISGID
        )
        group = path.parent.group()
        shutil.chown(path, group = group)           
    for dir_path, dir_names, files in os.walk(str(path)):
        for d in dir_names:
            name = dir_path + "/" + d
            os.chmod(
                name, 
                os.stat(name).st_mode | stat.S_IWUSR | stat.S_IWGRP
            )
            os.chmod(
                name, 
                os.stat(name).st_mode & ~stat.S_IWOTH
            )
            # if parent setgit set, set setgid and gid for subdirs
            if os.stat(path.parent).st_mode & stat.S_ISGID:
                os.chmod(
                    name, 
                    os.stat(name).st_mode | stat.S_ISGID
                )
                group = path.parent.group()
                shutil.chown(name, group = group)                              
        for f in files:
            name = dir_path + "/" + f
            os.chmod(
                name, 
                os.stat(name).st_mode | stat.S_IWGRP | stat.S_IWUSR
            )
            os.chmod(
                name, 
                os.stat(name).st_mode & ~stat.S_IXOTH & ~stat.S_IWOTH
            )
            # removes file execution permissions to all           
            os.chmod(
                name, 
                os.stat(name).st_mode & ~stat.S_IXUSR & ~stat.S_IXGRP & ~stat.S_IXOTH
            )
            # if parent setgit set, set gid for files
            if os.stat(path.parent).st_mode & stat.S_ISGID:
                group = path.parent.group()
                shutil.chown(name, group = group)

def get_tile(identifier) -> str:
    """Returns tile name from a string.

    :param string: string from which to extract the tile name.
    """
    return re.findall("_T([0-9]{2}[A-Z]{3})_", identifier)[0]




def get_Sen2Cor_version(path: Union[str, pathlib.PosixPath] = None):
    """ Returns 
        - the Sen2Cor version if a Sen2Cor path is provided, None if not
    """
    # if path:
        # path = Config().get("sen2cor_bashrc_path")
    return next(iter(re.findall('Sen2Cor-(\d{2}\.\d{2}\.\d{2})', str(path))), None)

# def get_current_Sen2Cor_version():
    # """Returns your current Sen2Cor version"""
    # sen2cor_bashrc_path = Config().get("sen2cor_bashrc_path")
    # return next(
        # iter(
            # re.findall(
                # "Sen2Cor-(\d{2}\.\d{2}\.\d{2})", str(sen2cor_bashrc_path)
            # )
        # ),
        # None,
    # )
    
# def get_latest_s2c_version_path(l1c_identifier):
    # """ Returns 
    # - the path of the first Sen2Cor compatible version, starting from sen2cor_bashrc_path then sen2cor_alternative_bashrc_path
    # - None if no compatible Sen2Cor version is provided
    # """   
    # from .products import L1cProduct
    # current_path = Config().get("sen2cor_bashrc_path")
    # alternate_path = Config().get("sen2cor_alternative_bashrc_path")
    
    # if version.parse(L1cProduct(l1c_identifier).processing_baseline) >= version.parse("4.0"):
        # if version.parse(get_Sen2Cor_version(current_path)) >= version.parse("2.10.01"):
            # return current_path
        # else:
            # if version.parse(get_Sen2Cor_version(alternate_path)) >= version.parse("2.10.01"):
                # return alternate_path
            # else:
                # return None
    # elif version.parse(L1cProduct(l1c_identifier).processing_baseline) >= version.parse("2.06"):
        # return current_path
    # else:
        # if version.parse(get_Sen2Cor_version(current_path)) >= version.parse("2.10.01"):
            # if alternate_path:
                # if version.parse(get_Sen2Cor_version(alternate_path)) >= version.parse("2.10.01"):
                    # return None
                # else:
                    # return alternate_path
            # else:
                # return None
        # else:
            # return current_path

def get_Sen2Cor_versions():
    """ Returns
    - all configured Sen2Cor versions
    """
    paths = Config().get("sen2cor_bashrc_paths")
    s2c_versions = []
    for path in paths:
        s2c_versions.append(get_Sen2Cor_version(path))
    return s2c_versions

def get_latest_Sen2Cor_version():
    """Returns your latest Sen2Cor version"""
    s2c_versions = get_Sen2Cor_versions()
    return max([(i,version.parse(i)) for i in s2c_versions], key=itemgetter(1))[0]
    
def get_latest_Sen2Cor_version_from_id(l1c_identifier):
    """Returns the latest Sen2Cor version matching the l1c identifier"""
    from .products import L1cProduct

    pb = version.parse(L1cProduct(l1c_identifier).processing_baseline)
    s2c_versions = get_Sen2Cor_versions()
    
    if pb >= version.parse("4.0"):
        s2c_version = max(
            [(i, version.parse(i)) for i in s2c_versions if version.parse(i) >= version.parse("2.10.01")], 
            key=itemgetter(1)
        )[0]
    elif pb >= version.parse("2.06"):
        s2c_version = get_latest_Sen2Cor_version()
    elif pb < version.parse("2.06"):
        matching_s2c_versions = [i for i in s2c_versions if version.parse(i) < version.parse("2.10.01")]
        if matching_s2c_versions:
            s2c_version = max([(i,version.parse(i)) for i in matching_s2c_versions], key=itemgetter(1))[0]
    else:
        s2c_version = None
    return s2c_version
    
def get_Sen2Cor_path_from_version(s2c_version):
        paths = Config().get("sen2cor_bashrc_paths")  
        s2c_path = [i for i in paths if s2c_version in i]
        if s2c_path:
            return s2c_path[0]
        else:
            return None    

def get_cm_dict(identifier) -> dict:
    """Returns cloudmask version from an identifier string.
    :param string: string from which to extract the version name.
    can be a cloudmask or an indice identifier
    """
    returned_val = None
    try:
        pat = re.compile(r".*(?P<cm_version>CM00[1-2])")
        returned_val = pat.match(identifier).groupdict()
        
    except Exception:
        try:
            pat = re.compile(
                r".*(?P<cm_version>CM003)"
                + "-PRB(?P<probability>.*)"
                + "-ITER(?P<iterations>.*)"
            )
            returned_val = pat.match(identifier).groupdict()
            
        except Exception:
            try:
                pat = re.compile(
                    r".*(?P<cm_version>CM004)"
                    + "-CSH(?P<cld_shad>.*)"
                    + "-CMP(?P<cld_med_prob>.*)"
                    + "-CHP(?P<cld_hi_prob>.*)"
                    + "-TCI(?P<thin_cir>.*)"
                    + "-ITER(?P<iterations>.*)"
                )
                returned_val = pat.match(identifier).groupdict()
            except Exception:
               pass
    
    if returned_val:
        if "probability" not in returned_val:
            returned_val["probability"] = 1
        if "iterations" not in returned_val:
            returned_val["iterations"] = 5
        if "cld_shad" not in returned_val:
            returned_val["cld_shad"] = True
        if "cld_med_prob" not in returned_val:
            returned_val["cld_med_prob"] = True
        if "cld_hi_prob" not in returned_val:
            returned_val["cld_hi_prob"] = True
        if "thin_cir" not in returned_val:
            returned_val["thin_cir"] = True
        
        returned_val["probability"] = int(returned_val["probability"])
        returned_val["iterations"] = int(returned_val["iterations"])
        returned_val["cld_shad"] = returned_val["cld_shad"] in ["1", 1, True, "True"] 
        returned_val["cld_med_prob"] = returned_val["cld_med_prob"] in ["1", 1, True, "True"]
        returned_val["cld_hi_prob"] = returned_val["cld_hi_prob"] in ["1", 1, True, "True"]
        returned_val["thin_cir"] = returned_val["thin_cir"] in ["1", 1, True, "True"]
    
    return returned_val
    
def get_cm_string_from_dict(cm_dict) -> str:
    if cm_dict:
        if cm_dict["cm_version"] == "CM001":
            cm_string = "CM001"
        elif cm_dict["cm_version"] == "CM002":
            cm_string = "CM002-B11"
        elif cm_dict["cm_version"] == "CM003":
            cm_string = (
                "CM003" +
                "-PRB" + str(cm_dict["probability"])  + 
                "-ITER" + str(cm_dict["iterations"])
            )
        elif cm_dict["cm_version"] == "CM004":
            cm_string = (
                "CM004" + 
                "-CSH" + str(1 * cm_dict["cld_shad"]) + 
                "-CMP" + str(1 * cm_dict["cld_med_prob"]) + 
                "-CHP" + str(1 * cm_dict["cld_hi_prob"]) + 
                "-TCI" + str(1 * cm_dict["thin_cir"]) + 
                "-ITER" + str(cm_dict["iterations"])
            )
        else:
            cm_string = None
    else:
        cm_string = None
    return cm_string
    
def get_indice_from_identifier(identifier) -> str:
    indice = (identifier.replace(".", "_").split("_")[7]).upper()
    return indice
    
def get_cloudmask_from_identifier(identifier) -> str:
    indice = (identifier.replace(".", "_").split("_")[7]).upper()
    return indice

# def get_cloudmask_indice_dict_from_strings(
    # cm_string: str = None,
    # indice_string: str = None,
# ) -> dict:
    # """Returns a cloudmas or indice dict for FamilyProduct class
    # :param string: string from which to extract the version name.
    # can be a cloudmask or an indice identifier
    # """
    # returned_val = None

