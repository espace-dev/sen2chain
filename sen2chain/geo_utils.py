# coding: utf-8

"""
Module for manipulating the Sentinel-2 tiles index geopackage file.
"""

import logging
import pickle
import fiona
from osgeo import ogr
from shapely.geometry import shape, Point, box
import datetime
import shutil

# type annotation
from typing import List, Set, Dict, Optional, Union
from pathlib import Path
import geopandas as gpd
from rasterio.mask import mask
import rasterio
from shapely.geometry import mapping
from os import scandir


from .config import SHARED_DATA, Config
from .library import Library
from .tiles import Tile
from .utils import str_to_datetime, datetime_to_str, get_cm_string_from_dict


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

TILES_INDEX = SHARED_DATA.get("tiles_index")
TILES_INDEX_DICT = SHARED_DATA.get("tiles_index_dict")


def serialise_tiles_index() -> None:
    """Saves sentinel_2_index.gpkg as a pickle file if it's missing."""
    tiles_dict = dict()

    with fiona.open(str(TILES_INDEX), "r") as tiles_index:
        for tile in tiles_index:
            name = tile["properties"]["Name"]
            geom = shape(tile["geometry"])
            tiles_dict[name] = geom

    with open(str(TILES_INDEX_DICT), "wb") as pfile:
        pickle.dump(tiles_dict, pfile)


def get_tiles_from_point(
    lon: float, lat: float, land_only: bool = False
) -> Set[str]:
    """
    Returns tiles containing a point (longitude, latitude)

    :param lon: longitude.
    :param lat: latitude.
    :param land_only: keep only tiles that contain lands.
    """
    intersected_tiles = set()

    with fiona.open(str(TILES_INDEX), "r") as tiles_index:
        point_geom = Point(lon, lat)
        if land_only:
            tiles_index = filter(
                lambda feat: feat["properties"]["land"] == "True", tiles_index
            )

        for tile in tiles_index:
            tile_geom = shape(tile["geometry"])
            if tile_geom.contains(point_geom):
                intersected_tiles.add(tile["properties"]["Name"])
    return intersected_tiles


def get_tiles_from_bbox(
    lon_min: float,
    lat_min: float,
    lon_max: float,
    lat_max: float,
    land_only=False,
) -> Set[str]:
    """
    Returns S2 tiles intersecting a bbox.

    :param lon_min: longitude.
    :param lat_min: latitude.
    :param lon_max: longitude.
    :param lat_max: latitude.
    :param land_only: keep only tiles that contain lands.
    """
    intersected_tiles = set()

    with fiona.open(str(TILES_INDEX), "r") as tiles_index:
        bbox_geom = box(lon_min, lat_min, lon_max, lat_max)
        if land_only:
            tiles_index = filter(
                lambda feat: feat["properties"]["land"] == "True", tiles_index
            )

        for tile in tiles_index:
            tile_geom = shape(tile["geometry"])
            if tile_geom.intersects(bbox_geom):
                intersected_tiles.add(tile["properties"]["Name"])

    return intersected_tiles


def get_tiles_from_file(
    vectors_file: str, land_only: bool = False
) -> Dict[str, Optional[List[str]]]:
    """
    Returns the intersected tiles for each feature id in the vectors file.

    :param vectors_file: path to a file containing vectors geometries.
    :param land_only: return only tiles that contain lands.
    """
    # OGR is used for better performances
    drv_gpkg = ogr.GetDriverByName("GPKG")
    tiles_index_ds = drv_gpkg.Open(str(TILES_INDEX), 0)
    tiles_index_lyr = tiles_index_ds.GetLayer(0)
    vectors_file_ds = ogr.Open(str(vectors_file), 0)
    vectors_file_lyr = vectors_file_ds.GetLayer(0)

    # filter on tiles with land
    if land_only:
        tiles_index_lyr.SetAttributeFilter("land = 'True'")

    # this dict will contain the geometry id as key and the list of
    # tiles it intersects as value
    intersected_tiles_dict = dict()  # type: Dict[str, Optional[List[str]]]
    feat_id = 0
    for feat in vectors_file_lyr:
        feat_geom = feat.GetGeometryRef()
        tiles_index_lyr.SetSpatialFilter(feat_geom)

        contained_in_tiles = []
        for filtered_t in tiles_index_lyr:
            tile = filtered_t.GetField("Name")
            contained_in_tiles.append(tile)

        intersected_tiles_dict[str(feat_id)] = (
            contained_in_tiles if contained_in_tiles else None
        )
        feat_id += 1

    return intersected_tiles_dict


def fill_fields(
    layer, 
    field_names, 
    tile_counts, 
    cm_string,
):
    _l1c_path = Path(Config().get("l1c_path"))
    _l2a_path = Path(Config().get("l2a_path"))
    _indices_path = Path(Config().get("indices_path"))

    for feat in layer:
        tile_name = feat.GetField("Name")
        logger.info("Filling values for tile {}".format(tile_name))
        
        last_date_all_indices = datetime.datetime(2015, 1, 1)
        
        ## L1C
        feat.SetField("L1C", tile_counts[tile_name]["l1c"])
        if tile_counts[tile_name]["l1c"]:
            l1c_list = sorted(
                [k.name for k in _l1c_path.glob(tile_name + "/*.SAFE")],
                key=lambda x:x[11:26]
            )
            feat.SetField(
                "L1C_F",
                str_to_datetime(
                    l1c_list[0][11:19],
                    "Ymd",
                ).strftime("%d/%m/%Y"),
            )
            feat.SetField(
                "L1C_L", 
                str_to_datetime(l1c_list[-1][11:19], "Ymd").strftime("%d/%m/%Y"),
            )
        else:
            feat.SetField("L1C_F", 0)
            feat.SetField("L1C_L", 0)

        ## L2A
        feat.SetField("L2A", tile_counts[tile_name]["l2a"])
        if tile_counts[tile_name]["l2a"]:
            l2a_list = sorted(
                [k.name for k in _l2a_path.glob(tile_name + "/*.SAFE")],
                key=lambda x:x[11:26]
            )
            feat.SetField(
                "L2A_F",
                str_to_datetime(
                    l2a_list[0][11:19],
                    "Ymd",
                ).strftime("%d/%m/%Y"),
            )
            feat.SetField(
                "L2A_L", 
                str_to_datetime(l2a_list[-1][11:19], "Ymd").strftime("%d/%m/%Y"),
            )
        else:
            feat.SetField("L2A_F", 0)
            feat.SetField("L2A_L", 0)

        ## Indices
        for field in field_names[7:]:
            if tile_counts[tile_name][field.split("_")[0]]:
                pattern = ("{}/{}/S2*_MSI*_N*_R*_T*/*" + cm_string + ".jp2").format(
                    field.split("_")[0], tile_name
                )
                indice_list = sorted(
                    [k.name for k in _indices_path.glob(pattern)],
                    key=lambda x:x[11:26]
                )
                if "_F" in field:
                    feat.SetField(
                        field,
                        str_to_datetime(
                            indice_list[0][11:19],
                            "Ymd",
                        ).strftime("%d/%m/%Y"),
                    )
                elif "_L" in field:
                    last_date = str_to_datetime(
                        indice_list[-1][11:19],
                        "Ymd",
                    )
                    feat.SetField(
                        field,
                        last_date.strftime("%d/%m/%Y"),
                    )
                    last_date_all_indices = max(last_date, last_date_all_indices)
                else:
                    feat.SetField(field, tile_counts[tile_name][field])
            else:
                feat.SetField(field, 0)  
        
        ## colors
        colors = ["grey", "green", "purple"]
        indice_count = 0
        for field in field_names[7:]:
            indice_count += tile_counts[tile_name][field.split("_")[0]]
        if indice_count:
            if datetime.datetime.now() - last_date_all_indices >= datetime.timedelta(6):
                feat.SetField("color", colors[1])
            else:
                feat.SetField("color", colors[2])
        else:
            feat.SetField("color", colors[0])
        
        layer.SetFeature(feat)

def full_db_2_shp(
    out_folder: str = None,
    cm_version: str = "CM001",
    probability: int = 1,
    iterations: int = 5,
    cld_shad: bool = True,
    cld_med_prob: bool = True,
    cld_hi_prob: bool = True,
    thin_cir: bool = True,
    zipfile: bool = False
):
    """
    Returns a vector file of the indice processed tiles

    :param out_shapefile: path to a output file vector.
    """
    if not out_folder:
        out_folder = Path(Config().get("temp_path"))

    if zipfile:
        out_folder = out_folder / "db_total_shp"
        out_folder.mkdir(exist_ok=True)
        
    cm_dict = {
        "cm_version": cm_version,
        "probability": probability,
        "iterations": iterations,
        "cld_shad": cld_shad,
        "cld_med_prob": cld_med_prob,
        "cld_hi_prob": cld_hi_prob,
        "thin_cir": thin_cir,
    }
    cm_dict["probability"] = int(cm_dict["probability"])
    cm_dict["iterations"] = int(cm_dict["iterations"])
    cm_dict["cld_shad"] = cm_dict["cld_shad"] in ["1", 1, True, "True"] 
    cm_dict["cld_med_prob"] = cm_dict["cld_med_prob"] in ["1", 1, True, "True"]
    cm_dict["cld_hi_prob"] = cm_dict["cld_hi_prob"] in ["1", 1, True, "True"]
    cm_dict["thin_cir"] = cm_dict["thin_cir"] in ["1", 1, True, "True"]
    cm_string = get_cm_string_from_dict(cm_dict)
        
    out_shapefile_total = str(Path(out_folder) / "db_total_shp.shp")
    drv_gpkg = ogr.GetDriverByName("GPKG")
    input_layer_ds = drv_gpkg.Open(str(TILES_INDEX), 0)
    input_layer_lyr = input_layer_ds.GetLayer(0)
    lib = Library()
    total_index = lib.tiles_l1c + lib.tiles_l2a
    for i in lib.indices:
        total_index.extend(getattr(lib, "tiles_" + i.lower()))
    total_index = list(set(total_index))
    tile_count = {}
    for key in total_index:
        logger.info(
            "Searching Tile {} {}/{}".format(
                key,
                total_index.index(key)+1,
                len(total_index), 
            )
        )    
        n_l1c = len([k.name for k in lib._l1c_path.glob(key + "/*.SAFE")])
        logger.info("L1C products: {}".format(n_l1c))
        n_l2a = len([k.name for k in lib._l2a_path.glob(key + "/*.SAFE")])
        logger.info("L2A products: {}".format(n_l2a))
        
        # test os.scandir
        # n_l1c = len([i for i in scandir(lib._l1c_path / key) if ".SAFE" in i.name])
        # n_l2a = len([i for i in scandir(lib._l2a_path / key) if ".SAFE" in i.name])
        
        tile_count[key] = {"l1c": n_l1c, "l2a": n_l2a}
        for i in lib.indices:
            pattern = ("{}/{}/S2*_MSI*_N*_R*_T*/*" + cm_string + ".jp2").format(i, key)
            tile_count[key][i] = len(
                [k.name for k in lib._indices_path.glob(pattern)]
            )
            logger.info("{} products: {}".format(i, tile_count[key][i]))            

        
        
        
    
    
    
    # Couche complète
    query_str = "or ".join(
        ['"{}" = "{}"'.format("Name", idx) for idx in tile_count.keys()]
    )
    input_layer_lyr.SetAttributeFilter(query_str)
    driver = ogr.GetDriverByName("ESRI Shapefile")
    out_ds = driver.CreateDataSource(out_shapefile_total)
    out_layer = out_ds.CopyLayer(input_layer_lyr, "tuiles")
    field_names = ["color"] + [
        i+j for i in ["L1C", "L2A"] + lib.indices for j in ["", "_F", "_L"]
    ]
    for name in field_names:
        field_name = ogr.FieldDefn(name, ogr.OFTString)
        field_name.SetWidth(10)
        out_layer.CreateField(field_name)
    fill_fields(out_layer, field_names, tile_count, cm_string)
    out_layer = None
    del input_layer_ds, input_layer_lyr, out_layer, out_ds

    if zipfile:
        shutil.make_archive(str(out_folder), 'zip', str(out_folder))

def crop_product_by_shp(
    raster_path: Union[str, Path] = None,
    vector_path: Union[str, Path] = None,
    buff: int = 0,
    out_folder: str = None,
):
    """
    Returns a cropped raster
    :param raster_path: path to input raster file
    :param vector_path: path to input shp
    :param buff: buffer size around shp (in raster unit, ie m)
    :param out_folder: path to a output foler.
    """
    if not out_folder:
        out_folder = Path(Config().get("temp_path"))

    crop_extent = gpd.read_file(str(vector_path))
    raster = rasterio.open(str(raster_path))
    crop_extent_new_proj = crop_extent.to_crs(raster.crs)
    extent_geojson = mapping(crop_extent_new_proj["geometry"][0].buffer(buff))

    out_img, out_transform = mask(
        dataset=raster, shapes=[extent_geojson], crop=True
    )

    out_meta = raster.meta.copy()
    out_meta.update(
        {
            "driver": "GTiff",
            "compress": "DEFLATE",
            "height": out_img.shape[1],
            "width": out_img.shape[2],
            "transform": out_transform,
        }
    )

    with rasterio.open(
        out_folder
        / (Path(raster_path).stem + "_cropped-" + str(buff) + "m.tif"),
        "w",
        **out_meta
    ) as dest:
        dest.write(out_img)
