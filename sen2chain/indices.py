# coding: utf-8

"""
Module for defining radiometric indices.
"""

import logging
import pathlib
from abc import ABCMeta, abstractmethod
from matplotlib import cm
import os

# type annotations
from typing import Union, List

from .indices_functions import (
    create_raw_ndr,
    create_raw_ireci,
    # create_raw_ndvi,
    # create_raw_ndre,
    # create_raw_ndwigao,
    # create_raw_ndwimcf,
    # create_raw_mndwi,
    create_raw_bigr,
    create_raw_birnir,
    create_raw_bibg,
    create_raw_evi,
    create_masked_indice,
    index_tiff_2_jp2,
)
from .colormap import matplotlib_colormap_to_rgb, create_colormap, create_rvb

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class Indice(metaclass=ABCMeta):
    """Indice abstract base class. Every indice class must implement these
    class attributes and object methods.

    * ``_name``: Name of the indice. Must be unique and without spaces.
    * ``_filename_template``: Template of the indice filename output. Will be
        filled with the product's identifier and the file extension
        e.g.: *{product_identifier}_INDICE_NAME{ext}*
    * ``_ext``: Extension of the indice filename. (eg: ".tif", ".jp2")

    process_indice(out_path, nodata_clouds, quicklook):
    """

    @property
    @abstractmethod
    def name(self):
        pass

    @property
    @abstractmethod
    def filename_template(self):
        pass

    @property
    @abstractmethod
    def ext(self):
        pass

    @abstractmethod
    def process_indice(self, out_path, reprocess, nodata_clouds, quicklook):
        pass


class Ndvi(Indice):
    """
    $$NDVI = {{NIR-VIR}\over{NIR+VIR}}$$

    NIR: band 08 (10m)
    VIR: band 04 (10m)
    """

    name = "NDVI"
    filename_template = "{product_identifier}_NDVI{ext}"
    ext = ".jp2"
    ext_raw = ".tif"
    ext_raw_aux = ".tif.aux.xml"
    colormap = cm.RdYlGn

    def __init__(self, l2a_product_object, cm_product_object):
        if (l2a_product_object or cm_product_object) is None:
            raise ValueError(
                "A L2aProduct and NewCloudMask objects must be provided"
            )
        else:
            self.l2a_product = l2a_product_object
            self.cm_product = cm_product_object

        self.out_path = None

        self.indice_stem = self.filename_template.format(
            product_identifier=self.l2a_product.identifier, ext=""
        )
        self.indice_filename = self.indice_stem + self.ext
        self.indice_raw = self.indice_stem + self.ext_raw

    def process_indice(
        self,
        out_path: pathlib.PosixPath,
        reprocess: bool = False,
        nodata_clouds: bool = False,
        quicklook: bool = False,
        baseline: str = "4.0",

    ) -> None:
        """
        Process NDVI
        :param out_path: Path to computed indice
        :type out_path: Path
        :param reprocess: When True, delete existing indice and process new one
        :type reprocess: bool, Default to False
        :param nodata_clouds: When True, mask indice with cloud mask.
        :type nodata_clouds: bool, Default to False
        :param quicklook: When True, process indice quicklook
        :type quicklook: bool, Default to True
        :param baseline: L2A Product baseline. Needed to compute the right indice formula.
        :type baseline: str
        """
        self.out_path = out_path
        if (out_path / self.indice_filename).exists() and not reprocess:
            logger.info("{} already exists".format(self.indice_filename))
        else:
            create_raw_ndr(
                b1_path=self.l2a_product.b08_10m,
                b2_path=self.l2a_product.b04_10m,
                out_path=(out_path / self.indice_raw),
                baseline=baseline,
            )
            index_tiff_2_jp2(
                img_path=(out_path / self.indice_raw),
                out_path=(out_path / self.indice_filename),
                quality=20,
            )

        if nodata_clouds:
            if not self.cm_product.path.exists():
                logger.info("Cloudmask does not exist, indice not masked")
                raise ValueError("Cloud mask does not exist")
            masked_indice_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext
            )
            masked_indice_raw = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw
            )
            masked_indice_raw_aux = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw_aux
            )

            if (out_path / masked_indice_filename).exists() and not reprocess:
                logger.info("{} already exists".format(masked_indice_filename))
            else:
                if (out_path / self.indice_raw).exists():
                    ndvi_name = out_path / self.indice_raw
                else:
                    ndvi_name = out_path / self.indice_filename
                create_masked_indice(
                    indice_path=ndvi_name,
                    cloud_mask_path=self.cm_product.path,
                    out_path=(out_path / masked_indice_raw),
                )
                index_tiff_2_jp2(
                    img_path=(out_path / masked_indice_raw),
                    out_path=(out_path / masked_indice_filename),
                    quality=20,
                )
                os.remove(str(out_path / masked_indice_raw))
                os.remove(str(out_path / masked_indice_raw_aux))
        try:
            os.remove(str(out_path / self.indice_raw))
            os.remove(str(out_path / (self.indice_stem + self.ext_raw_aux)))
            logger.info("Removing {}".format(self.indice_raw))
        except Exception:
            pass

        if quicklook:
            cmap = matplotlib_colormap_to_rgb(self.colormap, revers=False)

            quicklook_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + "_QL.tif"
            )

            if (self.out_path / quicklook_filename).exists() and not reprocess:
                logger.info("{} already exists".format(quicklook_filename))
            else:
                logger.info("creating quicklook")
                create_rvb(
                    raster=(self.out_path / self.indice_filename),
                    cloud_mask=self.cm_product.path,
                    lut_dict=cmap,
                    clouds_color="white",
                    out_path=(self.out_path / quicklook_filename),
                )


class NdwiMcf(Indice):
    """
    $$NDWI(McFeeters) = {{GREEN-NIR}\over{GREEN+NIR}}$$

    GREEN: band 03
    NIR: band 08
    """

    name = "NDWIMCF"
    filename_template = "{product_identifier}_NDWIMCF{ext}"
    ext = ".jp2"
    ext_raw = ".tif"
    ext_raw_aux = ".tif.aux.xml"
    colormap = cm.colors.LinearSegmentedColormap.from_list(
        "", ["green", "white", "blue"]
    )

    def __init__(self, l2a_product_object, cm_product_object):
        if (l2a_product_object or cm_product_object) is None:
            raise ValueError(
                "A L2aProduct and NewCloudMask objects must be provided"
            )
        else:
            self.l2a_product = l2a_product_object
            self.cm_product = cm_product_object

        self.out_path = None

        self.indice_stem = self.filename_template.format(
            product_identifier=self.l2a_product.identifier, ext=""
        )
        self.indice_filename = self.indice_stem + self.ext
        self.indice_raw = self.indice_stem + self.ext_raw

    def process_indice(
        self,
        out_path: pathlib.PosixPath,
        reprocess: bool = False,
        nodata_clouds: bool = False,
        quicklook: bool = False,
        baseline: str = "4.0",
    ) -> None:
        """
        Process NDWIMCF

        :param out_path: Path to computed indice
        :type out_path: Path
        :param reprocess: When True, delete existing indice and process new one
        :type reprocess: bool, Default to False
        :param nodata_clouds: When True, mask indice with cloud mask.
        :type nodata_clouds: bool, Default to False
        :param quicklook: When True, process indice quicklook
        :type quicklook: bool, Default to True
        :param baseline: L2A Product baseline. Needed to compute the right indice formula.
        :type baseline: str
        """
        self.out_path = out_path
        if (out_path / self.indice_filename).exists() and not reprocess:
            logger.info("{} already exists".format(self.indice_filename))
        else:
            create_raw_ndr(
				baseline=baseline,
                b1_path=self.l2a_product.b03_10m,
                b2_path=self.l2a_product.b08_10m,
                out_path=(out_path / self.indice_raw),
            )
            index_tiff_2_jp2(
                img_path=(out_path / self.indice_raw),
                out_path=(out_path / self.indice_filename),
                quality=20,
            )

        if nodata_clouds:
            if not self.cm_product.path.exists():
                logger.info("Cloudmask does not exist, indice not masked")
                raise ValueError("Cloud mask does not exist")
            masked_indice_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext
            )
            masked_indice_raw = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw
            )
            masked_indice_raw_aux = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw_aux
            )

            if (out_path / masked_indice_filename).exists() and not reprocess:
                logger.info("{} already exists".format(masked_indice_filename))
            else:
                if (out_path / self.indice_raw).exists():
                    ndwimcf_path = out_path / self.indice_raw
                else:
                    ndwimcf_path = out_path / self.indice_filename
                create_masked_indice(
                    indice_path=ndwimcf_path,
                    cloud_mask_path=self.cm_product.path,
                    out_path=(out_path / masked_indice_raw),
                )
                index_tiff_2_jp2(
                    img_path=(out_path / masked_indice_raw),
                    out_path=(out_path / masked_indice_filename),
                    quality=20,
                )
                os.remove(str(out_path / masked_indice_raw))
                os.remove(str(out_path / masked_indice_raw_aux))
        try:
            os.remove(str(out_path / self.indice_raw))
            os.remove(str(out_path / self.indice_stem + self.ext_raw_aux))

            logger.info("Removing {}".format(self.indice_raw))
        except Exception:
            pass

        if quicklook:
            cmap = matplotlib_colormap_to_rgb(self.colormap, revers=False)

            quicklook_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + "_QL.tif"
            )

            if (self.out_path / quicklook_filename).exists() and not reprocess:
                logger.info("{} already exists".format(quicklook_filename))
            else:
                logger.info("creating quicklook")
                create_rvb(
                    raster=(self.out_path / self.indice_filename),
                    cloud_mask=self.cm_product.path,
                    lut_dict=cmap,
                    clouds_color="white",
                    out_path=(self.out_path / quicklook_filename),
                )


class NdwiGao(Indice):
    """
    $$NDWI(Gao) = {{NIR-SWIR}\over{NIR+SWIR}}$$

    NIR: band 08
    SWIR: band 11

    Also called NDMI
    """

    name = "NDWIGAO"
    filename_template = "{product_identifier}_NDWIGAO{ext}"
    ext = ".jp2"
    ext_raw = ".tif"
    ext_raw_aux = ".tif.aux.xml"
    colormap = cm.RdYlBu

    def __init__(self, l2a_product_object, cm_product_object):
        if (l2a_product_object or cm_product_object) is None:
            raise ValueError(
                "A L2aProduct and NewCloudMask objects must be provided"
            )
        else:
            self.l2a_product = l2a_product_object
            self.cm_product = cm_product_object

        self.out_path = None

        self.indice_stem = self.filename_template.format(
            product_identifier=self.l2a_product.identifier, ext=""
        )
        self.indice_filename = self.indice_stem + self.ext
        self.indice_raw = self.indice_stem + self.ext_raw

    def process_indice(
        self,
        out_path: pathlib.PosixPath,
        reprocess: bool = False,
        nodata_clouds: bool = False,
        quicklook: bool = False,
        baseline: str = "4.0",
    ) -> None:
        """
        Process NDWIGAO

        :param out_path: Path to computed indice
        :type out_path: Path
        :param reprocess: When True, delete existing indice and process new one
        :type reprocess: bool, Default to False
        :param nodata_clouds: When True, mask indice with cloud mask.
        :type nodata_clouds: bool, Default to False
        :param quicklook: When True, process indice quicklook
        :type quicklook: bool, Default to True
        :param baseline: L2A Product baseline. Needed to compute the right indice formula.
        :type baseline: str
        """
        self.out_path = out_path

        if (out_path / self.indice_filename).exists() and not reprocess:
            logger.info("{} already exists".format(self.indice_filename))
        else:
            create_raw_ndr(
				baseline=baseline,
                b1_path=self.l2a_product.b08_10m,
                b2_path=self.l2a_product.b11_20m,
                out_path=(out_path / self.indice_raw),
            )
            index_tiff_2_jp2(
                img_path=(out_path / self.indice_raw),
                out_path=(out_path / self.indice_filename),
                quality=20,
            )

        if nodata_clouds:
            if not self.cm_product.path.exists():
                logger.info("Cloudmask does not exist, indice not masked")
                raise ValueError("Cloud mask does not exist")
            masked_indice_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext
            )
            masked_indice_raw = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw
            )
            masked_indice_raw_aux = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw_aux
            )

            if (out_path / masked_indice_filename).exists() and not reprocess:
                logger.info("{} already exists".format(masked_indice_filename))
            else:
                if (out_path / self.indice_raw).exists():
                    ndwigao_name = out_path / self.indice_raw
                else:
                    ndwigao_name = out_path / self.indice_filename
                create_masked_indice(
                    indice_path=ndwigao_name,
                    cloud_mask_path=self.cm_product.path,
                    out_path=(out_path / masked_indice_raw),
                )
                index_tiff_2_jp2(
                    img_path=(out_path / masked_indice_raw),
                    out_path=(out_path / masked_indice_filename),
                    quality=20,
                )
                os.remove(str(out_path / masked_indice_raw))
                os.remove(str(out_path / masked_indice_raw_aux))
        try:
            os.remove(str(out_path / self.indice_raw))
            os.remove(str(out_path / self.indice_stem + self.ext_raw_aux))
            logger.info("Removing {}".format(self.indice_raw))
        except Exception:
            pass

        if quicklook:
            cmap = matplotlib_colormap_to_rgb(self.colormap, revers=False)

            quicklook_filename = self.indice_stem + "_QUICKLOOK.tif"
            if (self.out_path / quicklook_filename).exists() and not reprocess:
                logger.info("{} already exists".format(quicklook_filename))
            else:
                logger.info("creating quicklook")
                create_rvb(
                    raster=(self.out_path / self.indice_filename),
                    cloud_mask=self.cm_product.path,
                    lut_dict=cmap,
                    clouds_color="white",
                    out_path=(self.out_path / quicklook_filename),
                )


class Mndwi(Indice):
    """
    $$MNDWI = {{GREEN-SWIR}\over{GREEN+SWIR}}$$

    GREEN: band 03
    SWIR: band 11
    """

    name = "MNDWI"
    filename_template = "{product_identifier}_MNDWI{ext}"
    ext = ".jp2"
    ext_raw = ".tif"
    ext_raw_aux = ".tif.aux.xml"
    colormap = cm.BrBG

    def __init__(self, l2a_product_object, cm_product_object):
        if (l2a_product_object or cm_product_object) is None:
            raise ValueError(
                "A L2aProduct and NewCloudMask objects must be provided"
            )
        else:
            self.l2a_product = l2a_product_object
            self.cm_product = cm_product_object

        self.out_path = None

        self.indice_stem = self.filename_template.format(
            product_identifier=self.l2a_product.identifier, ext=""
        )
        self.indice_filename = self.indice_stem + self.ext
        self.indice_raw = self.indice_stem + self.ext_raw

    def process_indice(
        self,
        out_path: pathlib.PosixPath,
        reprocess: bool = False,
        nodata_clouds: bool = False,
        quicklook: bool = False,
        baseline: str = "4.0",
    ) -> None:
        """
        Process MNDWI

        :param out_path: Path to computed indice
        :type out_path: Path
        :param reprocess: When True, delete existing indice and process new one
        :type reprocess: bool, Default to False
        :param nodata_clouds: When True, mask indice with cloud mask.
        :type nodata_clouds: bool, Default to False
        :param quicklook: When True, process indice quicklook
        :type quicklook: bool, Default to True
        :param baseline: L2A Product baseline. Needed to compute the right indice formula.
        :type baseline: str
        """
        self.out_path = out_path

        if (out_path / self.indice_filename).exists() and not reprocess:
            logger.info("{} already exists".format(self.indice_filename))
        else:
            create_raw_ndr(
				baseline=baseline,
                b1_path=self.l2a_product.b03_10m,
                b2_path=self.l2a_product.b11_20m,
                out_path=(out_path / self.indice_raw),
            )
            index_tiff_2_jp2(
                img_path=(out_path / self.indice_raw),
                out_path=(out_path / self.indice_filename),
                quality=20,
            )

        if nodata_clouds:
            if not self.cm_product.path.exists():
                logger.info("Cloudmask does not exist, indice not masked")
                raise ValueError("Cloud mask does not exist")
            masked_indice_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext
            )
            masked_indice_raw = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw
            )
            masked_indice_raw_aux = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw_aux
            )

            if (out_path / masked_indice_filename).exists() and not reprocess:
                logger.info("{} already exists".format(masked_indice_filename))
            else:
                if (out_path / self.indice_raw).exists():
                    mndwi_name = out_path / self.indice_raw
                else:
                    mndwi_name = out_path / self.indice_filename
                create_masked_indice(
                    indice_path=mndwi_name,
                    cloud_mask_path=self.cm_product.path,
                    out_path=(out_path / masked_indice_raw),
                )
                index_tiff_2_jp2(
                    img_path=(out_path / masked_indice_raw),
                    out_path=(out_path / masked_indice_filename),
                    quality=20,
                )
                os.remove(str(out_path / masked_indice_raw))
                os.remove(str(out_path / masked_indice_raw_aux))

        try:
            os.remove(str(out_path / self.indice_raw))
            os.remove(str(out_path / self.indice_stem + self.ext_raw_aux))
            logger.info("Removing {}".format(self.indice_raw))
        except Exception:
            pass

        if quicklook:
            cmap = matplotlib_colormap_to_rgb(self.colormap, revers=False)

            quicklook_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + "_QL.tif"
            )
            if (self.out_path / quicklook_filename).exists() and not reprocess:
                logger.info("{} already exists".format(quicklook_filename))
            else:
                logger.info("creating quicklook")
                create_rvb(
                    raster=(self.out_path / self.indice_filename),
                    cloud_mask=self.cm_product.path,
                    lut_dict=cmap,
                    clouds_color="white",
                    out_path=(self.out_path / quicklook_filename),
                )


class Ndre(Indice):
    """
    $$NDRE = {{NIR-RedEdge}\over{NIR+RedEdge}}$$

    NIR: band 08
    RedEdge: band 05
    """

    name = "NDRE"
    filename_template = "{product_identifier}_NDRE{ext}"
    ext = ".jp2"
    ext_raw = ".tif"
    ext_raw_aux = ".tif.aux.xml"
    colormap = cm.Spectral

    def __init__(self, l2a_product_object, cm_product_object):
        if (l2a_product_object or cm_product_object) is None:
            raise ValueError(
                "A L2aProduct and NewCloudMask objects must be provided"
            )
        else:
            self.l2a_product = l2a_product_object
            self.cm_product = cm_product_object

        self.out_path = None

        self.indice_stem = self.filename_template.format(
            product_identifier=self.l2a_product.identifier, ext=""
        )
        self.indice_filename = self.indice_stem + self.ext
        self.indice_raw = self.indice_stem + self.ext_raw

    def process_indice(
        self,
        out_path: pathlib.PosixPath,
        reprocess: bool = False,
        nodata_clouds: bool = False,
        quicklook: bool = False,
        baseline: str = "4.0",
    ) -> None:
        """
        Process NDRE

        :param out_path: Path to computed indice
        :type out_path: Path
        :param reprocess: When True, delete existing indice and process new one
        :type reprocess: bool, Default to False
        :param nodata_clouds: When True, mask indice with cloud mask.
        :type nodata_clouds: bool, Default to False
        :param quicklook: When True, process indice quicklook
        :type quicklook: bool, Default to True
        :param baseline: L2A Product baseline. Needed to compute the right indice formula.
        :type baseline: str
        """
        self.out_path = out_path

        if (out_path / self.indice_filename).exists() and not reprocess:
            logger.info("{} already exists".format(self.indice_filename))
        else:
            create_raw_ndr(
				baseline=baseline,
                b1_path=self.l2a_product.b08_10m,
                b2_path=self.l2a_product.b05_20m,
                out_path=(out_path / self.indice_raw),
            )
            index_tiff_2_jp2(
                img_path=(out_path / self.indice_raw),
                out_path=(out_path / self.indice_filename),
                quality=30,
            )

        if nodata_clouds:
            if not self.cm_product.path.exists():
                logger.info("Cloudmask does not exist, indice not masked")
                raise ValueError("Cloud mask does not exist")
            masked_indice_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext
            )
            masked_indice_raw = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw
            )
            masked_indice_raw_aux = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw_aux
            )

            if (out_path / masked_indice_filename).exists() and not reprocess:
                logger.info("{} already exists".format(masked_indice_filename))
            else:
                if (out_path / self.indice_raw).exists():
                    ndre_name = out_path / self.indice_raw
                else:
                    ndre_name = out_path / self.indice_filename
                create_masked_indice(
                    indice_path=ndre_name,
                    cloud_mask_path=self.cm_product.path,
                    out_path=(out_path / masked_indice_raw),
                )
                index_tiff_2_jp2(
                    img_path=(out_path / masked_indice_raw),
                    out_path=(out_path / masked_indice_filename),
                    quality=30,
                )
                os.remove(str(out_path / masked_indice_raw))
                os.remove(str(out_path / masked_indice_raw_aux))

        try:
            os.remove(str(out_path / self.indice_raw))
            os.remove(str(out_path / self.indice_stem + self.ext_raw_aux))
            logger.info("Removing {}".format(self.indice_raw))
        except Exception:
            pass
        if quicklook:
            cmap = matplotlib_colormap_to_rgb(self.colormap, revers=False)
            quicklook_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + "_QL.tif"
            )
            if (self.out_path / quicklook_filename).exists() and not reprocess:
                logger.info("{} already exists".format(quicklook_filename))
            else:
                logger.info("creating quicklook")
                create_rvb(
                    raster=(self.out_path / self.indice_filename),
                    cloud_mask=self.cm_product.path,
                    lut_dict=cmap,
                    clouds_color="white",
                    out_path=(self.out_path / quicklook_filename),
                )


class IRECI(Indice):
    """
    $$IRECI = {{NIR-R}\over{RE1/RE2}}$$

    NIR: band 783nm (B7 - 20m)
    R: band 665nm (B4 - 10m)
    RE1: band 705nm (B5 - 20m)
    RE2: band 740nm (B6 - 20m)
    """

    name = "IRECI"
    filename_template = "{product_identifier}_IRECI{ext}"
    ext = ".jp2"
    ext_raw = ".tif"
    ext_raw_aux = ".tif.aux.xml"
    colormap = cm.Spectral

    def __init__(self, l2a_product_object, cm_product_object):
        if (l2a_product_object or cm_product_object) is None:
            raise ValueError(
                "A L2aProduct and NewCloudMask objects must be provided"
            )
        else:
            self.l2a_product = l2a_product_object
            self.cm_product = cm_product_object

        self.out_path = None

        self.indice_stem = self.filename_template.format(
            product_identifier=self.l2a_product.identifier, ext=""
        )
        self.indice_filename = self.indice_stem + self.ext
        self.indice_raw = self.indice_stem + self.ext_raw

    def process_indice(
        self,
        out_path: pathlib.PosixPath,
        reprocess: bool = False,
        nodata_clouds: bool = False,
        quicklook: bool = False,
        baseline: str = "4.0",
    ) -> None:
        """
        Process IRECI

        :param out_path: Path to computed indice
        :type out_path: Path
        :param reprocess: When True, delete existing indice and process new one
        :type reprocess: bool, Default to False
        :param nodata_clouds: When True, mask indice with cloud mask.
        :type nodata_clouds: bool, Default to False
        :param quicklook: When True, process indice quicklook
        :type quicklook: bool, Default to True
        :param baseline: L2A Product baseline. Needed to compute the right indice formula.
        :type baseline: str
        """
        self.out_path = out_path

        if (out_path / self.indice_filename).exists() and not reprocess:
            logger.info("{} already exists".format(self.indice_filename))
        else:
            create_raw_ireci(
				baseline=baseline,
                b1_path=self.l2a_product.b07_20m,
                b2_path=self.l2a_product.b04_10m,
                b3_path=self.l2a_product.b05_20m,
                b4_path=self.l2a_product.b06_20m,
                out_path=(out_path / self.indice_raw),
            )
            index_tiff_2_jp2(
                img_path=(out_path / self.indice_raw),
                out_path=(out_path / self.indice_filename),
                quality=30,
            )

        if nodata_clouds:
            if not self.cm_product.path.exists():
                logger.info("Cloudmask does not exist, indice not masked")
                raise ValueError("Cloud mask does not exist")
            masked_indice_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext
            )
            masked_indice_raw = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw
            )
            masked_indice_raw_aux = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw_aux
            )

            if (out_path / masked_indice_filename).exists() and not reprocess:
                logger.info("{} already exists".format(masked_indice_filename))
            else:
                if (out_path / self.indice_raw).exists():
                    ndre_name = out_path / self.indice_raw
                else:
                    ndre_name = out_path / self.indice_filename
                create_masked_indice(
                    indice_path=ndre_name,
                    cloud_mask_path=self.cm_product.path,
                    out_path=(out_path / masked_indice_raw),
                )
                index_tiff_2_jp2(
                    img_path=(out_path / masked_indice_raw),
                    out_path=(out_path / masked_indice_filename),
                    quality=30,
                )
                os.remove(str(out_path / masked_indice_raw))
                os.remove(str(out_path / masked_indice_raw_aux))

        try:
            os.remove(str(out_path / self.indice_raw))
            os.remove(str(out_path / self.indice_stem + self.ext_raw_aux))
            logger.info("Removing {}".format(self.indice_raw))
        except Exception:
            pass
        if quicklook:
            cmap = matplotlib_colormap_to_rgb(self.colormap, revers=False)
            quicklook_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + "_QL.tif"
            )
            if (self.out_path / quicklook_filename).exists() and not reprocess:
                logger.info("{} already exists".format(quicklook_filename))
            else:
                logger.info("creating quicklook")
                create_rvb(
                    raster=(self.out_path / self.indice_filename),
                    cloud_mask=self.cm_product.path,
                    lut_dict=cmap,
                    clouds_color="white",
                    out_path=(self.out_path / quicklook_filename),
                )


class BIGR(Indice):
    """
    $$Brightness\,Index\,Green\,Red = \sqrt{ {GREEN^{2}+ RED^{2}}\over{2}}$$

    GREEN: band 03
    RED: band 04
    """

    name = "BIGR"
    filename_template = "{product_identifier}_BIGR{ext}"
    ext = ".jp2"
    ext_raw = ".tif"
    ext_raw_aux = ".tif.aux.xml"
    colormap = cm.pink

    def __init__(self, l2a_product_object, cm_product_object):
        if (l2a_product_object or cm_product_object) is None:
            raise ValueError(
                "A L2aProduct and NewCloudMask objects must be provided"
            )
        else:
            self.l2a_product = l2a_product_object
            self.cm_product = cm_product_object

        # output path
        self.out_path = None

        # filenames
        self.indice_stem = self.filename_template.format(
            product_identifier=self.l2a_product.identifier, ext=""
        )
        self.indice_filename = self.indice_stem + self.ext
        self.indice_raw = self.indice_stem + self.ext_raw

    def process_indice(
        self,
        out_path: pathlib.PosixPath,
        reprocess: bool = False,
        nodata_clouds: bool = False,
        quicklook: bool = False,
        baseline: str = "4.0",
    ) -> None:
        """
        Process BIGR

        :param out_path: Path to computed indice
        :type out_path: Path
        :param reprocess: When True, delete existing indice and process new one
        :type reprocess: bool, Default to False
        :param nodata_clouds: When True, mask indice with cloud mask.
        :type nodata_clouds: bool, Default to False
        :param quicklook: When True, process indice quicklook
        :type quicklook: bool, Default to True
        :param baseline: L2A Product baseline. Needed to compute the right indice formula.
        :type baseline: str
        """
        self.out_path = out_path

        if (out_path / self.indice_filename).exists() and not reprocess:
            logger.info("{} already exists".format(self.indice_filename))
        else:
            create_raw_bigr(
				baseline=baseline,
                red_path=self.l2a_product.b04_10m,
                green_path=self.l2a_product.b03_10m,
                out_path=(out_path / self.indice_raw),
            )
            index_tiff_2_jp2(
                img_path=(out_path / self.indice_raw),
                out_path=(out_path / self.indice_filename),
            )

        if nodata_clouds:
            if not self.cm_product.path.exists():
                logger.info("Cloudmask does not exist, indice not masked")
                raise ValueError("Cloud mask does not exist")
            masked_indice_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext
            )
            masked_indice_raw = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw
            )
            masked_indice_raw_aux = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw_aux
            )

            if (out_path / masked_indice_filename).exists() and not reprocess:
                logger.info("{} already exists".format(masked_indice_filename))
            else:
                if (out_path / self.indice_raw).exists():
                    bigr_name = out_path / self.indice_raw
                else:
                    bigr_name = out_path / self.indice_filename
                create_masked_indice(
                    indice_path=bigr_name,
                    cloud_mask_path=self.cm_product.path,
                    out_path=(out_path / masked_indice_raw),
                )
                index_tiff_2_jp2(
                    img_path=(out_path / masked_indice_raw),
                    out_path=(out_path / masked_indice_filename),
                )
                os.remove(str(out_path / masked_indice_raw))
                os.remove(str(out_path / masked_indice_raw_aux))

        try:
            os.remove(str(out_path / self.indice_raw))
            os.remove(str(out_path / self.indice_stem + self.ext_raw_aux))
            logger.info("Removing {}".format(self.indice_raw))
        except Exception:
            pass

        if quicklook:
            cmap = matplotlib_colormap_to_rgb(self.colormap, revers=False)

            quicklook_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + "_QL.tif"
            )
            if (self.out_path / quicklook_filename).exists() and not reprocess:
                logger.info("{} already exists".format(quicklook_filename))
            else:
                logger.info("creating quicklook")
                create_rvb(
                    raster=(self.out_path / self.indice_filename),
                    cloud_mask=self.cm_product.path,
                    lut_dict=cmap,
                    clouds_color="white",
                    out_path=(self.out_path / quicklook_filename),
                    stretch=(0, 2500),
                )


class BIRNIR(Indice):
    """
    $$Brightness\,Index\,Red\,Near\,InfraRed = \sqrt{ {RED^{2} + NIR^{2}}\over{2} }$$

    NIR: band 08
    RED: band 04
    """

    name = "BIRNIR"
    filename_template = "{product_identifier}_BIRNIR{ext}"
    ext = ".jp2"
    ext_raw = ".tif"
    ext_raw_aux = ".tif.aux.xml"
    colormap = cm.afmhot

    def __init__(self, l2a_product_object, cm_product_object):
        if (l2a_product_object or cm_product_object) is None:
            raise ValueError(
                "A L2aProduct and NewCloudMask objects must be provided"
            )
        else:
            self.l2a_product = l2a_product_object
            self.cm_product = cm_product_object

        # output path
        self.out_path = None

        # filenames
        self.indice_stem = self.filename_template.format(
            product_identifier=self.l2a_product.identifier, ext=""
        )
        self.indice_filename = self.indice_stem + self.ext
        self.indice_raw = self.indice_stem + self.ext_raw

    def process_indice(
        self,
        out_path: pathlib.PosixPath,
        reprocess: bool = False,
        nodata_clouds: bool = False,
        quicklook: bool = False,
        baseline: str = "4.0",
    ) -> None:
        """
        Process BIRNIR

        :param out_path: Path to computed indice
        :type out_path: Path
        :param reprocess: When True, delete existing indice and process new one
        :type reprocess: bool, Default to False
        :param nodata_clouds: When True, mask indice with cloud mask.
        :type nodata_clouds: bool, Default to False
        :param quicklook: When True, process indice quicklook
        :type quicklook: bool, Default to True
        :param baseline: L2A Product baseline. Needed to compute the right indice formula.
        :type baseline: str
        """
        self.out_path = out_path

        if (out_path / self.indice_filename).exists() and not reprocess:
            logger.info("{} already exists".format(self.indice_filename))
        else:
            create_raw_birnir(
				baseline=baseline,
                red_path=self.l2a_product.b04_10m,
                nir_path=self.l2a_product.b08_10m,
                out_path=(out_path / self.indice_raw),
            )
            index_tiff_2_jp2(
                img_path=(out_path / self.indice_raw),
                out_path=(out_path / self.indice_filename),
            )

        if nodata_clouds:
            if not self.cm_product.path.exists():
                logger.info("Cloudmask does not exist, indice not masked")
                raise ValueError("Cloud mask does not exist")
            masked_indice_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext
            )
            masked_indice_raw = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw
            )
            masked_indice_raw_aux = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw_aux
            )

            if (out_path / masked_indice_filename).exists() and not reprocess:
                logger.info("{} already exists".format(masked_indice_filename))
            else:
                if (out_path / self.indice_raw).exists():
                    birnir_name = out_path / self.indice_raw
                else:
                    birnir_name = out_path / self.indice_filename
                create_masked_indice(
                    indice_path=birnir_name,
                    cloud_mask_path=self.cm_product.path,
                    out_path=(out_path / masked_indice_raw),
                )
                index_tiff_2_jp2(
                    img_path=(out_path / masked_indice_raw),
                    out_path=(out_path / masked_indice_filename),
                )
                os.remove(str(out_path / masked_indice_raw))
                os.remove(str(out_path / masked_indice_raw_aux))

        try:
            os.remove(str(out_path / self.indice_raw))
            os.remove(str(out_path / self.indice_stem + self.ext_raw_aux))
            logger.info("Removing {}".format(self.indice_raw))
        except Exception:
            pass

        if quicklook:
            cmap = matplotlib_colormap_to_rgb(self.colormap, revers=False)

            quicklook_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + "_QL.tif"
            )
            if (self.out_path / quicklook_filename).exists() and not reprocess:
                logger.info("{} already exists".format(quicklook_filename))
            else:
                logger.info("creating quicklook")
                create_rvb(
                    raster=(self.out_path / self.indice_filename),
                    cloud_mask=self.cm_product.path,
                    lut_dict=cmap,
                    clouds_color="white",
                    out_path=(self.out_path / quicklook_filename),
                    stretch=(0, 5000),
                )


class BIBG(Indice):
    """
    $$Brightness\,Index\,Blue\,Green = \sqrt{{BLUE^{2} + GREEN^{2}}\over{2}}$$

    BLUE: band 02
    GREEN: band 03
    """

    name = "BIBG"
    filename_template = "{product_identifier}_BIBG{ext}"
    ext = ".jp2"
    ext_raw = ".tif"
    ext_raw_aux = ".tif.aux.xml"
    colormap = cm.bone

    def __init__(self, l2a_product_object, cm_product_object):
        if (l2a_product_object or cm_product_object) is None:
            raise ValueError(
                "A L2aProduct and NewCloudMask objects must be provided"
            )
        else:
            self.l2a_product = l2a_product_object
            self.cm_product = cm_product_object

        # output path
        self.out_path = None

        # filenames
        self.indice_stem = self.filename_template.format(
            product_identifier=self.l2a_product.identifier, ext=""
        )
        self.indice_filename = self.indice_stem + self.ext
        self.indice_raw = self.indice_stem + self.ext_raw

    def process_indice(
        self,
        out_path: pathlib.PosixPath,
        reprocess: bool = False,
        nodata_clouds: bool = False,
        quicklook: bool = False,
        baseline: str = "4.0",
    ) -> None:
        """
        Process BIBG

        :param out_path: Path to computed indice
        :type out_path: Path
        :param reprocess: When True, delete existing indice and process new one
        :type reprocess: bool, Default to False
        :param nodata_clouds: When True, mask indice with cloud mask.
        :type nodata_clouds: bool, Default to False
        :param quicklook: When True, process indice quicklook
        :type quicklook: bool, Default to True
        :param baseline: L2A Product baseline. Needed to compute the right indice formula.
        :type baseline: str
        """
        self.out_path = out_path

        if (out_path / self.indice_filename).exists() and not reprocess:
            logger.info("{} already exists".format(self.indice_filename))
        else:
            create_raw_bibg(
				baseline=baseline,
                blue_path=self.l2a_product.b02_10m,
                green_path=self.l2a_product.b03_10m,
                out_path=(out_path / self.indice_raw),
            )
            index_tiff_2_jp2(
                img_path=(out_path / self.indice_raw),
                out_path=(out_path / self.indice_filename),
            )
        if nodata_clouds:
            if not self.cm_product.path.exists():
                logger.info("Cloudmask does not exist, indice not masked")
                raise ValueError("Cloud mask does not exist")
            masked_indice_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext
            )
            masked_indice_raw = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw
            )
            masked_indice_raw_aux = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw_aux
            )

            if (out_path / masked_indice_filename).exists() and not reprocess:
                logger.info("{} already exists".format(masked_indice_filename))
            else:
                if (out_path / self.indice_raw).exists():
                    bibg_name = out_path / self.indice_raw
                else:
                    bibg_name = out_path / self.indice_filename
                create_masked_indice(
                    indice_path=bibg_name,
                    cloud_mask_path=self.cm_product.path,
                    out_path=(out_path / masked_indice_raw),
                )
                index_tiff_2_jp2(
                    img_path=(out_path / masked_indice_raw),
                    out_path=(out_path / masked_indice_filename),
                )
                os.remove(str(out_path / masked_indice_raw))
                os.remove(str(out_path / masked_indice_raw_aux))
        try:
            os.remove(str(out_path / self.indice_raw))
            os.remove(str(out_path / self.indice_stem + self.ext_raw_aux))
            logger.info("Removing {}".format(self.indice_raw))
        except Exception:
            pass
        if quicklook:
            cmap = matplotlib_colormap_to_rgb(self.colormap, revers=False)
            quicklook_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + "_QL.tif"
            )
            if (self.out_path / quicklook_filename).exists() and not reprocess:
                logger.info("{} already exists".format(quicklook_filename))
            else:
                logger.info("creating quicklook")
                create_rvb(
                    raster=(self.out_path / self.indice_filename),
                    cloud_mask=self.cm_product.path,
                    lut_dict=cmap,
                    clouds_color="white",
                    out_path=(self.out_path / quicklook_filename),
                    stretch=(0, 2500),
                )

class EVI(Indice):
    """
    $$Enhanced\,Vegetation\,Index = {2.5 * {( NIR - RED )}\over{( NIR + 6 * RED - 7.5 * BLUE ) + 1}}$$

    BLUE: band 02
    RED: band 04
    NIR: band 08
    """

    name = "EVI"
    filename_template = "{product_identifier}_EVI{ext}"
    ext = ".jp2"
    ext_raw = ".tif"
    ext_raw_aux = ".tif.aux.xml"
    colormap = cm.bone # à changer selon la bonne couleur

    def __init__(self, l2a_product_object, cm_product_object):
        if (l2a_product_object or cm_product_object) is None:
            raise ValueError(
                "A L2aProduct and NewCloudMask objects must be provided"
            )
        else:
            self.l2a_product = l2a_product_object
            self.cm_product = cm_product_object

        # output path
        self.out_path = None

        # filenames
        self.indice_stem = self.filename_template.format(
            product_identifier=self.l2a_product.identifier, ext=""
        )
        self.indice_filename = self.indice_stem + self.ext
        self.indice_raw = self.indice_stem + self.ext_raw

    def process_indice(
        self,
        out_path: pathlib.PosixPath,
        reprocess: bool = False,
        nodata_clouds: bool = False,
        quicklook: bool = False,
        baseline: str = "4.0",
    ) -> None:
        """
        Process EVI

        :param out_path: Path to computed indice
        :type out_path: Path
        :param reprocess: When True, delete existing indice and process new one
        :type reprocess: bool, Default to False
        :param nodata_clouds: When True, mask indice with cloud mask.
        :type nodata_clouds: bool, Default to False
        :param quicklook: When True, process indice quicklook
        :type quicklook: bool, Default to True
        :param baseline: L2A Product baseline. Needed to compute the right indice formula.
        :type baseline: str
        """
        self.out_path = out_path

        if (out_path / self.indice_filename).exists() and not reprocess:
            logger.info("{} already exists".format(self.indice_filename))
        else:
            create_raw_evi(
				baseline=baseline,
                blue_path=self.l2a_product.b02_10m,
                red_path=self.l2a_product.b04_10m,
                nir_path=self.l2a_product.b08_10m,
                out_path=(out_path / self.indice_raw),
            )
            index_tiff_2_jp2(
                img_path=(out_path / self.indice_raw),
                out_path=(out_path / self.indice_filename),
            )
        if nodata_clouds:
            if not self.cm_product.path.exists():
                logger.info("Cloudmask does not exist, indice not masked")
                raise ValueError("Cloud mask does not exist")
            masked_indice_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext
            )
            masked_indice_raw = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw
            )
            masked_indice_raw_aux = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw_aux
            )

            if (out_path / masked_indice_filename).exists() and not reprocess:
                logger.info("{} already exists".format(masked_indice_filename))
            else:
                if (out_path / self.indice_raw).exists():
                    evi_name = out_path / self.indice_raw
                else:
                    evi_name = out_path / self.indice_filename
                create_masked_indice(
                    indice_path=evi_name,
                    cloud_mask_path=self.cm_product.path,
                    out_path=(out_path / masked_indice_raw),
                )
                index_tiff_2_jp2(
                    img_path=(out_path / masked_indice_raw),
                    out_path=(out_path / masked_indice_filename),
                )
                os.remove(str(out_path / masked_indice_raw))
                os.remove(str(out_path / masked_indice_raw_aux))
        try:
            os.remove(str(out_path / self.indice_raw))
            os.remove(str(out_path / self.indice_stem + self.ext_raw_aux))
            logger.info("Removing {}".format(self.indice_raw))
        except Exception:
            pass
        if quicklook:
            cmap = matplotlib_colormap_to_rgb(self.colormap, revers=False)
            quicklook_filename = (
                self.indice_stem + "_" + self.cm_product.suffix + "_QL.tif"
            )
            if (self.out_path / quicklook_filename).exists() and not reprocess:
                logger.info("{} already exists".format(quicklook_filename))
            else:
                logger.info("creating quicklook")
                create_rvb(
                    raster=(self.out_path / self.indice_filename),
                    cloud_mask=self.cm_product.path,
                    lut_dict=cmap,
                    clouds_color="white",
                    out_path=(self.out_path / quicklook_filename),
                    stretch=(0, 2500),
                )


class NBR(Indice):
    """
    $$Normalized\,Burn\,Ratio = {{NIR-SWIR}\over{NIR+SWIR}}$$

    NIR: band 08
    SWIR: band 12

    """

    name = "NBR"
    filename_template = "{product_identifier}_NBR{ext}"
    ext = ".jp2"
    ext_raw = ".tif"
    ext_raw_aux = ".tif.aux.xml"
    colormap = cm.RdYlBu

    def __init__(self, l2a_product_object, cm_product_object):
        if (l2a_product_object or cm_product_object) is None:
            raise ValueError(
                "A L2aProduct and NewCloudMask objects must be provided"
            )
        else:
            self.l2a_product = l2a_product_object
            self.cm_product = cm_product_object

        self.out_path = None

        self.indice_stem = self.filename_template.format(
            product_identifier=self.l2a_product.identifier, ext=""
        )
        self.indice_filename = self.indice_stem + self.ext
        self.indice_raw = self.indice_stem + self.ext_raw

    def process_indice(
            self,
            out_path: pathlib.PosixPath,
            reprocess: bool = False,
            nodata_clouds: bool = False,
            quicklook: bool = False,
            baseline: str = "4.0",
    ) -> None:
        """
        Process NBR

        :param out_path: Path to computed indice
        :type out_path: Path
        :param reprocess: When True, delete existing indice and process new one
        :type reprocess: bool, Default to False
        :param nodata_clouds: When True, mask indice with cloud mask.
        :type nodata_clouds: bool, Default to False
        :param quicklook: When True, process indice quicklook
        :type quicklook: bool, Default to True
        :param baseline: L2A Product baseline. Needed to compute the right indice formula.
        :type baseline: str
        """
        self.out_path = out_path

        if (out_path / self.indice_filename).exists() and not reprocess:
            logger.info("{} already exists".format(self.indice_filename))
        else:
            create_raw_ndr(
				baseline=baseline,
                b1_path=self.l2a_product.b08_10m,
                b2_path=self.l2a_product.b12_20m,
                out_path=(out_path / self.indice_raw),
            )
            index_tiff_2_jp2(
                img_path=(out_path / self.indice_raw),
                out_path=(out_path / self.indice_filename),
                quality=20,
            )

        if nodata_clouds:
            if not self.cm_product.path.exists():
                logger.info("Cloudmask does not exist, indice not masked")
                raise ValueError("Cloud mask does not exist")
            masked_indice_filename = (
                    self.indice_stem + "_" + self.cm_product.suffix + self.ext
            )
            masked_indice_raw = (
                    self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw
            )
            masked_indice_raw_aux = (
                self.indice_stem + "_" + self.cm_product.suffix + self.ext_raw_aux
            )

            if (out_path / masked_indice_filename).exists() and not reprocess:
                logger.info("{} already exists".format(masked_indice_filename))
            else:
                if (out_path / self.indice_raw).exists():
                    nbr_name = out_path / self.indice_raw
                else:
                    nbr_name = out_path / self.indice_filename
                create_masked_indice(
                    indice_path=nbr_name,
                    cloud_mask_path=self.cm_product.path,
                    out_path=(out_path / masked_indice_raw),
                )
                index_tiff_2_jp2(
                    img_path=(out_path / masked_indice_raw),
                    out_path=(out_path / masked_indice_filename),
                    quality=20,
                )
                os.remove(str(out_path / masked_indice_raw))
                os.remove(str(out_path / masked_indice_raw_aux))

        try:
            os.remove(str(out_path / self.indice_raw))
            os.remove(str(out_path / self.indice_stem + self.ext_raw_aux))
            logger.info("Removing {}".format(self.indice_raw))
        except Exception:
            pass

        if quicklook:
            cmap = matplotlib_colormap_to_rgb(self.colormap, revers=False)

            quicklook_filename = self.indice_stem + "_QUICKLOOK.tif"
            if (self.out_path / quicklook_filename).exists() and not reprocess:
                logger.info("{} already exists".format(quicklook_filename))
            else:
                logger.info("creating quicklook")
                create_rvb(
                    raster=(self.out_path / self.indice_filename),
                    cloud_mask=self.cm_product.path,
                    lut_dict=cmap,
                    clouds_color="white",
                    out_path=(self.out_path / quicklook_filename),
                )


class IndicesCollectionMeta(type):
    """Adds special methods to IndicesCollection class."""

    def __iter__(cls):
        return iter(cls._indices_classes)

    def __contains__(cls, indice):
        return indice in cls._indices_classes

    def __len__(cls):
        return len(cls._indices_classes)

    def __str__(cls):
        return str(cls.list)

    @property
    def available_indices(cls) -> List[str]:
        """Returns indices classes names."""
        return list(cls._indices_classes.keys())

    @property
    def list(cls) -> List[str]:
        """Returns indices classes names."""
        return list(cls._indices_classes.keys())

    def get_indice_cls(cls, indice: str) -> Union["Indice", None]:
        """
        Returns the indice class.

        :param indice: name of the indice.
        """
        try:
            indice_cls = cls._indices_classes[indice]
        except KeyError:
            logger.error("Indice: {} is not defined".format(indice))
            return None
        else:
            return indice_cls


class IndicesCollection(metaclass=IndicesCollectionMeta):
    """Lists defined indices classes.

    Usage:
        >>> IndicesCollection.list

    """

    # TODO: Implement class as a singleton.

    _indices_classes = {
        cls.__dict__["name"]: cls for cls in Indice.__subclasses__()
    }
