# coding: utf-8

"""
Module for managing products and tiles in the library and temp folders.
"""

import pathlib
import logging
import re
import fiona
import shutil
from PIL import Image
import setuptools
import os
import time
from itertools import chain
from pathlib import Path
from collections import namedtuple
from datetime import datetime, timedelta
from pprint import pformat
import multiprocessing
from queue import Queue
from threading import Thread
from jpylyzer import jpylyzer
import subprocess

# type annotations
from typing import List, Dict, Iterable
from packaging import version

from .config import Config, SHARED_DATA
from .utils import (
    str_to_datetime, 
    datetime_to_str,
    human_size, 
    getFolderSize, 
    get_cm_dict,
    get_cm_string_from_dict,
    # get_latest_s2c_version_path,
    get_latest_Sen2Cor_version_from_id,
)
from .indices import IndicesCollection
from .products import (
    L1cProduct,
    L2aProduct,
    OldCloudMaskProduct,
    NewCloudMaskProduct,
    IndiceProduct,
)
from .multi_processing import (
    l2a_multiprocessing,
    cld_version_probability_iterations_reprocessing_multiprocessing,
    idx_multiprocessing,
)
from .download_eodag import S2cEodag

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


# namedtuple
TileProduct = namedtuple("TileProduct", ["identifier", "date", "cloud_cover"])


class ProductsList:
    """Class for managing tiles products lists. Each item is a TileProduct
    namedtuple.

    """

    def __init__(self):
        self._dict = dict()
        self._time_index = dict()

    @property
    def products(self) -> List[str]:
        """Returns products names."""
        return sorted(list(self._dict.keys()), key = lambda x:x[11:25])

    @property
    def dates(self) -> List[datetime]:
        """Returns products dates."""
        return list(self._time_index.keys())

    @property
    def first(self) -> TileProduct:
        """Returns oldest product."""
        try:
            min_date = min(self.dates)
        except ValueError:
            return TileProduct(None, None, None)
        prod = self._time_index[min_date]
        return TileProduct(prod, min_date, self._dict[prod]["cloud_cover"])

    @property
    def last(self) -> TileProduct:
        """Returns newest product."""
        try:
            max_date = max(self.dates)
        except ValueError:
            return TileProduct(None, None, None)
        prod = self._time_index[max_date]
        return TileProduct(prod, max_date, self._dict[prod]["cloud_cover"])

    def filter_dates(
        self,
        date_min: str = None, 
        date_max: str = None,
        date_exact: str = None,
    ) -> "ProductsList":
        """Filters products list in a time range.

        :param date_min: Oldest date.
        :type date_min: str
        :param date_max: Newest date.
        :type date_max: str
        """
        min_date = (
            str_to_datetime(date_min, "ymd") if date_min else self.first.date
        )
        max_date = (
            str_to_datetime(date_max, "ymd") if date_max else self.last.date
        )
        filtered = ProductsList()

        if date_exact:
            if type(date_exact) is list:
                for date in date_exact:
                    if len(date) == 2:
                        for k, v in self._dict.items():    
                            if (datetime.strftime(v["date"].date(), "%m") == date) and (min_date.date() <= v["date"].date() <= max_date.date()):
                                filtered[k] = {"date": v["date"], "cloud_cover": v["cloud_cover"]}      
                    elif len(date) == 4:
                        for k, v in self._dict.items():
                            if (str(v["date"].date().year) == date) and (min_date.date() <= v["date"].date() <= max_date.date()):
                                filtered[k] = {"date": v["date"], "cloud_cover": v["cloud_cover"]}
                    elif len(date) == 6:
                        for k, v in self._dict.items():
                            if (datetime.strftime(v["date"].date(), "%Y%m") == date) and (min_date.date() <= v["date"].date() <= max_date.date()):
                                filtered[k] = {"date": v["date"], "cloud_cover": v["cloud_cover"]}
                    elif  len(date) == 10:
                        for k, v in self._dict.items():
                            if (datetime.strftime(v["date"].date(), "%Y-%m-%d") == date) and (min_date.date() <= v["date"].date() <= max_date.date()):
                                filtered[k] = {"date": v["date"], "cloud_cover": v["cloud_cover"]}
            else:
                if len(date_exact) == 2:
                    for k, v in self._dict.items():    
                        if (datetime.strftime(v["date"].date(), "%m") == date_exact) and (min_date.date() <= v["date"].date() <= max_date.date()):
                            filtered[k] = {"date": v["date"], "cloud_cover": v["cloud_cover"]}      
                elif len(date_exact) == 4:
                    for k, v in self._dict.items():
                        if (str(v["date"].date().year) == date_exact) and (min_date.date() <= v["date"].date() <= max_date.date()):
                            filtered[k] = {"date": v["date"], "cloud_cover": v["cloud_cover"]}
                elif len(date_exact) == 6:
                    for k, v in self._dict.items():
                        if (datetime.strftime(v["date"].date(), "%Y%m") == date_exact) and (min_date.date() <= v["date"].date() <= max_date.date()):
                            filtered[k] = {"date": v["date"], "cloud_cover": v["cloud_cover"]}
                elif  len(date_exact) == 10:
                    for k, v in self._dict.items():
                        if (datetime.strftime(v["date"].date(), "%Y-%m-%d") == date_exact) and (min_date.date() <= v["date"].date() <= max_date.date()):
                            filtered[k] = {"date": v["date"], "cloud_cover": v["cloud_cover"]}
        else:
            for k, v in self._dict.items():
                if min_date.date() <= v["date"].date() <= max_date.date():
                    filtered[k] = {
                        "date": v["date"], 
                        "cloud_cover": v["cloud_cover"]
                    }
        return filtered
        
    def filter_clouds(
        self, 
        cover_min: int = 0, 
        cover_max: int = 100,
        select_none: bool = True,
    ) -> "ProductsList":
        """Filters products list with a cloud coverage range.

        :param cover_min: Minimum cloud coverage.
        :type cover_min: int. Default to 0
        :param cover_max: maximum cloud coverage.
        :type cover_max: int. Default to 100
        """
        filtered = ProductsList()
        for k, v in self._dict.items():
            if not v["cloud_cover"] is None:
                if cover_min <= v["cloud_cover"] <= cover_max:
                    filtered[k] = {
                        "date": v["date"],
                        "cloud_cover": v["cloud_cover"],
                    }
            elif select_none:
                filtered[k] = {
                    "date": v["date"],
                    "cloud_cover": v["cloud_cover"],
                }
        return filtered
        
    def filter_processing_baseline(
        self,
        pb_min: str = "0.0",
        pb_max: str = "9999",
    ) -> "ProductsList":
        """Filters products list with Sen2Cor processing baseline version.

        :param pb_min: minimum processing baseline version
        :type pb_min: str, Default to "0.0"
        :param pb_max: maximum processing baseline version
        :type pb_max: str, Default to "9999"
        """        
        filtered = ProductsList()
        for k, v in self._dict.items():
            if "L1C" in k:
                processing_baseline = L1cProduct(identifier=k).processing_baseline
                if version.parse(pb_min) <= version.parse(processing_baseline) <= version.parse(pb_max):
                    filtered[k] = {"date": v["date"], "cloud_cover": v["cloud_cover"]}
        return filtered
        
    def __len__(self) -> int:
        return len(self._dict)

    def __contains__(self, item) -> bool:
        return item in self._dict

    def __repr__(self) -> str:
        return pformat([self[k] for k in self.products], indent=1, width=1)

    def __iter__(self) -> Iterable:
        return (self[k] for k in self._dict)

    def __getitem__(self, item) -> TileProduct:
        if item in self._dict:
            return TileProduct(
                item, self._dict[item]["date"], self._dict[item]["cloud_cover"]
            )
        else:
            raise KeyError("{} not found".format(item))

    def __setitem__(self, item, value) -> None:
        self._dict[item] = {
            "date": value["date"],
            "cloud_cover": value["cloud_cover"],
        }
        self._time_index[value["date"]] = item


class CloudMaskList(ProductsList):
    """Class for managing mask product list"""

    @property
    def CM001(self) -> "CloudMaskList":
        """Returns Cloudmask 1 products"""
        filtered = CloudMaskList()
        for k, v in self._dict.items():
            if "_CM001" in k:
                filtered[k] = {
                    "date": v["date"],
                    "cloud_cover": v["cloud_cover"],
                }
        return filtered

    @property
    def CM002(self) -> "CloudMaskList":
        """Returns Cloudmask 2 products"""
        filtered = CloudMaskList()
        for k, v in self._dict.items():
            if "_CM002" in k:
                filtered[k] = {
                    "date": v["date"],
                    "cloud_cover": v["cloud_cover"],
                }
        return filtered

    @property
    def CM003(self) -> "CloudMaskList":
        """Returns Cloudmask 3 products"""
        filtered = CloudMaskList()
        for k, v in self._dict.items():
            if "_CM003" in k:
                filtered[k] = {
                    "date": v["date"],
                    "cloud_cover": v["cloud_cover"],
                }
        return filtered

    @property
    def CM004(self) -> "CloudMaskList":
        """Returns Cloudmask 2 products"""
        filtered = CloudMaskList()
        for k, v in self._dict.items():
            if "_CM004" in k:
                filtered[k] = {
                    "date": v["date"],
                    "cloud_cover": v["cloud_cover"],
                }
        return filtered

    def params(
        self,
        probability: int = 1,
        iterations: int = 5,
        cld_shad: bool = True,
        cld_med_prob: bool = True,
        cld_hi_prob: bool = True,
        thin_cir: bool = True,
    ):
        """Returns Cloudmask products based on their processing parameters.

        :param probability: Cloud Probability threshold value.
        :type probability: int, Default to 1
        :param iterations: Number of iterations of the dilatation kernel for cloud masking.
        :type iterations: int, Default to 5
        :param cld_shad: When True, uses Cloud Shadows class from Scene Classification.
        :type cld_shad: bool, Default to True
        :param cld_med_prob: When True, uses Cloud Medium Probability class from Scene Classification.
        :type cld_med_prob: bool, Default to True
        :param cld_hi_prob: When True, uses Cloud High Probability class from Scene Classification.
        :type cld_hi_prob: bool, Default to True
        :param thin_cir: When True, uses Thin Cirrus class from Scen Classification.
        :type thin_cir: bool, Default to True
        """
        filtered = CloudMaskList()
        for k, v in self._dict.items():
            if "_CM003" in k:
                if "-PRB" + str(probability) + "-ITER" + str(iterations) in k:
                    filtered[k] = {
                        "date": v["date"],
                        "cloud_cover": v["cloud_cover"],
                    }
            elif "_CM004" in k:
                if (
                    "-CSH"
                    + str(int(cld_shad))
                    + "-CMP"
                    + str(int(cld_med_prob))
                    + "-CHP"
                    + str(int(cld_hi_prob))
                    + "-TCI"
                    + str(int(thin_cir))
                    + "-ITER"
                    + str(iterations)
                    in k
                ):
                    filtered[k] = {
                        "date": v["date"],
                        "cloud_cover": v["cloud_cover"],
                    }
            else:
                filtered[k] = {
                    "date": v["date"],
                    "cloud_cover": v["cloud_cover"],
                }
        return filtered

    def cm_filter(
        self,
        cm_strings: list = [],
    ):
        """Returns filters Cloudmask products based on their cloudmask strings.

        :param cm_strings: A list of cloudmask strings, examples: "CM001", "CM004-CSH1-CMP1-CHP1-TCI1-ITER1".
        :type cm_strings: list, Default to []
        :param iterations: Number of iterations of the dilatation kernel for cloud masking.
        :type iterations: int, Default to 5
        :param cld_shad: When True, uses Cloud Shadows class from Scene Classification.
        :type cld_shad: bool, Default to True
        :param cld_med_prob: When True, uses Cloud Medium Probability class from Scene Classification.
        :type cld_med_prob: bool, Default to True
        :param cld_hi_prob: When True, uses Cloud High Probability class from Scene Classification.
        :type cld_hi_prob: bool, Default to True
        :param thin_cir: When True, uses Thin Cirrus class from Scen Classification.
        :type thin_cir: bool, Default to True
        """
        filtered = CloudMaskList()
        if cm_strings:
            for cm_string in cm_strings:
                for k, v in self._dict.items():
                    if cm_string in k:
                        filtered[k] = {
                            "date": v["date"],
                            "cloud_cover": v["cloud_cover"],
                        }
        else:
            for k, v in self._dict.items():
                filtered[k] = {
                    "date": v["date"],
                    "cloud_cover": v["cloud_cover"],
                }
        return filtered

# class IndicesList(ProductsList):
#     """Class for managing indices products lists.

#     """
#     @property
#     def raws(self) -> "ProductsList":
#         filtered = ProductsList()
#         for k, v in self._dict.items():
#             if not("MASK" in k) and not("QUICKLOOK" in k):
#                 filtered[k] = {"date": v["date"], "cloud_cover": v["cloud_cover"]}
#         return filtered

#     @property
#     def masks(self) -> "ProductsList":
#         filtre = ProductsList()
#         for k, v in self._dict.items():
#             if "MASK" in k:
#                 filtre[k] = {"date": v["date"], "cloud_cover": v["cloud_cover"]}
#         return filtre

#     @property
#     def quicklooks(self) -> "ProductsList":
#         filtered = ProductsList()
#         for k, v in self._dict.items():
#             if "QUICKLOOK" in k:
#                 filtered[k] = {"date": v["date"], "cloud_cover": v["cloud_cover"]}
#         return filtered


class NewIndiceList(CloudMaskList):
    """Class for managing indices products lists."""

    @property
    def raws(self) -> "NewIndiceList":
        """Returns products list of indices not masked"""
        filtered = NewIndiceList()
        for k, v in self._dict.items():
            if not ("_CM" in k):
                filtered[k] = {
                    "date": v["date"],
                    "cloud_cover": v["cloud_cover"],
                }
        return filtered

    @property
    def masks(self) -> "NewIndiceList":
        """Returns products list of masked indices"""
        filtred = NewIndiceList()
        for k, v in self._dict.items():
            if ("_CM" in k) and not ("_QL" in k):
                filtred[k] = {
                    "date": v["date"],
                    "cloud_cover": v["cloud_cover"],
                }
        return filtred

    @property
    def quicklooks(self) -> "NewIndiceList":
        """Returns products list of indices with quicklooks"""
        filtered = NewIndiceList()
        for k, v in self._dict.items():
            if "_QL" in k:
                filtered[k] = {
                    "date": v["date"],
                    "cloud_cover": v["cloud_cover"],
                }
        return filtered


class Tile:
    """Class for managing tiles in the library.

    :param name: Sentinel-2 Tile name.
    :type name: str, "XX123"

    Usage:
        >>> Tile("40KCC")
    """

    def __init__(self, name: str):
        self.name = name
        self._paths = {
            "l1c": Path(Config().get("l1c_path")) / name,
            "l2a": Path(Config().get("l2a_path")) / name,
            "indices": {},
            "cloudmasks": Path(Config().get("cloudmasks_path")) / name,
        }
        self._indices_path = Path(Config().get("indices_path"))
        self._cloudmasks_path = Path(Config().get("cloudmasks_path"))

        self._products = {
            "l1c": ProductsList(),
            "l2a": ProductsList(),
            "cloudmasks": ProductsList(),
            # "cloudmasks2" : ProductsList(),
            "indices": dict(),
        }

        self._get_indices_paths()
        self._get_l1c_list()
        self._get_l2a_list()
        self._get_cloudmasks()
        # self._get_indices_list()
        self._get_new_indice_list()
        self.indice_list = [indice for indice, path in self._paths["indices"].items()]
        self.cloudmask_list = self._get_cloudmask_list()

    def _get_indices_paths(self) -> None:
        """Updates self._paths"""
        for indice in self._indices_path.glob("*/{}".format(self.name)):
            self._paths["indices"][str(indice.parent.name.lower())] = indice

    def _get_l1c_list(self) -> None:
        """Scans L1C folder and adds products in a ProductsList."""
        for f in self._paths["l1c"].glob("*L1C_*.SAFE"):
            try:
                cloud_cover = L1cProduct(
                    identifier=f.name, tile=self.name, path=None
                ).cloud_coverage_assessment
                self._products["l1c"][f.name] = {
                    "date": Tile._get_date(f.name),
                    "cloud_cover": float(cloud_cover),
                }
            except Exception:
                logger.info("skipping {}, you can try to redownload this product, but probably metadata issue".format(f.name))

    def _get_l2a_list(self) -> None:
        """Scans L2A folder and adds products in a ProductsList."""
        for f in self._paths["l2a"].glob("*L2A*.SAFE"):
            l1c_name = f.name.replace("L2A_", "L1C_").replace(
                "_USER_", "_OPER_"
            )
            try:
                date = self._products["l1c"][l1c_name].date
                cloud_cover = self._products["l1c"][l1c_name].cloud_cover
                self._products["l2a"][f.name] = {
                    "date": date,
                    "cloud_cover": float(cloud_cover),
                }
            except KeyError:
                try:
                    date = Tile._get_date(f.name)
                    cloud_cover = L2aProduct(
                        f.name, self.name
                    ).cloud_coverage_assessment
                    self._products["l2a"][f.name] = {
                        "date": date,
                        "cloud_cover": float(cloud_cover),
                    }
                except Exception:
                    logger.info("skipping {}, you can try to redownload and compute this product".format(f.name))
                    

    # def _get_cloudmasks(self) -> None:
    #     """Scans L2A folder for cloud masks and adds corresponding L2A products in a ProductsList."""
    #     for f in self._paths["l2a"].glob("*L2A*_CLOUD_MASK.jp2"):
    #         l1c_name = f.name.replace("L2A_", "L1C_").replace("_USER_", "_OPER_").replace("_CLOUD_MASK.jp2", ".SAFE")
    #         try:
    #             date = self._products["l1c"][l1c_name].date
    #             cloud_cover = self._products["l1c"][l1c_name].cloud_cover
    #         except KeyError:
    #             date = Tile._get_date(f.name.replace("_CLOUD_MASK.jp2", ".SAFE"))
    #             cloud_cover = L2aProduct(f.name.replace("_CLOUD_MASK.jp2", ".SAFE"), self.name).cloud_coverage_assessment
    #         self._products["cloudmasks"][f.name.replace("_CLOUD_MASK.jp2", ".SAFE")] = {"date": date, "cloud_cover": float(cloud_cover)}

    def _get_cloudmasks(self) -> None:
        """Scans cloudmasks folder for cloud masks and adds corresponding L2A
        products in a ProductsList."""
        self._products["cloudmasks"] = CloudMaskList()
        for f in self._paths["cloudmasks"].glob("*L2A*/*_CM*.jp2"):
            l1c_name = (
                f.parent.name.replace("L2A_", "L1C_").replace(
                    "_USER_", "_OPER_"
                )
                + ".SAFE"
            )
            try:
                date = self._products["l1c"][l1c_name].date
                cloud_cover = float(
                    self._products["l1c"][l1c_name].cloud_cover
                )
            except KeyError:
                date = Tile._get_date(f.parent.name)
                l2a = L2aProduct(f.parent.name, self.name)
                if l2a.path.exists():
                    cloud_cover = float(l2a.cloud_coverage_assessment)
                else:
                    cloud_cover = None
            self._products["cloudmasks"][f.name] = {
                "date": date,
                "cloud_cover": cloud_cover,
                # "version": re.findall(r"_(CM...)", f.name)[0]
            }
    
    def _get_cloudmask_list(self):
        # for f in self._paths["cloudmasks"].glob("*L2A*/*_CM*.jp2"):

        # library_path = self._paths["cloudmasks"]
        cloudmask_list = []
        paths = list(self._paths["cloudmasks"].glob("*L2A*/*_CM*.jp2"))
        for item in paths:
            cm_dict = get_cm_dict(item.stem)
            cm_string = get_cm_string_from_dict(cm_dict)
            cloudmask_list.append(cm_string)
            
                # self.cloudmask_string.append(cm_string)
                # cloudmasks.append(cm_dict)
        return list(set(cloudmask_list))
    

    # def _get_indices_list(self) -> None:
    # """Scans indices folders and adds products in a IndicesList."""
    # for indice, path in self._paths["indices"].items():
    # if path.is_dir():
    # self._products["indices"][indice] = IndicesList()
    # indice_template = IndicesCollection.get_indice_cls(indice.upper()).filename_template
    # indice_ext = IndicesCollection.get_indice_cls(indice.upper()).ext
    # file_patterns = [indice_ext, 'QUICKLOOK.tif']
    # files_selected = []
    # for p in file_patterns:
    # files_selected.extend(path.glob("*/*{}".format(p)))
    # for f in files_selected:
    # try:
    # indice_pattern = re.sub("{.*?}", "", indice_template)
    # remove_pattern = "{}.*".format(indice_pattern)
    # l2a_name = re.sub(remove_pattern, '', f.name) + ".SAFE"
    # date = self._products["l2a"][l2a_name].date
    # cloud_cover = self._products["l2a"][l2a_name].cloud_cover
    # except KeyError:
    # date = Tile._get_date(f.name)
    # cloud_cover = None
    # self._products["indices"][indice][f.name] = {"date": date, "cloud_cover": cloud_cover}
    # self.__dict__[indice] = self._products["indices"][indice]

    def _get_new_indice_list(self) -> None:
        """Scans indice folders and adds products in a NewIndiceList."""
        for indice, path in self._paths["indices"].items():
            if path.is_dir():
                self._products["indices"][indice] = NewIndiceList()
                indice_template = IndicesCollection.get_indice_cls(
                    indice.upper()
                ).filename_template
                indice_ext = IndicesCollection.get_indice_cls(
                    indice.upper()
                ).ext
                # file_patterns = [indice_ext, '_QL.tif']
                file_patterns = [
                    indice.upper() + indice_ext,
                    indice.upper() + "_CM*" + indice_ext,
                    indice.upper() + "_CM*" + "_QL.tif",
                ]
                files_selected = []
                for p in file_patterns:
                    files_selected.extend(path.glob("*/*{}".format(p)))
                for f in files_selected:
                    indice_pattern = re.sub("{.*?}", "", indice_template)
                    remove_pattern = "{}.*".format(indice_pattern)
                    l2a_name = re.sub(remove_pattern, "", f.name) + ".SAFE"
                    try:
                        date = self._products["l2a"][l2a_name].date
                        cloud_cover = self._products["l2a"][
                            l2a_name
                        ].cloud_cover
                    except KeyError:
                        l1c_name = l2a_name.replace("L2A_", "L1C_")
                        try:
                            date = self._products["l1c"][l1c_name].date
                            cloud_cover = self._products["l1c"][
                                l1c_name
                            ].cloud_cover
                        except KeyError:
                            date = Tile._get_date(f.name)
                            cloud_cover = None
                    self._products["indices"][indice][f.name] = {
                        "date": date,
                        "cloud_cover": cloud_cover,
                    }
                self.__dict__[indice] = self._products["indices"][indice]
        
        # create empty lists for indices not produced, so that Tile.indice_name_absent returns an empty list instead of error "Tile has no argument named indice_name_absent" 
        lower = []
        for indice in IndicesCollection.list:
            lower.append(indice.lower())
        indices_absent = list(set(lower).difference(list(self._products["indices"].keys())))
        for i in indices_absent:
            self.__dict__[i] = []

    @staticmethod
    def is_tile_name_valid(tile_name: str) -> bool:
        """Checks if the tile_name is a valid Sentinel-2 tile.

        :param tile_name: tile's name.
        :type tile_name: str
        """
        if not re.match(r"^[0-9]{2}[A-Z]{3}$", tile_name):
            return False
        with fiona.open(str(SHARED_DATA["tiles_index"]), "r") as tiles_index:
            tiles_names = [tile["properties"]["Name"] for tile in tiles_index]
            return tile_name in tiles_names

    @staticmethod
    def _get_date(product_name: str) -> datetime:
        """Extracts the acquisition date of a products filename.

        :param product_name: Product name
        :type product_name: str
        """
        date_pattern = re.findall(r"[0-9]{8}T[0-9]{6}", product_name)
        if product_name.startswith("S2A_OPER"):
            date = date_pattern[1]
        else:
            date = date_pattern[0]
        return str_to_datetime(date, "filename")

    def __repr__(self) -> str:
        return "Tile('{}')".format(self.name)

    def __str__(self) -> str:
        return self.name

    @property
    def paths(self) -> Dict[str, pathlib.PosixPath]:
        """Returns all the paths related to a Tile object."""
        return self._paths

    @property
    def l1c(self) -> "ProductsList":
        """Returns tile's L1C products as a ProductsList."""
        return self._products["l1c"]

    @property
    def l2a(self) -> "ProductsList":
        """Returns tile's L2A products as a ProductsList."""
        return self._products["l2a"]

    # @property
    # def cloudmasks(self) -> "ProductsList":
    # """Returns tile's cloud masks products as a ProductsList."""
    # return self._products["cloudmasks"]

    @property
    def cloudmasks(self) -> "ProductsList":
        """Returns tile's cloud masks products as a ProductsList."""
        return self._products["cloudmasks"]

    @property
    def l1c_missings(self) -> "ProductsList":
        """Returns tile's L2A products that don't have a L1C as a ProductsList."""
        prods_list = ProductsList()
        missings_l1c_set = set(self.l2a.products) - {
            identifier.replace("L1C_", "L2A_").replace("__OPER__", "_USER_")
            for identifier in self.l1c.products
        }
        for prod in missings_l1c_set:
            prods_list[prod] = {
                "date": self._products["l2a"][prod].date,
                "cloud_cover": self._products["l2a"][prod].cloud_cover,
            }
        return prods_list

    @property
    def l2a_missings(self) -> "ProductsList":
        """Returns tile's L1C products that don't have a L2A as a ProductsList."""
        prods_list = ProductsList()
        missings_l2a_set = set(self.l1c.products) - {
            identifier.replace("L2A_", "L1C_").replace("_USER_", "_OPER_")
            for identifier in self.l2a.products
        }
        for prod in missings_l2a_set:
            prods_list[prod] = {
                "date": self._products["l1c"][prod].date,
                "cloud_cover": self._products["l1c"][prod].cloud_cover,
            }
        return prods_list

    # @property
    # def cloudmasks_missings(self) -> "ProductsList":
        # """Returns tile's L2A products that don't have a cloud mask as a ProductsList."""
        # prods_list = ProductsList()
        # missings_l2a_set = set(self.l2a.products) - {identifier for identifier in self.cloudmasks.products}
        # for prod in missings_l2a_set:
            # prods_list[prod] = {"date": self._products["l2a"][prod].date,
                                # "cloud_cover": self._products["l2a"][prod].cloud_cover}
        # return prods_list

    @property
    def l1c_processable(self) -> "ProductsList":
        """Returns tile's L1C products that can be processed with sen2cor installation
        """
        prod_list = ProductsList()
        # processable_l1c_set =  {identifier for identifier in self.l1c.products if get_latest_s2c_version_path(identifier)}
        processable_l1c_set =  {identifier for identifier in self.l1c.products if get_latest_Sen2Cor_version_from_id(identifier)}
        
        for prod in processable_l1c_set:
            prod_list[prod] = {"date": self._products["l1c"][prod].date,
                                "cloud_cover": self._products["l1c"][prod].cloud_cover}
        return prod_list
        
    # def cloudmasks_missing(self,
                           # cm_version: str = "CM001",
                           # probability: int = 1,
                           # iterations: int = 5,
                           # cld_shad: bool = True,
                           # cld_med_prob: bool = True,
                           # cld_hi_prob: bool = True,
                           # thin_cir: bool = True,
                           # ) -> "ProductsList":
        # """Returns tile's L2A products that don't have a cloud mask as a ProductsList."""

    def cloudmasks_missing(
        self,
        cm_version: str = "CM001",
        probability: int = 1,
        iterations: int = 5,
        cld_shad: bool = True,
        cld_med_prob: bool = True,
        cld_hi_prob: bool = True,
        thin_cir: bool = True,
    ) -> "ProductsList":
        """Returns tile's L2A products that don't have a cloud mask as a
        ProductsList.

        :param cm_version: Sen2chain's CloudMask version.
        :type cm_version: str, Default to "CM001"
        :param probability: Cloud Probability threshold value.
        :type probability: int, Default to 1
        :param iterations: Number of iterations of the dilatation kernel for cloud masking.
        :type iterations: int, Default to 5
        :param cld_shad: When True, uses Cloud Shadows class from Scene Classification.
        :type cld_shad: bool, Default to True
        :param cld_med_prob: When True, uses Cloud Medium Probability class from Scene Classification.
        :type cld_med_prob: bool, Default to True
        :param cld_hi_prob: When True, uses Cloud High Probability class from Scene Classification.
        :type cld_hi_prob: bool, Default to True
        :param thin_cir: When True, uses Thin Cirrus class from Scen Classification.
        :type thin_cir: bool, Default to True
        """
        prods_list = ProductsList()
        missings_l2a_set = set(self.l2a.products) - {
            (re.findall(r"(S2.+)_CM.+.jp2", identifier)[0] + ".SAFE")
            for identifier in getattr(self.cloudmasks, cm_version)
            .params(
                probability=probability,
                iterations=iterations,
                cld_shad=cld_shad,
                cld_med_prob=cld_med_prob,
                cld_hi_prob=cld_hi_prob,
                thin_cir=thin_cir,
            )
            .products
        }
        for prod in missings_l2a_set:
            prods_list[prod] = {
                "date": self._products["l2a"][prod].date,
                "cloud_cover": self._products["l2a"][prod].cloud_cover,
            }
        return prods_list

    @property
    def info(self):
        """Returns the number of products in the library."""
        logger.info("l1c: {}".format(len(self.l1c)))
        logger.info("l2a: {}".format(len(self.l2a)))
        logger.info("cloud_masks: {}".format(len(self.cloudmasks)))
        for indice, path in self._paths["indices"].items():
            logger.info(
                "{} (raw / masked): {} / {}".format(
                    indice,
                    len(getattr(self, indice).raws),
                    len(getattr(self, indice).masks),
                )
            )

    @property
    def size(self):
        """Returns folder size of different products in the library."""
        try:
            local = getFolderSize(str(self.paths["l1c"]))
            total = getFolderSize(str(self.paths["l1c"]), True)
            logger.info(
                "l1c: {} (local: {} / archived: {})".format(
                    human_size(total),
                    human_size(local),
                    human_size(total - local),
                )
            )
        except Exception:
            pass
        try:
            local = getFolderSize(str(self.paths["l2a"]))
            total = getFolderSize(str(self.paths["l2a"]), True)
            logger.info(
                "l2a: {} (local: {} / archived: {})".format(
                    human_size(total),
                    human_size(local),
                    human_size(total - local),
                )
            )
        except Exception:
            pass
        try:
            local = getFolderSize(str(self.paths["cloudmasks"]))
            total = getFolderSize(str(self.paths["cloudmasks"]), True)
            logger.info(
                "cloudmasks: {} (local: {} / archived: {})".format(
                    human_size(total),
                    human_size(local),
                    human_size(total - local),
                )
            )
        except Exception:
            pass
        for indice, path in self._paths["indices"].items():
            logger.info(
                "{}: {}".format(
                    indice, human_size(getFolderSize(str(path), True))
                )
            )

    def missing_indices(
        self,
        indice: str,
        nodata_clouds: bool = False,
        cm_version: list = "CM001",
        probability: int = 1,
        iterations: int = 5,
        cld_shad: bool = True,
        cld_med_prob: bool = True,
        cld_hi_prob: bool = True,
        thin_cir: bool = True,
    ) -> "ProductsList":
        """
        Returns tile's L2A products that don't have indices as a ProductsList, for a specific cloud mask.

        :param indice: Radiometric indice to scan for in your library.
        :type indice: str.
        :param nodata_clouds: When True, checks also in produced cloudmasks products.
        :type nodata_clouds: bool, Default to False
        :param cm_version: Sen2chain's CloudMask version.
        :type cm_version: str, Default to "CM001"
        :param probability: Cloud Probability threshold value.
        :type probability: int, Default to 1
        :param iterations: Number of iterations of the dilatation kernel for cloud masking.
        :type iterations: int, Default to 5
        :param cld_shad: When True, uses Cloud Shadows class from Scene Classification.
        :type cld_shad: bool, Default to True
        :param cld_med_prob: When True, uses Cloud Medium Probability class from Scene Classification.
        :type cld_med_prob: bool, Default to True
        :param cld_hi_prob: When True, uses Cloud High Probability class from Scene Classification.
        :type cld_hi_prob: bool, Default to True
        :param thin_cir: When True, uses Thin Cirrus class from Scene Classification.
        :type thin_cir: bool, Default to True
        """
        prodlist = ProductsList()
        try:
            if not nodata_clouds:
                missings_indice_set = set(self.l2a.products) - {
                    re.sub("_" + indice.upper() + ".+jp2", ".SAFE", identifier)
                    for identifier in getattr(
                        getattr(self, indice.lower()), "raws"
                    ).products
                }
            else:
                missings_indice_set = set(self.l2a.products) - {
                    re.sub(
                        "_" + indice.upper() + "_CM.+jp2", ".SAFE", identifier
                    )
                    for identifier in getattr(
                        getattr(getattr(self, indice.lower()), "masks"),
                        cm_version,
                    )
                    .params(
                        probability = probability, 
                        iterations = iterations,
                        cld_shad = cld_shad,
                        cld_med_prob = cld_med_prob,
                        cld_hi_prob = cld_hi_prob,
                        thin_cir = thin_cir,
                    )
                    .products
                }
        except Exception:
            logger.info("Problem finding missing indices setting all L2A list")
            missings_indice_set = set(self.l2a.products)

        for prod in missings_indice_set:
            prodlist[prod] = {
                "date": self._products["l2a"][prod].date,
                "cloud_cover": self._products["l2a"][prod].cloud_cover,
            }
        return prodlist

    def get_l1c(
        self,
        provider: str = "peps",
        providers: list = [], # to do, may be one day downloading for multiple providers at the same time...
        download: bool = True,
        dl_mode: str = "multit",
        start: str = "2015-01-01",
        end: str = "9999-12-31",
        new: bool = False,
        ref = "l1c",
        min_cloudcover: int = 0,
        max_cloudcover: int = 100,
        order_offline: bool = False,
        tries: int = 1,
        sleep: int = 0,
        min_pb: str = 0,
        max_pb: str = 98,

        
    ):
        """
        Function to search and/or download L1C products using EODAG tool. Only products missing from library are searched and downloaded.

        :param provider: EO products provider.
        :type provider: str, Default to peps
        :param download: Download condition. When False only search and returns L1C products available.
        :type download: bool, Default to True
        :param dl_mode: Download mode among sequential, multiprocessing or multithreading.
        :type dl_mode: str, Default to "multit" for multithreading.
        :param start: Period of Interest first date.
        :type start: str, Default to "2015-01-01"
        :param end: Period of Interest last date.
        :type end: str, Default to "9999-12-31"
        :param new: When True, Download or searches for products published since latest L1C inyour library.
        :type new: str, Default to False
        :param ref: Product reference name for EODAG search function.
        :type ref: str, Default to "l1c".
        :param min_cloudcover: Cloudcover minimum threshold value to filter searched products.
        :type min_cloudcover: int, Default to 0
        :param max_cloudcover: Cloudcover maximmum threshold value to filter searched products.
        :type max_cloudcover: int, Default to 0
        :param order_offline: When True, if product is offline, order it and skips to the next product in list. Another action will be needed to downlod the ordered product when it is online.
        :type order_offline: bool, Default to False
        :param tries: Number of search and download loops on available products.
        :type tries: int, Default to 1
        :param sleep: Waiting time (minutes) between loops.
        :type sleep: int, Default to 0
        :param min_pb: Min processing baseline to download
        :type min_pb: int, Default to "0511"
        :param max_pb: Max processing baseline to download
        :type max_pb: int, Default to "9999"        

        """
        if provider == "cop_dataspace":
            NUM_THREADS = 4
        elif provider == "peps":
            NUM_THREADS = 8
        else:
            NUM_THREADS = 1
        if new:
            if self.l1c:
                start = datetime_to_str(self.l1c.last.date + timedelta(days=1), date_format = 'ymd')
        
        dag = S2cEodag(self.name, provider = provider)
        
        # dag.search(start = start, end = end, ref = ref, min_cloudcover = min_cloudcover, max_cloudcover = max_cloudcover)
        
        if download:
            before_list_total = []
            after_list_total = []
            
            for essai in range(tries):
                logger.info("Download try #{}/{}".format(essai+1, tries))
                
                dag.search(
                    start = start, 
                    end = end, 
                    ref = ref, 
                    min_cloudcover = min_cloudcover, 
                    max_cloudcover = max_cloudcover,
                    min_pb = min_pb,
                    max_pb = max_pb,
                )

                before_list = [p.location for p in dag.products]
                # logger.info("before_list {}".format(before_list))
                before_list_total.extend(before_list)

                ## sequential
                if dl_mode == "seq":
                    for p in dag.products:
                        dag.download(p)
                        
                ## multithreading
                elif dl_mode == "multit":
                    q = Queue()
                    def do_stuff(q, dag):
                      while True:
                        try:
                          dag.download(q.get())
                        except Exception:
                          import traceback
                          logger.error(traceback.format_exc())
                          #TODO should cleanup the tile
                        finally:
                          q.task_done()
                    for p in dag.products:
                        q.put(p)
                    for t in range(NUM_THREADS):
                        worker = Thread(target=do_stuff, args = (q, dag))
                        worker.daemon = True
                        worker.start()
                    q.join()
                
                ## multiprocessing
                elif dl_mode == "multip":
                    nb_proc = NUM_THREADS
                    nb_proc = max(min(len(os.sched_getaffinity(0)) - 1, nb_proc), 1)
                    pool = multiprocessing.Pool(nb_proc)
                    results = [pool.map(dag.download, dag.products)]
                    pool.close()
                    pool.join()
                
                after_list = [p.location for p in dag.products]
                # logger.info("after_list {}".format(after_list))

                after_list_total.extend(after_list)
                changed_list =[
                    Path(after_list[i]).stem for i in range(
                        len(before_list)
                    ) if before_list[i] != after_list[i]
                ]
                # logger.info("changed_list {}".format(changed_list))
                if len(before_list):
                    if len(changed_list) == len(before_list):
                        logger.info("Downloaded all {} product(s)".format(len(before_list)))
                        break
                    else:
                        logger.info("Downloaded {}/{} product(s)".format(len(changed_list), len(before_list)))
                else:
                    logger.info("No product to download")
                        
                if (tries > 1) and (essai < (tries-1)):
                    logger.info("Sleeping {}min before retrying".format(sleep))
                    time.sleep(sleep*60)
            
            return list(
                set(
                    [
                        Path(
                            after_list_total[i]
                        ).stem for i in range(
                            len(before_list_total)
                        ) if before_list_total[i] != after_list_total[i]
                    ]
                )
            )
        elif order_offline:
            dag.search(
                start = start, 
                end = end, 
                ref = ref, 
                min_cloudcover = min_cloudcover, 
                max_cloudcover = max_cloudcover,
                min_pb = min_pb,
                max_pb = max_pb,
            )
            NUM_THREADS = 8
            q = Queue()
            def do_stuff(q, dag):
              while True:
                dag.order_offline(q.get())
                q.task_done()
            for p in dag.products:
                q.put(p)
            for t in range(NUM_THREADS):
                worker = Thread(target=do_stuff, args = (q, dag))
                worker.daemon = True
                worker.start()
            q.join()
        else:
            dag.search(
                start = start, 
                end = end, 
                ref = ref,
                min_cloudcover = min_cloudcover,
                max_cloudcover = max_cloudcover,
                min_pb = min_pb,
                max_pb = max_pb,
            )
            
        return dag.products
        
    def get_l2a(
        self,
        provider: str = "cop_dataspace",
        download: bool = True,
        dl_mode: str = "multit",
        start: str = "2024-07-01",
        end: str = "9999-12-31",
        new: bool = False,
        ref = "l2a",
        min_cloudcover: int = 0,
        max_cloudcover: int = 100,
        order_offline: bool = False,
        tries: int = 1,
        sleep: int = 0,
        min_pb: str = 5.11,
        max_pb: str = 98,
    ):
        """
        Function to search and/or download L2A products using EODAG tool. Only products missing from library are searched and downloaded.

        :param provider: EO products provider.
        :type provider: str, Default to peps
        :param download: Download condition. When False only search and returns L1C products available.
        :type download: bool, Default to True
        :param dl_mode: Download mode among sequential, multiprocessing or multithreading.
        :type dl_mode: str, Default to "multit" for multithreading.
        :param start: Period of Interest first date.
        :type start: str, Default to "2015-01-01"
        :param end: Period of Interest last date.
        :type end: str, Default to "9999-12-31"
        :param new: When True, Download or searches for products published since latest L1C inyour library.
        :type new: str, Default to False
        :param ref: Product reference name for EODAG search function.
        :type ref: str, Default to "l1c".
        :param min_cloudcover: Cloudcover minimum threshold value to filter searched products.
        :type min_cloudcover: int, Default to 0
        :param max_cloudcover: Cloudcover maximmum threshold value to filter searched products.
        :type max_cloudcover: int, Default to 0
        :param order_offline: When True, if product is offline, order it and skips to the next product in list. Another action will be needed to downlod the ordered product when it is online.
        :type order_offline: bool, Default to False
        :param tries: Number of search and download loops on available products.
        :type tries: int, Default to 1
        :param sleep: Waiting time (minutes) between loops.
        :type sleep: int, Default to 0
        :param min_pb: Min processing baseline to download
        :type min_pb: int, Default to "0511"
        :param max_pb: Max processing baseline to download
        :type max_pb: int, Default to "9999"        
        """
        
        if provider == "cop_dataspace":
            NUM_THREADS = 4
        elif provider == "peps":
            NUM_THREADS = 8
        else:
            NUM_THREADS = 1
        if new:
            if self.l2a:
                start = datetime_to_str(self.l2a.last.date + timedelta(days=1), date_format = 'ymd')
        dag = S2cEodag(self.name, provider = provider)
       
        if download:
            before_list_total = []
            after_list_total = []
            
            for essai in range(tries):
                logger.info("Download try #{}/{}".format(essai+1, tries))
                
                dag.search(
                    productType = "L2A",
                    start = start, 
                    end = end, 
                    ref = ref, 
                    min_cloudcover = min_cloudcover, 
                    max_cloudcover = max_cloudcover,
                    min_pb = min_pb,
                    max_pb = max_pb,
                )

                before_list = [p.location for p in dag.products]
                # logger.info("before_list {}".format(before_list))
                before_list_total.extend(before_list)

                ## sequential
                if dl_mode == "seq":
                    for p in dag.products:
                        dag.download(p)
                        
                ## multithreading
                elif dl_mode == "multit":
                    q = Queue()
                    def do_stuff(q, dag):
                      while True:
                        try:
                          dag.download(q.get())
                        except Exception:
                          import traceback
                          logger.error(traceback.format_exc())
                          #TODO should cleanup the tile
                        finally:
                          q.task_done()
                    for p in dag.products:
                        q.put(p)
                    for t in range(NUM_THREADS):
                        worker = Thread(target=do_stuff, args = (q, dag))
                        worker.daemon = True
                        worker.start()
                    q.join()
                
                ## multiprocessing
                elif dl_mode == "multip":
                    nb_proc = NUM_THREADS
                    nb_proc = max(min(len(os.sched_getaffinity(0)) - 1, nb_proc), 1)
                    pool = multiprocessing.Pool(nb_proc)
                    results = [pool.map(dag.download, dag.products)]
                    pool.close()
                    pool.join()
                
                after_list = [p.location for p in dag.products]
                # logger.info("after_list {}".format(after_list))

                after_list_total.extend(after_list)
                changed_list =[
                    Path(after_list[i]).stem for i in range(
                        len(before_list)
                    ) if before_list[i] != after_list[i]
                ]
                # logger.info("changed_list {}".format(changed_list))
                if len(before_list):
                    if len(changed_list) == len(before_list):
                        logger.info("Downloaded all {} product(s)".format(len(before_list)))
                        break
                    else:
                        logger.info("Downloaded {}/{} product(s)".format(len(changed_list), len(before_list)))
                else:
                    logger.info("No product to download")
                        
                if (tries > 1) and (essai < (tries-1)):
                    logger.info("Sleeping {}min before retrying".format(sleep))
                    time.sleep(sleep*60)
            
            return list(
                set(
                    [
                        Path(
                            after_list_total[i]
                        ).stem for i in range(
                            len(before_list_total)
                        ) if before_list_total[i] != after_list_total[i]
                    ]
                )
            )
        
        elif order_offline:
            dag.search(
                productType = "L2A",
                start = start, 
                end = end, 
                ref = ref, 
                min_cloudcover = min_cloudcover, 
                max_cloudcover = max_cloudcover,
                min_pb = min_pb,
                max_pb = max_pb,
            )
            NUM_THREADS = 8
            q = Queue()
            def do_stuff(q, dag):
              while True:
                dag.order_offline(q.get())
                q.task_done()
            for p in dag.products:
                q.put(p)
            for t in range(NUM_THREADS):
                worker = Thread(target=do_stuff, args = (q, dag))
                worker.daemon = True
                worker.start()
            q.join()
        else:
            dag.search(
                start = start,
                end = end,
                ref = ref, 
                min_cloudcover = min_cloudcover,
                max_cloudcover = max_cloudcover,
                productType = "L2A",
                min_pb = min_pb,
                max_pb = max_pb,
            )
        return dag.products

    def compute_l2a(
        self,
        reprocess: bool = False,
        p_60m_missing: bool = False,
        date_min: str = None,
        date_max: str = None,
        cover_min: int = 0,
        cover_max: int = 100,        
        nb_proc: int = 4,
        copy_l2a_sideproducts: bool = False,
        compute: bool = True,
        remove_used_l1c: bool = False,
    ):
        """
        Compute all missing L2A for L1C products between date_min and date_max. If no date specified, compute all L1C products available.

        :param reprocess: When True, reprocess L2A products in library.
        :type reprocess: bool, Default to False
        :param p_60m_missing: When True, in case of reprocessing, checks if 60m band is missing in L2A products, remove these products and reprocess them.
        :type p_60m_missing: bool, Default to False
        :param date_min: First date of L1C products to compute.
        :type date_min: str, Default to None
        :param date_max: Last date of L1C products to compute.
        :type date_max: str, Default to None
        :param cover_min: Cloud cover threshold minimum value to compute L1C from.
        :type cover_min: int, Default to 0
        :param cover_max: Cloud cover threshold maximum value to compute L1C from.
        :type cover_max: int, Default to 100
        :param nb_proc: Number of processors to enable for computing L1C products via multiprocessing. Check your computer specification to optimize your configuration.
        :type nb_proc: int, Default to 4
        :param copy_l2a_sideproducts: When True, copy Sen2cor outputs Cloud Probability and Scene Classification to Cloud Mask folder. From these outputs, cloud maks can be generated without L2A in library.
        :type copy_l2a_sideproducts: bool, Default to False
        :param compute: When False, returns L1C products without L2A products associated.
        :type compute: bool, Default to True
        :param remove_used_l1c: Remove L1C once L2A product is processed.
        :type remove_used_l1c: bool, Default to False


        """
        if reprocess:
            if p_60m_missing:
                l2a_remove_list = [
                    product.identifier
                    for product in self.l2a.filter_dates(
                        date_min=date_min, date_max=date_max
                    ).filter_clouds(
                        cover_min = cover_min, 
                        cover_max = cover_max
                    )
                    if not L2aProduct(product.identifier).b01_60m
                ]
            else:
                l2a_remove_list = [
                    product.identifier
                    for product in self.l2a.filter_dates(
                        date_min=date_min, date_max=date_max
                    ).filter_clouds(
                        cover_min = cover_min, 
                        cover_max = cover_max
                    )
                ]
            if l2a_remove_list:
                self.remove_l2a(l2a_remove_list)
        l1c_process_list = []
        temp_list = list(
                [
                    p.identifier,
                    copy_l2a_sideproducts,
                    remove_used_l1c,
                ]
                for p in self.l2a_missings.filter_dates(
                    date_min=date_min, 
                    date_max=date_max,
                ).filter_clouds(
                    cover_min = cover_min, 
                    cover_max = cover_max
                )
            )
        l1c_process_list.extend(temp_list)
        # l1c_process_list = list(chain.from_iterable(l1c_process_list))
        if l1c_process_list:
            logger.info(
                "{} l1c products to process:".format(len(l1c_process_list))
            )
            logger.info("{}".format(l1c_process_list))
        else:
            logger.info("All l2a products already computed")
        l2a_res = False
        if l1c_process_list and compute:
            l2a_res = l2a_multiprocessing(l1c_process_list, nb_proc=nb_proc)
            
    def compute_cloudmasks(
        self,
        cm_version: str = "CM001",
        probability: int = 1,
        iterations: int = 5,
        cld_shad: bool = True,
        cld_med_prob: bool = True,
        cld_hi_prob: bool = True,
        thin_cir: bool = True,
        reprocess: bool = False,
        date_min: str = None,
        date_max: str = None,
        nb_proc: int = 4,
    ):
        """Compute all (missing) cloud masks for L2A products. Only 1 Cloud mask version per command line.

        :param cm_version: Cloudmask version to compute. Can be either CM001, CM002, CM003, or CM004.
        :type cm_version: str, Default to "CM001"
        :param probability: Threshold of probability of clouds to be considered. Specific to CM003 & CM004.
        :type probability: int, Default to 1
        :param iterations: Number of iterations for the kernel dilatation process. Specific to CM003 & CM004.
        :param cld_shad: When True, uses Cloud Shadows class from Scene Classification.
        :type cld_shad: bool, Default to True
        :param cld_med_prob: When True, uses Cloud Medium Probability class from Scene Classification.
        :type cld_med_prob: bool, Default to True
        :param cld_hi_prob: When True, uses Cloud High Probability class from Scene Classification.
        :type cld_hi_prob: bool, Default to True
        :param thin_cir: When True, uses Thin Cirrus class from Scen Classification.
        :type thin_cir: bool, Default to True
        :param reprocess: When True, already processed cloudmask will be computed again.
        :type reprocess: bool, Default to False
        :param date_min: Products before this date wont be processed.
        :type date_min: str, Default to None
        :param date_max: Products after this date wont be processed.
        :type date_max: str, Default to None
        :param nb_proc: Number of processors to mobilize for computing. Limited to the number of proc of your computer.
        :type nb_proc: int, Default to 4
        """
        
        cm_version = cm_version.upper()
        if not reprocess:
            cld_l2a_process_list = list(
                [
                    p.identifier,
                    cm_version,
                    probability,
                    iterations,
                    cld_shad,
                    cld_med_prob,
                    cld_hi_prob,
                    thin_cir,
                    reprocess,
                ]
                for p in self.cloudmasks_missing(
                    cm_version=cm_version,
                    probability=probability,
                    iterations=iterations,
                    cld_shad=cld_shad,
                    cld_med_prob=cld_med_prob,
                    cld_hi_prob=cld_hi_prob,
                    thin_cir=thin_cir,
                ).filter_dates(date_min=date_min, date_max=date_max)
            )
        else:
            cld_l2a_process_list = list(
                [
                    p.identifier,
                    cm_version,
                    probability,
                    iterations,
                    cld_shad,
                    cld_med_prob,
                    cld_hi_prob,
                    thin_cir,
                    reprocess,
                ]
                for p in self.l2a.filter_dates(
                    date_min=date_min, date_max=date_max
                )
            )
        if cld_l2a_process_list:
            logger.info(
                "{} l2a product(s) to process".format(len(cld_l2a_process_list))
            )
            # logger.info("{}".format(cld_l2a_process_list))
            cld_version_probability_iterations_reprocessing_multiprocessing(
                cld_l2a_process_list, nb_proc=nb_proc
            )
        else:
            logger.info("All cloud masks already computed")
            # return False

    def compute_indices(
        self,
        indices: list = [],
        reprocess: bool = False,
        nodata_clouds: bool = True,
        quicklook: bool = False,
        cm_version: list = "CM004",
        probability: int = 1,
        iterations: int = 5,
        cld_shad: bool = True,
        cld_med_prob: bool = True,
        cld_hi_prob: bool = True,
        thin_cir: bool = True,
        date_min: str = None,
        date_max: str = None,
        nb_proc: int = 4,
    ):
        """
        Compute all missing radiometric indices for L2A products
        - Indices are given as a list
        - If indices are not provided, will compute missing dates of already existing indices for this tile (no new indice computed)
        - Indices won't be masked if no cloud masks are present, you have to compute cloudmasks first.
        - To mask indices, specify cm_version and specific cloud masks parameters.

        :param indices: List of indices to process L2A products.
        :type indices: list, Default to []
        :param reprocess: When True, reprocess indices already computed that meet the other parameters.
        :type reprocess: bool, Default to False
        :param nodata_clouds: When True, checks also in produced cloudmasks products.
        :type nodata_clouds: bool, Default to False
        :param quicklook: When True, compute quicklook of indice in JPG.
        :type quicklook: bool, Default to False
        :param cm_version: Cloudmask version to mask indice with. Can be either CM001, CM002, CM003 or CM004.
        :type cm_version: str, Default to "CM001"
        :param probability: Cloud mask probability threshold parameter specific to CM003 & CM004.
        :type probability: int, Default to 1
        :param iterations: Cloud mask CM003 and CM004 parameter; number of iterations for the kernel dilatation process.
        :type iterations: int, Default to 5
        :param cld_shad: Cloud mask CM003 and CM004 parameter; Cloud Shadows class from Scene Classification.
        :type cld_shad: bool, Default to True
        :param cld_med_prob: Cloud mask CM003 and CM004 parameter; Cloud Medium Probability class from Scene Classification.
        :type cld_med_prob: bool, Default to True
        :param cld_hi_prob: Cloud mask CM003 and CM004 parameter; Cloud High Probability class from Scene Classification.
        :type cld_hi_prob: bool, Default to True
        :param thin_cir: Cloud mask CM003 and CM004 parameter; Thin Cirrus class from Scene Classification.
        :type thin_cir: bool, Default to True
        :param reprocess: When True, already processed indices will be computed again.
        :type reprocess: bool, Default to False
        :param date_min: Products before this date wont be processed.
        :type date_min: str, Default to None
        :param date_max: Products after this date wont be processed.
        :type date_max: str, Default to None
        :param nb_proc: Number of processors to mobilize for computing. Limited to the number of processors of your computer.
        :type nb_proc: int, Default to 4
        """
        
        cm_version = cm_version.upper()
        if not indices:
            indices = list(self._paths["indices"].keys())
        # else:
        indices = [indice.upper() for indice in indices]
        indices_l2a_process_list = []
        for i in indices:
            if not reprocess:
                l2a_list = [
                    p.identifier
                    for p in self.missing_indices(
                        i,
                        nodata_clouds=nodata_clouds,
                        cm_version=cm_version,
                        probability=probability,
                        iterations=iterations,
                        cld_shad=cld_shad,
                        cld_med_prob=cld_med_prob,
                        cld_hi_prob=cld_hi_prob,
                        thin_cir=thin_cir,
                    ).filter_dates(date_min=date_min, date_max=date_max)
                ]
            else:
                l2a_list = [
                    p.identifier
                    for p in self.l2a.filter_dates(
                        date_min=date_min, date_max=date_max
                    )
                ]

            for j in l2a_list:
                indices_l2a_process_list.append(
                    [
                        j,
                        i,
                        reprocess,
                        nodata_clouds,
                        quicklook,
                        cm_version,
                        probability,
                        iterations,
                        cld_shad,
                        cld_med_prob,
                        cld_hi_prob,
                        thin_cir,
                    ]
                )
        if indices_l2a_process_list:
            logger.info(
                "{} indice products to process:".format(
                    len(indices_l2a_process_list)
                )
            )
            logger.info("{}".format(indices_l2a_process_list))
            indices_res = idx_multiprocessing(
                indices_l2a_process_list, nb_proc=nb_proc
            )
        else:
            logger.info("All indices already computed")

    def clean_lib(self, 
        remove_indice_tif: bool = False, 
        remove: bool = False,
        deep_search: bool = False,
        tempo_depth: int = 99999,   ##### to implement to limit deep search to last xx days
    ):
        """
        Search and clean corrupted files from processing error in Tile library.

        Normal clean scans for :
        - Unmoved L2A products in L1C library,
        - Corrupted L2A products in L2A library.

        Deep search scans for :
        - Corrupted L1C products in L1C library,
        - Corrupted Cloud masks (0kb).

        :param remove_indice_tif: If True will remove TIFF files present in indice folders.
        :type remove_indice_tif: bool, Default to False
        :param remove: When True will effectively remove corrupted files. When False, list identified problems.
        :type remove: bool, Default to False
        :param deep_search: When True, performs deep search in L1C, L2A and cloudmask library.
        :type deep_search: bool, Default to False
        :param tempo_depth: Temporal depth of clean_lib function. In days. To be developed later.
        :type tempo_depth: int, Default to None
        
        """
        # logger.info("Cleaning {} library".format(self.name))
        
        nb_id = 0
        nb_rm = 0
        prob_id = []
        
        # identify residual l2a from l1c folder
        for f in chain(
            self._paths["l1c"].glob("*L2A*.SAFE"),
            self._paths["l1c"].glob("*L2A*.tmp"),
        ):
            txt = "Identified {} in L1C folder".format(f.name)
            prob_id.append(txt)
            logger.info(txt)
            nb_id += 1
            if remove:
                try:
                    shutil.rmtree(str(f))
                    logger.info("Removing {} from L1C folder".format(f.name))
                    nb_rm += 1
                except Exception:
                    logger.error(
                        "Can't remove {} from L1C folder".format(f.name)
                    )
        
        # research residual zipfiles in l1c folder
        for f in chain(
            self._paths["l1c"].glob("*L1C*.zip"),
            # self._paths["l1c"].glob("*L2A*.tmp"),
        ):
            txt = "Identified remaining {} in L1C folder".format(f.name)
            prob_id.append(txt)
            logger.info(txt)
            nb_id += 1
            if remove:
                # try:
                f.unlink()
                logger.info("Removing {} from L1C folder".format(f.name))
                nb_rm += 1
                # except Exception:
                    # logger.error(
                        # "Can't remove {} from L1C folder".format(f.name)
                    # )
        
        # identify corrupted zip extractions from l1c folder
        for f in chain(
            self._paths["l1c"].glob("*L1C*"),
            # self._paths["l1c"].glob("*L2A*.tmp"),
        ):
            if not f.suffix:
                txt = "Identified {} corrupted zip extraction in L1C folder".format(f.name)
                prob_id.append(txt)
                logger.info(txt)
                nb_id += 1
                if remove:
                    if f.is_dir():
                        try:
                            shutil.rmtree(str(f))
                            logger.info("Removing {} folder from L1C folder".format(f.name))
                            nb_rm += 1
                        except Exception:
                            logger.error(
                                "Can't remove {} from L1C folder".format(f.name)
                            )
                    else:
                        f.unlink()
                        logger.info("Removing {} file from L1C folder".format(f.name))
                        nb_rm += 1
        
        # identify corrupted zip extractions from l2a folder
        for f in chain(
            self._paths["l2a"].glob("*L2A*"),
            # self._paths["l1c"].glob("*L2A*.tmp"),
        ):
            if not f.suffix:
                txt = "Identified {} corrupted zip extraction in L2A folder".format(f.name)
                prob_id.append(txt)
                logger.info(txt)
                nb_id += 1
                if remove:
                    if f.is_dir():
                        try:
                            shutil.rmtree(str(f))
                            logger.info("Removing {} folder from L2A folder".format(f.name))
                            nb_rm += 1
                        except Exception:
                            logger.error(
                                "Can't remove {} from L2A folder".format(f.name)
                            )
                    else:
                        f.unlink()
                        logger.info("Removing {} file from L2A folder".format(f.name))
                        nb_rm += 1
                
        # identify missing jp2 in L2A folder
        for f in self._paths["l2a"].glob("*L2A*.SAFE"):
            # Nb jp2 < 7
            if len(list(f.glob("GRANULE/*/IMG_DATA/R10m/*.jp2"))) < 7:
                txt = "Corrupted L2A {} in L2A folder (less than 7 jp2 files in R10m folder)".format(
                    f.name
                )
                prob_id.append(txt)
                logger.info(txt)
                nb_id += 1
                if remove:
                    try:
                        shutil.rmtree(str(f))
                        logger.info(
                            "Removing corrupted L2A {} from L2A folder".format(
                                f.name
                            )
                        )
                        nb_rm += 1
                    except Exception:
                        logger.error(
                            "Can't remove {} from L2A folder".format(f.name)
                        )
        if deep_search:
            # identify corrupted jp2 in l1c folder
            erase_set = set()
            for p in [p for p in self.l1c if p.date >= datetime.now() - timedelta(tempo_depth)]:
                l1c = L1cProduct(p.identifier)
                for f in l1c.path.glob("GRANULE/*/IMG_DATA/*.jp2"):
                # for f in self._paths["l1c"].glob("*/GRANULE/*/IMG_DATA/*.jp2"):
                    if f.stat().st_size == 0:
                        txt = "Identified 0b corrupted {} in L1C folder".format(f.name)
                        prob_id.append(txt)
                        logger.info(txt)
                        nb_id += 1
                        if remove:
                            erase_set.update({f.parent.parent.parent.parent})
                for e in erase_set:
                    try:
                        logger.info("Removing {} from L1C folder".format(e))
                        shutil.rmtree(str(e))
                        nb_rm += 1
                    except Exception:
                        logger.error("Can't remove {} from L1C folder".format(e))
            
            # identify 0B cloud masks
            for p in [p for p in self.cloudmasks if p.date >= datetime.now() - timedelta(tempo_depth)]:
                cm = NewCloudMaskProduct(p.identifier)
                f = cm.path
                # for f in self._paths["cloudmasks"].glob("*/*CM*.jp2"):
                if f.stat().st_size == 0:
                    txt = "Corrupted cloud mask {} in L2A folder".format(f.name)
                    prob_id.append(txt)
                    logger.info(txt)
                    nb_id += 1
                    if remove:
                        logger.info(
                            "Removing corrupted cloud mask {} from L2A folder".format(
                                f.name
                            )
                        )
                        f.unlink()
                        nb_rm += 1
            # identify wrong size l2a_QL
            for f in self._paths["l2a"].glob("QL/*_QL.tif"):
                if f.stat().st_size != 3617212:
                    txt = "Corrupted L2A QL {} in L2A QL folder".format(f.name)
                    prob_id.append(txt)
                    logger.info(txt)
                    nb_id += 1
                    if remove:
                        logger.info(
                            "Removing corrupted QL {} from L2A folder".format(
                                f.name
                            )
                        )
                        f.unlink()
                        nb_rm += 1
            
            # check jp2 validity 
            for key, sub_path in self._paths["indices"].items():
                sub_path = Path(sub_path)  # Convertit le chemin en objet Path
                print(f"Exploration du dossier {key}: {sub_path}")
                for jp2_file in sub_path.rglob("*.jp2"):
                    #print(f"  - Fichier : {jp2_file}")
                    test_validity = jpylyzer.checkOneFile(jp2_file).findtext('isValid')
                    if test_validity == 'False':
                        txt = "Invalid jp2 {} (jpylyzer)".format(jp2_file.name)
                        prob_id.append(txt)
                        logger.info(txt)
                        nb_id += 1
                        if remove:
                            logger.info(
                                "Removing invalid jp2 {} from L2A folder".format(
                                    jp2_file.name
                                )
                            )
                            jp2_file.unlink()
                            nb_rm += 1
                        
        # identify 0B or absent indice QL
        """
        for f in self._paths["indices"]:
            indices = set(p.stem for p in list(self._paths["indices"][f].glob("*_MSIL2A_*/")))
            
            for fp in set(p.stem for p in list(self._paths["indices"][f].glob("*_MSIL2A_*/")) if FamilyProduct(p.stem).date >= datetime.datetime.now() - datetime.timedelta(tempo_depth)):
                
            for p in [p.family_id for p self._paths["indices"][f].glob("*_MSIL2A_*/") if ]
            
            
            for indice in [p for p in getattr(self, f) if p.date >= datetime.datetime.now() - datetime.timedelta(tempo_depth)]:
                ind = IndiceProduct(indi.identifier)
                p = ind.path
            # for p in self._paths["indices"][f].glob("*_MSIL2A_*/"):
                # logger.info(p)
                if p.is_file():
                    txt = "Identified old indice format {}".format(p.name)
                    prob_id.append(txt)
                    logger.info(txt)
                    nb_id += 1
                    if remove:
                        logger.info(
                            "Removing old indice format {}".format(p.name)
                        )
                        p.unlink()
                        nb_rm += 1
                else:
                    # for q in p.glob("*_QUICKLOOK.tif"):
                    #     if not ((q.stat().st_size == 3617212) or
                    #             (q.stat().st_size == 4196652) or
                    #             (q.stat().st_size == 3617478)):
                    #         logger.info("Corrupted indice QL {} (bad size)".format(q.name))
                    #         if remove:
                    #             logger.info("Removing indice QL {}".format(q.name))
                    #             q.unlink()
                    if deep_search:
                        for q in list(p.glob("*.jp2")) + list(p.glob("*.tif")):
                            # logger.info(q)
                            try:
                                Image.MAX_IMAGE_PIXELS = 120560400
                                img = Image.open(str(q))  # open the image file
                                img.verify()  # verify that it is, in fact an image
                            except (IOError, SyntaxError) as e:
                                txt = "Bad file (PIL): {}".format(
                                    str(q.name)
                                )  # print out the names of corrupt files
                                prob_id.append(txt)
                                logger.info(txt)
                                nb_id += 1
                                if remove:
                                    logger.info(
                                        "Removing indice QL {}".format(q.name)
                                    )
                                    q.unlink()
                                    nb_rm += 1
                    for q in list(p.glob("*.jp2")):
                        if not (Path(str(q) + ".aux.xml")).exists():
                            txt = "Missing metadata: {}".format(q.name)
                            prob_id.append(txt)
                            logger.info(txt)
                            nb_id += 1
                            if remove:
                                logger.info("Removing jp2 {}".format(q.name))
                                q.unlink()
                                nb_rm += 1
                    if deep_search:
                        for q in p.glob("*.*"):
                            if q.stat().st_size == 0:
                                txt = "Corrupted file {} (0B size)".format(q.name)
                                prob_id.append(txt)
                                logger.info(txt)
                                nb_id += 1
                                if remove:
                                    logger.info(
                                        "Removing indice QL {}".format(q.name)
                                    )
                                    q.unlink()
                                    nb_rm += 1
                    if remove_indice_tif:
                        for q in p.glob("*" + f.upper() + ".tif"):
                            txt = "Identified indice in tif format {}".format(
                                q.name
                            )
                            prob_id.append(txt)
                            logger.info(txt)
                            nb_id += 1
                            if remove:
                                logger.info(
                                    "Removing indice QL {}".format(q.name)
                                )
                                q.unlink()
                                nb_rm += 1
        """
        
        
        return {
            "identified_problems": nb_id,
            "removed_problems": nb_rm,
            "problems": prob_id,
        }

    def archive_l1c(
        self,
        size_only: bool = False,
        force: bool = False,
    ):
        """
        Move L1C products to L1C archive folder.

        :param size_only: When True, only returns total size of L1C products that can be archived.
        :type size_only: bool, Default to False
        :param force: Archive all L1C products.
        :type force: bool, Default to False

        """

        l1c_archive_path = Path(Config().get("l1c_archive_path"))

        if force:
            prod_list = self.l1c
        else:
            prod_list = ProductsList()
            archive_l1c_set = {
                a
                for a in {
                    identifier.replace("L2A_", "L1C_").replace(
                        "_USER_", "__OPER__"
                    )
                    for identifier in self.l2a.products
                }
                if a in set(self.l1c.products)
            }

            for prod in archive_l1c_set:
                prod_list[prod] = {
                    "date": self._products["l1c"][prod].date,
                    "cloud_cover": self._products["l1c"][prod].cloud_cover,
                }
        count = 0
        total_size = 0
        if prod_list:
            for prod in prod_list:
                l1c = L1cProduct(prod.identifier)
                if not l1c.path.is_symlink():
                    count += 1
                    total_size += getFolderSize(str(l1c.path))
                    if not size_only:
                        move_path = l1c_archive_path / l1c.tile / l1c.path.name
                        logger.info("archiving {}".format(l1c.identifier))
                        move_path.parent.mkdir(exist_ok=True)
                        # shutil.move(str(l1c.path), str(move_path.parent))
                        setuptools.distutils.dir_util.copy_tree(
                            str(l1c.path), 
                            str(move_path),
                        )
                        setuptools.distutils.dir_util.remove_tree(str(l1c.path))
                        l1c.path.symlink_to(
                            move_path, target_is_directory=True
                        )
            if size_only:
                logger.info(
                    "{} l1c product(s) to archive ({})".format(
                        count, human_size(total_size)
                    )
                )
            else:
                logger.info(
                    "{} l1c product(s) archived ({})".format(
                        count, human_size(total_size)
                    )
                )
            return total_size
        if not count:
            logger.info("No L1C products to archive")
            return 0

    def archive_l2a(
        self,
        size_only: bool = False,
    ):
        """
        Move L2A products to L2A archive folder.

        :param size_only: When True, only returns total size of L2A products that can be archived.
        :type size_only: bool, Default to False

        """

        if (
            self.clean_lib()["identified_problems"]
            - self.clean_lib()["removed_problems"]
        ) == 0:

            l2a_archive_path = Path(Config().get("l2a_archive_path"))

            prod_list = self.l2a

            if prod_list:
                count = 0
                total_size = 0
                for prod in prod_list:
                    l2a = L2aProduct(prod.identifier)
                    if not l2a.path.is_symlink():
                        count += 1
                        total_size += getFolderSize(str(l2a.path))
                        if not size_only:
                            move_path = (
                                l2a_archive_path / l2a.tile / l2a.path.name
                            )
                            logger.info("archiving {}".format(l2a.identifier))
                            move_path.parent.mkdir(exist_ok=True)
                            shutil.move(str(l2a.path), str(move_path.parent))
                            l2a.path.symlink_to(
                                move_path, target_is_directory=True
                            )
                if size_only:
                    logger.info(
                        "{} l2a product(s) to archive ({})".format(
                            count, human_size(total_size)
                        )
                    )
                else:
                    logger.info(
                        "{} l2a product(s) archived ({})".format(
                            count, human_size(total_size)
                        )
                    )
                return total_size
            else:
                logger.info("No L2A products, nothing to archive")
                return 0
        else:
            logger.info(
                "Error(s) in l2a product(s) please correct them running clean_lib(remove=True) before archiving"
            )
            return 0

    def archive_all(
        self,
        force: bool = False,
        size_only: bool = False,
    ):
        """
        Chain archive_l1c and archive_l2a functions

        :param size_only: When True, only returns total size of L1C & L2A products that can be archived.
        :type size_only: bool, Default to False
        :param force: Archive all L1C products.
        :type force: bool, Default to False
        """
        l1c_size = self.archive_l1c(size_only=size_only, force=force)
        l2a_size = self.archive_l2a(size_only=size_only)
        return l1c_size + l2a_size

    def compute_ql(
        self,
        product_list: list = [],
        resolution: int = 750,
        jpg: bool = True,
    ):
        """
        Compute L2A quicklooks for the tile.

        :param product_list: List of products names.
        :type product_list: list, Default to []
        :param resolution: Quicklook resolution.
        :type resolution: int, Default to 750
        :param jpg: Quicklook default format JPG.
        :type jpg: bool, Default to True

        """
        for product in product_list:
            # l1c
            if "l1c" in [item.lower() for item in product_list]:
                for p in self.l1c:
                    l1c = L1cProduct(p.identifier)
                    l1c.process_ql(
                        out_resolution=(resolution, resolution), jpg=jpg
                    )
            # l2a
            if "l2a" in [item.lower() for item in product_list]:
                for p in self.l2a:
                    l2a = L2aProduct(p.identifier)
                    l2a.process_ql(
                        out_resolution=(resolution, resolution), jpg=jpg
                    )
            # indices

    def update_latest_ql(self):
        """
        Update the latest l2a quicklook for the tile and remove previous ones
        """
        p = self.l2a.last.identifier
        if p:
            l2a = L2aProduct(p)
            outfullpath = (
                l2a.path.parent
                / "QL"
                / (
                    l2a.tile
                    + "_"
                    + p[0:4]
                    + Tile._get_date(p).strftime("%Y%m%d")
                    + "_QL_latest.jpg"
                )
            )
            outfullpath.parent.mkdir(parents=True, exist_ok=True)
            old_ql = list((l2a.path.parent / "QL").glob("*_QL_latest.jpg*"))
            liste = [a for a in old_ql if str(outfullpath) not in str(a)]
            for f in liste:
                f.unlink()
            if outfullpath in old_ql:
                logger.info("{} - Latest QL already done".format(self.name))
                return
            else:
                l2a.process_ql(
                    out_path=outfullpath, out_resolution=(750, 750), jpg=True
                )
        else:
            logger.info("{} - No L2A product available".format(self.name))

    def move_old_quicklooks(self):
        """
        Move all old quicklooks to QL subfolder.
        """
        logger.info(
            "{}: Moving all quicklooks to QL/ subfolder".format(self.name)
        )
        (self._paths["l2a"] / "QL").mkdir(exist_ok=True, parents=True)
        for f in self._paths["l2a"].glob("*_QL*"):
            if f.is_file():
                f.replace(f.parent / "QL" / f.name)

    def update_old_cloudmasks(self):
        """
        Move and rename old cloudmasks to new cloudmask folder. Cloudmask xmls are removed.
        """
        # Move and rename old  masks / B11
        logger.info("Moving and renaming old masks")
        for f in self._paths["l2a"].glob("*L2A*_CLOUD_MASK*.jp2"):
            p = OldCloudMaskProduct(f.name)
            f_renamed = f.name.replace("CLOUD_MASK_B11", "CM002-B11").replace(
                "CLOUD_MASK", "CM001"
            )
            logger.info(f_renamed)
            p_new = NewCloudMaskProduct(identifier=f_renamed)
            p_new.path.parent.mkdir(exist_ok=True, parents=True)
            p.path.replace(p_new.path)
            p_new.init_md()

        # Remove xml
        logger.info("Removing xmls")
        for f in self._paths["l2a"].glob("*L2A*_CLOUD_MASK*.jp2.aux.xml"):
            f.unlink()

    def remove_very_old_cloudmasks(self):
        """
        Remove very old cloudmasks, matching pattern : *_CLOUD_MASK.tif
        """
        files = list(self._paths["l2a"].glob("*_CLOUD_MASK.tif"))
        logger.info(
            "{}: Removing {} very old cloudmasks".format(self.name, len(files))
        )
        for f in files:
            if f.is_file():
                f.unlink()

    def update_old_indices(self):
        """
        Rename old indices to match new cloudmask nomenclature
        """
        # Rename old indices to default cm_version CM001
        logger.info("Moving and renaming old indices")

        for indice, path in self._paths["indices"].items():
            logger.info(
                "{} - Processing: {}".format(self.name, indice.upper())
            )
            for f in list(Path(path).glob("*/*MASKED*")) + list(
                Path(path).glob("*/*QUICKLOOK*")
            ):
                f_renamed = f.name.replace("MASKED", "CM001").replace(
                    "QUICKLOOK", "CM001_QL"
                )
                f.rename(str(Path(f.parent / f_renamed)))
                logger.info(f_renamed)
                IndiceProduct(identifier=f_renamed)

    def init_md(self):
        """
        Initiate sen2chain metadata for all tile products (l2a, cloudmasks, indices (raw, masked, ql)).
        """
        logger.info("{} - Initiating products metadata".format(self.name))

        for l2a in [product.identifier for product in self.l2a]:
            L2aProduct(l2a)

        for cloudmask in [product.identifier for product in self.cloudmasks]:
            NewCloudMaskProduct(cloudmask)

        for indice in [
            val
            for sublist in [
                getattr(self, i) for i in [p for p in self.paths["indices"]]
            ]
            for val in sublist
        ]:
            IndiceProduct(identifier=indice.identifier)
    
    def remove_l1c_all(
        self,
        remove: bool = False,
    ):
        """
        Remove all L1C products.
        :param remove: When True all products are removed, when False nothing done just info
        :type remove: bool, Default to False
        """
        prod_list = [product.identifier for product in self.l1c]
        logger.info("{}: {} product(s) to remove".format(self.name, len(prod_list)))        
        if remove:
            for identifier in prod_list:
                l1c = L1cProduct(identifier)
                l1c.remove()
            logger.info("Removed: {} products".format(len(prod_list)))
            self._products["l1c"] = ProductsList()
            self._get_l1c_list()
    
    def remove_l1c_filter(
        self,
        l1c_list: list = None,
        remove: bool = False,
        cloudcover_above: int = 0,
        cloudcover_below: int = 100,
        select_none: bool = False,
        dates_after: str = "2015-01-01", 
        dates_before: str = "9999-12-31",
    ):
        """
        Remove L1C products according to cloud covers and dates.

        :param product_list: List of L1C products names to remove and filter. If not provided all L1C products are used. 
        :type product_list: list, Default to []
        :param remove: When True filtered products are removed, when False nothing done just info
        :type remove: bool, Default to False
        :param cloudcover_above: All products with cloud cover above this value will be removed
        :type cloudcover_above: int, Default to 0
        :param cloudcover_below: All products with cloud cover below this value will be removed
        :type cloudcover_below: int, Default to 100
        :param dates_after: All products more recent than this value will be removed
        :type dates_after: str, Default to "2015-01-01"   
        :param dates_before: All products older than this value will be removed
        :type dates_before: str, Default to "9999-12-31"     

        """
        if l1c_list is None:
            prod_list = self.l1c
        else:
            if type(l1c_list) == list:
                prod_list = ProductsList()
                # for k, v in self.l1c._dict.items():   
                    # logger.info(k.split(".")[0])
                    # if k.split(".")[0] in l1c_list:
                        # logger.info("toto")
                        # prod_list[k] = {"date": v["date"], "cloud_cover": v["cloud_cover"]}      
                for product in self.l1c:   
                    if Path(product.identifier).stem in l1c_list:
                        prod_list[product.identifier] = {
                            "date": product.date,
                            "cloud_cover": product.cloud_cover,
                        }     
        if prod_list:
            prod_list_filtered = prod_list.filter_clouds(
                cover_min = cloudcover_above, 
                cover_max = cloudcover_below,
                select_none = select_none,
            ).filter_dates(
                date_min = dates_after,
                date_max = dates_before,
            )                
        else:
            prod_list_filtered = prod_list
        logger.info("{} product(s) to remove".format(len(prod_list_filtered)))        
        if remove:
            for product in prod_list_filtered:
                l1c = L1cProduct(product.identifier)
                l1c.remove()
            logger.info("Removed: {} products".format(len(prod_list_filtered)))
            self._products["l1c"] = ProductsList()
            self._get_l1c_list()
    
    def remove_l2a_all(
        self,
        copy_l2a_sideproducts: bool = False,
        remove: bool = False,
    ):
        """
        Remove all L2A products.
        :param copy_l2a_sideproducts: When True, copy 2 L2A files to corresponding Cloudmask product folder, allowing to compute Cloudmasks later without the L2A product.
        :type copy_l2a_sideproducts: bool, Default to False
        :param remove: When True all products are removed, when False nothing done just info
        :type remove: bool, Default to False
        """
        prod_list = [product.identifier for product in self.l2a]
        logger.info("{} product(s) to remove".format(len(prod_list)))        
        if remove:
            for identifier in prod_list:
                l2a = L2aProduct(identifier)
                l2a.remove(copy_l2a_sideproducts = copy_l2a_sideproducts)
            logger.info("Removed: {} products".format(len(prod_list)))
            self._products["l2a"] = ProductsList()
            self._get_l2a_list()

    def remove_l2a_filter(
        self,
        l2a_list: list = None,
        copy_l2a_sideproducts: bool = False,
        remove: bool = False,
        cloudcover_above: int = 0,
        cloudcover_below: int = 100,
        select_none:bool = False,
        dates_after: str = "2015-01-01", 
        dates_before: str = "9999-12-31",
    ):
        """
        Remove L2A products according to cloud covers and dates.

        :param product_list: List of L2A products names to remove and filter. If not provided all l2a products are used. 
        :type product_list: list, Default to []
        :param copy_l2a_sideproducts: When True, copy 2 L2A files to corresponding Cloudmask product folder, allowing to compute Cloudmasks later without the L2A product.
        :type copy_l2a_sideproducts: bool, Default to False
        :param remove: When True filtered products are removed, when False nothing done just info
        :type remove: bool, Default to False
        :param cloudcover_above: All products with cloud cover above this value will be removed
        :type cloudcover_above: int, Default to 0
        :param cloudcover_below: All products with cloud cover below this value will be removed
        :type cloudcover_below: int, Default to 100
        :param dates_after: All products more recent than this value will be removed
        :type dates_after: str, Default to "2015-01-01"   
        :param dates_before: All products older than this value will be removed
        :type dates_before: str, Default to "9999-12-31"     

        """
        if l2a_list is None:
            prod_list = self.l2a
        else:
            if type(l2a_list) == list:
                prod_list = ProductsList()
                # for k, v in self.l1c._dict.items():   
                    # logger.info(k.split(".")[0])
                    # if k.split(".")[0] in l1c_list:
                        # logger.info("toto")
                        # prod_list[k] = {"date": v["date"], "cloud_cover": v["cloud_cover"]}      
                for product in self.l2a:   
                    if Path(product.identifier).stem in l2a_list:      
                        prod_list[product.identifier] = {
                            "date": product.date,
                            "cloud_cover": product.cloud_cover,
                        }
        if prod_list:
            prod_list_filtered = prod_list.filter_clouds(
                cover_min = cloudcover_above, 
                cover_max = cloudcover_below,
                select_none = select_none,
            ).filter_dates(
                date_min = dates_after,
                date_max = dates_before,
            )                
        else:
            prod_list_filtered = prod_list
        logger.info("{}: {} product(s) to remove".format(self.name, len(prod_list_filtered)))        
        if remove:
            for product in prod_list_filtered:
                l2a = L2aProduct(product.identifier)
                l2a.remove(copy_l2a_sideproducts = copy_l2a_sideproducts)
            logger.info("Removed: {} products".format(len(prod_list_filtered)))
            self._products["l2a"] = ProductsList()
            self._get_l2a_list()

    def copy_sp(self, l2a_product):
            l2a = L2aProduct(l2a_product)
            l2a.copy_l2a_sideproducts() 
    
    def copy_l2a_sideproducts(
        self,
        prod_list: list = None,
        nb_proc: int = 4,
    ):
        """
        Copy Cloud probability "msk_cldprb_20m" and Scene Classification scl_20m files from L2A Product to corresponding Cloud mask folder.

        :param product_list: List of products to copy L2A side products from.
        :type product_list: list, Default to []
        :param nb_proc: Number of processors to perform operation.
        :type nb_proc: int, Default to 4
        """
        if prod_list is None:
            prod_list = [product.identifier for product in self.l2a]
        if prod_list:
            nb_proc = max(min(len(os.sched_getaffinity(0)) - 1, nb_proc), 1)
            pool = multiprocessing.Pool(nb_proc)
            results = [pool.map(self.copy_sp, prod_list)]
            pool.close()
            pool.join()
        return True
    
    def remove_cloudmask_all(
        self,
        remove: bool = False,
    ):
        logger.info("{} cloudmasks to remove for {}".format(len(self.cloudmasks), self.name))
        if remove:
                path = self._paths["cloudmasks"]
                shutil.rmtree(str(path))
                logger.info("Removed all cloudmasks")
                self._products["cloudmasks"] = ProductsList()
                self._get_cloudmasks()
                self.cloudmask_list = self._get_cloudmask_list()


    def remove_cloudmask_filter(
        self,
        remove: bool = False,
        cloudcover_above: int = 0,
        cloudcover_below: int = 100,
        select_none:bool = False,
        dates_after: str = "2015-01-01", 
        dates_before: str = "9999-12-31",
        cm_strings: list = [],
    ):
        prod_list = self.cloudmasks
        if prod_list:
            prod_list_filtered = prod_list.cm_filter(
                cm_strings,
            ).filter_clouds(
                cover_min = cloudcover_above, 
                cover_max = cloudcover_below,
                select_none = select_none,
                
            ).filter_dates(
                date_min = dates_after,
                date_max = dates_before,
            )
            prod_list = prod_list_filtered
        logger.info("{}: {} CM product(s) to remove".format(self.name, len(prod_list))) 
        if remove:
            for product in prod_list:
                cm = NewCloudMaskProduct(product.identifier)
                cm.remove()
            logger.info("Removed: {} products".format(len(prod_list)))
            try:
                self._paths["cloudmasks"].rmdir()
                logger.info("Removing empty tile directory")
            except (OSError, KeyError):
                pass  
            self._products["cloudmasks"] = ProductsList()
            self._get_cloudmasks()
            self.cloudmask_list = self._get_cloudmask_list()

        
    def remove_indice_all(
        self,
        remove: bool = False,
    ):
        for indice, path in self._paths["indices"].items():
            if remove:
                shutil.rmtree(str(path))
                logger.info("{}: removing: {}".format(self.name, path.parent.stem))
            else:
                logger.info("{}: to remove: {}".format(self.name, path.parent.stem))
        if remove:
            self._paths["indices"] = {}
            self._indices_path = Path(Config().get("indices_path"))
            self._products["indices"] = dict()
            self._get_indices_paths()
            self._get_new_indice_list()
            self.indice_list = [indice for indice, path in self._paths["indices"].items()]

    def remove_indice_filter(
        self,
        remove: bool = False,
        indice_list: list = [],
        cloudcover_above: int = 0,
        cloudcover_below: int = 100,
        select_none: bool = False,
        dates_after: str = "2015-01-01", 
        dates_before: str = "9999-12-31",
        cm_strings: list = [],
        remove_ql: bool = True,

    ):
        if not indice_list:
            indice_list = self.indice_list
        for indice in indice_list:
            prod_list = getattr(self, indice)
            if prod_list:
                prod_list_filtered = prod_list.cm_filter(
                    cm_strings,
                ).filter_clouds(
                    cover_min = cloudcover_above, 
                    cover_max = cloudcover_below,
                    select_none = select_none,
                ).filter_dates(
                    date_min = dates_after,
                    date_max = dates_before,
                )
                prod_list = prod_list_filtered
            logger.info("{}: {}: {} product(s) to remove".format(self.name, indice, len(prod_list))) 
            if remove:
                for product in prod_list:
                    ind = IndiceProduct(product.identifier)
                    ind.remove(remove_ql = remove_ql)
                logger.info("{}: removed: {} products".format(self.name, len(prod_list)))
                try:
                    self._paths["indices"][indice].rmdir()
                    logger.info("Removing empty tile directory")
                except (OSError, KeyError):
                    pass  

        if remove:
            self._paths["indices"] = {}
            self._indices_path = Path(Config().get("indices_path"))
            self._products["indices"] = dict()
            self._get_indices_paths()
            self._get_new_indice_list()
            self.indice_list = [indice for indice, path in self._paths["indices"].items()]

        
        # for indice in indice_list:        
            # if indice in self._paths["indices"].keys():
                # path = self._paths["indices"][indice]
                # if remove:
                    # shutil.rmtree(str(path))
                    # logger.info("Removing: {}".format(path.parent.stem))
                # else:
                    # logger.info("To remove: {}".format(path.parent.stem))
        
        
        # if remove:
            # self._paths["indices"] = {}
            # self._indices_path = Path(Config().get("indices_path"))
            # self._products["indices"] = dict()
            # self._get_indices_paths()
            # self._get_new_indice_list()
            # self.indices = [indice for indice, path in self._paths["indices"].items()]

    def remove_all(
        self,
    ):
        pass

    def remove_filter(
        self,
    ):
        pass
        
    def copy_indice_server(
        self,
        copy: bool = False,
        user: str = "user",
        server: str = "server",
        path: str = "path",
        indice_list: list = [],
        cloudcover_above: int = 0,
        cloudcover_below: int = 100,
        select_none: bool = False,
        dates_after: str = "2015-01-01", 
        dates_before: str = "9999-12-31",
        cm_strings: list = [],
    ): 
        if not indice_list:
            indice_list = self.indice_list
        for indice in indice_list:
            prod_list = getattr(self, indice)
            if prod_list:
                prod_list_filtered = prod_list.cm_filter(
                    cm_strings,
                ).filter_clouds(
                    cover_min = cloudcover_above, 
                    cover_max = cloudcover_below,
                    select_none = select_none,
                ).filter_dates(
                    date_min = dates_after,
                    date_max = dates_before,
                )
                prod_list = prod_list_filtered
            logger.info("{}: {}: {} product(s) to copy".format(self.name, indice, len(prod_list))) 
            if copy:
                for product in prod_list:
                    ind = IndiceProduct(product.identifier)
                    path_ind = ind.path
                    copy_server= f"{user}@{server}:{path}"
                    #commande_scp = ["scp", "-r", f"{path_ind}",copy_server]
                    commande_rsync= ["rsync", "-avh", "-e", "ssh", f"{path_ind}",copy_server]
                    try : 
                        #resultat_scp = subprocess.run(commande_scp, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                        resultat_rsync = subprocess.run(commande_rsync, check=True, stdout = subprocess.PIPE, stderr=subprocess.PIPE)
                        print(f"Sortie standard de la commande :{commande_rsync}")
                    except subprocess.CalledProcessError as e:
                        print(f"Erreur de la commande : {e.returncode}. Erreur : {e.stderr.decode()}")
                logger.info("{}: copied: {} products".format(self.name, len(prod_list)))
