# coding: utf-8

"""
Module for managing products and tiles in the library and temp folders.
"""

import os
import shutil
import logging
import pathlib
import uuid

from itertools import chain
from pathlib import Path

# type annotations
from typing import List, Dict

from .config import Config, SHARED_DATA
from .products import L1cProduct
from .tiles import Tile
from .utils import human_size, human_size_decimal
from .multi_processing import (
    l2a_multiprocessing,
    cld_version_probability_iterations_reprocessing_multiprocessing,
    idx_multiprocessing,
)

s2_tiles_index = SHARED_DATA.get("tiles_index")

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Library:
    """Class for listing L1C, L2A and indices tiles in the library folders, and computing products on a list or all of the tiles in the library."""

    _temp_path = Path(Config().get("temp_path"))
    _l1c_path = Path(Config().get("l1c_path"))
    _l2a_path = Path(Config().get("l2a_path"))
    _indices_path = Path(Config().get("indices_path"))
    _cloudmasks_path = Path(Config().get("cloudmasks_path"))

    def __init__(self):
        self._tiles_l1c = [
            f.name for f in self._l1c_path.glob("*") if [
                k.name for k in self._l1c_path.glob(f.name + "/*.SAFE")
            ]
        ]
        
        self._tiles_l2a = [
            f.name for f in self._l2a_path.glob("*") if [
                k.name for k in self._l2a_path.glob(f.name + "/*.SAFE")
            ]
        ]

        self._tiles_cloudmasks = [
            f.name for f in self._cloudmasks_path.glob("*") if [
                k.name for k in self._cloudmasks_path.glob("{}/*/".format(f.name))
            ]
        ]
        
        self._indices = [f.name for f in self._indices_path.glob("*")]
        _tiles_indices = []
        for indice in self._indices:
            self.__dict__["tiles_" + indice.lower()] = [
                f.name for f in self._indices_path.glob("{}/*".format(indice)) if [
                    k.name for k in self._indices_path.glob("{}/{}/*/".format(indice, f.name))
                ]                    
            ]
            _tiles_indices.extend([t for t in getattr(self, "tiles_" + indice.lower())])
        self._tiles_indices = list(set(_tiles_indices))
                    
    @property
    def tiles_l1c(self) -> List[str]:
        """Returns tiles in the L1C library folder."""
        return self._tiles_l1c

    @property
    def tiles_l2a(self) -> List[str]:
        """Returns tiles in the L2A library folder."""
        return self._tiles_l2a
        
    @property
    def tiles_cloudmasks(self) -> List[str]:
        """Returns tiles in the cloudmask library folder."""
        return self._tiles_cloudmasks

    @property
    def tiles_indices(self) -> Dict[str, pathlib.PosixPath]:
        """Returns tiles in the indices library folder."""
        return self._tiles_indices
        
    @property
    def indices(self) -> Dict[str, pathlib.PosixPath]:
        """Returns tiles in the indices library folder."""
        return self._indices

    def clean(
        self,
        clean_list: list = [],
        remove_indice_tif: bool = False,
        remove: bool = False,
    ):
        """Function to clean corrupted files during processing errors from whole Library or selected Tiles. Calls Tile.clean_lib()

        :param clean_list: list of tiles to be cleaned. If not provided, will process all L1C tiles.
        :type clean_list: str, Default to []
        :param remove_indice_tif: If True, remove TIFF files in indice folders. Default value False.
        :type remove_indice_tif: bool, Default to False
        :param remove: If True, remove corrupted files. If False, listd identified problems.
        :type remove: bool, Default to False

        """
        nb_id = 0
        nb_rm = 0
        prob_id = []
        if not clean_list:
            clean_list = self.tiles_l1c
        for t in clean_list:
            try:
                til = Tile(t)
                counts = til.clean_lib(
                    remove=remove, remove_indice_tif=remove_indice_tif
                )
                nb_id += counts["identified_problems"]
                nb_rm += counts["removed_problems"]
                if counts["problems"]:
                    prob_id.append(counts["problems"])
                logger.info(
                    "{} - identified problems:{} - removed problems:{}".format(
                        til,
                        counts["identified_problems"],
                        counts["removed_problems"],
                    )
                )
            except Exception:
                pass
        return {
            "identified_problems": nb_id,
            "removed_problems": nb_rm,
            "problems": prob_id,
        }

    def archive_l1c(
        self,
        archive_list: list = [],
        force: bool = False,
        size_only: bool = False,
    ):
        """
        Function to archive l1c products from library folder to l1c_archive_path. Calls Tile.archive_l1c()

        :param archive_list: List of tiles to archive. If not provided, will process all L1C tiles.
        :type archive_list: list, Default to []
        :param force: Archive all L1C products.
        :type force: bool, Default to False
        :param size_only: When True, only returns total size of L2A products that can be archived.
        :type size_only: bool, Default to False        """

        total_size = 0
        if not archive_list:
            archive_list = self.tiles_l1c
        for t in archive_list:
            try:
                logger.info(t)
                til = Tile(t)
                size_tile = til.archive_l1c(size_only=size_only, force=force)
                if size_only:
                    total_size += size_tile
            except Exception:
                pass
        logger.info(
            "Total l1c size to move: {}".format(human_size_decimal(total_size))
        )

    def archive_l2a(
        self,
        tile_list: list = [],
        size_only: bool = False,
    ):

        """
        Function to archive l2a products from library folder to l2a_archive_path.
        Calls the archive_l2a function from Tile class, see there for details.

        :param tile_list: list of tiles to archive. If not provided, will
            process all l2a tiles.
        :type tile_list: list, Default to []
        :param size_only: When True, only returns total size of L2A products that can be archived.
        :type size_only: bool, Default to False

        """

        total_size = 0
        if not tile_list:
            tile_list = self.tiles_l2a
        for t in tile_list:
            try:
                logger.info(t)
                til = Tile(t)
                size_tile = til.archive_l2a(size_only=size_only)
                if size_only:
                    total_size += size_tile
            except Exception:
                logger.info("Problem while archiving")
        logger.info(
            "Total l2a size to move: {}".format(human_size_decimal(total_size))
        )

    def archive_all(
        self,
        archive_list: list = [],
        size_only: bool = False,
        force: bool = False,
    ):

        """
        Archive L1C and L2A products for a given list of tiles.

        :param archive_list: List of tiles to archive all L1C and L2A from. If empty archive all L1C and L2A.
        :type archive_list: list, Default to []
        :param size_only: When True, only returns total size of L1C products that can be archived.
        :type size_only: bool, Default to False
        :param force: Archive all L1C products.
        :type force: bool, Default to False
        """
        total_size = 0
        if not archive_list:
            archive_list = self.tiles_l1c
        for t in archive_list:
            try:
                logger.info(t)
                til = Tile(t)
                size_tile = til.archive_all(size_only=size_only, force=force)
                total_size += size_tile
            except Exception:
                pass
        logger.info(
            "Total size to move: {}".format(human_size_decimal(total_size))
        )

    def compute_ql(
        self,
        tile_list: list = [],
        product_list: list = [],
        resolution: int = 750,
        jpg: bool = True,
    ):
        """
        Produce L1C and/or L2A quicklooks, for provided tile_list or all the library tiles, at specified resolution and in jpg or tiff format

        :param tile_list: List of Sentinel-2 Tiles. If empty, select all Tiles in Library.
        :type tile_list: list, Default to []
        :param product_list: L1C or L2A products to compute quicklooks.
        :type product_list: list, Default to []
        :param resolution: Quicklook resolution
        :type resolution: int, Default to 750
        :param jpg: Quicklook format. JPG2 by default, else TIFF
        :type jpg: bool, Default to True
        """
        if not tile_list:
            tile_list = list(set(self.tiles_l1c + self.tiles_l2a))
        for tile in tile_list:
            Tile(tile).compute_ql(product_list, resolution, jpg)

    def update_latest_ql(self):
        """
        Commpute the latest quicklook for the L2A library tiles
        """
        try:
            for tile in self.tiles_l2a:
                Tile(tile).update_latest_ql()
        except Exception:
            pass

    # def update_latest_ql(self):
    #     """
    #     Produce or update the latest quicklook for the L2A library tiles
    #     """
    #     for tile in self.tiles_l2a:
    #         Tile(tile).update_latest_ql()

    def consolidate_ql(self, location=None, zipfile=False):
        """
        Copy all quicklook files to a single location
        """
        if not location:
            location = Config().get("temp_path")
        new_ql_folder = Path(location) / "QL"
        if (new_ql_folder.exists()):
            unique_str = uuid.uuid4().hex
            new_ql_folder.replace(Path(location) / "QL-{}".format(unique_str))
        new_ql_folder.mkdir()

        for tile in self.tiles_l2a:
            tile_ql_folder = Path(Config().get("l2a_path")) / tile / "QL"
            ql_folder_contents = list(tile_ql_folder.glob("*_QL_latest.jpg"))
            if len(ql_folder_contents) > 0:
                tile_ql = ql_folder_contents[0]
                shutil.copy2(str(tile_ql), str(new_ql_folder))

        if zipfile:
            shutil.make_archive(str(new_ql_folder), 'zip', str(new_ql_folder))

    def update_old_cloudmasks(self):
        """
        Compute the latest quicklook from the Cloudmask library tiles.
        """
        for tile in self.tiles_l2a:
            Tile(tile).update_old_cloudmasks()

    def remove_very_old_cloudmasks(self):
        """
        Remove very old cloudmasks, matching pattern : *_CLOUD_MASK.tif from the cloudmask library.
        """
        for tile in self.tiles_l2a:
            Tile(tile).remove_very_old_cloudmasks()

    def move_old_quicklooks(self):
        """
        Move all old quicklooks to QL subfolder from the L2A library, for each tile.
        """
        for tile in self.tiles_l2a:
            Tile(tile).move_old_quicklooks()

    def update_old_indices(self):
        """
        Rename old indices to match new cloudmask nomenclature.

        """
        for tile in {f.name for f in list(self._indices_path.glob("*/*/"))}:
            Tile(tile).update_old_indices()

    def init_md(self):
        """
        Initiate sen2chain metadata for all tile products (l2a, cloudmasks, indices (raw, masked, ql))
        for the L1C, L2A and Indice library tiles
        """
        for tile in set(
            [
                val
                for sublist in [
                    k for k in [getattr(self, t) for t in self.__dict__]
                ]
                for val in sublist
            ]
        ):
            Tile(tile).init_md()

    def compute_l2a(
        self,
        tile_list: list = [],
        date_min: str = None,
        date_max: str = None,
        nb_proc: int = 4,
    ):
        """
        Compute every L1C product to L2A for a list of tile and specific dates.

        :param tile_list: List of tiles. Compute Tile.compute_l2a() on each.
        :type tile_list: list, Default to []
        :param date_min: First date of Period of Interest.
        :type date_min: str, Default to None
        :param date_max: Last date of Period of Interest.
        :type date_max: str, Default to None
        :param nb_proc: Number of processors to use for computing.
        :type nb_proc: int, Default to 4
        """
        l1c_process_list = []
        for tile in tile_list:
            t = Tile(tile)
            l1c_process_list.append(
                list(
                    p.identifier
                    for p in t.l2a_missings.filter_dates(
                        date_min=date_min, date_max=date_max
                    )
                )
            )
        l1c_process_list = list(chain.from_iterable(l1c_process_list))
        logger.info(
            "l1c_process_list ({} files): \n{}".format(
                len(l1c_process_list), l1c_process_list
            )
        )

        if l1c_process_list:
            l2a_res = l2a_multiprocessing(l1c_process_list, nb_proc=nb_proc)

    def compute_cloudmasks(
        self,
        tile_list: list = [],
        cm_version: str = "CM001",
        probability: int = 1,
        iterations: int = 5,
        cld_shad: bool = True,
        cld_med_prob: bool = True,
        cld_hi_prob: bool = True,
        thin_cir: bool = True,
        reprocess: bool = False,
        date_min: str = None,
        date_max: str = None,
        nb_proc: int = 4,
    ):
        """
        Compute all (missing) cloud masks for L2A products, for a list of tiles. Only 1 Cloud mask version per command line.

        :param tile_list: List of tiles to compute cloudmasks from.
        :type tile_list: list, default to []
        :param cm_version: Cloudmask version to compute. Can be either CM001, CM002, CM003, or CM004.
        :type cm_version: str, Default to "CM001"
        :param probability: Threshold of probability of clouds to be considered. Specific to CM003 & CM004.
        :type probability: int, Default to 1
        :param iterations: Number of iterations for the kernel dilatation process. Specific to CM003 & CM004.
        :param cld_shad: When True, uses Cloud Shadows class from Scene Classification.
        :type cld_shad: bool, Default to True
        :param cld_med_prob: When True, uses Cloud Medium Probability class from Scene Classification.
        :type cld_med_prob: bool, Default to True
        :param cld_hi_prob: When True, uses Cloud High Probability class from Scene Classification.
        :type cld_hi_prob: bool, Default to True
        :param thin_cir: When True, uses Thin Cirrus class from Scen Classification.
        :type thin_cir: bool, Default to True
        :param reprocess: When True, already processed cloudmask will be computed again.
        :type reprocess: bool, Default to False
        :param date_min: Products before this date wont be processed.
        :type date_min: str, Default to None
        :param date_max: Products after this date wont be processed.
        :type date_max: str, Default to None
        :param nb_proc: Number of processors to mobilize for computing. Limited to the number of proc of your computer.
        :type nb_proc: int, Default to 4
        """
        cld_l2a_process_list = []
        for tile in tile_list:
            t = Tile(tile)
            if not reprocess:
                l2a_list = [
                    p.identifier
                    for p in t.cloudmasks_missing(
                        cm_version=cm_version,
                        probability=probability,
                        iterations=iterations,
                        cld_shad=cld_shad,
                        cld_med_prob=cld_med_prob,
                        cld_hi_prob=cld_hi_prob,
                        thin_cir=thin_cir,
                    ).filter_dates(date_min=date_min, date_max=date_max)
                ]
            else:
                l2a_list = [
                    p.identifier
                    for p in t.l2a.filter_dates(
                        date_min=date_min, date_max=date_max
                    )
                ]
            for j in l2a_list:
                cld_l2a_process_list.append(
                    [
                        j,
                        cm_version,
                        probability,
                        iterations,
                        cld_shad,
                        cld_med_prob,
                        cld_hi_prob,
                        thin_cir,
                        reprocess,
                    ]
                )
        logger.info(
            "cld_l2a_process_list ({} files): \n{}".format(
                len(cld_l2a_process_list), cld_l2a_process_list
            )
        )
        if cld_l2a_process_list:
            cld_res = cld_version_probability_iterations_reprocessing_multiprocessing(
                cld_l2a_process_list, nb_proc=nb_proc
            )

    def compute_indices(
        self,
        tile_list: list = [],
        indices: list = [],
        reprocess: bool = False,
        nodata_clouds: bool = True,
        quicklook: bool = False,
        cm_version: list = "cm001",
        probability: int = 1,
        iterations: int = 5,
        cld_shad: bool = True,
        cld_med_prob: bool = True,
        cld_hi_prob: bool = True,
        thin_cir: bool = True,
        date_min: str = None,
        date_max: str = None,
        nb_proc: int = 4,
    ):
        """
        Compute all missing radiometric indices for L2A products, for a list of tiles.
        - Indices are given as a list
        - If indices are not provided, will compute missing dates of already existing indices for this tile (no new indice computed)
        - Indices won't be masked if no cloud masks are present, you have to compute cloudmasks first.
        - To mask indices, specify cm_version and specific cloud masks parameters.

        :param tile_list: List of tiles to compute indices.
        :type tile_list: list, Default to []
        :param indices: List of indices to process L2A products.
        :type indices: list, Default to []
        :param reprocess: When True, reprocess indices already computed that meet the other parameters.
        :type reprocess: bool, Default to False
        :param nodata_clouds: When True, checks also in produced cloudmasks products.
        :type nodata_clouds: bool, Default to False
        :param quicklook: When True, compute quicklook of indice in JPG.
        :type quicklook: bool, Default to False
        :param cm_version: Cloudmask version to mask indice with. Can be either CM001, CM002, CM003 or CM004.
        :type cm_version: str, Default to "CM001"
        :param probability: Cloud mask probability threshold parameter specific to CM003 & CM004.
        :type probability: int, Default to 1
        :param iterations: Cloud mask CM003 and CM004 parameter; number of iterations for the kernel dilatation process.
        :type iterations: int, Default to 5
        :param cld_shad: Cloud mask CM003 and CM004 parameter; Cloud Shadows class from Scene Classification.
        :type cld_shad: bool, Default to True
        :param cld_med_prob: Cloud mask CM003 and CM004 parameter; Cloud Medium Probability class from Scene Classification.
        :type cld_med_prob: bool, Default to True
        :param cld_hi_prob: Cloud mask CM003 and CM004 parameter; Cloud High Probability class from Scene Classification.
        :type cld_hi_prob: bool, Default to True
        :param thin_cir: Cloud mask CM003 and CM004 parameter; Thin Cirrus class from Scene Classification.
        :type thin_cir: bool, Default to True
        :param reprocess: When True, already processed indices will be computed again.
        :type reprocess: bool, Default to False
        :param date_min: Products before this date wont be processed.
        :type date_min: str, Default to None
        :param date_max: Products after this date wont be processed.
        :type date_max: str, Default to None
        :param nb_proc: Number of processors to mobilize for computing. Limited to the number of processors of your computer.
        :type nb_proc: int, Default to 4
        """
        indices_l2a_process_list = []
        for tile in tile_list:
            t = Tile(tile)
            for i in indices:
                if not reprocess:
                    l2a_list = [
                        p.identifier
                        for p in t.missing_indices(
                            i,
                            nodata_clouds=nodata_clouds,
                            cm_version=cm_version,
                            probability=probability,
                            iterations=iterations,
                            cld_shad=cld_shad,
                            cld_med_prob=cld_med_prob,
                            cld_hi_prob=cld_hi_prob,
                            thin_cir=thin_cir,
                        ).filter_dates(date_min=date_min, date_max=date_max)
                    ]
                else:
                    l2a_list = [
                        p.identifier
                        for p in t.l2a.filter_dates(
                            date_min=date_min, date_max=date_max
                        )
                    ]

                for j in l2a_list:
                    indices_l2a_process_list.append(
                        [
                            j,
                            i,
                            reprocess,
                            nodata_clouds,
                            quicklook,
                            cm_version,
                            probability,
                            iterations,
                            cld_shad,
                            cld_med_prob,
                            cld_hi_prob,
                            thin_cir,
                        ]
                    )
        logger.info(
            "indices_l2a_process_list ({} files): \n{}".format(
                len(indices_l2a_process_list), indices_l2a_process_list
            )
        )
        if indices_l2a_process_list:
            indices_res = idx_multiprocessing(
                indices_l2a_process_list, nb_proc=nb_proc
            )
    
    def remove_l1c_all(
        self,
        tile_list: list = None,
        remove: bool = False,
    ):
        """
        Produce L1C and/or L2A quicklooks, for provided tile_list or all the library tiles, at specified resolution and in jpg or tiff format

        :param tile_list: List of Sentinel-2 Tiles. If empty, select all Tiles in Library.
        :type tile_list: list, Default to []
        :param product_list: L1C or L2A products to compute quicklooks.
        :type product_list: list, Default to []
        :param resolution: Quicklook resolution
        :type resolution: int, Default to 750
        :param jpg: Quicklook format. JPG2 by default, else TIFF
        :type jpg: bool, Default to True
        """
        if tile_list is None:
            tile_list = self.tiles_l1c
        logger.info("{} L1C tile(s) to remove".format(len(tile_list)))
        for tile in tile_list:
            Tile(tile).remove_l1c_all(remove = remove)
        self._tiles_l1c = [
            f.name for f in self._l1c_path.glob("*") if [
                k.name for k in self._l1c_path.glob(f.name + "/*.SAFE")
            ]
        ]
        
    def remove_l1c_filter(
        self,
        tile_list: list = None,
        remove: bool = False,
        cloudcover_above: int = 0,
        cloudcover_below: int = 100,
        dates_after: str = "2015-01-01", 
        dates_before: str = "9999-12-31",
    ):
        if tile_list is None:
            tile_list = self.tiles_l1c
        logger.info("{} L1C tile(s) to process".format(len(tile_list)))
        for tile in tile_list:
            Tile(tile).remove_l1c_filter(
                remove = remove,
                cloudcover_above = cloudcover_above,
                cloudcover_below = cloudcover_below,
                dates_after = dates_after,
                dates_before = dates_before,
            )
        self._tiles_l1c = [
            f.name for f in self._l1c_path.glob("*") if [
                k.name for k in self._l1c_path.glob(f.name + "/*.SAFE")
            ]
        ]
        
    def remove_l2a_all(
        self,
        tile_list: list = None,
        copy_l2a_sideproducts: bool = False,
        remove: bool = False,
    ):
        """
        Produce L1C and/or L2A quicklooks, for provided tile_list or all the library tiles, at specified resolution and in jpg or tiff format

        :param tile_list: List of Sentinel-2 Tiles. If empty, select all Tiles in Library.
        :type tile_list: list, Default to []
        :param product_list: L1C or L2A products to compute quicklooks.
        :type product_list: list, Default to []
        :param resolution: Quicklook resolution
        :type resolution: int, Default to 750
        :param jpg: Quicklook format. JPG2 by default, else TIFF
        :type jpg: bool, Default to True
        """
        if tile_list is None:
            tile_list = self.tiles_l2a
        logger.info("{} L2A tile(s) to remove".format(len(tile_list)))
        for tile in tile_list:
            Tile(tile).remove_l2a_all(
                remove = remove,
                copy_l2a_sideproducts = copy_l2a_sideproducts ,
            )
        self._tiles_l2a = [
            f.name for f in self._l2a_path.glob("*") if [
                k.name for k in self._l2a_path.glob(f.name + "/*.SAFE")
            ]
        ]
        
    def remove_l2a_filter(
        self,
        tile_list: list = None,
        copy_l2a_sideproducts: bool = False,
        remove: bool = False,
        cloudcover_above: int = 0,
        cloudcover_below: int = 100,
        dates_after: str = "2015-01-01", 
        dates_before: str = "9999-12-31",
    ):
        if tile_list is None:
            tile_list = self.tiles_l2a
        logger.info("{} L2A tile(s) to process".format(len(tile_list)))
        for tile in tile_list:
            Tile(tile).remove_l2a_filter(
                copy_l2a_sideproducts = copy_l2a_sideproducts,
                remove = remove,
                cloudcover_above = cloudcover_above,
                cloudcover_below = cloudcover_below,
                dates_after = dates_after,
                dates_before = dates_before,
            )
        self._tiles_l2a = [
            f.name for f in self._l2a_path.glob("*") if [
                k.name for k in self._l2a_path.glob(f.name + "/*.SAFE")
            ]
        ]
    
    def remove_cloudmask_all(
        self,
        tile_list: list = None,
        remove: bool = False,
    ):
        if tile_list is None:
            tile_list = self.tiles_cloudmasks
        logger.info("{} cloudmask tile(s) to process".format(len(tile_list)))
        for tile in tile_list:
            Tile(tile).remove_cloudmask_all(remove = remove)            
        self._tiles_cloudmasks = [
            f.name for f in self._cloudmasks_path.glob("*") if [
                k.name for k in self._cloudmasks_path.glob("{}/*/".format(f.name))
            ]
        ]    
    
    def remove_cloudmask_filter(
        self,
        tile_list: list = None,
        remove: bool = False,
        cloudcover_above: int = 0,
        cloudcover_below: int = 100,
        dates_after: str = "2015-01-01", 
        dates_before: str = "9999-12-31",
        cm_strings: list = [],
    ):
        if tile_list is None:
            tile_list = self.tiles_cloudmasks
        logger.info("{} cloudmask tile(s) to process".format(len(tile_list)))
        for tile in tile_list:
            Tile(tile).remove_cloudmask_filter(
                remove = remove,
                cloudcover_above = cloudcover_above,
                cloudcover_below = cloudcover_below,
                dates_after = dates_after,
                dates_before = dates_before,
                cm_strings = cm_strings,
            )
        self._tiles_cloudmasks = [
            f.name for f in self._cloudmasks_path.glob("*") if [
                k.name for k in self._cloudmasks_path.glob("{}/*/".format(f.name))
            ]
        ]
    
    def remove_indices_all(
        self,
        tile_list: list = None,
        remove: bool = False,
    ):
        if tile_list is None:
            tile_list = self.tiles_indices
        logger.info("{} indice tile(s) to process".format(len(tile_list)))
        for tile in tile_list:
            Tile(tile).remove_indices_all(remove = remove)            
        self._indices = [f.name for f in self._indices_path.glob("*")]
        _tiles_indices = []
        for indice in self._indices:
            self.__dict__["tiles_" + indice.lower()] = [
                f.name for f in self._indices_path.glob("{}/*".format(indice)) if [
                    k.name for k in self._indices_path.glob("{}/{}/*/".format(indice, f.name))
                ]                    
            ]
            _tiles_indices.extend([t for t in getattr(self, "tiles_" + indice.lower())])
        self._tiles_indices = list(set(_tiles_indices))

    def remove_indice_filter(
        self,
        tile_list: list = None,
        remove: bool = False,
        cloudcover_above: int = 0,
        cloudcover_below: int = 100,
        dates_after: str = "2015-01-01", 
        dates_before: str = "9999-12-31",
        cm_strings: list = [],
        remove_ql: bool = True,

    ):
        if tile_list is None:
            tile_list = self.tiles_indices
        logger.info("{} indice tile(s) to process".format(len(tile_list)))
        for tile in tile_list:
            Tile(tile).remove_indice_filter(
                remove = remove,
                cloudcover_above = cloudcover_above,
                cloudcover_below = cloudcover_below,
                dates_after = dates_after,
                dates_before = dates_before,
                cm_strings = cm_strings,
                remove_ql = remove_ql,
            )
        self._indices = [f.name for f in self._indices_path.glob("*")]
        _tiles_indices = []
        for indice in self._indices:
            self.__dict__["tiles_" + indice.lower()] = [
                f.name for f in self._indices_path.glob("{}/*".format(indice)) if [
                    k.name for k in self._indices_path.glob("{}/{}/*/".format(indice, f.name))
                ]                    
            ]
            _tiles_indices.extend([t for t in getattr(self, "tiles_" + indice.lower())])
        self._tiles_indices = list(set(_tiles_indices))

class TempContainer:
    """Class for managing a downloaded L1C products.

    :param l1c_identifier: L1C product's identifier.
    :type l1c_identifier: str, Default to None
    :param tile: Product tile name.
    :type tile: str, Default to None
    """

    _temp_path = Path(Config().get("temp_path"))

    def __init__(self, l1c_identifier=None, tile: str = None):
        self.tile = tile
        self.temp_id = l1c_identifier + "_TEMP"
        self.temp_path = self._temp_path / self.temp_id
        self.l1c = L1cProduct(
            identifier=l1c_identifier, tile=tile, path=self.temp_path
        )

    def create_temp_folder(self) -> "TempContainer":
        """Creates the L1C product's temporary folder in the TEMP library folder."""
        logger.info("{}: creating temp folder".format(self.l1c.identifier))
        try:
            self.temp_path.mkdir(exist_ok=True)
        except FileNotFoundError:
            print("Invalid temp folder path:", self.temp_path)
            raise
        return self

    def unzip_l1c(self) -> "TempContainer":
        """Unzips the downloaded L1C zip file in the product's temporary folder."""
        zip_file = self.temp_path / (self.l1c.identifier + ".zip")
        if zip_file.exists():
            logger.info(
                "{} : Unzipping L1C archive".format(self.l1c.identifier)
            )
            shutil.unpack_archive(str(zip_file), str(self.temp_path))
            sorted(zip_file.parent.glob("*.SAFE"))[0].rename(
                zip_file.parent / (zip_file.stem + ".SAFE")
            )
            os.remove(str(zip_file))
        return self

    def delete_temp_folder(self) -> "TempContainer":
        """Deletes the L1C product's temporary folder."""
        logger.info("Deleting temp folder: {}".format(self.temp_path))
        if not list(self.temp_path.glob("*.SAFE")):
            try:
                shutil.rmtree(str(self.temp_path))
            except FileNotFoundError:
                logger.warning(
                    "Temp folder does not exist: {}".format(self.temp_path)
                )
        else:
            logger.error("Temp folder contains a SAFE product.")
        return self
