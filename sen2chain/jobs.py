# coding: utf-8

"""
Module for managing sen2chain processing jobs
"""

import logging
from pathlib import Path
import pandas as pd
import datetime, time
from itertools import chain, groupby
import re
import setuptools
import sys
from crontab import CronTab
from collections import OrderedDict
from configparser import ConfigParser
import copy
import numpy as np
from threading import Thread


from .config import SHARED_DATA, Config
from .indices import IndicesCollection
from .library import Library
from .tiles import Tile
from .utils import datetime_to_str
from .multi_processing import (
    l2a_multiprocessing,
    idx_multiprocessing,
    cld_version_probability_iterations_reprocessing_multiprocessing,
)
from .download_eodag import (
    S2cEodag_download, 
    S2cEodag_download_uniq,
)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Jobs:
    """
    Class to list and remove existing jobs. Returns a list of existing job and job status when instanciated.
    
    Usage:
        >>> Jobs()
    """
    
    def __init__(self):
        self._jobs_all = list(Config()._JOBS_DIR.glob("job_*.cfg"))
        self._jobs_all.extend(list(Config()._JOBS_DIR.glob("job_*.py")))
        self._jid_set = set([t.stem[4:] for t in self._jobs_all])
        self._config_files_exist = []
        self._py_scripts_exist = []
        self._logs = []
        self._timing = []
        self._cron_status = []
        self._cron_timing = []
        for jid in self._jid_set:
            logger.disabled = True
            j = Job(jid)
            self._config_files_exist.append(j._config_path.exists())
            self._py_scripts_exist.append(j._python_script_path.exists())
            self._logs.append(j.logs)
            self._timing.append(j.timing)
            self._cron_status.append(j.cron_status)
            self._cron_timing.append(j.cron_timing)
            logger.disabled = False

    def __repr__(self):
        if self._jobs_all:
            return repr(
                pd.DataFrame(
                    OrderedDict(
                        [
                            ("job_id", list(self._jid_set)),
                            ("config_file", self._config_files_exist),
                            ("python_script", self._py_scripts_exist),
                            ("logging", self._logs),
                            ("timing", self._timing),
                            ("cron_status", self._cron_status),
                            ("cron_timing", self._cron_timing),
                        ]
                    )
                )
            )
        else:
            logger.info("No Jobs here")
            return repr(None)

        # >>> job.render().split(' ')[0:4]

    @property
    def list(self):
        return [t.stem[4:] for t in self._jobs_all]

    def remove(self, jid: str = None):
        """
        Remove Job from Job list and cron. Delete job configuration file and execution file.

        :param jid: Job identifier.
        :type jid: str
        """
        if jid in self.list:
            logger.disabled = True
            j = Job(jid)
            logger.disabled = False
            j.cron_remove()
            if j._python_script_path.exists():
                j._python_script_path.unlink()
                logger.info("Removing Python script...")
            if j._config_path.exists():
                j._config_path.unlink()
                logger.info("Removing config file...")
        else:
            logger.info("Job {} not found, doing nothing...".format(jid))


class Job:
    """
    Class to create, edit, save and launch job routines. A job allows you to download L1C, process L2A, cloudmasks and radiometric indices at once, for 1 or more Tile.
    Jobs can be set in cron, and a logging option is available.

    A job is made of :
    - a configuration file, located in sen2chain_data/config/jobs/
    - a .py file if job is set in cron. This .py launches the run() command

    Job config file contains global job parameters and tasks.
    A task only targets a single Tile, meaning if you want to download and process multiple tile products you need to have the equal number of task lines in your configuration file.
    We recommend manually editing the configuration file if you plan to set up a job with multiple Tiles, as editing with the command line is only for 1 task at a time.

    """

    # logger.propagate = False
    def __init__(self, jid: str, with_cron: bool = True):
        self.false_list = ["", "False", False, None, "None", [], [""], [None], ["None"], [False], ["False"]]
        self._config_path = Config()._JOBS_DIR / ("job_" + jid + ".cfg")
        self._python_script_path = Config()._JOBS_DIR / ("job_" + jid + ".py")
        self.jid = jid
        self.cols = [
            "tile",
            "date_min",
            "date_max",
            "max_clouds",
            "provider",
            "download",
            "compute_l2a",
            "cloudmasks",
            "indices",
            "remove",
            "comments"
        ]
        if self._config_path.exists():
            logger.info("Reading existing config...")
            self.read(self._config_path)
        else:
            logger.info("Creating new job...")
            self.init()
        self._cron = CronTab(user=True) if with_cron else None
        self.cron_status = self.get_cron_status() if with_cron else None

        self._log_folder_path = Path(Config().get("log_path")) / ("job_" + jid)
        self.remove_used_l1c = False

    def __repr__(self):
        return (
            "logs: {}"
            "\ntiming: {}"
            "\ncron_status: {}"
            # "\nprovider: {}"
            "\ndownload tries: {} x / sleep: {} min"
            "\nnb_proc: {}"
            "\ncopy_l2a_sideproducts: {}"
            "\nclean_before: {}"
            "\nclean_after: {}"
            "\nprocess_by_line: {}"            
            "\n\ntasks:\n{}".format(
                self.logs, 
                self.timing,
                self.cron_status,
                # self.provider,
                self.tries,
                self.sleep,
                self.nb_proc,
                self.copy_l2a_sideproducts,
                self.clean_before,
                self.clean_after,
                self.process_by_line,                
                self.tasks,
            )
        )
        
    def init(self):
        raw_job_cfg = SHARED_DATA.get("raw_job_cfg")
        self.read(raw_job_cfg)
        first_row = OrderedDict(
            [
                ("tile", ["40KCB"]),
                # ("date_min", [datetime_to_str(datetime.datetime.now(), date_format = 'ymd')]),
                # ("date_max", [datetime_to_str(datetime.datetime.now(), date_format = 'ymd')]),
                ("date_min", ["today-5"]),
                ("date_max", ["today"]),                
                ("max_clouds", [100]),
                ("provider", ["peps"]),
                ("download", ["False"]),
                ("compute_l2a", ["False"]),
                ("cloudmasks", ["False"]),
                ("indices", ["False"]),
                ("remove", ["False"]),
                ("comments", ["Init line sur La Réunion"]),
            ]
        )
        self.tasks = pd.DataFrame(first_row)

    def task_add(self, row: dict = None):
        """
        Add a task to the job.

        :param row: Dict of specific parameters for task actions. If no dict provided, will provide a default task format.
        :type row: Dict, Default to None
        """
        if not row:
            logger.info("No row provided, using default")
            row = pd.DataFrame(
                {
                    "tile": ["40KCB"],
                    "date_min": [datetime_to_str(datetime.datetime.now(), date_format = 'ymd')],
                    "date_max": [datetime_to_str(datetime.datetime.now(), date_format = 'ymd')],
                    "max_clouds": [100],
                    "l1c": ["False"],
                    "l2a": ["False"],
                    "cloudmasks": ["False"],
                    "indices": ["False"],
                    "remove": ["False"],
                    "comments": [""],
                }
            )
        # self.tasks = self.tasks.append(row, ignore_index=True)
        self.tasks = pd.concat([self.tasks, row], ignore_index=True)[
            self.tasks.columns
        ]
        self.format_cm_indices()
        logger.info("\n{}".format(self.tasks))

    def task_edit(self, task_id: int = None, **kwargs):
        """
        Edit an existing task, identified by task_id (row number)

        :param task_id: Task identifier, or row number in config file.
        :type task_id: int
        :param kwargs: Any keys/items pairs from task actions options.

        """
        if task_id is None:
            logger.info(
                "Please provide task_id to edit, if no task in job, create_task first"
            )
        else:
            if task_id in self.tasks.index:
                for arg, val in kwargs.items():
                    if arg in self.tasks.columns:
                        self.tasks.loc[task_id, arg] = val
                        logger.info(
                            "Line {}: {} updated to {}".format(
                                task_id, arg, val
                            )
                        )
                    else:
                        logger.info("{} not found".format(arg))
                self.format_cm_indices()
                logger.info("\n{}".format(self.tasks))
            else:
                logger.info("task_id not found")

    def task_remove(self, task_id: int = None):
        """
        Remove a task from job. The task line is deleted from the config file.

        :param task_id: Task identifier (row number).
        :type task_id: int

        """
        if task_id is None:
            logger.info("Please provide task_id to remove")
        else:
            if task_id in self.tasks.index:
                self.tasks.drop(labels=task_id, axis=0, inplace=True)
                self.tasks.reset_index(inplace=True, drop=True)
                logger.info("\n{}".format(self.tasks))
            else:
                logger.info("task_id not found")

    def save(self):
        """
        Save Job config.cfg file to disk. Mandatory before running job.
        """
        with open(str(self._config_path), "w") as ict:
            comments_header ="\n".join(
                [
                    "### Parameters", 
                    "# logs: True | False", 
                    "# timing: in cron format", 
                    # "# provider: the provider to download l1c prodicts, default peps, values: peps | cop_dataspace", 
                    "# tries: the number of times the download should loop before stopping, to download OFFLINE products", 
                    "# sleep: the time in min to wait between loops", 
                    "# nb_proc: the number of cpu cores to use for this job, default 8", 
                    "# copy_l2a_side_products: to duplicate msk_cldprb_20m and scl_20m from l2a folder to cloudmask folder after l2a production.", 
                    "#   Interesting if you plan to remove l2a to save disk space, but want to keep these 2 files for cloudmask generation and better extraction", 
                    "#   Possible values: True | False", 
                    "# clean_before: set to False or True to call a Libray.clean(remove=True) on selected tiles", 
                    "#   before starting the job, default True", 
                    "# clean_before: set to False or True call a Libray.clean(remove=True) on selected tiles", 
                    "#   after finishing the job, default True", 
                    "# process_by_line: split job execution into n lines for processing of large datasets ", 
                    "#", 
                    "### Table of task entries", 
                    '# Each record line starting with "!" can be commented (and skipped)',
                    "# tile: tile identifier, format ##XXX, comment line using ! before tile name", 
                    "# date_min the start date for this task, possible values:", 
                    "#    empty (2015-01-01 will be used) | any date | today-xx (xx nb of days before today to consider)", 
                    "# date_max the last date for this task, possible values:", 
                    "#    empty (9999-12-31 will be used) | any date | today", 
                    "# max_clouds: max cloud cover to consider for downloading images, computing l2a, cloudmask and indice products", 
                    "# provider: the provider where to get the data (l1c/l2a), possible values:",
                    "#    empty (peps will be used) | peps | cop_dataspace",
                    "# pb_min the minimum processing baseline for this task, possible values:",
                    "#    empty (0 will be used) | any positive float number",
                    "# pb_max the maximum processing baseline for this task, possible values:",
                    "#    empty (98 will be used) | any positive float number",
                    "# download: product type to download: l1c|l2a|False", 
                    "# compute_l2a: computing l2a using sen2chain / sen2cor: True | False", 
                    "# cloudmasks: the cloudmask(s) to compute and use to mask indice(s). Possible values range from none (False) to multiple cloudmasks:", 
                    "#    empty or False | CM001/CM002/CM003-PRB1-ITER5/CM004-CSH1-CMP1-CHP1-TCI1-ITER0/etc.", 
                    "# indices: empty or False | All | NDVI/NDWIGAO/etc.", 
                    "# remove: used to remove L1C and L2A products, considering only new products (-new) (dowloaded or produced) or the whole time serie (-all)", 
                    "#    and with possible filtering to remove products above specified cloud couver (-ccXX)", 
                    "#    example of possible values: empty or False | l1c-new-cc80 | l1c-all | l2a-new | l1c-all/l2a-new-cc25", 
                    "# comments: free user comments, ie tile name, etc.", 
                    "",
                    "",
                ]
            )
            header = "\n".join(
                [
                    "logs = " + str(self.logs),
                    "timing = " + self.timing,
                    # "provider = " + self.provider,
                    "tries = " + str(self.tries),
                    "sleep = " + str(self.sleep),
                    "nb_proc = " + str(self.nb_proc),
                    "copy_l2a_sideproducts = " + str(self.copy_l2a_sideproducts),
                    "clean_before = " + str(self.clean_before),
                    "clean_after = " + str(self.clean_after),
                    "process_by_line = " + str(self.process_by_line),
                    "",
                    "",
                ]
            )
            
            for line in comments_header:
                ict.write(line)
            
            for line in header:
                ict.write(line)
            # self.tasks.to_csv(ict)
            self.unformat_cm_indices()
            self.unformat_remove()
            self.tasks.to_csv(ict, index=False, sep=";", columns = self.cols)
            self.format_cm_indices() 
            self.format_remove()
            
    def get_cron_status(self):
        """
        Returns job cron status (present/absent/enabled/disabled).
        """
        iter = list(self._cron.find_comment("sen2chain_job_" + self.jid))
        if iter:
            for job in iter:
                if job.is_enabled():
                    status = "enabled"
                else:
                    status = "disabled"
                self.cron_job = job
                self.cron_timing = str(self.cron_job.slices)
        else:
            status = "absent"
            self.cron_job = None
            self.cron_timing = None
        return status

    def create_python_script(self):
        """
        Creates python script for cron.
        """
        lines = ["# -*- coding:utf-8 -*-\n"]
        lines.append("\n")
        lines.append('"""\n')
        lines.append("Module for managing sen2chain processing jobs\n")
        lines.append('"""\n')
        lines.append("\n")
        lines.append("from sen2chain import Job\n")
        lines.append(
            'Job("'
            + self.jid
            + '").run(clean_before = False, clean_after = False)\n'
        )
        with open(str(self._python_script_path), "w") as f:
            f.writelines(lines)

    def cron_enable(self):
        """
        Enables job in cron.
        """
        self.save()
        self.create_python_script()
        iter = list(self._cron.find_comment("sen2chain_job_" + self.jid))
        if iter:
            for job in iter:
                logger.info("Enabling job")
                if self.timing:
                    job.setall(self.timing)
                job.enable()
        else:
            job = self._cron.new(
                command=sys.executable + " " + str(self._python_script_path),
                comment="sen2chain_job_" + self.jid,
            )
            if self.timing:
                job.setall(self.timing)
            else:
                job.setall("0 0 * * *")
            job.enable()
            logger.info("Enabling job...")

        # logger.info("Time: {}".format(job.time))
        self._cron.write()
        # new.enable(False)
        self.get_cron_status()

    def cron_disable(self):
        """
        Disable job in cron. The corresponding line is commented in cron.
        """
        iter = list(self._cron.find_comment("sen2chain_job_" + self.jid))
        if iter:
            for job in iter:
                logger.info("Disabling job...")
                job.enable(False)
            self._cron.write()
            self.get_cron_status()

    def cron_remove(self):
        """
        Remove job from cron. The corresponding line in cron file is deleted.
        """
        iter = list(self._cron.find_comment("sen2chain_job_" + self.jid))
        if iter:
            for job in iter:
                logger.info("Removing job from cron...")
                self._cron.remove(job)
            self._cron.write()
            self.get_cron_status()

    def read(self, path):
        parser = ConfigParser(allow_no_value=True, strict = False)
        with open(str(path)) as stream:
            parser.read_string(
                "[top]\n" + stream.read(),
            )  # This line does the trick.
        self.logs = bool(
            setuptools.distutils.util.strtobool(
                parser["top"]["logs"]
            )
        )
        self.timing = parser["top"]["timing"]
        try:
            self.provider = parser["top"]["provider"]
        except Exception:
            self.provider = "peps"
        try:
            self.tries = int(parser["top"]["tries"])
        except Exception:
            self.tries = 1  
        try:
            self.sleep = int(parser["top"]["sleep"])
        except Exception:
            self.sleep = 0
        try:
            self.nb_proc = int(parser["top"]["nb_proc"])
        except Exception:
            self.nb_proc = 8
        try:
            self.copy_l2a_sideproducts = bool(
                setuptools.distutils.util.strtobool(
                    parser["top"]["copy_l2a_sideproducts"]
                )
            )
        except Exception:
            self.copy_l2a_sideproducts = False
        try:
            self.clean_before = bool(setuptools.distutils.util.strtobool(
                    parser["top"]["clean_before"]
                )
            )
        except Exception:
            self.clean_before = True
        try:
            self.clean_after = bool(setuptools.distutils.util.strtobool(
                    parser["top"]["clean_after"]
                )
            )
        except Exception:
            self.clean_after= True
        try:
            self.process_by_line = int(parser["top"]["process_by_line"])
        except Exception:
            self.process_by_line = 0         
        
        max_val = 20
        for i in range(1, max_val):
            if i == max_val:
                logger.info("Probable issue while reading job...")
            try:
                self.tasks = pd.read_csv(
                    path,
                    sep=";",
                    na_values="",
                    na_filter=False,
                    comment="#",
                    dtype=str,
                    header = i,
                    skip_blank_lines = True,
                )
                if 'comments' not in self.tasks:
                    self.tasks['comments'] = ""
                if 'cloudmask' in self.tasks:
                    self.tasks.rename(
                        columns={"cloudmask": "cloudmasks"}, 
                        inplace=True,
                    )
                if 'l1c' in self.tasks:
                    self.tasks.rename(
                        columns={"l1c": "download"}, 
                        inplace=True,
                    )
                if 'l2a' in self.tasks:
                    self.tasks.rename(
                        columns={"l2a": "compute_l2a"}, 
                        inplace=True,
                    )
                if 'provider' not in self.tasks:
                    self.tasks['provider'] = self.provider

                self.tasks = self.tasks[self.cols]
                self.format_cm_indices()  
                self.format_remove() 
                self.format_download()
                self.format_compute_l2a()
                self.format_provider()
                self.format_dates()
                self.format_max_clouds()
                self.check_tasks()                
                break
            except Exception:
                pass
                
    def format_cm_indices(self):
        for index, row in self.tasks.iterrows():
            if not row.indices in self.false_list:
                if row.indices == "All":
                    self.tasks.at[
                        index, "indices"
                    ] = IndicesCollection.available_indices
                else:
                    if not isinstance(self.tasks.at[index, "indices"] , list):
                        self.tasks.at[index, "indices"] = str(row.indices).split(
                            "/"
                        )
            else:
                self.tasks.at[index, "indices"] = "False"
            # if self.tasks.at[index, "indices"]:
                # row.indices = [ind for ind in row.indices if ind != ""]            
               
            if not row.cloudmasks in self.false_list:
                if not isinstance(self.tasks.at[index, "cloudmasks"] , list):
                    self.tasks.at[index, "cloudmasks"] = str(row.cloudmasks).split("/")
            else:
                self.tasks.at[index, "cloudmasks"] = "False"
            # row.cloudmasks = [cm for cm in row.cloudmasks if cm != ""]
            
    def unformat_cm_indices(self):
        for index, row in self.tasks.iterrows():
            if not((row.indices == "False") or (row.indices == "All")):
                # logger.info(row.indices)
                self.tasks.at[index, "indices"] = '/'.join(self.tasks.at[index, "indices"])   
            if not (row.cloudmasks == "False"):
                self.tasks.at[index, "cloudmasks"] = '/'.join(self.tasks.at[index, "cloudmasks"])   
    
    def format_remove(self):
        for index, row in self.tasks.iterrows():
            if not row.remove in self.false_list:
                if not isinstance(self.tasks.at[index, "remove"] , list):
                    self.tasks.at[index, "remove"] = str(row.remove).split(
                        "/"
                    )
                    self.tasks.at[index, "remove"] = [b + "-new" if b == "l1c" or b == "l2a" else b for b in self.tasks.at[index, "remove"]]
            else:
                self.tasks.at[index, "remove"] = "False"                
                    
    def unformat_remove(self):
        for index, row in self.tasks.iterrows():
            if not (row.remove == "False"):
                self.tasks.at[index, "remove"] = '/'.join(self.tasks.at[index, "remove"])  
    
    def format_download(self):
        for index, row in self.tasks.iterrows():
            if row.download == "True":
                self.tasks.at[index, "download"] = "l1c" 
            elif not row.download in self.false_list:
                pass
            else:
                self.tasks.at[index, "download"] = "False" 
    
    def format_compute_l2a(self):
        for index, row in self.tasks.iterrows():
            if row.compute_l2a in self.false_list:
                self.tasks.at[index, "compute_l2a"] = "False" 
    
    def format_provider(self):
        for index, row in self.tasks.iterrows():
            if row.provider in self.false_list:
                self.tasks.at[index, "provider"] = "peps" 
    
    def format_max_clouds(self):
        for index, row in self.tasks.iterrows():
            if row.max_clouds in self.false_list:
                self.tasks.at[index, "max_clouds"] = 100

    def format_dates(self):
        for index, row in self.tasks.iterrows():
            if row.date_min in self.false_list:
                self.tasks.at[index, "date_min"] = "2015-01-01"
            if row.date_max in self.false_list:
                self.tasks.at[index, "date_max"] = "9999-12-12"
            
    
    def fill_dates_clouds(
        self,
        tasks, 
    ):
        """
        Fill in dates and cloud cover values with correct values.
        """
        for index, row in tasks.iterrows():
            if not row.max_clouds:
                tasks.at[index, "max_clouds"] = 100
            else:
                tasks.at[index, "max_clouds"] = int(tasks.at[index, "max_clouds"])
            if not row.date_min:
                # self.tasks.at[index, "start_time"] = (datetime.datetime.now()-datetime.timedelta(days=delta_t)).strftime('%Y-%m-%d')
                tasks.at[index, "date_min"] = datetime.datetime.strptime(
                    "2015-01-01", "%Y-%m-%d"
                ).strftime("%Y-%m-%d")
            elif "today" in row.date_min:
                tasks.at[index, "date_min"] = (
                    datetime.datetime.now() - datetime.timedelta(
                        days = int(row.date_min.split("-")[1])
                    )
                ).strftime("%Y-%m-%d") 
            if not row.date_max:
                tasks.at[index, "date_max"] = datetime.datetime.strptime(
                    "9999-12-31", "%Y-%m-%d"
                ).strftime("%Y-%m-%d")
            elif row.date_max == "today":
                tasks.at[index, "date_max"] = (
                    datetime.datetime.now() + datetime.timedelta(days=1)
                ).strftime("%Y-%m-%d")   
        return tasks

    
    @staticmethod
    def get_cm_version(identifier) -> str:
        """
        Returns cloudmask version from a cloud mask identifier string.
        
        :param identifier: string pattern from which the cloud mask version is extracted.
        :type identifier: str
        """
        try:
            pat = re.compile(r"(?P<cm_version>CM00[1-2])")
            returned_val = pat.match(identifier).groupdict()
        except Exception:
            try:
                pat = re.compile(
                    r"(?P<cm_version>CM003)"
                    + "-PRB(?P<probability>.*)"
                    + "-ITER(?P<iterations>.*)"
                )
                returned_val = pat.match(identifier).groupdict()
            except Exception:
                try:
                    pat = re.compile(
                        r"(?P<cm_version>CM004)"
                        + "-CSH(?P<cld_shad>.*)"
                        + "-CMP(?P<cld_med_prob>.*)"
                        + "-CHP(?P<cld_hi_prob>.*)"
                        + "-TCI(?P<thin_cir>.*)"
                        + "-ITER(?P<iterations>.*)"
                    )
                    returned_val = pat.match(identifier).groupdict()
                except Exception:
                    returned_val = {
                        "cm_version": "CM001",
                        "probability": 1,
                        "iterations": 5,
                        "cld_shad": True,
                        "cld_med_prob": True,
                        "cld_hi_prob": True, 
                        "thin_cir": True, 
                    }        
        

        if "probability" not in returned_val:
            returned_val["probability"] = 1
        if "iterations" not in returned_val:
            returned_val["iterations"] = 5
        if "cld_shad" not in returned_val:
            returned_val["cld_shad"] = True
        if "cld_med_prob" not in returned_val:
            returned_val["cld_med_prob"] = True
        if "cld_hi_prob" not in returned_val:
            returned_val["cld_hi_prob"] = True
        if "thin_cir" not in returned_val:
            returned_val["thin_cir"] = True
        
        returned_val["probability"] = int(returned_val["probability"])
        returned_val["iterations"] = int(returned_val["iterations"])
        returned_val["cld_shad"] = returned_val["cld_shad"] in ["1", 1, True, "True"] 
        returned_val["cld_med_prob"] = returned_val["cld_med_prob"] in ["1", 1, True, "True"]
        returned_val["cld_hi_prob"] = returned_val["cld_hi_prob"] in ["1", 1, True, "True"]
        returned_val["thin_cir"] = returned_val["thin_cir"] in ["1", 1, True, "True"]

        return returned_val
    
    def _get_ref(
        self,
        index,
        row,
    ):
        max_cloudcover = 0
        ref = []
        # if bool(setuptools.distutils.util.strtobool(str(row.l1c))):
        if (row.download == "l1c") or (row.download == "l2a"):
            if not row.remove in self.false_list:
                if bool(setuptools.distutils.util.strtobool(str(row.compute_l2a))):
                    # row.cloudmasks = [cm for cm in row.cloudmasks if cm != ""]
                    # row.indices = [ind for ind in row.indices if ind != ""]
                    if not(row.indices in self.false_list) or not(row.cloudmasks in self.false_list):                    
                        if isinstance(self.tasks.at[index, "cloudmasks"] , list):
                            for cm in row.cloudmasks:
                                ref.append(cm)
                        if isinstance(self.tasks.at[index, "indices"] , list):
                            for ind in row.indices:
                                ref.extend([ind])
                                if isinstance(self.tasks.at[index, "cloudmasks"] , list):
                                    for cm in row.cloudmasks:
                                        ref.extend([ind + "_" + cm])
                        ref = list(set(ref))
                        max_cloudcover = int(row.max_clouds)
                    else:
                        if "l2a" not in "/".join(row.remove).lower():
                            ref = ["l2a"]
                            max_cloudcover = int(row.max_clouds)
                        elif "l1c" not in "/".join(row.remove).lower():
                            ref = ["l1c"]
                            max_cloudcover = int(row.max_clouds)
                        else:
                            for rm in row.remove:
                                if "-cc" in rm:
                                    cloudcover = int(rm.split("-cc")[1])
                                else:
                                    cloudcover = 0
                                if cloudcover >= max_cloudcover:
                                    max_cloudcover = cloudcover
                                    ref = rm.split("-")[0]                        
                else:
                    ref = ["l1c"]
                    for rm in row.remove:
                        if "-cc" in rm and rm.split("-")[0] == "l1c":
                            cloudcover = int(rm.split("-cc")[1])
                        else:
                            cloudcover = 0
                        if cloudcover >= max_cloudcover:
                            max_cloudcover = cloudcover
                if int(row.max_clouds) > max_cloudcover:
                    logger.info(
                        "Download not optimal for line {}, downloading useless l1c:\n{}".format(
                            index, 
                            self.tasks.iloc[index:index+1]
                        )
                    )
            else:
                if (row.download == "l1c"):
                    ref = ["l1c"]
                else:
                    ref = ["l2a"]
        return ref
    
    def check_tasks(
        self,
    ):
        if 'ref' not in self.tasks:
            self.tasks['ref'] = ""
        for index, row in self.tasks.iterrows():
            self.tasks.at[index, "ref"] = self._get_ref(index, row)
                
    def run(
        self,
        # nb_proc: int = self.nb_proc,
        clean_before: bool = False,
        clean_after: bool = False,
    ):
        """
        Runs job immediatly. Job needs to be saved first.
        Tasks are performed line by line.
        
        :param clean_before: When True, clean the library of every tile in tasks before launching job.
        :type clean_before: bool, Default to False
        :param clean_after: When True, clean the library of every tile in tasks at the end of job processing.
        :type clean_after: bool, Default to False
        """
        
        tasks = copy.deepcopy(self.tasks)
        tasks = self.fill_dates_clouds(tasks)
        
        if self.provider == "cop_dataspace":
            nb_threads = 4
        else:
            nb_threads = 8
        
        if self.logs:
            self._log_folder_path.mkdir(exist_ok=True)
            self._log_file = self._log_folder_path / (
                "job_"
                + self.jid
                + "_run_"
                + datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
                + ".log"
            )
            f = open(str(self._log_file), "w")
            f.write("{}\nStart\n\n".format(datetime.datetime.now()))
            f.write(repr(self) + "\n\n")
            f.flush()
        
        # tasks = self.tasks
        tasks = tasks[tasks["tile"].str.contains("!") == False]
        logger.info("Running task(s):\n{}".format(tasks))
        logger.info("Processing by lines: {}".format(self.process_by_line))
        
        # list_df = [df[i:i+n] for i in range(0,len(df),n)]
        
        if not tasks.empty:
            ## Cleaning before
            if clean_before or self.clean_before:
                logger.info("Cleaning Tiles")
                if self.logs:
                    f.write("{} - Cleaning Tiles\n\n".format(datetime.datetime.now()))
                    f.flush()
                
                clean_list = []
                for index, row in tasks.iterrows():
                    clean_list.append(row.tile)
                lib = Library()
                pb_before = lib.clean(
                    clean_list, remove=True, remove_indice_tif=True
                )
                # lib.clean(clean_list, remove=False)
            
            if self.process_by_line > 0:
                split_tasks = np.array_split(
                    tasks, 
                    tasks.shape[0] // self.process_by_line + min(1, tasks.shape[0] % self.process_by_line)
                )
            else:
                split_tasks = [tasks]
            for chunk in split_tasks:

                ## L1C download - all tiles multi
                logger.info("Downloading l1c products")
                if self.logs:
                    f.write("{}\nDownloading l1c products\n".format(datetime.datetime.now()))
                    f.flush()
                
                all_downloaded_products = []
                for essai in range(self.tries):
                    logger.info("Download try #{}/{}".format(essai+1, self.tries))
                    if self.logs:
                        f.write("Download try #{}/{}\n".format(essai+1, self.tries))
                        f.flush()
                    ## Get new product list to download
                    download_list= []
                    for index, row in chunk.iterrows():
                        ref = self._get_ref(index, row)
                        # if bool(setuptools.distutils.util.strtobool(str(row.download))):
                        if not row.download in self.false_list:
                            tile_download_list = []
                            if row.download == "l1c":
                                t = Tile(row.tile)
                                tile_download_list = t.get_l1c(
                                    provider = row.provider,
                                    download = False,
                                    start = row.date_min,
                                    end = row.date_max,
                                    new = False,
                                    ref = ref,
                                    max_cloudcover = row.max_clouds,
                                )
                            if row.download == "l2a":
                                t = Tile(row.tile)
                                tile_download_list = t.get_l2a(
                                    provider = row.provider,
                                    download = False,
                                    start = row.date_min,
                                    end = row.date_max,
                                    new = False,
                                    ref = ref,
                                    max_cloudcover = row.max_clouds,
                                )
                            download_list.extend(tile_download_list)
                            logger.info("Checked tile: {} - {} new product(s) to download".format(t.name, len(tile_download_list)))
                            if self.logs:
                                if len(tile_download_list):
                                    f.write("{}\n{} - {} new product(s) to download\n".format(
                                        datetime.datetime.now(),
                                        t.name, 
                                        len(tile_download_list)), 
                                    )
                                    f.flush()
                    # seen = set()
                    result = {}
                    for eop in download_list:
                        result.update({eop.properties["title"]: eop})
                    download_list = [eop for eop in result.values()]
                    download_list_peps = [eop for eop in result.values() if eop.provider == "peps"]
                    download_list_cop_dataspace = [eop for eop in result.values() if eop.provider == "cop_dataspace"]
                                        
                    # logger.info("download_list: {}".format(download_list))
                    # logger.info("download_list_peps: {}".format(download_list_peps))
                    # logger.info("download_list_cop_dataspace: {}".format(download_list_cop_dataspace))
                    
                    ### download 
                    before_list = [p.location for p in download_list]
                    if essai == 0:
                        total_before_list = [p.location for p in download_list]
                    logger.info(
                        "{} product(s) to download ({} on PEPS, {} on COP_DATASPACE)".format(
                            len(download_list), len(download_list_peps), len(download_list_cop_dataspace)
                        )
                    )
                    if self.logs:
                        f.write("Total: {} product(s) to download\n".format(len(download_list)))
                        f.flush()
                    # S2cEodag_download(download_list, "seq")
                    
                    #### Multithreading single provider - deprecated
                    # S2cEodag_download(download_list, "multit", nb_threads=self.provider)
                    
                    #### Multithreading multiproviders
                    threads = [
                        Thread(
                            target = S2cEodag_download, 
                            args = (download_list_peps, "multit",), 
                            kwargs={'nb_threads': 8}
                        ),
                        Thread(
                            target = S2cEodag_download, 
                            args = (download_list_cop_dataspace, "multit",),
                            kwargs = {'nb_threads': 4}
                        ),
                    ]
                    for thread in threads:
                        thread.daemon = True
                        thread.start()
                    for thread in threads:
                        thread.join()
                    
                               
                    # S2cEodag_download(download_list, "multip")
                    after_list = [p.location for p in download_list]
                    downloaded_products = [Path(after_list[i]).stem for i in range(len(before_list)) if before_list[i] != after_list[i]]
                    all_downloaded_products.extend(downloaded_products)
                    if self.logs:
                        f.write("{}\n".format(datetime.datetime.now()))
                        f.flush()       
                    if len(downloaded_products) == len(before_list):
                        logger.info("Downloaded all {} product(s)".format(len(downloaded_products), downloaded_products))
                        if downloaded_products:
                            logger.info("{}".format(downloaded_products))
                        if self.logs:
                            f.write("Downloaded all {} product(s)\n".format(len(downloaded_products)))
                            if downloaded_products:
                                for pro in downloaded_products:
                                    f.write("{}\n".format(pro))
                                f.write("\n")
                            f.flush()                    
                        break
                    else:
                        logger.info("Downloaded {}/{} product(s)".format(len(downloaded_products), len(before_list)))
                        if self.logs:
                            f.write("Downloaded {}/{} product(s)\n".format(len(downloaded_products), len(before_list)))
                            if downloaded_products:
                                for pro in downloaded_products:
                                    f.write("{}\n".format(pro))
                            if essai == self.tries-1:
                                f.write("\n")
                            f.flush()                    
                    if (self.tries > 1) and (essai < (self.tries-1)):
                        logger.info("Sleeping {}min before retrying".format(self.sleep))
                        if self.logs:
                            f.write("Sleeping {}min before retrying\n".format(self.sleep))
                            f.flush()
                        time.sleep(self.sleep*60)
                
                # remove duplicates from all_downloaded_products list
                all_downloaded_products.sort()
                uniq = list(k for k,_ in groupby(all_downloaded_products))
                all_downloaded_products = uniq
                
                logger.info(
                    "Total {}/{} downloaded product(s)".format(
                        len(all_downloaded_products),
                        len(total_before_list),
                    )
                )
                if self.logs:
                    f.write(
                        "Total {}/{} downloaded product(s)\n\n".format(
                            len(all_downloaded_products),
                            len(total_before_list),
                        )
                    )
                    f.flush()
                
                # Traitement des L1C en L2A
                time.sleep(30)    # was 90 don't know why... 
                logger.info("Computing l2a")
                if self.logs:
                    f.write("{}\nComputing l2a\n".format(datetime.datetime.now()))
                    f.flush()

                l1c_process_list = []
                for index, row in chunk.iterrows():
                    if bool(setuptools.distutils.util.strtobool(str(row.compute_l2a))):
                        t = Tile(row.tile)
                        l1c_to_process = list(
                            [
                                p.identifier,
                                self.copy_l2a_sideproducts,
                                self.remove_used_l1c,
                            ]
                            for p in t.l2a_missings.filter_dates(
                                date_min=row.date_min, date_max=row.date_max
                            ).filter_clouds(cover_max = row.max_clouds)
                        )
                        logger.info(
                            "{} adding {} l1c file(s)".format(
                                row.tile, len(l1c_to_process)
                            )
                        )
                        l1c_process_list.extend(l1c_to_process)
                        
                # l1c_process_list = list(
                    # set(
                        # chain.from_iterable(l1c_process_list)
                    # )
                # )
                
                ### remove duplicate l1c from l1c_process_list
                l1c_process_list.sort()
                uniq = list(k for k,_ in groupby(l1c_process_list))
                l1c_process_list = uniq
                
                logger.info(
                    "Process list ({} files): {}".format(
                        len(l1c_process_list), l1c_process_list
                    )
                )
                if self.logs:
                    f.write(
                        "Process list ({} files): {}\n\n".format(
                            len(l1c_process_list), l1c_process_list
                        )
                    )
                    f.flush()
                
                ### Computing L2A
                l2a_res = False
                if l1c_process_list:
                    l2a_res = l2a_multiprocessing(
                        l1c_process_list, nb_proc = self.nb_proc
                    )
                
                # Remove L1C (newly downloaded or all time series according to cloud cover filter)
                for index, row in chunk.iterrows():
                    if not (row.remove == "False" or not row.remove):
                        for rm in row.remove:
                            if "-cc" in rm:
                                cloudcover_above = int(rm.split("-cc")[1])
                            else:
                                cloudcover_above = 0
                            if "l1c-new" in rm.lower():
                                t = Tile(row.tile)
                                prodlist = [p for p in all_downloaded_products if row.tile in p]
                                prodlist_cleaned = [p for p in prodlist if p in [Path(q.identifier).stem for q in t.l1c]]
                                prodlist_skipped = [p for p in prodlist if p not in [Path(q.identifier).stem for q in t.l1c]]
                                prodlist_processed = [p for p in prodlist_cleaned if p.replace("L1C", "L2A") in [Path(q.identifier).stem for q in t.l2a]]
                                prodlist_unprocessed = [p for p in prodlist_cleaned if p.replace("L1C", "L2A") not in [Path(q.identifier).stem for q in t.l2a]]
                                logger.info(
                                    "{} removing downloaded l1c product(s) with cloud cover above {}: {}\n".format(
                                        row.tile,
                                        cloudcover_above,
                                        prodlist_processed,
                                    )
                                )
                                if prodlist_skipped:
                                    logger.info(
                                        "{} skipping {} l1c product(s), download error?:\n{}\n".format(
                                            row.tile,
                                            len(prodlist_skipped),
                                            prodlist_skipped,
                                        )
                                    ) 
                                if prodlist_unprocessed:
                                    logger.info(
                                        "{} {} l1c product(s) not processed to l2a:\n{}\n".format(
                                            row.tile,
                                            len(prodlist_unprocessed),
                                            prodlist_unprocessed,
                                        )
                                    ) 
                                if self.logs:
                                    f.write(
                                        "{}\nRemoving downloaded l1c products with cloud cover above {} for tile: {}\n{}\n".format(
                                            datetime.datetime.now(), 
                                            cloudcover_above,
                                            row.tile,
                                            prodlist_processed,
                                        )
                                    )
                                    if prodlist_skipped:
                                        f.write(
                                            "\nSkipping {} l1c product(s), download error?:\n{}\n".format(
                                                len(prodlist_skipped),
                                                prodlist_skipped,
                                            )
                                        )
                                    if prodlist_unprocessed:
                                        f.write(
                                            "\n{} l1c product(s) not processed to l1a:\n{}".format(
                                                len(prodlist_unprocessed),
                                                prodlist_unprocessed,
                                            )
                                        )
                                    else:
                                        f.write("\n")
                                    f.flush()                      
                                t.remove_l1c_filter(l1c_list = prodlist_processed, cloudcover_above = cloudcover_above, remove = True)
                                
                            if "l1c-all" in rm.lower():
                                t = Tile(row.tile)
                                logger.info(
                                    "{} removing all l1c product(s) with cloud cover above {}".format(
                                        row.tile,
                                        cloudcover_above,
                                    )
                                )
                                if self.logs:
                                    f.write(
                                        "{}\nRemoving all l1c products with cloud cover above {} for tile: {}\n\n".format(
                                            datetime.datetime.now(), 
                                            cloudcover_above,
                                            row.tile,
                                        )
                                    )
                                    f.flush()                      
                                t.remove_l1c_filter(cloudcover_above = cloudcover_above, remove = True)
                
                # Comuting cloudmasks (from L2A)
                logger.info("Computing cloudmasks")
                if self.logs:
                    f.write("{}\nComputing cloudmasks\n".format(datetime.datetime.now()))
                    f.flush()
                reprocess = False
                cld_l2a_process_list = []
                for index, row in chunk.iterrows():
                    # if not bool(distutils.util.strtobool(str(row.cloudmask))):
                    if not (row.cloudmasks == "False" or not row.cloudmasks):
                        for cm in row.cloudmasks:
                            cloudmask = self.get_cm_version(cm)                    
                            t = Tile(row.tile)
                            l2a_to_process = [
                                p.identifier
                                for p in t.cloudmasks_missing(
                                    cm_version = cloudmask["cm_version"].upper(),
                                    probability = cloudmask["probability"],
                                    iterations = cloudmask["iterations"],
                                    cld_shad = cloudmask["cld_shad"],
                                    cld_med_prob = cloudmask["cld_med_prob"],
                                    cld_hi_prob = cloudmask["cld_hi_prob"],
                                    thin_cir = cloudmask["thin_cir"],                       
                                ).filter_dates(
                                    date_min=row.date_min, date_max=row.date_max
                                )
                            ]
                            for j in l2a_to_process:
                                l2a_cm_details = [
                                    j,
                                    cloudmask["cm_version"].upper(),
                                    cloudmask["probability"],
                                    cloudmask["iterations"],
                                    cloudmask["cld_shad"],
                                    cloudmask["cld_med_prob"],
                                    cloudmask["cld_hi_prob"],
                                    cloudmask["thin_cir"],
                                    reprocess,
                                ]
                                cld_l2a_process_list.append(l2a_cm_details)
                            if len(l2a_to_process):
                                logger.info(
                                    "{} - {} - {} l2a product(s) to process".format(
                                        row.tile, 
                                        cm,
                                        len(l2a_to_process),
                                    )
                                )
                                if self.logs:
                                    if len(l2a_to_process):
                                        f.write(
                                            "{} - {} - {} l2a product(s) to process\n".format(
                                                row.tile, 
                                                cm,
                                                len(l2a_to_process),
                                            )
                                        )
                                        f.flush()
                # removing duplicate cloudmasks before computing
                cld_l2a_process_list.sort()
                uniq = list(k for k,_ in groupby(cld_l2a_process_list))
                cld_l2a_process_list = uniq
                
                logger.info(
                    "Cloudmask computing process list ({} l2a products): {}".format(
                        len(cld_l2a_process_list), cld_l2a_process_list
                    )
                )
                if self.logs:
                    f.write(
                        "Cloudmask computing process list ({} l2a products):\n".format(
                            len(cld_l2a_process_list),                     )
                    )
                    for cld in cld_l2a_process_list:
                        f.write("{}\n".format(cld))
                    f.write("\n")
                    f.flush()
                
                cld_res = False
                if cld_l2a_process_list:
                    cld_res = cld_version_probability_iterations_reprocessing_multiprocessing(
                        cld_l2a_process_list, nb_proc = self.nb_proc
                    )
                
                # Traitement des L2A (indices)
                logger.info("Computing indices")
                if self.logs:
                    f.write("{}\nComputing indices\n".format(datetime.datetime.now()))
                    f.flush()
                
                quicklook = False
                indices_l2a_process_list = []
                for index, row in chunk.iterrows():
                    if not (row.indices == "False" or not row.indices):
                        logger.info("row.indices: {}".format(row.indices))
                        nodata_clouds = not (row.cloudmasks == "False" or not row.cloudmasks)
                        if nodata_clouds:
                            for cm in row.cloudmasks:
                                cloudmask = self.get_cm_version(cm) 
                                t = Tile(row.tile)
                                for i in row.indices:
                                    l2a_to_process = [
                                        p.identifier
                                        for p in t.missing_indices(
                                            indice=i,
                                            nodata_clouds=nodata_clouds,
                                            cm_version = cloudmask["cm_version"].upper(),
                                            probability = cloudmask["probability"],
                                            iterations = cloudmask["iterations"],
                                            cld_shad = cloudmask["cld_shad"],
                                            cld_med_prob = cloudmask["cld_med_prob"],
                                            cld_hi_prob = cloudmask["cld_hi_prob"],
                                            thin_cir = cloudmask["thin_cir"],                                  
                                        ).filter_dates(
                                            date_min=row.date_min, date_max=row.date_max
                                        )
                                    ]
                                    for j in l2a_to_process:
                                        l2a_ind_details = [
                                            j,
                                            i,
                                            reprocess,
                                            nodata_clouds,
                                            quicklook,
                                            cloudmask["cm_version"].upper(),
                                            cloudmask["probability"],
                                            cloudmask["iterations"],
                                            cloudmask["cld_shad"],
                                            cloudmask["cld_med_prob"],
                                            cloudmask["cld_hi_prob"],
                                            cloudmask["thin_cir"],
                                        ]
                                        indices_l2a_process_list.append(l2a_ind_details)
                                    if len(l2a_to_process):
                                        logger.info(
                                            "{} - {} - {} - {} l2a product(s) to process".format(
                                                row.tile, 
                                                cm,
                                                i, 
                                                len(l2a_to_process)
                                            )
                                        )
                                        if self.logs:
                                            if len(l2a_to_process):
                                                f.write(
                                                    "{} - {} - {} - {} l2a product(s) to process\n".format(
                                                        row.tile, 
                                                        cm, 
                                                        i,
                                                        len(l2a_to_process),
                                                    )
                                                )
                                                f.flush()
                                    
                # removing duplicate indice computing
                indices_l2a_process_list.sort()
                uniq = list(k for k,_ in groupby(indices_l2a_process_list))
                indices_l2a_process_list = uniq
                
                                    
                logger.info(
                    "Indice computing process list ({} l2a products): {}".format(
                        len(indices_l2a_process_list), indices_l2a_process_list
                    )
                )
                if self.logs:
                    f.write(
                        "Indice computing process list ({} l2a products): \n".format(
                            len(indices_l2a_process_list),
                        )
                    )
                    for ind in indices_l2a_process_list:
                        f.write("{}\n".format(ind))
                    f.write("\n")
                    f.flush()            
                
                indices_res = False
                if indices_l2a_process_list:
                    indices_res = idx_multiprocessing(
                        indices_l2a_process_list, nb_proc = self.nb_proc
                    )

                # Remove L2A
                l2a_remove_list = [
                    l1c[0].replace(
                        "L1C_", "L2A_"
                    ).replace(
                        "_OPER_", "_USER_"
                    ).replace(
                        ".SAFE", ""
                    ) for l1c in l1c_process_list
                ]
                for index, row in chunk.iterrows():
                    if not (row.remove == "False" or not row.remove):
                        for rm in row.remove:
                            if "-cc" in rm:
                                cloudcover_above = int(rm.split("-cc")[1])
                            else:
                                cloudcover_above = 0
                            if "l2a-new" in rm.lower():
                                t = Tile(row.tile)
                                prodlist = [p for p in l2a_remove_list if row.tile in p]
                                prodlist_cleaned = [
                                    p for p in prodlist if p in [Path(q.identifier).stem for q in t.l2a]
                                ]
                                prodlist_skipped = [
                                    p for p in prodlist if p not in [Path(q.identifier).stem for q in t.l2a]
                                ]
                                logger.info(
                                    "{} removing produced l2a product(s) with cloud cover above {}: {}".format(
                                        row.tile,
                                        cloudcover_above,
                                        prodlist_cleaned,
                                    )
                                )
                                if prodlist_skipped:
                                    logger.info(
                                        "{} skipping {} l2a product(s), Sen2Cor error?:\n{}".format(
                                            row.tile,
                                            len(prodlist_skipped),
                                            prodlist_skipped,
                                        )
                                    )                          
                                if self.logs:
                                    f.write(
                                        "{}\nRemoving produced l2a products with cloud cover above {} for tile: {}\n{}\n".format(
                                            datetime.datetime.now(), 
                                            cloudcover_above,
                                            row.tile,
                                            prodlist_cleaned,
                                        )
                                    )
                                    if prodlist_skipped:
                                        f.write(
                                            "\nSkipping {} l2a product(s), download error?:\n{}".format(
                                                len(prodlist_skipped),
                                                prodlist_skipped,
                                            )
                                        )  
                                    else:
                                        f.write("\n")
                                    f.flush()
                                t.remove_l2a_filter(l2a_list = prodlist, cloudcover_above = cloudcover_above, remove = True)
                                
                            if "l2a-all" in rm.lower():
                                t = Tile(row.tile)
                                logger.info(
                                    "{} removing all l2a product(s) with cloud cover above {}".format(
                                        row.tile,
                                        cloudcover_above,
                                    )
                                )
                                if self.logs:
                                    f.write(
                                        "{}\nRemoving all l2a products with cloud cover above {} for tile: {}\n\n".format(
                                            datetime.datetime.now(), 
                                            cloudcover_above,
                                            row.tile,
                                        )
                                    )
                                    f.flush()                      
                                t.remove_l2a_filter(cloudcover_above = cloudcover_above, remove = True)
            
            # Cleaning after
            if clean_after or self.clean_after:
                logger.info("Cleaning Tiles")
                if self.logs:
                    f.write("{}\nCleaning Tiles\n\n".format(datetime.datetime.now()))
                    f.flush()
                
                clean_list = []
                for index, row in tasks.iterrows():
                    clean_list.append(row.tile)
                lib = Library()
                pb_before = lib.clean(
                    clean_list, remove=True, remove_indice_tif=True
                )
                # lib.clean(clean_list, remove=False)

        else:
            logger.info("No task defined for this job, doing nothing")
            if self.logs:
                f.write("No task defined for this job, doing nothing\n\n")
                f.flush()

        if self.logs:
            f.write("{}\nFin".format(datetime.datetime.now()))
            f.close()
