# coding: utf-8

"""
Module for managing products and tiles in the library and temp folders.
"""

import pathlib
import shutil
import logging
import re
import os

from pathlib import Path

# type annotations
from typing import (
    List, 
    Tuple, 
    Optional, 
    Union
)
from packaging import version
from .utils import (
    grouper,
    set_permissions,
    get_Sen2Cor_version,
    get_latest_Sen2Cor_version_from_id,
    get_Sen2Cor_path_from_version,
    get_cm_dict,
    get_cm_string_from_dict,
    get_indice_from_identifier
)
from .config import Config, SHARED_DATA
from .xmlparser import MetadataParser, Sen2ChainMetadataParser
from .sen2cor import process_sen2cor
from .cloud_mask import (
    create_cloud_mask,
    create_cloud_mask_v2,
    create_cloud_mask_b11,
    create_cloud_mask_v003,
    create_cloud_mask_v004,
)
from .indices import IndicesCollection
from .colormap import (
    create_l2a_ql,
    create_l1c_ql,
    create_l1c_ql_v2,
    create_l2a_ql_v2,
)


s2_tiles_index = SHARED_DATA.get("tiles_index")

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Product:
    """Base product template class. This class is designed to be instanciated only by sub-classes.

    :param identifier: L1C product's identifier or SAFE filename.
    :type identifier: str, Default to None
    :param tile: Tile name, format "XX123"
    :type tile: str, Default to None
    :param path: Parent SAFE folder path
    :type path: pathlib.PosixPath, Default to None
    """

    _library_path = Path("")
    _metadata_filename_templates = ()

    def __new__(cls, *args, **kwargs):
        """Prevents from direct instantiation."""
        if cls is Product:
            raise TypeError("Product may not be instantiated")
        return object.__new__(cls)

    def __init__(
        self,
        identifier: str = None,
        tile: str = None,
        path: pathlib.PosixPath = None,
    ) -> None:
        if identifier is None:
            raise ValueError("Product identifier is empty")
        else:
            self.identifier = identifier.upper().replace(".SAFE", "")

        if tile is None:
            try:
                self.tile = Product.get_tile(identifier)
                self._tiled = True
            except IndexError:
                logger.error("Tile must be specified for old products")
                raise
        else:
            self.tile = tile
            try:
                Product.get_tile(identifier)
                self._tiled = True
            except IndexError:
                self._tiled = False

        self.safe = self.identifier + ".SAFE"
        self.library_path = self._library_path / self.tile / self.safe

        if path is None:
            self.path = self.library_path
            logger.debug(
                "No path specified, using library's: {}".format(self.path)
            )
        else:
            self.path = path / self.safe

        if self.path.absolute() is self.library_path.absolute():
            self._library_product = True
        else:
            self._library_product = False

        self._metadata_path = None
        self._metadata_parser = None

    @staticmethod
    def get_tile(string) -> str:
        """Returns Tile name from a string.

        :param string: String from which to extract the tile name.
        :type string: str
        :returns: Tile name, pattern "T" + "12ABC"
        """
        return re.findall("_T([0-9]{2}[A-Z]{3})_", string)[0]

    def _update_metadata_parser(self) -> None:
        """Instanciates the metadaparser object."""
        if self._metadata_parser is None:
            try:
                self._get_metadata_path()
                self._metadata_parser = MetadataParser(
                    metadata_path=self._metadata_path, tile=self.tile
                )
            except Exception as e:
                logger.debug("{}".format(e))
                logger.error(
                    "{}: could not load metadata: {}".format(
                        self.identifier, self._metadata_path
                    )
                )

    def _get_metadata_path(self) -> None:
        """Searchs and sets the metadata path of the product."""
        logger.debug("{}: looking for metadata path".format(self.identifier))
        if self._tiled:
            filename = self._tiled_metadata
        else:
            filename = (
                self.identifier.replace("_PRD_", "_MTD_").replace(
                    "_MSI", "_SAF"
                )
                + ".xml"
            )
        metadata_path = self.path / filename
        if not metadata_path.exists():
            self._metadata_path = None
            logger.debug("{} metadata file not found".format(self.safe))
        self._metadata_path = metadata_path
        logger.debug("Found: {}".format(self._metadata_path))

    def _get_metadata_value(self, key: str) -> str:
        """Returns a metadata value according to a key.

        :param key: tag of the metadata to search.
        :type key: str
        :returns: Metadata value
        """
        self._update_metadata_parser()
        return self._metadata_parser.get_metadata_value(key=key)

    def _get_band_path(self, key: str, res=None) -> str:
        """Returns a band path according to a key.

        :param key: tag of the metadata to search.
        :type key: str
        :param res: Band resolution (ex: "60m")
        :type res: str, Default to None
        :returns: Path to band
        """
        self._update_metadata_parser()
        try:
            return self._metadata_parser.get_band_path(key=key, res=res)
        except Exception:
            return None

    @property
    def in_library(self) -> bool:
        """Is the product in the library folder."""
        return self.library_path.exists()
            
    def archive(self) -> "Product":
        """Moves the product's SAFE folder to the library folder."""
        logger.info("archiving {} to {}".format(self.path, self.library_path))
        self.library_path.parent.mkdir(exist_ok=True)
        shutil.move(str(self.path), str(self.library_path.parent))
        return self

    def __str__(self) -> Optional[str]:
        try:
            return str(self.safe)
        except TypeError:
            return None
            
    def set_permissions(self):
        try:
            set_permissions(self.path)
        except Exception:
            logger.info("Cannot set permissions to: {}".format(self.identifier))

class L1cProduct(Product):
    """L1C product class.

    :param identifier: L1C product identifier or SAFE filename
    :type identifier: str, Default to None
    :param tile: Product's tile
    :type tile: str, Default to None
    :param path: Parent folder
    :type path: str, Default to None
    """

    _library_path = Path(Config().get("l1c_path"))
    _tiled_metadata = "MTD_MSIL1C.xml"

    def __init__(
        self, identifier: str = None, tile: str = None, path: str = None
    ):
    
        super().__init__(identifier=identifier, tile=tile, path=path)
        
        if not re.match(r".*L1C_.*", self.identifier):
            raise ValueError("Invalid L1C product name")
    
    def processable_to_l2a(
        self, 
        s2c_path: Union[str, pathlib.PosixPath] = None,
    ) -> "L1cProduct":
        """ Check if L1C can be processed to L2A with Sen2Cor

         :param s2c_path: Sen2Cor path from Sen2Chain config
         :type s2c_path: str or Path, Default to None
         :returns: bool
         """
        # s2c_path = s2c_path or get_latest_s2c_version_path(self.identifier)
        s2c_path = s2c_path or get_Sen2Cor_path_from_version(get_latest_Sen2Cor_version_from_id(self.identifier))
        if s2c_path:
            return True
        else:
            logger.info("{} not processable, check your Sen2Cor install".format(self.identifier))
            return False
    
    def process_l2a(
        self, 
        reprocess: bool = False,
        s2c_path: Union[str, pathlib.PosixPath] = None,
        copy_l2a_sideproducts: bool = False,
    ) -> "L1cProduct":
        """
        Process L1C with Sen2Cor

        :param reprocess: When True, delete L2A in library before processing L1C.
        :type reprocess: bool, Default to False
        :param s2c_path: Sen2Cor path
        :type s2c_path: str or Path
        :param copy_l2a_sideproducts:
        :param copy_l2a_sideproducts: When True, copy Sen2Cor outputs Cloud Probability and Scene Classification to Cloud Mask folder. From these outputs, cloud maks can be generated without L2A in library.
        :type copy_l2a_sideproducts: bool, Default to False
        """
        logger.info("{}: processing L2A".format(self.identifier))

        l2a_identifier = self.identifier.replace("L1C_", "L2A_").replace(
            "_OPER_", "_USER_"
        )
        l2a_path = self.path.parent / (l2a_identifier + ".SAFE")
        l2a_prod = L2aProduct(l2a_identifier, self.tile, l2a_path.parent)
        
        process_it = False
        if not l2a_prod.in_library and not l2a_path.exists():
            # s2c_path = s2c_path or get_latest_s2c_version_path(self.identifier)
            s2c_path = s2c_path or get_Sen2Cor_path_from_version(get_latest_Sen2Cor_version_from_id(self.identifier))
            
            if s2c_path:
                process_it = True
            else:
                logger.info("{} not processable, check your Sen2Cor install".format(self.identifier))
        else:
            if not reprocess:
                logger.info("{} already exists.".format(l2a_identifier))
            
            else:
                if l2a_prod.in_library:
                    shutil.rmtree(str(l2a_prod.path))
                    logger.info("Deleted {}".format(l2a_prod.path))
                else:
                    # if reprocessing from the temp folder
                    shutil.rmtree(str(l2a_path))
                    logger.info("Deleted {}".format(l2a_path))
                
                #### check here if better to delete after sen2cor version check compatible... ??
                # s2c_path = s2c_path or get_latest_s2c_version_path(self.identifier)
                s2c_path = s2c_path or get_Sen2Cor_path_from_version(get_latest_Sen2Cor_version_from_id(self.identifier))
                if s2c_path:
                    process_it = True

        if process_it:
            process_sen2cor(
                l1c_product_path = self.path, 
                l2a_product_path = l2a_path, 
                s2c_path = s2c_path,
                pb = self.processing_baseline,
            )
            # sen2cor creates the L2A in the parent folder of the L1C.
            # Therefore, if the L1C computed is in the L1C library folder,
            # the newly L2A must be moved to his L2A library folder.
            if self._library_product:
                l2a_prod.archive()
            l2a_prod = L2aProduct(l2a_identifier)
            l2a_prod.set_permissions()
            l2a_prod.update_md(sen2cor_version = get_Sen2Cor_version(s2c_path))
            
            if copy_l2a_sideproducts:
                if l2a_prod.path.exists():
                    cloudmask = NewCloudMaskProduct(l2a_identifier = l2a_identifier)
                    cloudmask.path.parent.mkdir(parents = True, exist_ok = True)
                    if not (cloudmask.path.parent / Path(cloudmask.msk_cldprb_20m).name).exists():
                        logger.info("Copying msk_cldprb_20m to cloumask folder")
                        shutil.copy2(cloudmask.msk_cldprb_20m, cloudmask.path.parent)
                    if not (cloudmask.path.parent / Path(cloudmask.scl_20m).name).exists():
                        shutil.copy2(cloudmask.scl_20m, cloudmask.path.parent)
                        logger.info("Copying scl_20m to cloumask folder")
        return self

    def process_ql(
        self,
        reprocess: bool = False,
        out_path: pathlib.PosixPath = None,
        out_resolution: Tuple[int, int] = (100, 100),
        jpg=False,
    ) -> "L1cProduct":
        """
        Compute L1C product quicklook.

        :param reprocess: When True, delete old quicklook and process new one.
        :type reprocess: bool, Default to False
        :param out_path: Path to save quicklook to
        :type out_path: Path
        :param out_resolution: Quicklook resolution. Tuple pair
        :type out_resolution: Tuple[int, int], Default to (100, 100)
        :param jpg: Quicklook default format (True = JPG, False = TIFF)
        :type jpg: bool, Default to False
        """
        logger.info("{}: processing L1C Quicklook".format(self.identifier))

        if jpg:
            ql_filename = (
                self.identifier + "_QL-" + str(out_resolution[0]) + "m.jpg"
            )
        else:
            ql_filename = (
                self.identifier + "_QL-" + str(out_resolution[0]) + "m.tif"
            )

        if out_path is None:
            ql_folder = self.library_path.parent / "QL"
            ql_folder.mkdir(parents=True, exist_ok=True)
            ql_path = ql_folder / ql_filename
        else:
            ql_path = Path(out_path)

        if ql_path.exists() and not reprocess:
            logger.info("{} Quicklook already exists".format(self.identifier))
        else:
            # create_l1c_ql(tci = self.tci,
            # out_path = ql_path,
            # out_resolution = out_resolution,
            # jpg = jpg)
            create_l1c_ql_v2(
                tci=self.tci,
                out_path=ql_path,
                out_resolution=out_resolution,
                jpg=jpg,
            )
            self.user_ql = ql_path

        return ql_path
    
    def remove(
        self, 
        zipfile: bool = False,
    ):
        """
        Remove L1C in library. Can also remove L1C zip folder.

        :param zipfile: When True, remove zipfile of corresponding L1C Product
        :type zipfile: bool, Default to False
        """
        if zipfile:
            self.zip_path.unlink(missing_ok = True)
            logger.info("Removing {}".format(self.zip_path.name))
        else:
            if self.path.exists():
                if self.path.is_symlink():
                    l1c_path = os.readlink(str(self.path))
                    logger.info("Removing: {}".format(l1c_path))
                    shutil.rmtree(l1c_path)
                    logger.info("Removing symlink: {}".format(self.path))
                    self.path.unlink()
                else:
                    # l1c_path = os.readlink(str(l1c.path))
                    logger.info("Removing: {}".format(self.path))
                    try:
                        shutil.rmtree(str(self.path))
                    except Exception:
                        logger.error("Cannot remove product: {}".format(self.path))

    @property
    def owner(self) -> str:
        """Returns L1CProduct owner"""
        return self.path.owner()
    
    @property
    def zip_path(self) -> str:
        """Returns zip path"""
        zip_path = self.path.parent / (self.path.stem + ".zip")
        if zip_path.exists():
            return zip_path
        else:
            return None
    
    # METADATA
    @property
    def product_start_time(self):
        """Returns product start time"""
        return self._get_metadata_value(key="PRODUCT_START_TIME")

    @property
    def product_stop_time(self):
        "Returns product stop time"
        return self._get_metadata_value(key="PRODUCT_STOP_TIME")

    @property
    def product_uri(self):
        return self._get_metadata_value(key="PRODUCT_URI")

    @property
    def product_type(self):
        return self._get_metadata_value(key="PRODUCT_TYPE")

    @property
    def processing_baseline(self):
        return self._get_metadata_value(key="PROCESSING_BASELINE")

    @property
    def generation_time(self):
        return self._get_metadata_value(key="GENERATION_TIME")

    @property
    def spacecraft_name(self):
        return self._get_metadata_value(key="SPACECRAFT_NAME")

    @property
    def datatake_type(self):
        return self._get_metadata_value(key="DATATAKE_TYPE")

    @property
    def datatake_sensing_start(self):
        return self._get_metadata_value(key="DATATAKE_SENSING_START")

    @property
    def sensing_orbit_number(self):
        return self._get_metadata_value(key="SENSING_ORBIT_NUMBER")

    @property
    def sensing_orbit_direction(self):
        return self._get_metadata_value(key="SENSING_ORBIT_DIRECTION")

    @property
    def cloud_coverage_assessment(self):
        return self._get_metadata_value(key="Cloud_Coverage_Assessment")

    @property
    def footprint(self):
        return self._get_metadata_value(key="EXT_POS_LIST")

    # BANDS PATHS
    @property
    def b01(self):
        return self._get_band_path(key="B01")

    @property
    def b02(self):
        return self._get_band_path(key="B02")

    @property
    def b03(self):
        return self._get_band_path(key="B03")

    @property
    def b04(self):
        return self._get_band_path(key="B04")

    @property
    def b05(self):
        return self._get_band_path(key="B05")

    @property
    def b06(self):
        return self._get_band_path(key="B06")

    @property
    def b07(self):
        return self._get_band_path(key="B07")

    @property
    def b08(self):
        return self._get_band_path(key="B08")

    @property
    def b8a(self):
        return self._get_band_path(key="B8A")

    @property
    def b09(self):
        return self._get_band_path(key="B09")

    @property
    def b10(self):
        return self._get_band_path(key="B10")

    @property
    def b11(self):
        return self._get_band_path(key="B11")

    @property
    def b12(self):
        return self._get_band_path(key="B12")

    @property
    def tci(self):
        return self._get_band_path(key="TCI")


class L2aProduct(Product):
    """L2A product class.

    :param identifier: L2A product's identifier or SAFE filename.
    :type identifier: str
    :param tile: Tile's name.
    :type tile: str
    :param path: Parent's SAFE folder path.
    :type path: str
    """

    _library_path = Path(Config().get("l2a_path"))
    _indices_library_path = Path(Config().get("indices_path"))
    _tiled_metadata = "MTD_MSIL2A.xml"

    def __init__(
        self, 
        identifier: str = None, 
        tile: str = None, 
        path: str = None
    ) -> None:
        super().__init__(identifier=identifier, tile=tile, path=path)
        if not re.match(r".*L2A_.*", identifier):
            raise ValueError("Invalid L2A product name")

        self.indices_path = self._indices_library_path

        # user cloud mask
        # self.user_cloud_mask = self.path.parent / (self.identifier + "_CLOUD_MASK.jp2")
        # if not self.user_cloud_mask.exists():
        # self.user_cloud_mask = None
        # # user cloud mask b11
        # self.user_cloud_mask_b11 = self.path.parent / (self.identifier + "_CLOUD_MASK_B11.jp2")
        # if not self.user_cloud_mask_b11.exists():
        # self.user_cloud_mask_b11 = None
        # user QL
        self.user_ql = self.path.parent / (self.identifier + "_QL.tif")
        if not self.user_ql.exists():
            self.user_ql = None

        # versions
        self._sen2chain_info_path = self.path / "sen2chain_info.xml"
        if (
            self._sen2chain_info_path.parent.exists()
            and not self._sen2chain_info_path.exists()
        ):
            Sen2ChainMetadataParser(self._sen2chain_info_path).init_metadata()

    def process_ql(
        self,
        reprocess: bool = False,
        out_path: pathlib.PosixPath = None,
        out_resolution: Tuple[int, int] = (100, 100),
        jpg=False,
    ) -> "L2aProduct":
        """
        Compute L2A product quicklook.

        :param reprocess: When True, delete old quicklook and process new one.
        :type reprocess: bool, Default to False
        :param out_path: Path to save quicklook to
        :type out_path: Path
        :param out_resolution: Quicklook resolution. Tuple pair
        :type out_resolution: Tuple[int, int], Default to (100, 100)
        :param jpg: Quicklook default format (True = JPG, False = TIFF)
        :type jpg: bool, Default to False
        """
        logger.info("{}: processing L2A Quicklook".format(self.identifier))

        if jpg:
            ql_filename = (
                self.identifier + "_QL-" + str(out_resolution[0]) + "m.jpg"
            )
        else:
            ql_filename = (
                self.identifier + "_QL-" + str(out_resolution[0]) + "m.tif"
            )

        if out_path is None:
            ql_folder = self.library_path.parent / "QL"
            ql_folder.mkdir(parents=True, exist_ok=True)
            ql_path = ql_folder / ql_filename
        else:
            ql_path = Path(out_path)

        if ql_path.exists() and not reprocess:
            logger.info("{} Quicklook already exists".format(self.identifier))
        else:
            # create_l2a_ql(b02 = self.b02_10m,
            # b03 = self.b03_10m,
            # b04 = self.b04_10m,
            # out_path = ql_path,
            # out_resolution = out_resolution,
            # jpg = jpg)

            create_l2a_ql_v2(
                tci=self.tci_10m,
                out_path=ql_path,
                out_resolution=out_resolution,
                jpg=jpg,
            )

            self.user_ql = ql_path

        return self

    def process_cloud_mask(
        self,
        buffering: bool = True,
        reprocess: bool = False,
        out_path: pathlib.PosixPath = None,
    ) -> "L2aProduct":
        """
        Compute Cloud mask from L2A product

        :param reprocess: When True, delete old cloud mask and process new one.
        :type reprocess: bool, Default to False
        :param bufffering: Cloud mask parameter. When True, perform erosion-dilatation cycles around cloud pixels. See more in Cloudmask section.
        :type buffering: bool, Default to True
        :param out_path: Path to save cloudmask.
        :type out_path: Path
        """
        logger.info("{}: processing cloud_mask".format(self.identifier))

        cloud_mask_filename = self.identifier + "_CLOUD_MASK.tif"

        if out_path is None:
            cloud_mask_folder = self.library_path.parent
            cloud_mask_folder.mkdir(parents=True, exist_ok=True)
            cloud_mask_path = cloud_mask_folder / cloud_mask_filename
        else:
            cloud_mask_path = Path(out_path)

        if cloud_mask_path.exists() and not reprocess:
            logger.info("{} cloud mask already exists".format(self.identifier))
        else:
            create_cloud_mask(
                self.cld_20m,
                buffering=buffering,
                erosion=-40,
                dilatation=100,
                out_path=cloud_mask_path,
            )
            self.user_cloud_mask = cloud_mask_path
        return self

    def process_cloud_mask_v2(
        self,
        buffering: bool = True,
        reprocess: bool = False,
        out_path_mask=None,
        out_path_mask_b11=None,
    ) -> "L2aProduct":
        """
        Compute Cloud mask 2 from L2A product.

        :param reprocess: When True, delete old cloud mask and process new one.
        :type reprocess: bool, Default to False
        :param bufffering: Cloud mask parameter. When True, perform erosion-dilatation cycles around cloud pixels. See more in Cloudmask section.
        :type buffering: bool, Default to True
        :param out_path_mask: Path to save cloudmask.
        :type out_path_mask: Path
        :param out_path_mask_b11: Path to save cloud mask produced with b11 band
        :type out_path_mask_b11: Path
        """
        logger.info("{}: processing cloud_mask_v2".format(self.identifier))

        cloud_mask_filename = self.identifier + "_CLOUD_MASK.jp2"
        cloud_mask_b11_filename = (
            str(
                Path(cloud_mask_filename).parent
                / Path(cloud_mask_filename).stem
            )
            + "_B11.jp2"
        )

        if out_path_mask is None:
            cloud_mask_folder = self.library_path.parent
            cloud_mask_folder.mkdir(parents=True, exist_ok=True)
            cloud_mask_path = cloud_mask_folder / cloud_mask_filename
        else:
            cloud_mask_path = Path(out_path_mask)

        if out_path_mask_b11 is None:
            cloud_mask_folder = self.library_path.parent
            cloud_mask_folder.mkdir(parents=True, exist_ok=True)
            cloud_mask_b11_path = cloud_mask_folder / cloud_mask_b11_filename
        else:
            cloud_mask_b11_path = Path(out_path_mask_b11)

        if cloud_mask_path.exists() and not reprocess:
            logger.info(
                "{} cloud mask v2 already exists".format(self.identifier)
            )
        else:
            create_cloud_mask_v2(
                self.cld_20m, erosion=1, dilatation=5, out_path=cloud_mask_path
            )
            self.user_cloud_mask = cloud_mask_path

        if cloud_mask_b11_path.exists() and not reprocess:
            logger.info(
                "{} cloud mask v2 masked b11 already exists".format(
                    self.identifier
                )
            )
        else:
            create_cloud_mask_b11(
                self.user_cloud_mask,
                self.b11_20m,
                dilatation=5,
                out_path=cloud_mask_b11_path,
            )
            self.user_cloud_mask_b11 = cloud_mask_b11_path

        return self

    def compute_cloud_mask(
        self,
        cm_version: str = "CM001",
        probability: int = 1,
        iterations: int = 5,
        cld_shad: bool = True,
        cld_med_prob: bool = True,
        cld_hi_prob: bool = True,
        thin_cir: bool = True,
        reprocess: bool = False,
        # out_path_mask = None,
        # out_path_mask_b11 = None
    ) -> "L2aProduct":
        """
        Compute cloud mask rom L2A Product. Provide cm_version and corresponding parameters.

        :param cm_version: Cloudmask version to compute. Can be either CM001, CM002, CM003, or CM004.
        :type cm_version: str, Default to "CM001"
        :param probability: Threshold of probability of clouds to be considered. Specific to CM003 & CM004.
        :type probability: int, Default to 1
        :param iterations: Number of iterations for the kernel dilatation process. Specific to CM003 & CM004.
        :param cld_shad: When True, uses Cloud Shadows class from Scene Classification.
        :type cld_shad: bool, Default to True
        :param cld_med_prob: When True, uses Cloud Medium Probability class from Scene Classification.
        :type cld_med_prob: bool, Default to True
        :param cld_hi_prob: When True, uses Cloud High Probability class from Scene Classification.
        :type cld_hi_prob: bool, Default to True
        :param thin_cir: When True, uses Thin Cirrus class from Scen Classification.
        :type thin_cir: bool, Default to True
        :param reprocess: When True, already processed cloudmask will be computed again.
        :type reprocess: bool, Default to False
        """
        cm_version = cm_version.upper()
        if cm_version == "CM003":
            logger.info(
                "Computing cloudmask version {}, probability {}%, iteration(s) {}: {}".format(
                    cm_version, probability, iterations, self.identifier
                )
            )
        elif cm_version == "CM004":
            logger.info(
                "Computing cloudmask version {}, ITER {}, SHAD {}, MED-PRB {}, HI-PRB {}, CIRR {}: {}".format(
                    cm_version,
                    iterations,
                    cld_shad,
                    cld_med_prob,
                    cld_hi_prob,
                    thin_cir,
                    self.identifier,
                )
            )
        else:
            logger.info(
                "Computing cloudmask version {}: {}".format(
                    cm_version, self.identifier
                )
            )

        cloudmask = NewCloudMaskProduct(
            l2a_identifier = self.identifier,
            sen2chain_processing_version = self.sen2chain_processing_version,
            cm_version = cm_version,
            probability = probability,
            iterations = iterations,
            cld_shad = cld_shad,
            cld_med_prob = cld_med_prob,
            cld_hi_prob = cld_hi_prob,
            thin_cir = thin_cir,
        )

        if cloudmask.path.exists() and not reprocess:
            logger.info(
                "{} cloud mask already computed".format(cloudmask.identifier)
            )
        # elif not self.path.exists():
            # logger.info(
                # "l2a not present cannot compute cloud mask".format(cloudmask.identifier)
            # )
        else:
            if not cloudmask.path.parent.exists():
                cloudmask.path.parent.mkdir(parents=True)
            if cm_version == "CM001":
                if cloudmask.msk_cldprb_20m:
                    cloudmask.path.unlink(missing_ok = True)
                    cloudmask._info_path.unlink(missing_ok = True)
                    create_cloud_mask_v2(
                        cloudmask.msk_cldprb_20m,
                        erosion=1,
                        dilatation=5,
                        out_path=cloudmask.path,
                    )
                else:
                    logger.info("Skipping: Sen2Cor cloud probability band not found")
            elif cm_version == "CM002":
                if self.path.exists():
                    cloudmask_cm001 = NewCloudMaskProduct(
                        l2a_identifier=self.identifier,
                        sen2chain_processing_version=self.sen2chain_processing_version,
                        cm_version="CM001",
                    )
                    if cloudmask_cm001.path.exists():
                        cloudmask.path.unlink(missing_ok = True)
                        cloudmask._info_path.unlink(missing_ok = True)
                        create_cloud_mask_b11(
                            cloudmask_cm001.path,
                            self.b11_20m,
                            dilatation=5,
                            out_path=Path(
                                str(cloudmask_cm001.path).replace(
                                    "CM001", "CM002-B11"
                                )
                            ),
                        )
                    else:
                        logger.info(
                            "No cloudmask version cm001 found, please compute this one first"
                        )
                else:
                    logger.info("Skipping: no B11 from L2A")
            elif cm_version == "CM003":
                if cloudmask.msk_cldprb_20m:
                    cloudmask.path.unlink(missing_ok = True)
                    cloudmask._info_path.unlink(missing_ok = True)
                    create_cloud_mask_v003(
                        cloud_mask = cloudmask.msk_cldprb_20m,
                        out_path = cloudmask.path,
                        probability = probability,
                        iterations = iterations,
                    )

            elif cm_version == "CM004":
                if cloudmask.scl_20m:
                    cloudmask.path.unlink(missing_ok = True)
                    cloudmask._info_path.unlink(missing_ok = True)
                    create_cloud_mask_v004(
                        scl_path = cloudmask.scl_20m,
                        out_path = cloudmask.path,
                        iterations = iterations,
                        cld_shad = cld_shad,
                        cld_med_prob = cld_med_prob,
                        cld_hi_prob = cld_hi_prob,
                        thin_cir = thin_cir,
                    )
            else:
                logger.info("Wrong cloudmask version {}".format(cm_version))
            cloudmask.init_md()
            
            #### copy msk_cldprb_20m and scl_20m from L2A folder
            # if not (cloudmask.path.parent / Path(cloudmask.msk_cldprb_20m).name).exists():
                # shutil.copy2(cloudmask.msk_cldprb_20m, cloudmask.path.parent)
            # if not (cloudmask.path.parent / Path(cloudmask.scl_20m).name).exists():
                # shutil.copy2(cloudmask.scl_20m, cloudmask.path.parent)

        # return self

    def process_indices(
        self,
        indices_list: List[str] = [],
        nodata_clouds: bool = False,
        quicklook: bool = False,
        reprocess: bool = False,
        out_path: str = None,
    ) -> "L2aProduct":
        """
        Process indices provided in indices_list. Calls for process_indice() method of Indice Class.

        :param indices_list: list of valid indices names.
        :type indices_list: List[str], Default to []
        :param nodata_clouds: When True, mask indice raster with the cloud mask or not.
        :type nodata_clouds: bool, Default to False
        :param quicklook: When True, process a quicklook (if available for the indice).
        :type quicklook: bool, Default to False
        :param reprocess: When True, delete existing indice(s) and process new ones.
        :type reprocess: bool, Default to False
        :param out_path: If specified, a folder {identifier}_INDICES containing the processed indices will be created in the out_path.
        :type out_path: str
        """
        logger.info("{}: processing_indices".format(self.identifier))

        if not isinstance(indices_list, list):
            raise TypeError("Indices must be provided as a list.")

        for indice in set(indices_list):
            logger.info("Processing {}: {}".format(indice, self.identifier))
            indice_cls = IndicesCollection.get_indice_cls(indice)
            if indice_cls is None:
                print(
                    "Indices available: {}".format(
                        IndicesCollection.available_indices
                    )
                )
                raise ValueError("Indice not defined")

            if out_path is None:
                # logger.info(self.identifier)
                indice_path = (
                    self.indices_path
                    / indice.upper()
                    / self.tile
                    / self.identifier
                )
            else:
                indice_path = (
                    Path(out_path)
                    / (self.identifier + "_INDICES")
                    / indice.upper()
                )
            indice_path.mkdir(parents=True, exist_ok=True)
            indice_obj = indice_cls(self)
            indice_obj.process_indice(
                out_path=indice_path,
                nodata_clouds=nodata_clouds,
                quicklook=quicklook,
                reprocess=reprocess,
            )
        return self

    def compute_indice(
        self,
        indice: str = None,
        reprocess: bool = False,
        nodata_clouds: bool = True,
        quicklook: bool = False,
        cm_version: str = "CM001",
        probability: int = 1,
        iterations: int = 5,
        cld_shad: bool = True,
        cld_med_prob: bool = True,
        cld_hi_prob: bool = True,
        thin_cir: bool = True,
        out_path: str = None,
    ) -> "L2aProduct":
        """
        Base computing fonction called to compute and mask a single indice and mask it with a single cloudmask version.
        :param indice: A valid indice name
        :type indice: str
        :param nodata_clouds: When True, mask indice raster with the cloud mask or not.
        :type nodata_clouds: bool, Default to False
        :param quicklook: When True, process a quicklook (if available for the indice).
        :type quicklook: bool, Default to False
        :param reprocess: When True, delete existing indice and process new one.
        :type reprocess: bool, Default to False
        :param cm_version: Cloudmask version to mask indice with. Can be either CM001, CM002, CM003, or CM004.
        :type cm_version: str, Default to "CM001"
        :param probability: Threshold of probability of clouds to identify the correct cloud mask used. Specific to CM003 & CM004.
        :type probability: int, Default to 1
        :param iterations: Number of iterations for the kernel dilatation process, used to identify the correct cloud mask. Specific to CM003 & CM004.
        :type iterations: int, Default to 5
        :param cld_shad: Cloud Shadows class from Scene Classification, used to identify the correct cloud mask.
        :type cld_shad: bool, Default to True
        :param cld_med_prob: Cloud Medium Probability class from Scene Classification, used to identify the correct cloud mask.
        :type cld_med_prob: bool, Default to True
        :param cld_hi_prob: Cloud High Probability class from Scene Classification, used to identify the correct cloud mask.
        :type cld_hi_prob: bool, Default to True
        :param thin_cir: Thin Cirrus class from Scen Classification, used to identify the correct cloud mask.
        :type thin_cir: bool, Default to True

        :param out_path: If specified, a folder {identifier}_INDICES containing the processed indices will be created in the out_path.
        :type out_path: str
        """
        logger.info("Computing indice {}: {}".format(indice, self.identifier))

        # if not isinstance(indices_list, list):
        # raise TypeError("Indices must be provided as a list.")

        baseline=L2aProduct(self.identifier).processing_baseline

        # for indice in set(indices_list):
        # logger.info("Processing {}: {}".format(indice, self.identifier))
        indice_cls = IndicesCollection.get_indice_cls(indice)
        if indice_cls is None:
            print(
                "Indices available: {}".format(
                    IndicesCollection.available_indices
                )
            )
            raise ValueError("Indice not defined")

        if out_path is None:
            # logger.info(self.identifier)
            indice_path = (
                self.indices_path
                / indice.upper()
                / self.tile
                / self.identifier
            )
        else:
            indice_path = (
                Path(out_path)
                / (self.identifier + "_INDICES")
                / indice.upper()
            )
        indice_path.mkdir(parents=True, exist_ok=True)

        indice_obj = indice_cls(
            self,
            NewCloudMaskProduct(
                l2a_identifier = self.identifier,
                cm_version = cm_version,
                probability = probability,
                iterations = iterations,
                cld_shad = cld_shad,
                cld_med_prob = cld_med_prob,
                cld_hi_prob = cld_hi_prob,
                thin_cir = thin_cir,
            ),
        )

        indice_obj.process_indice(
            out_path=indice_path,
            nodata_clouds=nodata_clouds,
            quicklook=quicklook,
            reprocess=reprocess,
            baseline=baseline,
        )
        if nodata_clouds:
            indice_raw = IndiceProduct(
                l2a_identifier=self.identifier,
                indice=indice,
                masked=not nodata_clouds,
                cm_version=cm_version,
                probability=probability,
                iterations=iterations,
                cld_shad=cld_shad,
                cld_med_prob=cld_med_prob,
                cld_hi_prob=cld_hi_prob,
                thin_cir=thin_cir,
            )

            indice_masked = IndiceProduct(
                l2a_identifier=self.identifier,
                indice=indice,
                masked=nodata_clouds,
                cm_version=cm_version,
                probability=probability,
                iterations=iterations,
                cld_shad=cld_shad,
                cld_med_prob=cld_med_prob,
                cld_hi_prob=cld_hi_prob,
                thin_cir=thin_cir,
            )
            if quicklook:
                indice_masked_ql = IndiceProduct(
                    identifier=indice_masked.identifier.replace(
                        ".jp2", "_QL.tif"
                    )
                )

        else:
            IndiceProduct(
                l2a_identifier=self.identifier,
                indice=indice,
                masked=nodata_clouds,
                cm_version=cm_version,
                probability=probability,
                iterations=iterations,
                cld_shad=cld_shad,
                cld_med_prob=cld_med_prob,
                cld_hi_prob=cld_hi_prob,
                thin_cir=thin_cir,
            )
        return self

    # def set_permissions(self):
        # try:
            # set_permissions(self.path)
        # except Exception:
            # logger.info("Cannot set permissions to: {}".format(self.identifier))

    # def has_indice(self, indice):
    # """
    # Returns if the indice file exists on disk.

    # :param indice: name of the indice.
    # """
    # indice_cls = IndicesCollection().get_indice_cls(indice)

    # if indice_cls is None:
    # raise ValueError("{} is not a valid indice name.".format(indice))

    # template = indice_cls.filename_template
    # ext = indice_cls.ext
    # indice_filename = template.format(product_identifier=self.identifier,
    # ext=ext)
    # return (self.indices_path / indice / self.tile / indice_filename).exists()

    # @property
    # def sen2cor_version(self):
    #     """ Used Sen2Cor version"""
    #     return Sen2ChainMetadataParser(self.identifier):

    #     sen2chain_info_path = self.path / "Sen2Chain_info.xml"
    #     if sen2chain_info_path.exists():
    #         return "xml present"
    #         Sen2ChainMetadataParser(self.identifier)

    #     else:
    #         return None

    def update_md(
        self,
        sen2chain_version: str = None,
        sen2chain_processing_version: str = None,
        sen2cor_version: str = None,
    ):
        """Set custom sen2chain, sen2chain_processing and sen2cor versions"""
        Sen2ChainMetadataParser(self._sen2chain_info_path).set_metadata(
            sen2chain_version=sen2chain_version,
            sen2chain_processing_version=sen2chain_processing_version,
            sen2cor_version=sen2cor_version,
        )

    def remove(
        self,
        copy_l2a_sideproducts: bool = False,
    ):
        """
        Remove L2A Product.

        :param copy_l2a_sideproducts: When True, copy 2 L2A files to corresponding Cloudmask product folder, allowing to compute Cloudmasks later without the L2A product.
        :type copy_l2a_sideproducts: bool, Default to False
        """
        
        if copy_l2a_sideproducts:
            self.copy_l2a_sideproducts()

        if self.path.exists():
            if self.path.is_symlink():
                l2a_path = os.readlink(str(self.path))
                logger.info("Removing: {}".format(l2a_path))
                shutil.rmtree(l2a_path)
                logger.info("Removing symlink: {}".format(self.path))
                self.path.unlink()
            else:
                shutil.rmtree(str(self.path))
                logger.info("Removing: {}".format(self.path))
        else:
            logger.info("L2A product not on disk")
    
    def copy_l2a_sideproducts(self):
        """
        Copy Cloud probability "msk_cldprb_20m" and Scene Classification scl_20m files from L2A Product to corresponding Cloud mask folder.
        """
        if self.path.exists():
            cloudmask = NewCloudMaskProduct(l2a_identifier = self.identifier)
            cloudmask.path.parent.mkdir(parents = True, exist_ok = True)
            if not (cloudmask.path.parent / Path(cloudmask.msk_cldprb_20m).name).exists():
                logger.info("{} - Copying msk_cldprb_20m to cloumask folder".format(self.identifier))
                shutil.copy2(cloudmask.msk_cldprb_20m, cloudmask.path.parent)
            else:
                logger.info("{} - msk_cldprb_20m already copied to cloumask folder".format(self.identifier))
            if not (cloudmask.path.parent / Path(cloudmask.scl_20m).name).exists():
                logger.info("{} - Copying scl_20m to cloumask folder".format(self.identifier))
                shutil.copy2(cloudmask.scl_20m, cloudmask.path.parent)
            else:
                logger.info("{} - scl_20m already copied to cloumask folder".format(self.identifier))
    
    @property
    def sen2chain_version(self):
        return Sen2ChainMetadataParser(
            self._sen2chain_info_path
        ).get_metadata_value("SEN2CHAIN_VERSION")

    @property
    def sen2chain_processing_version(self):
        return Sen2ChainMetadataParser(
            self._sen2chain_info_path
        ).get_metadata_value("SEN2CHAIN_PROCESSING_VERSION")

    @property
    def sen2cor_version(self):
        return Sen2ChainMetadataParser(
            self._sen2chain_info_path
        ).get_metadata_value("SEN2COR_VERSION")

    @property
    def processing_baseline(self):
        return self._get_metadata_value(key="PROCESSING_BASELINE")

    @property
    def generation_time(self):
        return self._get_metadata_value(key="GENERATION_TIME")
    
    @property
    def zip_path(self) -> str:
        """Returns zip path"""
        zip_path = self.path.parent / (self.path.stem + ".zip")
        if zip_path.exists():
            return zip_path
        else:
            return None

    # METADATA
    @property
    def product_uri_1c(self):
        return self._get_metadata_value(key="PRODUCT_URI_1C")

    @property
    def product_uri_2a(self):
        return self._get_metadata_value(key="PRODUCT_URI_2A")

    @property
    def cloud_coverage_assessment(self):
        return self._get_metadata_value(key="Cloud_Coverage_Assessment")

    @property
    def nodata_pixel_percentage(self):
        return self._get_metadata_value(key="NODATA_PIXEL_PERCENTAGE")

    @property
    def saturated_defective_pixel_percentage(self):
        return self._get_metadata_value(
            key="SATURATED_DEFECTIVE_PIXEL_PERCENTAGE"
        )

    @property
    def dark_features_percentage(self):
        return self._get_metadata_value(key="DARK_FEATURES_PERCENTAGE")

    @property
    def cloud_shadow_percentage(self):
        return self._get_metadata_value(key="CLOUD_SHADOW_PERCENTAGE")

    @property
    def vegetation_percentage(self):
        return self._get_metadata_value(key="VEGETATION_PERCENTAGE")

    @property
    def not_vegetated_percentage(self):
        return self._get_metadata_value(key="NOT_VEGETATED_PERCENTAGE")

    @property
    def water_percentage(self):
        return self._get_metadata_value(key="WATER_PERCENTAGE")

    @property
    def unclassified_percentage(self):
        return self._get_metadata_value(key="UNCLASSIFIED_PERCENTAGE")

    @property
    def medium_proba_clouds_percentage(self):
        return self._get_metadata_value(key="MEDIUM_PROBA_CLOUDS_PERCENTAGE")

    @property
    def high_proba_clouds_percentage(self):
        return self._get_metadata_value(key="HIGH_PROBA_CLOUDS_PERCENTAGE")

    @property
    def thin_cirrus_percentage(self):
        return self._get_metadata_value(key="THIN_CIRRUS_PERCENTAGE")

    # FOOTPRINT
    @property
    def footprint(self) -> List[Tuple[float, float]]:
        """Returns product's footprint as list of coordinates couples."""
        fp_str = self._get_metadata_value(key="EXT_POS_LIST")
        fp_list = fp_str.strip().split()
        fp_coords = grouper(fp_list, 2)
        fp_coords_float = [(float(lat), float(lon)) for lon, lat in fp_coords]
        return fp_coords_float
    
    # BANDS
    
    # BOA QUANTIFICATION AND OFFSET VALUES
    @property
    def boa_quantification(self):
        return self._get_metadata_value(key="BOA_QUANTIFICATION_VALUE")   
    
    @property
    def b00_offset(self):
        return self._get_metadata_value(key='BOA_ADD_OFFSET[@band_id="0"]')   
    
    @property
    def b01_offset(self):
        return self._get_metadata_value(key='BOA_ADD_OFFSET[@band_id="1"]')   
    
    @property
    def b02_offset(self):
        return self._get_metadata_value(key='BOA_ADD_OFFSET[@band_id="2"]')   
    
    @property
    def b03_offset(self):
        return self._get_metadata_value(key='BOA_ADD_OFFSET[@band_id="3"]')   
    
    @property
    def b04_offset(self):
        return self._get_metadata_value(key='BOA_ADD_OFFSET[@band_id="4"]')   
        
    @property
    def b05_offset(self):
        return self._get_metadata_value(key='BOA_ADD_OFFSET[@band_id="5"]')   
        
    @property
    def b06_offset(self):
        return self._get_metadata_value(key='BOA_ADD_OFFSET[@band_id="6"]')   
        
    @property
    def b07_offset(self):
        return self._get_metadata_value(key='BOA_ADD_OFFSET[@band_id="7"]')   
        
    @property
    def b08_offset(self):
        return self._get_metadata_value(key='BOA_ADD_OFFSET[@band_id="8"]')   
        
    @property
    def b09_offset(self):
        return self._get_metadata_value(key='BOA_ADD_OFFSET[@band_id="9"]')   
        
    @property
    def b10_offset(self):
        return self._get_metadata_value(key='BOA_ADD_OFFSET[@band_id="10"]')   
    
    @property
    def b11_offset(self):
        return self._get_metadata_value(key='BOA_ADD_OFFSET[@band_id="11"]')   

    @property
    def b12_offset(self):
        return self._get_metadata_value(key='BOA_ADD_OFFSET[@band_id="12"]')   
    
    # 10m bands
    @property
    def b02_10m(self):
        return self._get_band_path(key="B02", res="10m")

    @property
    def b03_10m(self):
        return self._get_band_path(key="B03", res="10m")

    @property
    def b04_10m(self):
        return self._get_band_path(key="B04", res="10m")

    @property
    def b08_10m(self):
        return self._get_band_path(key="B08", res="10m")

    @property
    def aot_10m(self):
        return self._get_band_path(key="AOT", res="10m")

    @property
    def wvp_10m(self):
        return self._get_band_path(key="WVP", res="10m")

    @property
    def tci_10m(self):
        return self._get_band_path(key="TCI", res="10m")

    # 20m bands
    @property
    def b02_20m(self):
        return self._get_band_path(key="B02", res="20m")

    @property
    def b03_20m(self):
        return self._get_band_path(key="B03", res="20m")

    @property
    def b04_20m(self):
        return self._get_band_path(key="B04", res="20m")

    @property
    def b05_20m(self):
        return self._get_band_path(key="B05", res="20m")

    @property
    def b06_20m(self):
        return self._get_band_path(key="B06", res="20m")

    @property
    def b07_20m(self):
        return self._get_band_path(key="B07", res="20m")

    @property
    def b8a_20m(self):
        return self._get_band_path(key="B8A", res="20m")

    @property
    def b11_20m(self):
        return self._get_band_path(key="B11", res="20m")

    @property
    def b12_20m(self):
        return self._get_band_path(key="B12", res="20m")

    @property
    def scl_20m(self):
        return self._get_band_path(key="SCL", res="20m")

    @property
    def snw_20m(self):
        """old sen2cor snow-mask's name"""
        # try:
        return self._get_band_path(key="SNW")
        # except Exception:
        # logger.warning("'snw_20m deprecated', use: 'msk_snwprb_20m'")
        # return self.msk_snwprb_20m

    @property
    def msk_snwprb_20m(self):
        """new sen2cor cloud-mask's name"""
        # try:
        return self._get_band_path(key="SNW")
        # except Exception:
        # return self._get_band_path(key="SNWPRB", res="20m")

    @property
    def cld_20m(self):
        """old sen2cor cloud-mask's name"""
        # try:
        return self._get_band_path(key="CLD", res="20m")
        # except Exception:
        # logger.warning("'cld_20m' deprecated', use: 'msk_cldprb_20m'")
        # return self.msk_cldprb_20m

    @property
    def msk_cldprb_20m(self):
        """new sen2cor cloud-mask's name"""
        # try:
        # return self._get_band_path(key="MSK_CLDPRB", res="20m")
        # except Exception:
        return self._get_band_path(key="CLD", res="20m")

    @property
    def aot_20m(self):
        return self._get_band_path(key="AOT", res="20m")

    @property
    def wvp_20m(self):
        return self._get_band_path(key="WVP", res="20m")

    @property
    def tci_20m(self):
        return self._get_band_path(key="TCI", res="20m")

    # 60m bands
    @property
    def b01_60m(self):
        return self._get_band_path(key="B01", res="60m")

    @property
    def b02_60m(self):
        return self._get_band_path(key="B02", res="60m")

    @property
    def b03_60m(self):
        return self._get_band_path(key="B03", res="60m")

    @property
    def b04_60m(self):
        return self._get_band_path(key="B04", res="60m")

    @property
    def b05_60m(self):
        return self._get_band_path(key="B05", res="60m")

    @property
    def b06_60m(self):
        return self._get_band_path(key="B06", res="60m")

    @property
    def b07_60m(self):
        return self._get_band_path(key="B07", res="60m")

    @property
    def b8a_60m(self):
        return self._get_band_path(key="B8A", res="60m")

    @property
    def b09_60m(self):
        return self._get_band_path(key="B09", res="60m")

    @property
    def b11_60m(self):
        return self._get_band_path(key="B11", res="60m")

    @property
    def b12_60m(self):
        return self._get_band_path(key="B12", res="60m")

    @property
    def scl_60m(self):
        return self._get_band_path(key="SCL", res="60m")

    @property
    def snw_60m(self):
        return self._get_band_path(key="SNW", res="60m")

    @property
    def cld_60m(self):
        return self._get_band_path(key="CLD", res="60m")

    @property
    def aot_60m(self):
        return self._get_band_path(key="AOT", res="60m")

    @property
    def wvp_60m(self):
        return self._get_band_path(key="WVP", res="60m")

    @property
    def tci_60m(self):
        return self._get_band_path(key="TCI", res="60m")


class OldCloudMaskProduct:
    """Old cloud mask product class.

    :param identifier: CloudMask product's identifier or SAFE filename.
    :type identifier: str
    """

    _library_path = Path(Config().get("l2a_path"))

    def __init__(self, identifier: str = None) -> None:
        if identifier is None:
            raise ValueError("Product identifier is empty")
        else:
            self.identifier = identifier
        self.tile = self.get_tile(identifier)
        self.l2a = self.get_l2a(identifier)
        self.path = self._library_path / self.tile / self.identifier

    @staticmethod
    def get_tile(identifier) -> str:
        """Returns tile name from a string.

        :param identifier: string from which to extract the tile name.
        """
        return re.findall("_T([0-9]{2}[A-Z]{3})_", identifier)[0]

    @staticmethod
    def get_l2a(identifier) -> str:
        """Returns L2A name from a old cloud mask identifier string.
        :param identifier: string from which to extract the L2A name.
        """
        return re.findall(r"(S2.+)_CLOUD_MASK.+jp2", identifier)[0]


class NewCloudMaskProduct:
    """
    Cloud mask product class.

    :param identifier: Cloud mask product's identifier or SAFE filename.
    :type identifier: str
    :param l2a_identifier: L2A Product identifier
    :type l2a_identifier: str
    :param sen2chain_processing_version: Sen2chain processing version used to process Cloud mask
    :type sen2chain_processing_version: str
    :param cm_version: Cloudmask version to mask indice with. Can be either CM001, CM002, CM003, or CM004.
    :type cm_version: str, Default to "CM001"
    :param probability: Threshold of probability of clouds to identify the correct cloud mask used. Specific to CM003 & CM004.
    :type probability: int, Default to 1
    :param iterations: Number of iterations for the kernel dilatation process, used to identify the correct cloud mask. Specific to CM003 & CM004.
    :type iterations: int, Default to 5
    :param cld_shad: Cloud Shadows class from Scene Classification, used to identify the correct cloud mask.
    :type cld_shad: bool, Default to True
    :param cld_med_prob: Cloud Medium Probability class from Scene Classification, used to identify the correct cloud mask.
    :type cld_med_prob: bool, Default to True
    :param cld_hi_prob: Cloud High Probability class from Scene Classification, used to identify the correct cloud mask.
    :type cld_hi_prob: bool, Default to True
    :param thin_cir: Thin Cirrus class from Scen Classification, used to identify the correct cloud mask.
    :type thin_cir: bool, Default to True
    """

    _library_path = Path(Config().get("cloudmasks_path"))

    def __init__(
        self,
        identifier: str = None,
        l2a_identifier: str = None,
        sen2chain_processing_version: str = None,
        cm_version: str = "CM001",
        probability: int = 1,
        iterations: int = 5,
        cld_shad: bool = True,
        cld_med_prob: bool = True,
        cld_hi_prob: bool = True,
        thin_cir: bool = True,
    ) -> None:
        if not (identifier or l2a_identifier):
            raise ValueError("Product identifieror L2a identifier cannot be empty")
        else:
            self.tile = self.get_tile(identifier or l2a_identifier)
            self.l2a = Path(l2a_identifier or self.get_l2a(identifier)).stem
            if identifier:
                self.mask_params = self.get_cm_version(identifier)
            else:
                self.mask_params = {
                    "cm_version": cm_version.upper(),
                    "probability": probability,
                    "iterations": iterations,
                    "cld_shad": cld_shad,
                    "cld_med_prob": cld_med_prob,
                    "cld_hi_prob": cld_hi_prob,
                    "thin_cir": thin_cir,
                }
            
            if self.mask_params["cm_version"] == "CM001":
                self.suffix = "CM001"
            elif self.mask_params["cm_version"] == "CM002":
                self.suffix = "CM002-B11"
            elif self.mask_params["cm_version"] == "CM003":
                self.suffix = (
                    "CM003" +
                    "-PRB" + str(self.mask_params["probability"])  + 
                    "-ITER" + str(self.mask_params["iterations"])
                )
            elif self.mask_params["cm_version"] == "CM004":
                self.suffix = (
                    "CM004" + 
                    "-CSH" + str(1 * self.mask_params["cld_shad"]) + 
                    "-CMP" + str(1 * self.mask_params["cld_med_prob"]) + 
                    "-CHP" + str(1 * self.mask_params["cld_hi_prob"]) + 
                    "-TCI" + str(1 * self.mask_params["thin_cir"]) + 
                    "-ITER" + str(self.mask_params["iterations"])
                )

                # self.suffix = [
                    # i
                    # for i in [
                        # "CM001",
                        # "CM002-B11",
                        # "CM003-PRB" + str(probability) + "-ITER" + str(iterations),
                        # "CM004-CSH"
                        # + str(int(cld_shad))
                        # + "-CMP"
                        # + str(int(cld_med_prob))
                        # + "-CHP"
                        # + str(int(cld_hi_prob))
                        # + "-TCI"
                        # + str(int(thin_cir))
                        # + "-ITER"
                        # + str(iterations),
                    # ]
                    # if cm_version.upper() in i
                # ][0]
            if identifier:
                self.identifier = Path(identifier).stem
            else:
                self.identifier = self.l2a + "_" + self.suffix
                
            # self.cm_version, self.probability, self.iterations = self.get_cm_version(self.identifier)
            self.mask_info = self.get_cm_version(self.identifier)

            self.path = (
                self._library_path / self.tile / self.l2a / (self.identifier + ".jp2")
            )
         
            ## set paths for msk_cldprb_20m and scl_20m
            self.msk_cldprb_20m = None
            self.scl_20m = None
            l2a = L2aProduct(self.l2a)
            if l2a.path.exists():
                self.msk_cldprb_20m = l2a.msk_cldprb_20m
                self.scl_20m = l2a.scl_20m
            else:
                msk_cldprb_20m = self.path.parent / "MSK_CLDPRB_20m.jp2"
                if msk_cldprb_20m.exists():
                    self.msk_cldprb_20m = msk_cldprb_20m
                scl_20m = self.path.parent / ("T" + self.tile + "_" + self.identifier[11:26] + "_SCL_20m.jp2")
                if scl_20m.exists():
                    self.scl_20m = scl_20m

            self._info_path = self.path.parent / (self.path.stem + ".xml")
            self.init_md()

    @staticmethod
    def get_tile(identifier) -> str:
        """Returns tile name from a string.

        :param identifier: String to extract the tile name from.
        """
        return re.findall("_T([0-9]{2}[A-Z]{3})_", identifier)[0]

    @staticmethod
    def get_identifier(l2a_identifier) -> str:
        """Returns L2A name from an old cloud mask identifier string.
        :param l2a_identifier: String to extract the L2A name from.
        """
        return re.findall(r"(S2.+)_CM.+jp2", identifier)[0]

    # @staticmethod
    # def get_l2a(identifier) -> str:
    # """Returns l2a name from a old cloud mask identifier string.
    # :param string: string from which to extract the l2a name.
    # """
    # return re.findall(r"(S2.+)_CM.+jp2", identifier)[0]

    @staticmethod
    def get_l2a(identifier) -> str:
        """Returns L2A name from an old cloud mask identifier string.
        :param identifier: String from which to extract the L2A name.
        """
        return re.findall(
            r"(S2._.+_[0-9]{8}T[0-9]{6}_N[0-9]{4}_R[0-9]{3}_T[0-9]{2}[A-Z]{3}_[0-9]{8}T[0-9]{6})_.*",
            identifier,
        )[0]

    @staticmethod
    def get_cm_version(identifier) -> str:
        """Returns cloudmask version from a cloud mask identifier string.
        :param identifier: string from which to extract the version name.
        """
        identifier = Path(identifier).stem
        try:
            pat = re.compile(r"S2.+_(?P<cm_version>CM00[1-2])")
            val = pat.match(identifier).groupdict()
            val.update(
                {
                    "probability": 1,
                    "iterations": 5,
                    "cld_shad": True,
                    "cld_med_prob": True,
                    "cld_hi_prob": True,
                    "thin_cir": True,
                }
            )
            return val
        except Exception:
            pass
        try:
            pat = re.compile(
                r"S2.+_(?P<cm_version>CM003)"
                + "-PRB(?P<probability>.*)"
                + "-ITER(?P<iterations>.*)"
                # + ".jp2"
            )
            val = pat.match(identifier).groupdict()
            val.update(
                {
                    "cld_shad": True,
                    "cld_med_prob": True,
                    "cld_hi_prob": True,
                    "thin_cir": True,
                }
            )
            return val
            
        except Exception:
            pass
        try:
            pat = re.compile(
                r"S2.+_(?P<cm_version>CM004)"
                + "-CSH(?P<cld_shad>.*)"
                + "-CMP(?P<cld_med_prob>.*)"
                + "-CHP(?P<cld_hi_prob>.*)"
                + "-TCI(?P<thin_cir>.*)"
                + "-ITER(?P<iterations>.*)"
                # + ".jp2"
            )
            return pat.match(identifier).groupdict()
        except Exception:
            pass

        # try:
        #     return re.findall(r"S2.+_(CM004)-CSH(.*)-CMP(.*)-CHP(.*)-TCI(.*)-ITER(.*)\.", identifier)[0]
        # except Exception:
        #     try:
        #         return [re.findall(r"S2.+_(CM003)-PRB(.*)-ITER(.*)\.", identifier)[0], None, None]
        #     except Exception:
        #         try:
        #             return [re.findall(r"S2.+_(CM00[1-2]).+", identifier)[0], None, None]
        #         except Exception:
        #             return [None, None, None]

    @property
    def sen2chain_version(self):
        return Sen2ChainMetadataParser(self._info_path).get_metadata_value(
            "SEN2CHAIN_VERSION"
        )

    @property
    def sen2chain_processing_version(self):
        return Sen2ChainMetadataParser(self._info_path).get_metadata_value(
            "SEN2CHAIN_PROCESSING_VERSION"
        )

    @property
    def sen2cor_version(self):
        return Sen2ChainMetadataParser(self._info_path).get_metadata_value(
            "SEN2COR_VERSION"
        )

    def init_md(self):
        if self.path.exists() and not self._info_path.exists():
            l2a = L2aProduct(self.l2a)
            if l2a._sen2chain_info_path.exists():
                Sen2ChainMetadataParser(self._info_path).set_metadata(
                    sen2chain_version=l2a.sen2chain_version,
                    sen2chain_processing_version=l2a.sen2chain_processing_version,
                    sen2cor_version=l2a.sen2cor_version,
                )
            else:
                Sen2ChainMetadataParser(self._info_path).init_metadata()

    def update_md(
        self,
        sen2chain_version: str = None,
        sen2chain_processing_version: str = None,
        sen2cor_version: str = None,
    ):
        """Set custom sen2chain, sen2chain_processing and sen2cor versions"""
        Sen2ChainMetadataParser(self._info_path).set_metadata(
            sen2chain_version=sen2chain_version,
            sen2chain_processing_version=sen2chain_processing_version,
            sen2cor_version=sen2cor_version,
        )
        
    def remove(self):
        """
        Remove NewCloudMaskProduct Product.
        """
        if self.path.exists():
            os.remove(str(self.path))
            os.remove(str(self._info_path))
            self._qgis_metadata_path = self.path.parent / (self.path.stem + ".jp2.aux.xml")
            try:
                os.remove(str(self._qgis_metadata_path))
            except FileNotFoundError:
                pass
            logger.info("Removing: {}".format(self.path))
        else:
            logger.info("NewCloudMaskProduct product not on disk")
        try:
            self.path.parent.rmdir()
            logger.info("Removing empty product directory")
        except OSError:
            pass
        try:
            self.path.parent.parent.rmdir()
            logger.info("Removing empty tile directory")
        except OSError:
            pass 

class IndiceProduct:
    """Indice Product class used to instanciate an Indice Product. Defined by an identifier, and every parameter needed to compute it and every parameter needed to compute the cloudmask you mask it with.

    :param identifier: Indice product's identifier or SAFE filename.
    :type identifier: str
    :param l2a_identifier: L2A Product identifier from which Indice was computed from
    :type l2a_identifier: str
    :param indice: Radiometric indice to scan for in your library.
    :type indice: str.
    :param sen2chain_processing_version: Sen2chain processing version used to process Indice
    :type sen2chain_processing_version: str
    :param masked: True if indice was masked.
    :type masked: bool, Default to False
    :param nodata_clouds: When True, checks also in produced cloudmasks products.
    :type nodata_clouds: bool, Default to False
    :param cm_version: Sen2chain's CloudMask version.
    :type cm_version: str, Default to "CM001"
    :param probability: Cloud Probability threshold value.
    :type probability: int, Default to 1
    :param iterations: Number of iterations of the dilatation kernel for cloud masking.
    :type iterations: int, Default to 5
    :param cld_shad: When True, uses Cloud Shadows class from Scene Classification.
    :type cld_shad: bool, Default to True
    :param cld_med_prob: When True, uses Cloud Medium Probability class from Scene Classification.
    :type cld_med_prob: bool, Default to True
    :param cld_hi_prob: When True, uses Cloud High Probability class from Scene Classification.
    :type cld_hi_prob: bool, Default to True
    :param thin_cir: When True, uses Thin Cirrus class from Scene Classification.
    :type thin_cir: bool, Default to True
    """

    # _library_path = Path(Config().get("cloudmasks_path"))
    _indices_path = Path(Config().get("indices_path"))

    def __init__(
        self,
        identifier: str = None,
        l2a_identifier: str = None,
        indice: str = None,
        sen2chain_processing_version: str = None,
        masked: bool = False,
        cm_version: str = "CM001",
        probability: int = 1,
        iterations: int = 5,
        cld_shad: bool = True,
        cld_med_prob: bool = True,
        cld_hi_prob: bool = True,
        thin_cir: bool = True,
    ) -> None:
        if not (identifier or (l2a_identifier and indice)):
            raise ValueError(
                "Product identifier or (L2a identifier and indice) cannot be empty"
            )
        else:
            self.tile = NewCloudMaskProduct.get_tile(
                identifier or l2a_identifier
            )
            self.l2a = Path(
                l2a_identifier or NewCloudMaskProduct.get_l2a(identifier)
            ).stem
            self.indice = (
                indice or identifier.replace(".", "_").split("_")[7]
            ).upper()
            self.masked = masked
            
            if identifier:
                self.mask_params = NewCloudMaskProduct.get_cm_version(identifier)
                if self.mask_params:
                    self.masked = True
            else:
                if self.masked == True:
                    self.mask_params = {
                        "cm_version": cm_version.upper(),
                        "probability": probability,
                        "iterations": iterations,
                        "cld_shad": cld_shad,
                        "cld_med_prob": cld_med_prob,
                        "cld_hi_prob": cld_hi_prob,
                        "thin_cir": thin_cir,
                    }
                else:
                    self.mask_params = None
            if self.masked:
                if self.mask_params["cm_version"] == "CM001":
                    self.suffix = "CM001"
                elif self.mask_params["cm_version"] == "CM002":
                    self.suffix = "CM002-B11"
                elif self.mask_params["cm_version"] == "CM003":
                    self.suffix = (
                        "CM003" +
                        "-PRB" + str(self.mask_params["probability"])  + 
                        "-ITER" + str(self.mask_params["iterations"])
                    )
                elif self.mask_params["cm_version"] == "CM004":
                    self.suffix = (
                        "CM004" + 
                        "-CSH" + str(1 * self.mask_params["cld_shad"]) + 
                        "-CMP" + str(1 * self.mask_params["cld_med_prob"]) + 
                        "-CHP" + str(1 * self.mask_params["cld_hi_prob"]) + 
                        "-TCI" + str(1 * self.mask_params["thin_cir"]) + 
                        "-ITER" + str(self.mask_params["iterations"])
                    )
                if identifier:
                    self.identifier = Path(identifier).stem 
                else:
                    self.identifier = self.l2a + "_" + self.indice  + "_" + self.suffix
                self.mask_info = NewCloudMaskProduct.get_cm_version(
                    self.identifier
                )
            else:
                self.suffix = ""
                if identifier:         
                    self.identifier = Path(identifier).stem 
                else:
                    self.identifier = self.l2a + "_" + self.indice
                self.mask_info = None
            self.path = (
                self._indices_path
                / self.indice
                / self.tile
                / self.l2a
                / (self.identifier + ".jp2")
            )
            if self.masked:
                self.unmasked_path = Path(
                    str(self.path).replace(
                        "_" + self.suffix, ""
                    )
                )
            else:
                self.unmasked_path = self.path 

            ## set paths for msk_cldprb_20m and scl_20m
            self.msk_cldprb_20m = None
            self.scl_20m = None
            cloudmask = NewCloudMaskProduct(l2a_identifier = self.l2a)
            self.msk_cldprb_20m = cloudmask.msk_cldprb_20m
            self.scl_20m = cloudmask.scl_20m
            self._info_path = self.path.parent / (self.path.stem + ".xml")
            self._qgis_md_path = self.path.parent / (self.path.stem + ".jp2.aux.xml")
            self._tiff_path = self.path.parent / (self.path.stem + ".tif")
            self._tiff_md_path = self.path.parent / (self.path.stem + ".tif.aux.xml")
            self._ql_path = self.path.parent / (self.path.stem + "_QL.tif")
            self._ql_md_path = self.path.parent / (self.path.stem + "_QL.xml")
            self.init_md()

    @staticmethod
    def get_indice(identifier) -> str:
        """ """
        return re.findall(r"S2.+_(.+)_.*jp2", identifier)[0]

    def init_md(self):
        if self.path.exists() and not self._info_path.exists():
            l2a = L2aProduct(self.l2a)
            if l2a._sen2chain_info_path.exists():
                Sen2ChainMetadataParser(self._info_path).set_metadata(
                    sen2chain_version=l2a.sen2chain_version,
                    sen2chain_processing_version=l2a.sen2chain_processing_version,
                    sen2cor_version=l2a.sen2cor_version,
                )
            else:
                Sen2ChainMetadataParser(self._info_path).init_metadata()
    
    def remove(
        self,
        remove_ql: bool = True,
    ):
        """
        Remove IndiceProduct Product.
        """
        if self.path.exists():
            os.remove(str(self.path))
            os.remove(str(self._info_path))
            if self._qgis_md_path.exists():
                os.remove(str(self._qgis_md_path))
            if self._tiff_path.exists():
                os.remove(str(self._tiff_path))
            if self._tiff_md_path.exists():
                os.remove(str(self._tiff_md_path))
            logger.info("Removing: {}".format(self.path.stem))
        else:
            logger.info("IndiceProduct product not on disk")
        if remove_ql:
            if self._ql_path.exists():
                os.remove(str(self._ql_path))
                os.remove(str(self._ql_md_path))
                logger.info("Removing QL: {}".format(self._ql_path.stem))
        try:
            self.path.parent.rmdir()
            logger.info("Removing empty product directory")
        except OSError:
            pass  
        try:
            self.path.parent.parent.rmdir()
            logger.info("Removing empty tile directory")
        except OSError:
            pass  

class FamilyProduct(dict):
    """Family product class. A Family product groups L1C Product, L2A Product, NewCloudMaskProduct, IndiceProduct or FamilyProduct.
    A Family Product is used to scan the products library before downloading L1C products. If some references of a Family Product exist in the library, the L1C product is not downloaded.

    :param identifier: A Product identifier, can be a L1cProduct, L2aProduct, NewCloudMaskProduct, IndiceProduct or FamilyProduct identifier.
    :type identifier: str
    """
    
    _l1c_library_path = Path(Config().get("l1c_path"))
    _l2a_library_path = Path(Config().get("l2a_path"))
    _cloudmask_library_path = Path(Config().get("cloudmasks_path"))
    _indice_library_path = Path(Config().get("indices_path"))
    
    def __init__(
        self,
        identifier: str = None,
    ):
        if not (identifier):
            raise ValueError(
                "Identifier cannot be empty"
            )
        else:
            fid_tile = self.get_family_id_tile(identifier)
            if fid_tile:
                self.update(fid_tile)
                self.family_id = fid_tile["family_id"]
                self.tile = fid_tile["tile"]
                from .tiles import Tile
                self.date = Tile._get_date(identifier)
                self.l1c_id = self.get_l1c()
                self.l2a_id = self.get_l2a()
                self.cloudmasks = self.get_cloudmasks()
                self.indices = self.get_indices()
                self.cm_ind_string_list = self.get_cm_ind_string_list()
                self.processing_baseline = fid_tile["processing_baseline"][:-2] + "." + fid_tile["processing_baseline"][-2:]
            else:
                logger.info("Invalid identifier {}".format(identifier))
            for key, val in self.__dict__.items():
                self[key] = val

    def get_family_id_tile(self, identifier):
        """Extract Tile from identifier

        :param identifier: Any identifier of the FamilyProduct
        :type identifier: str
        :returns: Tile name
        :rtype: str

        """
        try:
            pat = re.compile(        
                r".*(?P<family_id>"
                + "[0-9]{8}T[0-9]{6}"
                + "_N(?P<processing_baseline>[0-9]{4})_R[0-9]{3}"
                + "_T(?P<tile>[0-9]{2}[A-Z]{3})"
                + "_[0-9]{8}T[0-9]{6}"
                + ").*"
            )
            return pat.match(identifier).groupdict()
        except Exception:
            pass
        
    def get_l1c(self):
        """Returns L1C Product path"""
        library_path = self._l1c_library_path / self.tile
        paths = list(library_path.glob("*L1C*" + self.family_id + "*"))
        if len(paths) > 0:
            return paths[0].stem
        else:
            return None

    def get_l2a(self):
        """Returns L2A Product path"""
        library_path = self._l2a_library_path / self.tile
        paths = list(library_path.glob("*L2A*" + self.family_id + "*"))
        if len(paths) > 0:
            return paths[0].stem
        else:
            return None    
    
    def get_cloudmasks(self):
        """Returns Cloudmask Products version in library"""
        cloudmasks = []
        self.cloudmask_string = []
        try:
            library_path = self._cloudmask_library_path / self.tile
            paths = list(library_path.glob("*L2A*" + self.family_id + "/*L2A*" + self.family_id + "_CM*.jp2"))
            for item in paths:
                cm_dict = get_cm_dict(item.stem)
                cm_string = get_cm_string_from_dict(cm_dict)
                cm_dict["cm_string"] = cm_string
                self.cloudmask_string.append(cm_string)
                cloudmasks.append(cm_dict)
            self.cloudmask_string = list(set(self.cloudmask_string))
        except Exception:
            pass
        return cloudmasks    
        
    def get_indices(self):
        """Returns Indices Products in library for FamilyProduct"""
        indices = []
        self.indice_strings = []
        try:
            library_path = self._indice_library_path
            paths = list(
                library_path.glob(
                    "*/"
                    + self.tile + "/"
                    + "*L2A*" + self.family_id + "/"
                    + "*.jp2"
                )
            )
            for item in paths:
                # logger.info(item)
                indice_dict = {}
                indice_dict["indice"] = get_indice_from_identifier(item.stem)
                self.indice_strings.append(indice_dict["indice"])
                cm_dict = get_cm_dict(item.stem)
                if cm_dict:
                    indice_dict.update(cm_dict)
                    indice_dict["cm_string"] = get_cm_string_from_dict(cm_dict)
                    indice_dict["indice_string"] = indice_dict["indice"] + "_" + indice_dict["cm_string"]
                else:
                    indice_dict["indice_string"] = indice_dict["indice"]
                indices.append(indice_dict)
            self.indice_strings = list(set(self.indice_strings))
        except Exception:
            pass
        return indices  
          
    def get_cm_ind_string_list(self):
        """Returns FamilyProduc existing cloud masks and indices in library"""
        cm_ind_string_list = []
        if self.cloudmasks:
            cm_ind_string_list.extend([cm["cm_string"] for cm in self.cloudmasks])
        if self.indices:
            cm_ind_string_list.extend([ind["indice_string"] for ind in self.indices])     
        return cm_ind_string_list
    
    def remove_indice(
        self,
    ):
        pass
    
    def remove_cm(
        self,
    ):
        pass
