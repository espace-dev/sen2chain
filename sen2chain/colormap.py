# coding: utf-8

"""
Module for computing colormaps from single band rasters.
"""

import logging
import pathlib
import csv
from pathlib import Path
from typing import Any

# import colorcet as cc
import rasterio
from rasterio.warp import calculate_default_transform, reproject, Resampling
import matplotlib
from matplotlib.colors import LinearSegmentedColormap
import numpy as np
from typing import List, Dict, Tuple, Union


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


# TODO: add colormap.lut to matplotlib colormap

# def colorcet_colormap_to_rgb(cmap, revers=False):
#     """
#     """
#     if revers:
#         d = reversed(cmap)
#     else:
#         d = cmap
#     d2 = {i: hex_to_rgb(hex_string) for i, hex_string in enumerate(d)}
#
#     return d2
#
# def hex_to_rgb(hex_string):
#     """
#     Converts a hexidecimal code to a RGB value.
#
#     :param hex_string: hexidecimal string.
#     """
#     hexs = hex_string.lstrip('#')
#     size = len(hexs)
#     r, g, b = tuple(int(hexs[i:i + size // 3], 16) for i in range(0, size, size // 3))
#     return r, g, b


def matplotlib_colormap_to_rgb(
    cmap: matplotlib.colors.LinearSegmentedColormap, revers=False
) -> Dict[int, Tuple[int, int, int]]:
    """
    Returns a matplotlib colormap as a dictionnary of RGB values.
    See: https://matplotlib.org/examples/color/colormaps_reference.html

    :param cmap: matplotlib colormap.
    :param revers: if true, reverse the colormap.
    """

    def float_to_integer(flt):
        """Returns a normalised ([0-1]) value in the [0-255] range."""
        return int(round(flt * 255))

    cmap_list = [cmap(i) for i in range(0, 256)]  # type: List[Any]
    if revers:
        cmap_list = reversed(cmap_list)

    cmap_dict = {
        i: (
            float_to_integer(v[0]),
            float_to_integer(v[1]),
            float_to_integer(v[2]),
        )
        for i, v in enumerate(cmap_list)
    }  # type: Dict[int, Tuple[int, int, int]]
    return cmap_dict


def get_tsv_colormap(lut_path: str) -> Dict[int, Tuple[int, int, int]]:
    """
    Reads a tsv look-up table (values separated by tabs) and returns
    the content as a dictionnary : {i:(r, g, b), ...}.

    :param lut_path: look-up table path.
    """
    logger.debug("retrieving look-up table from {}".format(lut_path))

    if not Path(lut_path).exists():
        raise IOError("File not found!")

    with open(str(lut_path), "r") as csv_file:
        reader = csv.reader(csv_file, delimiter=" ")
        colormap_dict = dict()  # type: Dict[int, Tuple[int, int, int]]
        for row in reader:
            color = tuple(
                [int(row[1]), int(row[2]), int(row[3])]
            )  # type: Tuple[int, int, int]
            colormap_dict[int(row[0])] = color
    return colormap_dict


def create_colormap(
    raster: Union[str, pathlib.PosixPath],
    lut_dict: Dict[int, Tuple[int, int, int]],
    cloud_mask: str = None,
    out_path: Union[str, pathlib.PosixPath] = "./colormap.tif",
    out_crs: str = None,
    clouds_color: str = "black",
    out_resolution: Tuple[int, int] = (100, 100),
) -> str:
    """
    Colormapping of a single-band raster with a look-up table passed as
    a dictionary {i:(r, g, b), ...}. If a cloud mask is provided, clouds
    are colored in black

    :param raster: path to raster.
    :param lut_dict: dictionary containing RGB values.
    :param cloud_mask: path to the cloud mask.
    :param out_path: path to the output.
    :param out_crs: output CRS.
    :param clouds_color: color of the clouds (black or white).
    :param out_resolution: output resolution.
    """
    # LOGGER.info("creating colormap")

    # cloud mask
    if not Path(str(cloud_mask)).is_file():
        cloud_mask = None
        logger.error(
            "Cloud mask path wrong or not provided \
        \nClouds will not be colored"
        )

    # clouds color
    if clouds_color.lower() == "black":
        cld_val = 0
        # lut_dict[0] = (0, 0, 0)
    elif clouds_color.lower() == "white":
        cld_val = 255
        # lut_dict[0] = (255, 255, 255)
    else:
        logger.warning(
            'Available clouds colors: "black" or "white" \
        \nApplying default: black'
        )
    lut_dict[0] = (0, 0, 0)
    lut_dict[255] = (255, 255, 255)

    # check out_crs
    if out_crs:
        try:
            out_crs = CRS.from_string(out_crs.upper())
        except Exception as e:
            out_crs = None
            print("Invalid CRS: {}\nUsing source raster CRS".format(e))

    with rasterio.open(str(raster)) as src:
        raster_band = src.read(1)
        profile = src.profile

        if cloud_mask:
            with rasterio.open(str(cloud_mask)) as cld_src:
                clouds_band = cld_src.read(1)
                # resample cloud_mask to raster grid
                cld_reproj = np.empty(raster_band.shape, dtype=np.uint8)
                reproject(
                    source=clouds_band,
                    destination=cld_reproj,
                    src_transform=cld_src.transform,
                    src_crs=cld_src.crs,
                    dst_transform=src.transform,
                    dst_crs=src.crs,
                    resampling=Resampling.nearest,
                )
                # clouds
                raster_band = np.where(cld_reproj == 0, raster_band, 16383)

        # band_mask_borders = np.where(raster_band != 32767, raster_band, -10000)

        cmap = np.where(raster_band == 16383, -10000, raster_band)
        cmap = (128 * (cmap / 10000 + 1) * ((cmap + 10000) > 0)).astype(
            np.uint8
        )
        cmap = np.where(cld_reproj == 1, cld_val, cmap)
        # compute default transform, width and height to fit the out resolution
        dst_transform, dst_width, dst_height = calculate_default_transform(
            src.crs,
            src.crs,
            src.width,
            src.height,
            *src.bounds,
            resolution=out_resolution
        )

        out_crs = src.crs if not out_crs else out_crs
        profile.update(
            nodata=0,
            driver="Gtiff",
            # compress="DEFLATE",
            dtype=np.uint8,
            transform=dst_transform,
            width=dst_width,
            height=dst_height,
            crs=out_crs,
        )
    # write colormap to out_path
    with rasterio.open(str(out_path), "w", **profile) as dst:
        dst.write(cmap, 1)
        dst.write_colormap(1, lut_dict)

    return str(Path(str(out_path)).absolute)


def create_rvb(
    raster: Union[str, pathlib.PosixPath],
    lut_dict: Dict[int, Tuple[int, int, int]],
    cloud_mask: str = None,
    out_path: Union[str, pathlib.PosixPath] = "./colormap_rvb.tif",
    out_crs: str = None,
    clouds_color: str = "black",
    out_resolution: Tuple[int, int] = (100, 100),
    stretch: Tuple[float, float] = (-10000, 10000),
) -> str:
    """
    Colormapping of a single-band raster with a look-up table passed as
    a dictionary {i:(r, g, b), ...}. If a cloud mask is provided, clouds
    are colored in black

    :param raster: path to raster.
    :param lut_dict: dictionary containing RGB values.
    :param cloud_mask: path to the cloud mask.
    :param out_path: path to the output.
    :param out_crs: output CRS.
    :param clouds_color: color of the clouds (black or white).
    :param out_resolution: output resolution (pixel size (m)).
    """
    # LOGGER.info("creating colormap")
    logger.info("creating rvb QL")
    # cloud mask
    if not Path(str(cloud_mask)).is_file():
        cloud_mask = None
        logger.error(
            "Cloud mask path wrong or not provided \
        \nClouds will not be colored"
        )

    # clouds color
    if clouds_color.lower() == "black":
        cld_val = 0
        # lut_dict[0] = (0, 0, 0)
    elif clouds_color.lower() == "white":
        cld_val = 255
        # lut_dict[0] = (255, 255, 255)
    else:
        logger.warning(
            'Available clouds colors: "black" or "white" \
        \nApplying default: black'
        )
    lut_dict[0] = (0, 0, 0)
    lut_dict[255] = (255, 255, 255)

    # check out_crs
    if out_crs:
        try:
            out_crs = CRS.from_string(out_crs.upper())
        except Exception as e:
            out_crs = None
            print("Invalid CRS: {}\nUsing source raster CRS".format(e))

    fact = int(1000 // out_resolution[0])
    fact_cld = int(1000 // (2 * out_resolution[0]))

    with rasterio.open(str(raster)) as src:
        raster_band = src.read(
            1, out_shape=(1, int(src.height // fact), int(src.height // fact))
        )
        profile = src.profile

        if cloud_mask:
            with rasterio.open(str(cloud_mask)) as cld_src:
                cld_reproj = cld_src.read(
                    1,
                    out_shape=(
                        1,
                        int(cld_src.height // fact_cld),
                        int(cld_src.height // fact_cld),
                    ),
                )
                cld_band = cld_src.read(1)
                raster_band = np.where(cld_reproj == 0, raster_band, 16383)

        # band_mask_borders = np.where(raster_band != 32767, raster_band, -10000)

        # cmap = np.where(raster_band == 16383, -10000, raster_band)
        # cmap = (128 * (cmap/10000 + 1) * ((cmap+10000) > 0)).astype(np.uint8)

        cmap = np.where(raster_band == 16383, stretch[0], raster_band)
        cmap = np.clip(
            (
                255
                * (cmap - stretch[0]).astype(np.float)
                / (stretch[1] - stretch[0])
            ),
            0,
            255,
        ).astype(np.uint8)

        cmap = np.where(cld_reproj == 1, cld_val, cmap)
        # compute default transform, width and height to fit the out resolution
        dst_transform, dst_width, dst_height = calculate_default_transform(
            src.crs,
            src.crs,
            src.width,
            src.height,
            *src.bounds,
            resolution=out_resolution
        )

        out_crs = src.crs if not out_crs else out_crs
        profile.update(
            nodata=0,
            tiled=False,
            driver="Gtiff",
            dtype=np.uint8,
            transform=dst_transform,
            width=dst_width,
            height=dst_height,
            crs=out_crs,
            count=3,
        )

    with rasterio.open(str(out_path), "w", **profile) as dst:
        for k in range(3):
            cmap_temp = np.copy(cmap)
            for idx in np.nditer(cmap_temp, op_flags=["readwrite"]):
                idx[...] = lut_dict[int(idx)][k]
            dst.write(cmap_temp.astype(np.uint8), k + 1)
    return str(Path(str(out_path)).absolute)


def create_l2a_ql(
    b02: Union[str, pathlib.PosixPath],
    b03: Union[str, pathlib.PosixPath],
    b04: Union[str, pathlib.PosixPath],
    out_path: Union[str, pathlib.PosixPath] = "./L2A_QL.tif",
    out_resolution: Tuple[int, int] = (100, 100),
    jpg=False,
) -> str:
    """
    Creating a color RVB quicklook from 3 single band files passed as arguments
    :param b02: path to B raster
    :param b03: path to G raster
    :param b04: path to R raster
    :param out_path: path to the output.
    :param out_resolution: output resolution, default 100mx100m .
    """

    logger.info("creating L2A QL")
    file_list = [str(b04), str(b03), str(b02)]

    with rasterio.open(file_list[0]) as src:
        profile = src.profile
        dst_transform, dst_width, dst_height = calculate_default_transform(
            src.crs,
            src.crs,
            src.width,
            src.height,
            *src.bounds,
            resolution=out_resolution
        )

    if out_path.suffix == ".jpg" or jpg:
        profile.update(
            nodata=0,
            driver="JPEG",
            dtype=np.uint8,
            transform=dst_transform,
            width=dst_width,
            height=dst_height,
            tiled=False,
            count=len(file_list),
        )
        profile.pop("tiled", None)
        profile.pop("blockxsize", None)
        profile.pop("blockysize", None)

    else:
        profile.update(
            nodata=0,
            driver="Gtiff",
            dtype=np.uint8,
            transform=dst_transform,
            width=dst_width,
            height=dst_height,
            tiled=False,
            count=len(file_list),
        )

    with rasterio.open(str(out_path), "w", **profile) as dst:
        for id, layer in enumerate(file_list, start=1):
            with rasterio.open(layer) as src1:
                raster_band = src1.read(1)
                raster_band = np.where(raster_band > 2000, 2000, raster_band)
                raster_band = np.where(
                    raster_band > 1, (raster_band / 2000) * 255, raster_band
                )
                raster_band = raster_band.astype(np.uint8)
                dst.write_band(id, raster_band)
    return str(Path(str(out_path)).absolute)


def create_l1c_ql(
    tci: Union[str, pathlib.PosixPath],
    out_path: Union[str, pathlib.PosixPath] = "./L1C_QL.tif",
    out_resolution: Tuple[int, int] = (100, 100),
    jpg=False,
) -> str:
    """
    Creating a color RVB quicklook from tci 3 bands file passed as argument
    :param tci: path to tci raster
    :param out_path: path to the output.
    :param out_resolution: output resolution, default 100mx100m .
    """

    logger.info("creating L1C QL")

    with rasterio.open(str(tci)) as src:
        profile = src.profile

        fact = int(1000 // out_resolution[0])

        dst_transform, dst_width, dst_height = calculate_default_transform(
            src.crs,
            src.crs,
            src.width,
            src.height,
            *src.bounds,
            resolution=out_resolution
        )

        if out_path.suffix == ".jpg" or jpg:
            profile.update(
                nodata=0,
                driver="JPEG",
                dtype=np.uint8,
                transform=dst_transform,
                width=dst_width,
                height=dst_height,
                tiled=False,
                count=3,
            )
            profile.pop("tiled", None)
            profile.pop("blockxsize", None)
            profile.pop("blockysize", None)
            profile.pop("interleave", None)
        else:
            profile.update(
                nodata=0,
                driver="Gtiff",
                dtype=np.uint8,
                transform=dst_transform,
                width=dst_width,
                height=dst_height,
                tiled=False,
                count=3,
            )

        with rasterio.open(str(out_path), "w", **profile) as dst:
            # with rasterio.open(str(tci)) as src1:
            for band_id in range(1, 4):
                logger.info(band_id)
                raster_band = src.read(
                    band_id,
                    out_shape=(
                        1,
                        int(src.height // fact),
                        int(src.height // fact),
                    ),
                ).astype(np.uint8)
                # raster_band = np.where(raster_band > 2000, 2000, raster_band)
                # raster_band = np.where(raster_band > 1, (raster_band/2000)*255, raster_band)
                # raster_band = raster_band.astype(np.uint8)
                dst.write_band(band_id, raster_band)
    return str(Path(str(out_path)).absolute)


def create_l1c_ql_v2(
    tci: Union[str, pathlib.PosixPath],
    out_path: Union[str, pathlib.PosixPath] = "./L1C_QL.tif",
    out_resolution: Tuple[int, int] = (100, 100),
    jpg=False,
) -> str:
    """
    Creating a color RVB quicklook from tci 3 bands file passed as argument
    :param tci: path to tci raster
    :param out_path: path to the output.
    :param out_resolution: output resolution, default 100mx100m .
    """
    with rasterio.open(str(tci)) as src:
        profile = src.profile

        fact = int(out_resolution[0] // 10)

        dst_transform, dst_width, dst_height = calculate_default_transform(
            src.crs,
            src.crs,
            src.width,
            src.height,
            *src.bounds,
            resolution=out_resolution
        )

        logger.info(
            "creating L1C QL - {}m/px - {}px".format(
                out_resolution[0], dst_width
            )
        )

        if out_path.suffix == ".jpg" or jpg:
            profile.update(
                nodata=0,
                driver="JPEG",
                dtype=np.uint8,
                transform=dst_transform,
                width=dst_width,
                height=dst_height,
                tiled=False,
                count=3,
            )
            profile.pop("tiled", None)
            profile.pop("blockxsize", None)
            profile.pop("blockysize", None)
            profile.pop("interleave", None)
        else:
            profile.update(
                nodata=0,
                driver="Gtiff",
                dtype=np.uint8,
                transform=dst_transform,
                width=dst_width,
                height=dst_height,
                tiled=False,
                count=3,
            )
        with rasterio.open(str(out_path), "w", **profile) as dst:
            raster_band = src.read(
                out_shape=(
                    src.count,
                    dst_width,
                    dst_height,
                ),
                resampling=Resampling.nearest,
            ).astype(np.uint8)
            dst.write(raster_band)
    return str(Path(str(out_path)).absolute)


def create_l2a_ql_v2(
    tci: Union[str, pathlib.PosixPath],
    out_path: Union[str, pathlib.PosixPath] = "./L2A_QL.tif",
    out_resolution: Tuple[int, int] = (100, 100),
    jpg=False,
) -> str:
    """
    Creating a color RVB quicklook from tci 3 bands file passed as argument
    :param tci: path to tci raster
    :param out_path: path to the output.
    :param out_resolution: output resolution, default 100mx100m .
    """
    with rasterio.open(str(tci)) as src:
        profile = src.profile

        fact = int(out_resolution[0] // 10)

        dst_transform, dst_width, dst_height = calculate_default_transform(
            src.crs,
            src.crs,
            src.width,
            src.height,
            *src.bounds,
            resolution=out_resolution
        )

        logger.info(
            "creating L2A QL - {}m/px - {}px".format(
                out_resolution[0], dst_width
            )
        )

        if out_path.suffix == ".jpg" or jpg:
            profile.update(
                nodata=0,
                driver="JPEG",
                dtype=np.uint8,
                transform=dst_transform,
                width=dst_width,
                height=dst_height,
                tiled=False,
                count=3,
            )
            profile.pop("tiled", None)
            profile.pop("blockxsize", None)
            profile.pop("blockysize", None)
            profile.pop("interleave", None)
        else:
            profile.update(
                nodata=0,
                driver="Gtiff",
                dtype=np.uint8,
                transform=dst_transform,
                width=dst_width,
                height=dst_height,
                tiled=False,
                count=3,
            )
        with rasterio.open(str(out_path), "w", **profile) as dst:
            raster_band = src.read(
                out_shape=(
                    src.count,
                    dst_width,
                    dst_height,
                ),
                resampling=Resampling.nearest,
            ).astype(np.uint8)
            dst.write(raster_band)

    return str(Path(str(out_path)).absolute)
