# coding: utf-8

"""
Module for computing cloud-mask from a sen2cor clouds classification raster.
"""

import os
import logging
import pathlib
from pathlib import Path
import pyproj

import shapely.geometry
from shapely.geometry import (
    box,
    Polygon,
    MultiPolygon,
    GeometryCollection,
    mapping,
    shape,
)
from shapely.ops import transform
import rasterio
from rasterio.features import shapes, rasterize
import numpy as np
from itertools import compress
from scipy import ndimage
from typing import Sequence, List, Dict, Union
from osgeo import gdal

from .config import Config

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def max_dtype_value(dtype: np.dtype):
    """Returns the numpy dtype maximum value."""
    return np.iinfo(dtype).max


def mask_footprint(
    raster_band: Union[str, pathlib.PosixPath],
    footprint: List[float],
    out_path: Union[str, pathlib.PosixPath] = "./footprint_masked.tif",
) -> None:
    """Masks a raster band with a footprint.

    :param raster_band: path to the raster band.
    :param footprint: footprint.
    :param out_path: path to output.
    """
    with rasterio.open(str(raster_band), "r") as src:
        profile = src.profile
        band = src.read(1)

        fp_poly = Polygon(footprint)
        # FIXME: use def instead of lambda assignment
        projection = lambda x, y: pyproj.transform(
            pyproj.Proj(init="epsg:4326"),
            pyproj.Proj(profile["crs"].to_string()),
            x,
            y,
        )
        fp_poly_reproj = transform(projection, fp_poly)

        fp_band = rasterize(
            shapes=[mapping(fp_poly_reproj)],
            all_touched=True,
            dtype=np.uint8,
            out_shape=(profile["width"], profile["height"]),
            transform=profile["affine"],
        )

        nodata_value = max_dtype_value(profile["dtype"])
        band_masked = np.where(fp_band == 1, band, nodata_value).astype(
            np.uint16
        )

        profile.update(
            driver="Gtiff",
            nodata=nodata_value,
            tiled=False,
            transform=profile["affine"],
        )

    with rasterio.open(str(out_path), "w", **profile) as out:
        out.write(band_masked, 1)


def katana(
    geometry: Union[
        shapely.geometry.polygon.Polygon,
        shapely.geometry.multipolygon.MultiPolygon,
    ],
    threshold: int = 2500,
    count: int = 0,
) -> List[shapely.geometry.polygon.Polygon]:
    """
    Splits a Polygon into two parts across it's shortest dimension
    recursively

    :param geometry: polygon or multipolygon to split.
    :param threshold: maximum value of the widest dimension.
    :param count: number of iterations.
    """
    # Credits: Joshua Arnott
    # Licence: BSD 2-clause
    # 'https://snorfalorpagus.net/blog/2016/03/13/splitting-large-polygons-for-
    # faster-intersections/'
    logger.debug("Splitting polygons.")

    bounds = geometry.bounds
    width = bounds[2] - bounds[0]
    height = bounds[3] - bounds[1]

    if max(width, height) <= threshold or count == 250:
        # either the polygon is smaller than the threshold, or the maximum
        # number of recursions has been reached
        return [geometry]

    if height >= width:
        # split left to right
        part_1 = box(bounds[0], bounds[1], bounds[2], bounds[1] + height / 2)
        part_2 = box(bounds[0], bounds[1] + height / 2, bounds[2], bounds[3])

    else:
        # split top to bottom
        part_1 = box(bounds[0], bounds[1], bounds[0] + width / 2, bounds[3])
        part_2 = box(bounds[0] + width / 2, bounds[1], bounds[2], bounds[3])

    result = []
    for each in (
        part_1,
        part_2,
    ):
        intersec = geometry.intersection(each)
        if not isinstance(intersec, GeometryCollection):
            intersec = [intersec]
        for part in intersec:
            if isinstance(part, (Polygon, MultiPolygon)):
                result.extend(katana(part, threshold, count + 1))

    if count > 0:
        return result

    # convert multipart into singlepart
    final_result = []
    for geom in result:
        if isinstance(geom, MultiPolygon):
            final_result.extend(geom)
        else:
            final_result.append(geom)
    return final_result


def erosion_dilatation(
    features: Sequence[Dict],
    threshold: int = 2500,
    erosion: int = -20,
    dilatation: int = 100,
) -> Dict:
    """
    Erosion then dilatation.

    :param features: list of GeoJSON like objects.
    :param threshold: maximum value of the widest dimension of the features.
    :param erosion: size of the outer buffer.
    :param dilatation: size of the inner buffer.
    """
    for feat in features:
        geom = shape(feat["geometry"])

        for part in katana(geom.buffer(0.0), threshold, count=0):
            # buffer(0.0) avoids ring self-intersection error
            eroded = shape(part).buffer(float(erosion))
            new_geom = eroded if int(eroded.area) else "POLYGON EMPTY"

            if (
                str(new_geom) != "POLYGON EMPTY"
            ):  # small geometries are empty after erosion
                yield (
                    {
                        "properties": feat["properties"],
                        "geometry": mapping(
                            new_geom.buffer(float(dilatation))
                        ),
                    }
                )


def create_cloud_mask(
    cloud_mask: Union[str, pathlib.PosixPath],
    out_path="./cloud_mask.tif",
    buffering: bool = True,
    erosion: int = None,
    dilatation: int = None,
) -> None:
    """
    create cloud mask
    uint8

    :param cloud_mask: path to the cloud mask raster.
    :param out_path: path to the output.
    :param buffering: if true, computes erosion/dilation.
    :param erosion: size of the outer buffer.
    :param dilatation: size of the inner buffer.
    """
    logger.info("Creating cloud-mask.")
    # normalisation
    with rasterio.open(str(cloud_mask), "r") as src:
        profile = src.profile
        band = src.read(1)

    # normalisation
    # print("normalisation")
    mean = band.mean()
    std_dev = np.std(band)
    band_norm = (band - mean) / std_dev

    # positive array
    # print("positive array")
    band_norm01 = np.where(band_norm > 0, 1, 0)
    band_norm01 = band_norm01.astype(np.uint8, copy=False)

    # cloud-less array
    # print("cloud-less array")
    band_norm01_masked = np.ma.masked_where(band_norm01 == 1, band_norm01).mask

    with rasterio.open(
        str(out_path),
        "w",
        driver="Gtiff",
        height=profile["height"],
        width=profile["width"],
        count=1,
        dtype=np.uint8,
        transform=profile["affine"],
        crs=profile["crs"],
        tiled=False,
    ) as out:

        # with erosion-dilatation
        if buffering:
            logger.info("Erosion-dilatation.")

            # vectorization
            vectors = (
                {"properties": {"raster_val": v}, "geometry": s}
                for i, (s, v) in enumerate(
                    shapes(
                        band_norm01,
                        mask=band_norm01_masked,
                        transform=profile["affine"],
                    )
                )
            ) < qw

            # buffering
            buffers = erosion_dilatation(
                vectors, erosion=erosion, dilatation=dilatation
            )
            shapes_values = ((f["geometry"], 1) for f in buffers)

            # rasterisation
            burned = rasterize(
                shapes=shapes_values,
                all_touched=False,
                dtype=np.uint8,
                out_shape=(profile["width"], profile["height"]),
                transform=profile["affine"],
            )

            # write band
            out.write(burned, 1)

        # without erosion dilatation
        else:
            out.write(band_norm01, 1)
            logger.info("Computed cloud-mask at: {}.".format(str(out_path)))

    # return str(Path(str(out_path)).absolute)


def create_cloud_mask_v2(
    cloud_mask: Union[str, pathlib.PosixPath],
    out_path="./cloud_mask_v2.jp2",
    erosion: int = 1,
    dilatation: int = 5,
) -> None:
    """
    Create cloud mask
    :param cloud_mask: path to the cloud mask raster.
    :param out_path: path to the output.
    :param erosion: size of the outer buffer in px.
    :param dilatation: size of the inner buffer in px.
    """

    out_temp_path = Path(Config().get("temp_path"))
    out_dilate = str(out_temp_path / (out_path.stem + "_tmp_dilate.tif"))
    out_temp2 = str(out_temp_path / (out_path.stem + "_tmp_dilate_2.tif"))

    CLD_seuil = 25
    with rasterio.open(str(cloud_mask)) as cld_src:
        cld = cld_src.read(1).astype(np.int16)
        cld_profile = cld_src.profile
    cld_bin = np.where(cld > CLD_seuil, 1, 0).astype(np.uint8)
    cld_erode = ndimage.binary_erosion(cld_bin).astype(cld_bin.dtype)
    kernel = np.array(
        [
            [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
            [0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
            [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
        ]
    )
    cld_dilate = ndimage.binary_dilation(cld_erode, kernel).astype(
        cld_erode.dtype
    )

    cld_profile.update(
        driver="Gtiff",
        compress="NONE",
        tiled=False,
        dtype=np.int8,
        transform=cld_src.transform,
        count=1,
    )

    cld_profile.pop("tiled", None)
    cld_profile.pop("nodata", None)
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(out_dilate, "w", **cld_profile) as dst:
            dst.write(cld_dilate.astype(np.int8), 1)

    # Save to JP2000
    src_ds = gdal.Open(out_dilate)
    src_ds = gdal.Translate(out_temp2, src_ds, outputType=gdal.GDT_Byte)
    driver = gdal.GetDriverByName("JP2OpenJPEG")
    dst_ds = driver.CreateCopy(
        str(out_path),
        src_ds,
        options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"],
    )
    dst_ds = None
    src_ds = None
    os.remove(out_dilate)
    os.remove(out_temp2)
    logger.info("Done: {}".format(out_path.name))


def create_cloud_mask_b11(
    cloud_mask: Union[str, pathlib.PosixPath],
    b11_path: Union[str, pathlib.PosixPath],
    out_path="./cloud_mask_b11.jp2",
    dilatation: int = 5,
) -> None:
    """
    Marking cloud mask v2 with B11 values to reduce overdetection of clouds for
    some specific water bodies (uint8).

    :param cloud_mask: path to the cloud mask raster (sen2chain).
    :param b11_path: path to the l2a b11.
    :param out_path: path to the output.
    :param erosion: size of the outer buffer in px.
    :param dilatation: size of the inner buffer in px.
    """

    out_temp_path = Path(Config().get("temp_path"))
    out_mask = str(out_temp_path / (out_path.stem + "_tmp_mask.tif"))
    out_temp2 = str(out_temp_path / (out_path.stem + "_tmp_mask_2.tif"))

    b11_seuil = 1500
    with rasterio.open(str(b11_path)) as b11_src:
        b11 = b11_src.read(1).astype(np.int16)
    b11_bin = np.where(b11 < b11_seuil, 1, 0).astype(np.uint8)

    kernel = np.array(
        [
            [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
            [0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
            [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
        ]
    )
    b11_dilate = ndimage.binary_dilation(b11_bin, kernel).astype(b11_bin.dtype)

    with rasterio.open(str(cloud_mask)) as cld_src:
        cld = cld_src.read(1).astype(np.int16)
        cld_profile = cld_src.profile
    cld_mskd = ((cld == 1) * (b11_dilate == 0)).astype(np.uint8)

    kernel = np.array(
        [
            [0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
            [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
            [0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
            [0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0],
        ]
    )
    cld_mskd_dilate = ndimage.binary_dilation(cld_mskd, kernel).astype(
        cld_mskd.dtype
    )

    cld_profile.update(
        driver="Gtiff",
        compress="NONE",
        tiled=False,
        dtype=np.int8,
        transform=cld_src.transform,
        count=1,
    )

    cld_profile.pop("tiled", None)
    cld_profile.pop("nodata", None)
    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(out_mask, "w", **cld_profile) as dst:
            dst.write(cld_mskd_dilate.astype(np.int8), 1)

    # Save to JP2000
    src_ds = gdal.Open(out_mask)
    src_ds = gdal.Translate(out_temp2, src_ds, outputType=gdal.GDT_Byte)
    driver = gdal.GetDriverByName("JP2OpenJPEG")
    dst_ds = driver.CreateCopy(
        str(out_path),
        src_ds,
        options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"],
    )
    dst_ds = None
    src_ds = None

    os.remove(out_mask)
    os.remov(out_temp2)
    logger.info("Done: {}".format(out_path.name))


def create_cloud_mask_v003(
    cloud_mask: Union[str, pathlib.PosixPath],
    out_path="./CM003.jp2",
    probability: int = 1,
    iterations: int = 1,
) -> None:
    
    """
    Create cloud mask version cm003. This cloudmask uses a simple thresholding
    and dilatations over the 20m cloud_probability band from Sen2Cor. The
    threshold value and number of dilatation cycles can be manually modified by
    the user. Default values 1% and 5 cycles.
    :param cloud_mask: path to the 20m cloud mask raster.
    :param out_path: path to the output file.
    :param probability: threshold in percent for the 20m cloud_probability band binarisation.
    :param iterations: number of dilatation cylces to apply.
    """

    out_temp_path = Path(Config().get("temp_path"))
    out_temp = str(out_temp_path / (out_path.stem + "_tmp_cm003.tif"))
    out_temp2 = str(out_temp_path / (out_path.stem + "_tmp_cm003_2.tif"))

    with rasterio.open(str(cloud_mask)) as cld_src:
        cld_profile = cld_src.profile
        cld = cld_src.read(1).astype(np.int8)

    cld = np.where(cld >= probability, 1, 0)

    kernel = np.array(
        [
            [0, 1, 1, 1, 0],
            [1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1],
            [0, 1, 1, 1, 0],
        ]
    )

    cld_dilated = ndimage.binary_dilation(cld, kernel, iterations=iterations)
    cld_profile.update(
        driver="Gtiff", compress="DEFLATE", tiled=False, dtype=np.int8
    )

    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_temp), "w", **cld_profile) as dst:
            dst.write(cld_dilated.astype(np.int8), 1)

    # Save to JP2000
    src_ds = gdal.Open(out_temp)
    src_ds = gdal.Translate(out_temp2, src_ds, outputType=gdal.GDT_Byte)
    driver = gdal.GetDriverByName("JP2OpenJPEG")
    dst_ds = driver.CreateCopy(
        str(out_path),
        src_ds,
        options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"],
    )
    dst_ds = None
    src_ds = None

    os.remove(out_temp)
    os.remove(out_temp2)
    logger.info("Done: {}".format(out_path.name))


def create_cloud_mask_v004(
    scl_path: Union[str, pathlib.PosixPath],
    out_path="./CM004.jp2",
    iterations: int = 1,
    cld_shad: bool = True,
    cld_med_prob: bool = True,
    cld_hi_prob: bool = True,
    thin_cir: bool = True,
) -> None:
    """
    create cloud mask version cm004. This cloudmask uses 20m resolution SCL
    image from Sen2Cor. The number of dilatation cycles can be manually
    modified by the user. Default value: 5 cycles.
    :param scl_path: path to the Sen2Cor 20m scene classification raster.
    :param out_path: path to the output file.
    :param iterations: number of dilatation cylces to apply.
    :param cld_shad: usage of the CLOUD_SHADOWS (3)
    :param cld_med_prob: usage of the CLOUD_MEDIUM_PROBABILITY (8)
    :param cld_hi_prob: usage of the CLOUD_HIGH_PROBABILITY (9)
    :param thin_cir: usage of the THIN_CIRRUS (10)
    """

    out_temp_path = Path(Config().get("temp_path"))
    out_temp = str(out_temp_path / (out_path.stem + "_tmp_cm004.tif"))
    out_temp2 = str(out_temp_path / (out_path.stem + "_tmp_cm004_2.tif"))

    with rasterio.open(str(scl_path)) as scl_src:
        scl_profile = scl_src.profile
        scl = scl_src.read(1).astype(np.int8)

    list_values = list(
        compress(
            [3, 8, 9, 10], [cld_shad, cld_med_prob, cld_hi_prob, thin_cir]
        )
    )
    cld = np.isin(scl, list_values)

    if iterations > 0:
        kernel = np.array(
            [
                [0, 1, 1, 1, 0],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [0, 1, 1, 1, 0],
            ]
        )
        cld_dilated = ndimage.binary_dilation(
            cld, kernel, iterations=iterations
        )
    else:
        cld_dilated = cld

    scl_profile.update(
        driver="Gtiff", compress="DEFLATE", tiled=False, dtype=np.int8
    )

    with rasterio.Env(GDAL_CACHEMAX=512) as env:
        with rasterio.open(str(out_temp), "w", **scl_profile) as dst:
            dst.write(cld_dilated.astype(np.int8), 1)

    # Save to JP2000
    src_ds = gdal.Open(out_temp)
    src_ds = gdal.Translate(out_temp2, src_ds, outputType=gdal.GDT_Byte)
    
    driver = gdal.GetDriverByName("JP2OpenJPEG")
    dst_ds = driver.CreateCopy(
        str(out_path),
        src_ds,
        options=["CODEC=JP2", "QUALITY=100", "REVERSIBLE=YES", "YCBCR420=NO"],
    )
    dst_ds = None
    src_ds = None
    
    os.remove(out_temp)
    os.remove(out_temp2)
    
    logger.info("Done : {}".format(out_path.name))
