# coding: utf-8

import multiprocessing, subprocess
import os, signal
import time
import logging
from functools import partial
import sys

# import psutil

from .products import L1cProduct, L2aProduct

logger = logging.getLogger("Multiprocessing")
logging.basicConfig(level=logging.INFO)
logger.setLevel(logging.INFO)


def multi(product_copyl2asideproducts):
    product = product_copyl2asideproducts[0]
    copy_l2a_sideproducts = product_copyl2asideproducts[1]
    remove_used_l1c = product_copyl2asideproducts[2]
    proc = None
    try:
        fwd = os.path.dirname(os.path.realpath(__file__))
        logger.info("Processing {}".format(product))
        l1c = L1cProduct(product)
        if l1c.processable_to_l2a():
            cmd = [
                "setsid",
                sys.executable,
                fwd + "/multiprocess_l2a.py",
                product,
                str(copy_l2a_sideproducts),
            ]
            # logger.info(cmd)
            proc = subprocess.Popen(cmd)

            # l1c = L1cProduct(product)
            l2a_identifier = l1c.identifier.replace("L1C_", "L2A_").replace(
                "_OPER_", "_USER_"
            )
            l2a_prod = L2aProduct(l2a_identifier)
            timeout = time.time() + 2 * 60 * 60
            while not (l2a_prod.in_library):
                if time.time() > timeout:
                    logger.info(
                        "Timeout (2h) reached for Sen2Cor processing, killing process {}".format(
                            proc.pid
                        )
                    )
                    os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
                    break
                time.sleep(5)
            logger.info("End {}".format(product))
            if l2a_prod.in_library and remove_used_l1c:
                l1c.remove()
    except Exception:
        logger.info("Plante {}".format(product))
        if proc:
            logger.info("Killing process {}".format(proc.pid))
            os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
        pass


def l2a_multiprocessing(process_list, nb_proc=4):
    """ """
    nb_proc = max(min(len(os.sched_getaffinity(0)) - 1, nb_proc), 1)
    with multiprocessing.Pool(nb_proc) as pool:
        pool.map(multi, process_list)
    return True


# def multi_cldidx(indice_list, l2a_identifier):
#     l2a = L2aProduct(l2a_identifier)
#     l2a.process_cloud_mask_v2()
#     l2a.process_indices(indice_list, True, True)

# def cldidx_multiprocessing(process_list, indice_list=["NDVI", "NDWIGAO", "NDWIMCF"], nb_proc=4):
#     """ """
#     nb_proc = max(min(len(os.sched_getaffinity(0))-1, nb_proc), 1)
#     pool = multiprocessing.Pool(nb_proc)
#     results = [pool.map(partial(multi_cldidx, indice_list), process_list)]
#     pool.close()
#     pool.join()
#     return True

# def multi_cld(l2a_identifier):
#     l2a = L2aProduct(l2a_identifier)
#     try:
#         l2a.process_cloud_mask_v2()
#     except Exception:
#         pass

# def cld_multiprocessing(process_list, nb_proc=4):
#     """ """
#     nb_proc = max(min(len(os.sched_getaffinity(0))-1, nb_proc), 1)
#     pool = multiprocessing.Pool(nb_proc)
#     results = [pool.map(multi_cld, process_list)]
#     pool.close()
#     pool.join()
#     return True


def multi_cld_ver_pro_iter_repro(l2a_ver_pro_iter_repro):
    l2a = L2aProduct(l2a_ver_pro_iter_repro[0])
    cm_version = l2a_ver_pro_iter_repro[1]
    probability = l2a_ver_pro_iter_repro[2]
    iterations = l2a_ver_pro_iter_repro[3]
    cld_shad = l2a_ver_pro_iter_repro[4]
    cld_med_prob = l2a_ver_pro_iter_repro[5]
    cld_hi_prob = l2a_ver_pro_iter_repro[6]
    thin_cir = l2a_ver_pro_iter_repro[7]
    reprocess = l2a_ver_pro_iter_repro[8]
    try:
        l2a.compute_cloud_mask(
            cm_version=cm_version,
            probability=probability,
            iterations=iterations,
            cld_shad=cld_shad,
            cld_med_prob=cld_med_prob,
            cld_hi_prob=cld_hi_prob,
            thin_cir=thin_cir,
            reprocess=reprocess,
        )
    except Exception:
        pass


def cld_version_probability_iterations_reprocessing_multiprocessing(
    process_list, nb_proc=4
):
    """ """
    nb_proc = max(min(len(os.sched_getaffinity(0)) - 1, nb_proc), 1)
    pool = multiprocessing.Pool(nb_proc)
    results = [pool.map(multi_cld_ver_pro_iter_repro, process_list)]
    pool.close()
    pool.join()
    return True


def multi_idx(l2a_id_idx):
    l2a_identifier = l2a_id_idx[0]
    indice = l2a_id_idx[1]
    reprocess = l2a_id_idx[2]
    nodata_clouds = l2a_id_idx[3]
    quicklook = l2a_id_idx[4]
    cm_version = l2a_id_idx[5]
    probability = l2a_id_idx[6]
    iterations = l2a_id_idx[7]
    cld_shad = l2a_id_idx[8]
    cld_med_prob = l2a_id_idx[9]
    cld_hi_prob = l2a_id_idx[10]
    thin_cir = l2a_id_idx[11]
    l2a = L2aProduct(l2a_identifier)
    try:
        l2a.compute_indice(
            indice=indice,
            reprocess=reprocess,
            nodata_clouds=nodata_clouds,
            quicklook=quicklook,
            cm_version=cm_version,
            probability=probability,
            iterations=iterations,
            cld_shad=cld_shad,
            cld_med_prob=cld_med_prob,
            cld_hi_prob=cld_hi_prob,
            thin_cir=thin_cir,
        )
    except Exception:
        pass


def idx_multiprocessing(
    process_list,
    nb_proc=4,
):
    """ """
    nb_proc = max(min(len(os.sched_getaffinity(0)) - 1, nb_proc), 1)
    pool = multiprocessing.Pool(nb_proc)
    results = [pool.map(multi_idx, process_list)]
    pool.close()
    pool.join()
    return True
