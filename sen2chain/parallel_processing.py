# coding: utf-8

import concurrent.futures
import urllib.request
import time

# from .tiles import Tile
# from .products import L1cProduct, L2aProduct
from sen2chain import Tile, L1cProduct, L2aProduct


def process(
    l1c_identifier,
    reprocess_l2a=False,
    reprocess_cloud_mask=False,
    indices_list=[],
    reprocess_indices=False,
    nodata_clouds=False,
    quicklook=False,
):
    """Process a list of products in paralle.

    :param l1c_identifier:
    :param reprocess_l2a:
    :param reprocess_cloud_mask:
    :param indices_list:
    :param reprocess_indices:
    :param nodata_clouds:
    :param quicklook:
    """

    try:
        L1cProduct(l1c_identifier).process_l2a(reprocess=reprocess_l2a)

        l2a_identifier = l1c_identifier.replace("1C_", "2A_")
        L2aProduct(l2a_identifier).process_cloud_mask(
            reprocess=reprocess_cloud_mask
        )
        L2aProduct(l2a_identifier).process_indices(
            indices_list=indices_list,
            nodata_clouds=nodata_clouds,
            quicklook=quicklook,
            reprocess=reprocess_indices,
        )
    except Exception as e:
        return "FAILED", e
    return "success", None


def parallel_processing(
    identifiers_list,
    max_workers=3,
    reprocess_l2a=False,
    reprocess_cloud_mask=False,
    indices_list=[],
    reprocess_indices=False,
    nodata_clouds=False,
    quicklook=False,
):
    """ """

    prods = [L1cProduct(p.identifier) for p in identifiers_list]

    with concurrent.futures.ProcessPoolExecutor(
        max_workers=max_workers
    ) as executor:
        future_to_mot = {
            executor.submit(process, p.identifier, **kwargs): p for p in prods
        }
        for future in concurrent.futures.as_completed(future_to_mot):
            identifier = future_to_mot[future]
            issue = future.result()
            print(
                "{} processing: {}. Errors: {}.".format(
                    identifier, issue[0], issue[1]
                )
            )


if __name__ == "__main__":

    kwargs = {
        "reprocess_l2a": False,
        "reprocess_cloud_mask": False,
        "indices_list": ["NDVI", "NDWIGAO", "NDWIMCP"],
        "nodata_clouds": True,
        "quicklook": True,
        "reprocess_indices": False,
    }

    identifiers_list = Tile("38LPM").l1c

    parallel_processing(
        identifiers_list=identifiers_list, max_workers=8, **kwargs
    )


# parallel(identifiers_list, process_indices
