Sen2Cor 2.9
============

.. role:: bash(code)
    :language: bash

* Download `Sen2Cor v2.09.00 <http://step.esa.int/main/snap-supported-plugins/sen2cor/sen2cor-v2-9/>`_ and put it to its final install directory.

* Make it executable :bash:`chmod +x Sen2Cor-02.09.00-Linux64.run`
	
* Install Sen2Cor :bash:`./Sen2Cor-02.09.00-Linux64.run`

* Update your configuration:

1. Move or symlink `ESACCI-LC package for SenCor module <https://earth.esa.int/eogateway/ftp/Sentinel-2/ESACCI-LC-L4-ALL-FOR-SEN2COR-2.10.tar.gz>`_ content to Sen2Cor directory:
  
.. code-block:: bash

    ln -s /path/to/ESACCI-LC-L4-LCCS-Map-300m-P1Y-2015-v2.0.7.tif Sen2Cor-02.09.00-Linux64/lib/python2.7/site-packages/sen2cor/aux_data/
    ln -s /path/to/ESACCI-LC-L4-WB-Map-150m-P13Y-2000-v4.0.tif Sen2Cor-02.09.00-Linux64/lib/python2.7/site-packages/sen2cor/aux_data/
    ln -s /path/to/ESACCI-LC-L4-Snow-Cond-500m-P13Y7D-2000-2012-v2.0 Sen2Cor-02.09.00-Linux64/lib/python2.7/site-packages/sen2cor/aux_data/


2. Create or symlink DEM directory
  
.. code-block:: bash
	
    ln -s /path/to/dem ~/sen2cor/2.9/dem

3. Update Sen2Cor config file :bash:`nano ~/sen2cor/2.9/cfg/L2A_GIPP.xml`
  
.. code-block:: html

	<DEM_Directory>dem/srtm</DEM_Directory>
	<DEM_Reference>http://data_public:GDdci@data.cgiar-csi.org/srtm/tiles/GeoTIFF/</DEM_Reference>
	<DEM_Terrain_Correction>FALSE</DEM_Terrain_Correction>  # If you don't want DEM terrain correction

4. Update Sen2Chain configuration file :bash:`nano ~/sen2chain_data/config/config.cfg`

.. code-block:: bash

    sen2cor_bashrc_path = /path/to/Sen2Cor-02.09.00-Linux64/L2A_Bashrc


* Of course you have to update Sen2Chain
