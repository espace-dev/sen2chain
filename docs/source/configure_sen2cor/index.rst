.. _configure_sen2cor:

===================================
Configure previous Sen2Cor versions
===================================
 
To retrieve information about the Sen2Cor version and process L1C products accordingly, Sen2chain uses Sen2Cor install paths listed in ``~/sen2chain_data/config/config.cfg``.

Therefore, please keep your Sen2Cor install directory names unchanged (ie Sen2Cor-02.0\*.0\*-Linux64) so that Sen2Chain finds this pattern.

If you have multiple Sen2Cor versions installed, your Sen2chain configuration file should look like so :

.. code-block:: bash

	[SEN2COR PATH]
	sen2cor_bashrc_paths = /home/thomas/Sen2Cor-02.10.00-Linux64/L2A_Bashrc
		/home/thomas/Sen2Cor-02.10.01-Linux64/L2A_Bashrc
		/home/thomas/Sen2Cor-02.09.00-Linux64/L2A_Bashrc



Sen2Chain will not override your Sen2Cor configuration file. 
In Sen2Cor configuration file ``~/sen2cor/2.11/cfg/L2A_GIPP.xml``, you have to directly edit DEM_Directory and DEM_Reference paths in order to auto-download and use DEM correction. 

If a DEM is used the land cover data should be used to improve Sen2Cor classification algorithms.

At ESPACE-DEV in La Reunion we still don't use the DEM correction as it seems not relevant in high shadow / steep slope environments (as in Reunion Island). 


.. toctree::
   :maxdepth: 2
   :caption: Versions:
   
   sen2cor_2_9
   sen2cor_2_10
