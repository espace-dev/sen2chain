=====================
Installation
=====================

Installing sen2chain
--------------------

This package requires Python 3, GDAL, OTB and `Sen2Cor <http://step.esa.int/main/third-party-plugins-2/sen2cor/>`_.

.. note:: The current version of sen2chain is not available on Windows (yet).


Prerequisites
^^^^^^^^^^^^^
GDAL

.. code-block:: console

    sudo apt install gdal-bin libgdal-dev python3-gdal
    
Orfeo Toolbox (OTB) and its python bindings
    
.. code-block:: console

    wget https://www.orfeo-toolbox.org/packages/OTB-6.6.1-Linux64.run
    chmod +x OTB-6.6.1-Linux64.run
    ./OTB-6.6.1-Linux64.run
    
    
As indicated in the `OTB installation 
<https://www.orfeo-toolbox.org/CookBook/Installation.html#linux-x86-64>`_, you need to modify some files to have functional OTB installation :

* To use Python3 you need in the file otbenv.profile modify ``lib/python`` into ``lib/python3``, for variable PYTHONPATH

* You need to add a symlink to libpython3.5m.so.rh-python35-1.0 to make it works. The method is described in the FAQ on the OTB installation page. Note that you can create this symlink regardless of your version of Python3 by pointing to the file libpython3.Xm.so..

* Finally, you must initialize the environment each time you launch a terminal with

.. code-block:: console

    source otbenv.profile
    
You can also indicate in your user's .profile the path to ``otbenv.profile`` (example : ``source /home/user/OTB-6.6.0-Linux64/otbenv.profile``)

Then you can install Python Numpy for OTB bindings
    
.. code-block:: console

    sudo apt install python3-numpy

Sen2Cor

.. code-block:: console

    wget http://step.esa.int/thirdparties/sen2cor/2.5.5/Sen2Cor-02.05.05-Linux64.run
    chmod +x Sen2Cor-02.05.05-Linux64.run && ./Sen2Cor-02.05.05-Linux64.run

Install sen2chain from sources
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Python pip

.. code-block:: console

    sudo apt install python3-pip
    
    
Git

.. code-block:: console

    sudo apt-get install git

Download Sen2Chain and install the python dependencies:

.. code-block:: console

    git clone https://framagit.org/jebins/sen2chain.git
    cd sen2chain
    pip3 install --user -e .


Configuration
-------------

The configuration file is located at: ``~/sen2chain_data/config/config.cfg``.
This file (and corresponding path) is only created after the first Sen2Chain import in python.

Configure Sentinel-2 images library
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

At first launch default folders are created in ``~/sen2chain_data/data``. You can change the settings by editing the configuration file.

Sen2cor configuration
^^^^^^^^^^^^^^^^^^^^^

The full path to the sen2cor bashrc file is needed to process L1C to L2A. 
e.g.: ``/home/user/Sen2Cor-02.05.05-Linux64/L2A_Bashrc``

Hubs logins
^^^^^^^^^^^
In order to dowload images, you need a login on the `PEPS <https://peps.cnes.fr/rocket/#/home/>`_  and `Scihub <https://scihub.copernicus.eu/dhus>`_ servers.

* The PEPS login and password must be stored in ``~/sen2chain_data/config/peps.txt`` like so:

.. code-block:: html

    username password

* The Scihub login and password are stored in the configuration file.
