==============
Indices
==============

Here are the radiometric indices available for computing in Sen2Chain. Indices can me masked by any of the 4 cloudmasks.

.. list-table::
    :widths: 50
    :header-rows: 0
    
    * - $$NDVI = {{NIR-RED}\over{NIR+RED}}$$
    * - $$NDWI(McFeeters) = {{GREEN-NIR}\over{GREEN+NIR}}$$
    * - $$NDWI(Gao) = {{NIR-SWIR}\over{NIR+SWIR}}$$
    * - $$MNDWI = {{GREEN-SWIR}\over{GREEN+SWIR}}$$
    * - $$NDRE = {{NIR-RedEdge}\over{NIR+RedEdge}}$$
    * - $$IRECI = {{NIR-R}\over{RE1/RE2}}$$
    * - $$Brightness\,Index\,Blue\,Green = \sqrt{{BLUE^{2} + GREEN^{2}}\over{2}}$$
    * - $$Brightness\,Index\,Red\,Near\,InfraRed = \sqrt{ {RED^{2} + NIR^{2}}\over{2} }$$
    * - $$Brightness\,Index\,Green\,Red = \sqrt{ {GREEN^{2}+ RED^{2}}\over{2}}$$
    * - $$Enhanced\,Vegetation\,Index = {2.5 * {( NIR - RED )}\over{( NIR + 6 * RED - 7.5 * BLUE ) + 1}}$$
    * - $$Normalized\,Burn\,Ratio = {{NIR-SWIR}\over{NIR+SWIR}}$$

To compute, call for compute_indices() Tile's method. Specify the indices and the Cloudmask version you want to mask them with, including the optional parameters of the cloudmask version (only 1 at a time)

.. code-block:: python

    Tile('40KCB').compute_indices(indices=['NDVI','BIBG','MNDWI'],cm_version='CM001', ...)

