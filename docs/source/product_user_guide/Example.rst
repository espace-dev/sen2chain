=========================
Quick Use Case
=========================

Now that Sen2Chain different products were explained, a simple use case can be introduced :

If we want to compute the radiometric indices NDVI and NDWIGAO on La Reunion Island (Tile '40KCB'), for January 2023 :

**First, download the corresponding L1C products :**

.. code-block:: python

    from sen2chain import Tile
    Tile('40KCB').get_l1c(start="2023-01-01",end="2023-01-30")
    
.. note::
    Tile.get_l1c() method uses EODAG to download products, and PEPS as a default EO data provider.

**Then, compute the corresponding L2A Product :**

.. code-block:: python

    Tile('40KCB').compute_l2a(date_min="2023-01-01",date_max="2023-01-30", nb_proc=6)
    
This method only computes missing products for the given tile. Reprocessing is available by setting the parameter reprocess to True.

**Then, choose and compute a cloudmask :**

Here, we go for Cloudmask CM004, with 1 iteration of erosion-dilatation of clouds, and other parameters to default=True

.. code-block:: python

    Tile('40KCB').compute_cloudmasks(cm_version='CM004', iterations = 1, date_min="2023-01-01", date_max="2023-01-30", nb_proc=6)
    
    
**Finally, compute NDVI and NDWIGAO and mask them with CM004 :**

.. code-block:: python

    Tile('40KCB').compute_indices(indices=['NDVI','NDWIGAO'], cm_version='CM004', iterations = 1, date_min="2023-01-01", date_max="2023-01-30", nb_proc=6)
    

Multiprocessing
****************

Depending on your infrastructures, Sen2Chain processes can be launched using multiprocessing. The parameter *nb_proc* lets you select the correct number of processors you need.

