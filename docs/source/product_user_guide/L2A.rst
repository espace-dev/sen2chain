=====================================
L1C & L2A
=====================================

L1C Product
-----------

Sentinel-2 Level-1C product is composed of 110x110 km2 tiles (ortho-images in UTM/WGS84 projection) where per-pixel radiometric measurements are provided in Top Of Atmosphere (TOA) reflectances along with the parameters to transform them into radiances.

Level-1C products are resampled with a constant Ground Sampling Distance (GSD) of 10, 20 and 60 m depending on the native resolution of the different spectral bands. 

Level-1C products additionally include quality information like Cloud Masks, and auxiliary data like ECMWF data (total column of ozone, total column of water vapour and mean sea level pressure) and CAMS (Copernicus Atmosphere Monitoring Service) aerosols content information.

Sen2Chain processes are all applied on the Tile object, from downloading a L1C Product to computing radiometric indices.

To download a L1C Product on a given Tile, use Sen2Chain's Tile.get_l1c() method :

.. code-block:: python
   
    from sen2chain import Tile
    Tile('40KCB').get_l1c(provider="peps", start="2023-01-01",end="2023-01-05")


L2A Product
------------

| Sen2Chain uses ESA's Sen2cor processing chain to process Top-Of-Atmosphere Level 1C input data into L2A Surface Reflectance products.

| Sen2Cor creates Bottom-Of-Atmosphere, optionally terrain- and cirrus corrected reflectance images; and additionally, Aerosol Optical Thickness-, Water Vapor-, Scene Classification Map and Quality Indicators for cloud and snow probabilities.

| Its output product format is equivalent to the Level 1C User Product: JPEG 2000 images, with three different resolutions, 60, 20 and 10 m.

To process L1C products into L2A, call for compute_l2a() Tile's method

.. code-block:: python

    Tile('40KCB').compute_l2a(...)
