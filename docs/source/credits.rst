=======
Credits
=======

Sen2chain was developped in the frame of Sentinel-2 Malaria project, funded by `CNES <https://cnes.fr/en>`_ (TOSCA 2017-2019), under the coordination of Vincent Herbreteau (`IRD <https://en.ird.fr>`_ - `UMR ESPACE-DEV <http://www.espace-dev.fr>`_) and Christophe Révillion (`Univ La Réunion <http://www.univ-reunion.fr/university-of-reunion-island>`_ - UMR ESPACE-DEV). This project aims to use Sentinel-2 imagery for malaria monitoring in Madagascar and South Africa.


.. image:: logos.png
    :align: center
    :alt: logos