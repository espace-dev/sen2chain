.. module:: sen2chain.jobs

=======
Jobs
=======

Constructor
************

.. autosummary:: 

    sen2chain.jobs.Jobs
    
.. autosummary::

    Jobs.remove
    
    
======================================================

======
Job
======

Constructor
************

.. autosummary:: 

    sen2chain.jobs.Job
    

Editing Job
*************

.. autosummary::

    Job.task_add
    Job.task_edit
    Job.task_remove
    Job.save
    
Managing Job
**************

.. autosummary::

    Job.get_cron_status
    Job.create_python_script
    Job.cron_enable
    Job.cron_disable
    Job.cron_remove
    
    
Run Job
********

.. autosummary::

	Job.run
   
 
===============================================

.. autoclass:: sen2chain.jobs.Jobs
    :members:

===============================================

.. autoclass:: sen2chain.jobs.Job
    :members:
    
    
    
    
