=====================
Indices
=====================

.. autosummary:: 

	sen2chain.indices.Ndvi
	sen2chain.indices.NdwiMcf
	sen2chain.indices.NdwiGao
	sen2chain.indices.Mndwi
	sen2chain.indices.Ndre
	sen2chain.indices.IRECI
	sen2chain.indices.BIGR
    sen2chain.indices.BIRNIR
    sen2chain.indices.BIBG
    sen2chain.indices.NBR
    sen2chain.indices.EVI
	
    
=================================================

NDVI
*************

.. autoclass:: sen2chain.indices.Ndvi
	:members:


=================================================

NDWIMCF
*************

.. autoclass:: sen2chain.indices.NdwiMcf
	:members:


=================================================

NDWIGAO
*************

.. autoclass:: sen2chain.indices.NdwiGao
    :members:

=================================================

MNDWI
*************

.. autoclass:: sen2chain.indices.Mndwi
	:members:


=================================================

NDRE
*************

.. autoclass:: sen2chain.indices.Ndre
	:members:


=================================================

IRECI
*************

.. autoclass:: sen2chain.indices.IRECI
	:members:


=================================================

BIGR
*************

.. autoclass:: sen2chain.indices.BIGR
	:members:


=================================================

BIRNIR
*************

.. autoclass:: sen2chain.indices.BIRNIR
	:members:


=================================================

BIBG
*************

.. autoclass:: sen2chain.indices.BIBG
	:members:


=================================================

EVI
*************

.. autoclass:: sen2chain.indices.EVI
	:members:


=================================================


.. automodule:: sen2chain.indices
   :members:
