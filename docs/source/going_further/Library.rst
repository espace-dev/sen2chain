Working on multiple tiles : Library
====================================

The _Library_ class offers the possibility to get information and perform some actions on all or a group of tiles.

Initialization:

.. code-block:: python

	>>> from sen2chain import Library
	>>> l = Library()

## Database information and management

### Information

- This function returns tiles in the L1C library folder

.. code-block:: python

	>> l.tiles_l1c

- This function returns tiles in the L2A library folder

.. code-block:: python

	>>> l.tiles_l2a

- This function returns tiles in the indices library folder

.. code-block:: python

	>>> l.indices

### Cleaning

The _clean_ function performs the _clean_lib_ function (See Tile Section/link) for each tile present in L1C database or in the provided list. Use the _remove = True_ parameter to effectively remove products (default value False)

.. code-block:: python

	>>> l.clean()    # All L1C database, nothing removed
	>>> l.clean(clean_list = ["40KCB", "38KND"])    # 2 tiles analysed, nothing removed
	>>> l.clean(clean_list = ["38KQE"], remove = True) # 1 tile analysed, error SAFE folders removed

## Computing products

### Computing L2A

You can compute L2A products for multiple provided tiles using multiprocessing

.. code-block:: python

	>>> # Compute all missing L2A products for the 2 provided tiles, 
	>>> # after the specified date, 
	>>> # and using 6 CPU cores
	>>> l.compute_l2a(tile_list = ["40KCB", "38KND"], 
                  date_min = "2020-01-01", 
                  nb_proc = 6)               

### Computing cloudmasks

You can compute cloudmasks products for multiple provided tiles using multiprocessing, with specific parameters (see Tile for description).

.. code-block:: python

	>>> l.compute_cloudmasks(tile_list = ["40KCB", "38KND"],
                         cm_version = "cm001",
                         probability = 1,
                         iterations = 5,
                         reprocess = False,
                         date_min = None,
                         date_max = None,
                         nb_proc = 4)

### Computing indices

You can compute index products for multiple provided tiles using multiprocessing, with specific parameters (see Tile for description).

.. code-block:: python

	>>> l.compute_indices(tile_list = ["40KCB"],
                      indice_list = ["NDVI", "NDWIGAO"],                        
                      reprocess = False,
                      nodata_clouds = True,
                      cm_version = "cm001",
                      probability = 1,
                      iterations = 5,
                      date_min = None,
                      date_max = "2021-12-31",
                      nb_proc = 4)

### Computing L1C and L2A quicklooks

You can compute quicklooks for multiple provided tiles for L1C and/or L2A products. If no tile is provided, whole L1C + L2A product database is used. You can set specific output QL resolution (default 750m/px) and specific output format (JPEG by default or TIFF).

.. code-block:: python

	>>> l.compute_ql(tile_list = ["40KCB"],
                 product_list = ["l1c", "l2a"],                        
                 resolution = 750,
                 jpg = True)
