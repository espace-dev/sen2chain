=====================
Time Series
=====================

Sen2chain latest functionality is Time Series Extraction. It allows to spatially extract masked radiometric indices from a spatial layer, like geojson or shapefile, and produce spatial statistics of these radiometric indices on the time period of your preferences. 

Make sure the radiometric indices were produced for the right cloudmask version, otherwise an empty file will be generated.

Multiprocessing is available to speed up calculations. Finally, output data is exported to csv format.

.. code-block:: python

    from sen2chain import TimeSeries
    ts=TimeSeries(vectors_file='/path/to/shapefile.shp',date_min=date_min,date_max=date_max,indices=['NDVI','NBR','BIGR'],cm_version='CM004',iterations =1)
    ....
    ....
    ts.to_csv(out_path='/path/out_path')

With this command, the data is not reprojected. Vector data must therefore be in WGS 84 (EPSG: 4326) for extraction to work properly.
    
