Setting up the virtual environement
======

| Since the latest versions of Ubuntu (> 23.04), it is highly recommended to use a virtual environment for Python to avoid conflicts and prevent breaking your system. So, it's best to use virtualization to install Sen2Chain properly. There are different virtual environments for Python. Here we present the procedure for venv. 

| First you need to install venv Python :

.. code-block:: bash
    
	~$ sudo apt install python3-venv
    
| Then create a venv workspace (called here venv_sen2chain) where Sen2Chain will be installed :

.. code-block:: bash

	~$ python3 -m venv --system-site-package ~/.venv_sen2chain

| The --system-site-package is important because it allows you to retrieve globally installed packages such as GDAL. The command will create a folder containing the Python packages for this virtual environment.

| Next, activate the new environment (the base name of command prompt will change):

.. code-block:: bash

	~$ source ~/.venv_sen2chain/bin/activate
	
| This virtual environment can then be deactivated as follows

.. code-block:: bash

	~$ deactivate

| Once the environment has been activated, the following python packages can be installed.

