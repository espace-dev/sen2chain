EODAG
======

| `EODAG <https://eodag.readthedocs.io/en/stable/>`_ is a command-line tool and a Python package for searching and downloading remotely sensed images while offering a unified API for data access regardless of the data provider. 

| The EODAG package can be installed with pip.


.. code-block:: bash
    
    ~$ python -m pip install eodag
    
Then, import eodag in a python console to initialize the package :

.. code-block:: python

    >>> from eodag import EODataAccessGateway
    >>> dag=EODataAccessGateway()
    >>> exit()
    
    
Hubs logins & EODAG configuration file  
=======================================

`Register <https://eodag.readthedocs.io/en/stable/getting_started_guide/register.html>`_ to PEPS, cop_data_space or any provider you deem relevant for your work, and get the required credentials (username/password).

| Follow the guidelines provided in `EODAG's documentation <https://eodag.readthedocs.io/en/stable/getting_started_guide/configure.html>`_ to configure the plugin. 
| You can create a dedicated configuration file or edit the default one in: 

.. code-block:: bash
    
    ~$ ~/.config/eodag/eodag.yml


.. code-block:: bash
	
	~$ PEPS :
		  priority: 1 # Highest number for your preferred provider, see more in eodag documentation 
		  api: 
			  credentials: 
				  username: "PLEASE_CHANGE_ME" # Your own username 
				  password: "PLEASE_CHANGE_ME" # Your own password

