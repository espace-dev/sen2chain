.. _sen2cor:

Sen2Cor Configuration
----------------------------------

.. note::
    If you want to process products from 2017 and before, you need to install previous versions of Sen2Cor and follow the guidelines :ref:`here <configure_sen2cor>`.

This section focuses on Sen2Cor 2.12 configuration. 

1. To configure sen2cor, you must first download the context data used for certain Sentinel-2 pre-processing steps here `ESACCI-LC package for SC module <https://earth.esa.int/eogateway/ftp/Sentinel-2/ESACCI-LC-L4-ALL-FOR-SEN2COR-2.10.tar.gz>`_

2. Then, move or create symlink to Sen2Cor directory:

.. code-block:: bash

	~$ ln -s /path/to/ESACCI-LC-L4-ALL-FOR-SEN2COR-2.10/ESACCI-LC-L4-LCCS-Map-300m-P1Y-2015-v2.0.7.tif /path/to/Sen2Cor-02.12.03-Linux64/lib/python2.7/site-packages/sen2cor/aux_data/
	~$ ln -s /path/to/ESACCI-LC-L4-ALL-FOR-SEN2COR-2.10/ESACCI-LC-L4-WB-Map-150m-P13Y-2000-v4.0.tif /path/to/Sen2Cor-02.12.03-Linux64/lib/python2.7/site-packages/sen2cor/aux_data/
	~$ ln -s /path/to/ESACCI-LC-L4-ALL-FOR-SEN2COR-2.10/ESACCI-LC-L4-Snow-Cond-500m-MONTHLY-2000-2012-v2.4 /path/to/Sen2Cor-02.12.03-Linux64/lib/python2.7/site-packages/sen2cor/aux_data/
	
2. Create or symlink DEM directory

.. code-block:: bash

	~$ ln -s /path/to/dem ~/sen2cor/2.12/dem
	
3. Update Sen2Cor configuration file ``nano ~/sen2cor/2.11/cfg/L2A_GIPP.xml``

.. code-block:: html

	<DEM_Directory>dem/srtm</DEM_Directory>
	<DEM_Reference>http://srtm.csi.cgiar.org/wp-content/uploads/files/srtm_5x5/TIFF/</DEM_Reference>
	<DEM_Terrain_Correction>FALSE</DEM_Terrain_Correction>  # If you don't want DEM terrain correction

4. Update Sen2Chain config file ``nano ~/sen2chain_data/config/config.cfg``

.. code-block:: bash

	~$ sen2cor_bashrc_paths = /path/to/Sen2Cor-02.12.03-Linux64/L2A_Bashrc
	
5. Then update Sen2Chain by using any method :

.. code-block:: python

    >>> from sen2chain import Tile
    >>> Tile('40KCB').info
    
