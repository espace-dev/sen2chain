Prerequisites
===============

Sen2Chain requires pip, GDAL, Git and Sen2Cor

pip
-----
.. code-block:: bash
    
    ~$ sudo apt install python3-pip

GDAL
-----
.. code-block:: bash
    
    ~$ sudo apt install gdal-bin libgdal-dev python3-gdal

Git
----
.. code-block:: bash

    ~$ sudo apt-get install git

Sen2cor 2.12.03
----------------

1. Download Sen2Cor v2.12.03

.. code-block:: bash

    ~$ wget https://step.esa.int/thirdparties/sen2cor/2.12.0/Sen2Cor-02.12.03-Linux64.run


2. Make it executable

.. code-block:: bash

    ~$ chmod +x Sen2Cor-02.12.03-Linux64.run
    

3. Install Sen2Cor 

.. code-block:: bash

    ~$ ./Sen2Cor-02.12.03-Linux64.run


.. note::
   Installation guide and requirements for previous versions of Sen2Chain and Sen2Cor are available :ref:`here <configure_sen2cor>`. Previous versions are needed to process imagery before 2018.


| You can add extra data input to Sen2Cor in order to improve some features of Sen2Chain. 
| These functions are not directly mobilized by Sen2Chain but for optimal operation you can add ESA landcover data and the SRTM. Please find the process at this :ref:`page <sen2cor>`.
