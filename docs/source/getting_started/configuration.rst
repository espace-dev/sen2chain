Sen2Chain
==========

Installation
--------------

Download Sen2Chain and install the python dependencies

.. code-block:: bash

    git clone https://framagit.org/espace-dev/sen2chain.git 
    cd sen2chain
    pip3 install --user -e .
    
    
Or install directly from Git :

.. code-block:: bash
    
    python3 -m pip install git+https://framagit.org/espace-dev/sen2chain.git
    
Once installed, import Sen2Chain in a python console to initialize the package :

.. code-block:: python

    from sen2chain import Tile
    exit()
    


Update Sen2Chain
-----------------

| Updates are regularly made on the processing chain, and can be easily retrieved with Git.
| Go to the install directory, pull the last updates from Framagit, and if necessary reinstall Sen2Chain (to get new requirement libraries).

.. code-block:: bash

	cd sen2chain 
	git pull 
	pip3 install -e .
	
	
Sen2Chain Configuration
-----------------------

After the first launch of Sen2Chain, default folders are created in ``~/sen2chain_data/data``


You can change the settings by editing the configuration file: ``nano ~/sen2chain_data/config/config.cfg``

.. code-block:: bash

	# Directory paths that will receive the different products from the processing steps of Sen2Chain.
	[DATA PATHS]
	temp_path = /default/path/sen2chain_data/data/TEMP
	l1c_path = /default/path/sen2chain_data/data/L1C
	l1c_archive_path = /default/path/sen2chain_data/data/L1C_ARCHIVE
	l2a_path = /default/path/sen2chain_data/data/L2A
	l2a_archive_path = /default/path/sen2chain_data/data/L2A_ARCHIVE
	indices_path = /default/path/sen2chain_data/data/INDICES
	time_series_path = /default/path/sen2chain_data/data/TIME_SERIES
	temporal_summaries_path = /default/path/sen2chain_data/data/TEMPORAL_SUMMARIES
	cloudmasks_path = /default/path/sen2chain_data/data/CLOUDMASKS
	extraction_path = /default/path/sen2chain_data/data/EXTRACTION

	# Path to L2A_Bashrc file of your Sen2Cor installation
	[SEN2COR PATH]
	sen2cor_bashrc_path = /default/path/Sen2Cor-02.10.01-Linux64/L2A_Bashrc
	sen2cor_alternative_bashrc_path = /default/path/older_sen2cor/L2A_Bashrc

	# Path for Jobs routine logs
	[LOG PATH]
	log_path = /default/path/sen2chain_data/logs


**Sen2Chain is now ready to run!**
