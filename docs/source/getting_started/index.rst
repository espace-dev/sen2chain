===========================
Getting started
===========================

Why sen2chain, how to install, configure it, go!


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   my_install
   venv_python
   eodag
   configuration
   sen2cor
   
   
