=====================
Quickstart
=====================

Managing Sentinel-2 images
--------------------------


Library
^^^^^^^

.. code-block:: python

Lists tiles in your library 

  >>> from sen2chain import Library
  >>> Library().l1c
  ['40KCB', '38KQV', '38KQB']
  >>> Library().l2a
  ['38KQV', '38KQB']

Tiles
^^^^^

.. code-block:: python

  >>> from sen2chain import Tile
  >>> reunion = Tile("40KCB")
  >>> reunion.l1c
  LISTE DE PRODUITS
  >>> reunion.l2a.last.date)
  datetime.datetime(2018, 9, 4, 6, 35, 11)
  >>> reunion.l2a.filter_dates("2018-01-01")
  >>> reunion.l2a.filter_dates("2018-01-01").filter_clouds(0,20)
  [TileProduct(identifier='S2A_MSIL2A_20161003T070232_N0204_R120_T38KQB_20161003T070230_NDVI_CLOUD_MASK.tif', date=datetime.datetime(2016, 10, 3, 7, 2, 32), cloud_cover=8.7407)]
  >>> reunion.ndvi.filter_clouds(0,20)
  >>> reunion.l2a_missings
  [TileProduct(identifier='S2A_MSIL1C_20180904T063511_N0206_R134_T40KCB_20180904T090928.SAFE', date=datetime.datetime(2018, 9, 4, 6, 35, 11), cloud_cover=28.3281),
  TileProduct(identifier='S2A_OPER_PRD_MSIL1C_PDMC_20160331T090121_R134_V20160328T063507_20160328T063507.SAFE', date=datetime.datetime(2016, 3, 28, 6, 35, 7), cloud_cover=1.273775),
  TileProduct(identifier='S2A_OPER_PRD_MSIL1C_PDMC_20160119T023918_R134_V20160118T063524_20160118T063524.SAFE', date=datetime.datetime(2016, 1, 18, 6, 35, 24), cloud_cover=33.5),


Products
^^^^^^^^
Makes it easier to work with products metadata.

.. code-block:: python

    >>> from sen2chain import L1cProduct
    >>> l1c = L1cProduct("S2A_MSIL1C_20170111T070211_N0204_R120_T38KQV_20170111T070319.SAFE")
    >>> l1c = L1cProduct("S2A_OPER_PRD_MSIL1C_PDMC_20170505T161637_R134_V20151129T063512_20151129T063512.SAFE", tile="40KCB")
    >>> l1c.tile
    >>> l1c.sensing_start_time
    >>> l1c.footprint
    >>> l1c.b04
    
.. code-block:: python

    >>> from sen2chain import L2aProduct
    >>> l2a = L2aProduct("")
    >>> l2a.cloud_coverage_assessment
    

Downloading and processing Sentinel-2 Images
--------------------------------------------
Downloading
^^^^^^^^^^^
.. code-block:: python

    >>> from sen2chain import DataRequest, DownloadAndProcess
    >>> request = DataRequest().from_tiles(["40KCB", "40KEC"])
    >>> request = DataRequest().from_point()
    >>> request = DataRequest().from_bbox()
    >>> request = DataRequest().from_file("study_sites.shp")
    >>> DownloadAndProcess(request)

Processing
^^^^^^^^^^

2A level processing
^^^^^^^^^^^^^^^^^^^
Sen2cor

.. code-block:: python

    >>> l2a = L2aProduct("")
    >>> l2a.process_l2a()

Cloud mask and radiometric indices
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    >>> l2a.process_cloud_mask(buffering=True)
    >>> l2a.process_indices(indices=["NDVI", "NDVI_GAO"], clouds_nodata=True, colormap=True)
    
    
Time Series
^^^^^^^^^^^

.. code-block:: python

    >>> from sen2chain import TimeSeries
    >>> ts = Timeries(polygons.geojson, indices=["NDVI"], date_min="2018-01-01", date_max"2018-01-31", cover_min=0, cover_max=20)
    >>> ts.data
    >>> ts.to_csv()


Automatization
--------------

.. code-block:: python
    
    >>> auto = Automatization()
    >>> auto.run()
    

.. code-block:: python
    
    >>> auto_ts = TimeSeriesAutomatization()
    >>> auto_ts.run()
    
    