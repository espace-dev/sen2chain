.. Sen2Chain documentation master file, created by
   sphinx-quickstart on Fri Jan  6 15:30:06 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sen2Chain documentation!
=====================================

| Sen2Chain is a simple utility written in Python 3 to download and process Sentinel-2 images. The tool can also be used to automate these processes.
| It uses `EODAG <https://eodag.readthedocs.io/en/stable/>`_ package to find and download data, and `ESA's Sen2Cor <https://step.esa.int/main/snap-supported-plugins/sen2cor/>`_ processor to perform atmospheric, terrain and cirrus correction.

| Main features:

- Downloading of L1C (top of atmosphere) products from available providers;
- Processing of L1C to L2A (bottom of atmosphere) products;
- Computing of cloud masks and radiometric indices;
- Time series extraction of thoses indices in regions of interest;
- Full automatization of the above features.

All these features can be launched in multiprocessing.

Contributing
************
**Scientific projects :**

TOSCA S2-Malaria project, funded by CNES (TOSCA 2017-2020);
INTERREG Renovrisk-impact project (2018-2020).

**Development and improvements :**

Pascal Mouquet, Thomas Germain, Jérémy Commins, Charlotte Wolff, Juliette Miranville, George Ge

**Conceptualization and Coordination :**

Vincent Herbreteau, Christophe Révillion

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   getting_started/index
   going_further/index
   product_user_guide/index
   package_content

.. toctree::
   :maxdepth: 2
   :caption: Extra:
  
   configure_sen2cor/index
   contribute
   [Git repository] <https://framagit.org/espace-dev/sen2chain>
   
.. image:: ird_logo.png
    :width: 24%
.. image:: esdev3.PNG
    :width: 24%
.. image:: univ_reunion_logo.jpg
    :width: 24%
.. image:: cnes_logo.png
    :width: 24%
