.. module:: sen2chain.tiles

=====
Tile
=====

Constructor
************

.. autosummary::

	sen2chain.tiles.Tile
	
Downloading L1C
****************      

.. autosummary::

	Tile.get_l1c
	
Processing Tile product:
*************************

.. autosummary::

	Tile.compute_l2a
	Tile.compute_cloudmasks
	Tile.compute_indices
	Tile.compute_ql
	Tile.update_latest_ql
	Tile.move_old_quicklooks
	Tile.update_old_cloudmasks
	Tile.update_old_indices
		
Managing Tile library
*************************
   
.. autosummary::

	Tile.l1c
    Tile.l2a
    Tile.cloudmasks
    Tile.l1c_missings
	Tile.l2a_missings
	Tile.l1c_processable
	Tile.cloudmasks_missing
	Tile.missing_indices
    Tile.info
    Tile.size
    Tile.clean_lib
	Tile.archive_l1c
    Tile.archive_l2a
    Tile.archive_all
    Tile.remove_l1c
    Tile.remove_l2a
    Tile.init_md

===========================================
	
.. autoclass:: sen2chain.tiles.Tile
	:members:
	
	
