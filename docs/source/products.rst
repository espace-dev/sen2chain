.. module:: sen2chain.products

========
Products
========

Product Class
**************

.. autosummary:: 
	
	sen2chain.products.Product


.. autosummary:: 
	
	sen2chain.products.L1cProduct


.. autosummary:: 
	
	sen2chain.products.L2aProduct
	
	
.. autosummary:: 
	
	sen2chain.products.NewCloudMaskProduct


.. autosummary:: 
	
	sen2chain.products.IndiceProduct
	
	
=================================================

Product Class
*************

.. autoclass:: sen2chain.products.Product
	:members:


=================================================

L1C Product Class
*****************

.. autoclass:: sen2chain.products.L1cProduct
	:members:
	

=================================================

L2A Product Class
*****************

.. autoclass:: sen2chain.products.L2aProduct
	:members:

=================================================

Cloudmask Product Class
***********************

.. autoclass:: sen2chain.products.NewCloudMaskProduct
	:members:


=================================================

Indice Product Class
********************

.. autoclass:: sen2chain.products.IndiceProduct
	:members:

