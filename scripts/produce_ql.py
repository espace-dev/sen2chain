#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 09:07:29 2018

Script pour mettre à jour les latest ql 

@author: impact
"""
from sen2chain import Library

Library().update_latest_ql()
