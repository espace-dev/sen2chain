#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 09:07:29 2018

Script pour extraire un shapefile des tuiles de la librairie

@author: impact
"""
from sen2chain import full_db_2_shp, Config
from pathlib import Path

full_db_2_shp(
    Path(
        Config().get("extraction_path"),
        "SEN2EXTRACT", 
        "SHP",
    ),
    cm_version = "CM004",
    probability = 1,
    iterations = 1,
    cld_shad = True,
    cld_med_prob = True,
    cld_hi_prob = True,
    thin_cir = True,
)
