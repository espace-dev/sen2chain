**Bug description**
<!--- A clear and concise description of what the bug is. -->

**Steps to reproduce**
<!--- Steps to reproduce the behavior -->
1.
2.
3.
4.

**System description**
<!--- Operating system, Python and Sen2Chain versions, additional environment context -->


