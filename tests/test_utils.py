# -*- coding: utf-8 -*-

import sys
import os
import pytest
import datetime

sys.path.insert(0, os.path.abspath('..'))

from sen2chain import utils
from sen2chain.utils import DATES_FORMATS


def test_format_word():
    string = utils.format_word("  STrinG ")
    assert string == "string"

    with pytest.raises(TypeError):
        utils.format_word(10)


def test_grouper():
    iterable = ("a", "b", "c", "d", "e", "f")
    group = utils.grouper(iterable, 2)
    assert list(group) == [("a", "b"), ("c", "d"), ("e", "f")]


def test_str_to_datetime_ymd():
    date_format = "ymd"
    date_datetime = datetime.datetime(2018, 1, 3, 0, 0, 0)
    assert date_datetime == utils.str_to_datetime("2018-01-03", date_format)

    with pytest.raises(ValueError):
        utils.str_to_datetime("20180103", date_format)
        utils.str_to_datetime("20180103T081321", date_format)
        utils.str_to_datetime("2018-01-03T08:13:21.026Z", date_format)


def test_str_to_datetime_filename():
    date_format = "filename"
    date_datetime = datetime.datetime(2018, 1, 3, 8, 13, 21)
    assert date_datetime == utils.str_to_datetime("20180103T081321", date_format)

    with pytest.raises(ValueError):
        utils.str_to_datetime("20180103", date_format)
        utils.str_to_datetime("2018-01-03", date_format)
        utils.str_to_datetime("2018-01-03T08:13:21.026Z", date_format)


def test_str_to_datetime_metadata():
    date_format = "metadata"
    date_datetime = datetime.datetime(2018, 1, 3, 8, 13, 21, 26000)
    assert date_datetime == utils.str_to_datetime("2018-01-03T08:13:21.026Z", date_format)

    with pytest.raises(ValueError):
        utils.str_to_datetime("20180103", date_format)
        utils.str_to_datetime("2018-01-03", date_format)
        utils.str_to_datetime("20180103T081321", date_format)



