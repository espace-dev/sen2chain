# -*- coding: utf-8 -*-

import os
import datetime
from pathlib import Path
from pprint import pprint

from sen2chain import Tile, L1cProduct, L2aProduct


INPUT_FOLDER = Path(os.path.join(os.path.dirname(os.path.realpath(__file__)), "inputs"))
OUTPUT_FOLDER = Path(os.path.join(os.path.dirname(os.path.realpath(__file__)), "outputs"))


def test_tiled_l1c_2017():
    """
    """
    p = L1cProduct("S2A_MSIL1C_20170611T063511_N0205_R134_T40KCB_20170611T063510", path=INPUT_FOLDER)
    b04 = p.b04
    cloud_cover = p.cloud_coverage_assessment

    assert "T40KCB_20170611T063511_B04.jp2" in b04
    assert cloud_cover == "4.909099999999999"


def test_non_tiled_l1c_2016():
    """
    """
    p = L1cProduct("S2A_OPER_PRD_MSIL1C_PDMC_20160119T023918_R134_V20160118T063524_20160118T063524.SAFE", tile="40KCB", path=INPUT_FOLDER)
    b04 = p.b04
    cloud_cover = p.cloud_coverage_assessment

    assert "S2A_OPER_MSI_L1C_TL_MTI__20160118T095515_A002991_T40KCB_B04.jp2" in b04
    assert cloud_cover == "33.5"


def test_non_tiled_l1c_2015():
    """
    """
    p = L1cProduct("S2A_OPER_PRD_MSIL1C_PDMC_20161212T050749_R134_V20151020T063512_20151020T063512.SAFE", tile="40KCB", path=INPUT_FOLDER)
    b04 = p.b04
    cloud_cover = p.cloud_coverage_assessment

    assert "S2A_OPER_MSI_L1C_TL_EPA__20161210T102618_A001704_T40KCB_B04.jp2" in b04
    assert cloud_cover == "35.00729115928188"


def test_tiled_l2a_2017():
    """
    """
    p = L2aProduct("S2A_MSIL2A_20170611T063511_N0205_R134_T40KCB_20170611T063510", path=INPUT_FOLDER)
    b04 = p.b04_10m
    cloud_cover = p.cloud_coverage_assessment

    assert "L2A_T40KCB_20170611T063511_B04_10m.jp2" in b04
    assert cloud_cover == "7.600021"


def test_non_tiled_l2a_product_2016():
    """
    """
    p = L2aProduct("S2A_USER_PRD_MSIL2A_PDMC_20160119T023918_R134_V20160118T063524_20160118T063524.SAFE", tile="40KCB", path=INPUT_FOLDER)
    b04 = p.b04_10m
    cloud_cover = p.cloud_coverage_assessment

    assert "S2A_USER_MSI_L2A_TL_MTI__20160118T095515_A002991_T40KCB_B04_10m.jp2" in b04
    assert cloud_cover == "75.420268"


#p = L2aProduct("S2A_USER_PRD_MSIL2A_PDMC_20160725T115341_R120_V20160725T070238_20160725T070238.SAFE", tile="38KQV", path=INPUT_FOLDER)
p = L2aProduct("S2B_MSIL2A_20180920T085619_N0206_R007_T33KUA_20180920T124627.SAFE", tile="33KUA", path=INPUT_FOLDER)
#print(p.cld_20m)
#print(p.b04_10m)
#print(Path(p.cld_20m).exists())
