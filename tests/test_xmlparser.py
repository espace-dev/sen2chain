# -*- coding: utf-8 -*-

import os
import xml
from pathlib import Path

from sen2chain import L1cProduct, L2aProduct
from sen2chain.xmlparser import MetadataParser


INPUT_FOLDER = Path(os.path.join(os.path.dirname(os.path.realpath(__file__)), "inputs"))
OUTPUT_FOLDER = Path(os.path.join(os.path.dirname(os.path.realpath(__file__)), "outputs"))

# L1C

def test_get_root_non_tiled_l1c_2016():
    """
    """
    p = L1cProduct("S2A_OPER_PRD_MSIL1C_PDMC_20160119T023918_R134_V20160118T063524_20160118T063524.SAFE", tile="40KCB", path=INPUT_FOLDER)
    p._get_metadata_path()

    mtd = p._metadata_path
    parser = MetadataParser(mtd, tile=p.tile)
    root = parser._root

    assert isinstance(root, xml.etree.ElementTree.Element)


def test_get_root_tiled_l1c_2016():
    """
    """
    p = L1cProduct("S2A_MSIL1C_20170611T063511_N0205_R134_T40KCB_20170611T063510.SAFE", path=INPUT_FOLDER)
    p._get_metadata_path()

    mtd = p._metadata_path
    parser = MetadataParser(mtd, tile=p.tile)
    root = parser._root

    assert isinstance(root, xml.etree.ElementTree.Element)


def test_get_root_tiled_l1c_2017():
    """
    """
    p = L1cProduct("S2A_MSIL1C_20170611T063511_N0205_R134_T40KCB_20170611T063510.SAFE", path=INPUT_FOLDER)
    p._get_metadata_path()

    mtd = p._metadata_path
    parser = MetadataParser(mtd, tile=p.tile)
    root = parser._root

    assert isinstance(root, xml.etree.ElementTree.Element)



# L2A


def test_get_root_tiled_l2a_2017():
    """
    """
    p = L2aProduct("S2A_MSIL2A_20170611T063511_N0205_R134_T40KCB_20170611T063510.SAFE", path=INPUT_FOLDER)
    p._get_metadata_path()

    mtd = p._metadata_path
    parser = MetadataParser(mtd, tile=p.tile)
    root = parser._root

    assert isinstance(root, xml.etree.ElementTree.Element)




#p = L2aProduct("S2A_USER_PRD_MSIL2A_PDMC_20160119T023918_R134_V20160118T063524_20160118T063524.SAFE", tile="40KCB", path=INPUT_FOLDER)
#p._get_metadata_path()
#mtd = p._metadata_path
#parser = MetadataParser(mtd, tile=p.tile)
#root = parser._root
#print(root)
#print(p.cloud_coverage_assessment)

#p = L2aProduct("S2A_USER_PRD_MSIL2A_PDMC_20161212T050749_R134_V20151020T063512_20151020T063512.SAFE", tile="40KCB", path=INPUT_FOLDER)
#p._get_metadata_path()
#mtd = p._metadata_path
#parser = MetadataParser(mtd, tile=p.tile)
#root = parser._root
#print(root)
#print(p.cloud_coverage_assessment)


#p = L2aProduct("S2A_MSIL2A_20170611T063511_N0205_R134_T40KCB_20170611T063510.SAFE", path=INPUT_FOLDER)
p = L2aProduct("S2A_USER_PRD_MSIL2A_PDMC_20160725T115341_R120_V20160725T070238_20160725T070238.SAFE", tile="38KQV", path=INPUT_FOLDER)
#p = L2aProduct("S2B_MSIL2A_20180920T085619_N0206_R007_T33KUA_20180920T124627.SAFE", tile="33KUA", path=INPUT_FOLDER)
p._get_metadata_path()
mtd = p._metadata_path
parser = MetadataParser(mtd, tile=p.tile)
#parser._get_psd()
#root = parser._root


print(parser._image_string)
print(parser.tile)
print(parser._safe_path)
print(parser._root)
print(parser._psd)
print(parser._granule_string)
print(parser._granule)


#print(Path(parser.get_band_path("CLD")).exists())
#print(Path(parser.get_band_path("CLD")).exists())
#print(parser.get_band_path("CLD"))
#print(parser.get_band_path("SNW"))

#print([v.text for v in parser._root.findall(".//{0}".format(parser._image_string))][0].split("/"))

pattern = "CLD"

print([str(f.text) + ".jp2"
       for f in parser._root.findall(".//{}[@granuleIdentifier='{}']/{}".format(parser._granule_string, parser._granule, parser._image_string))
       if pattern in f.text][0])
