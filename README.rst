Sen2Chain
=========


Sen2Chain is a simple utility written in Python 3 to download and process Sentinel-2 images.
It uses EODAG package to find and download data, and ESA’s Sen2Cor processor to perform atmospheric, terrain and cirrus correction.

Main features:

* Downloading of L1C (top of atmosphere) products from EODAG available providers;
* Processing of L1C to L2A (bottom of atmosphere) products;
* Computing of radiometric indices;
* Time series extraction of thoses indices;
* Full automatization of the above features.

All these features can be launched in multiprocessing.

Documentation
------------

Documentation for the Sen2Chain tool is available on the Read the Docs platform. It explains how to install and configure the processing chain. It also explains how to use the tool's main functions.
Please follow this `link <https://sen2chain.readthedocs.io>`_.

Contributing
------------

Scientific projects :

* TOSCA S2-Malaria project, funded by CNES (TOSCA 2017-2020); 
* INTERREG Renovrisk-impact project (2018-2020).

Development and improvment :

* Pascal Mouquet ;
* Julie Mollies
* Thomas Germain ;
* Jérémy Commins ;
* Charlotte Wolff.

Conceptualization and Coordination :

* Vincent Herbreteau ;
* Christophe Révillion.

.. image:: docs/source/esdev4.png
    :align: center
    :alt: logos

This project is open to anyone willing to help. You can contribute by adding new features, reporting and fixing bugs, or propose new ideas.

Funding
-------

.. image:: docs/source/logo_framagit_funding.JPG
    :align: center
    :alt: logos

License
-------

GPLv3+
