# -*- coding:utf-8 -*-

"""
    Download L1C products from a tile and dates list provided by a csv file
    
    peps and scihub hub limits can be adjusted (line 53)
    - usually PEPS is faster for recent data (limit max 8)
    - scihub is better for a few months old products (limit max 2)
    
"""

import logging
import pandas as pd
from sen2chain import DataRequest, DownloadAndProcess, Tile
import datetime
import os
import time
import math

logger = logging.getLogger("Downloading L1C")
logging.basicConfig(level=logging.INFO)

fwd = os.path.dirname(os.path.realpath(__file__))

# Path to csv process list
tiles_file = fwd + "/download_tiles.csv"

# default number of days to check before today 
# if this field is empty in the csv file
delta_t = 15

tiles_list = pd.read_csv(tiles_file, sep = ';', na_values="", comment='#')

for index, row in tiles_list.iterrows():
    try:
        if math.isnan(row.start_time):
            row.start_time = (datetime.datetime.now()-datetime.timedelta(days=delta_t)).strftime('%Y-%m-%d')
    except Exception:
        pass
    try:
        if math.isnan(row.end_time):
            row.end_time = (datetime.datetime.now()+datetime.timedelta(days=1)).strftime('%Y-%m-%d')
    except Exception:
        pass

    tuile = Tile(row.tile)
    req = DataRequest(start_date=row.start_time, end_date=row.end_time).from_tiles([row.tile])
    DownloadAndProcess(req, hubs_limit={"peps":0, "scihub":8}, process_products=False, indices_list=[], nodata_clouds=False, quicklook=False, max_processes=8)
    time.sleep(1)
    tuile = Tile(row.tile)

