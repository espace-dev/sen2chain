# -*- coding:utf-8 -*-

#~ Starts processing on L1C that have been downloaded 
#~ but have not been processed
#~ More examples and details on the sen2chain wiki : https://framagit.org/jebins/sen2chain/wikis/Home

from sen2chain import Tile, L1cProduct, L2aProduct


def process_missings(tile):
    """
    Processing function
    """
    t = Tile(tile)
    for p in t.l2a_missings:
        l1c = L1cProduct(p.identifier, t.name)
        try:
            l1c.process_l2a()
        except Exception:
            print("FAILED:", p.identifier)
            continue
                
    for p in t.l2a:
        l2a = L2aProduct(p.identifier, t.name)
        try:
            l2a.process_cloud_mask_v2()
#~ list of indices to be caculated (available : NDVI/NDWIGAO/NDWIMCF/BIGR/BIRNIR/BIBG),
#~ And calculation of clouds_nodata (True/False) and quicklook (True/False) 
            l2a.process_indices(["NDVI","NDWIGAO","NDWIMCF","MNDWI"], True, True)
        except Exception:
            print("FAILED:", p.identifier)
            continue

# list of tiles to be processed (Reunion Island example)
tiles = ["40KCB"]

for t in tiles:
    print("Processing:", t)
    process_missings(t)
