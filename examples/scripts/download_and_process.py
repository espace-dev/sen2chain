# -*- coding:utf-8 -*-

#~ Download and process Sentinel 2 images
#~ More examples and details on the sen2chain wiki : https://framagit.org/jebins/sen2chain/wikis/Home 


from sen2chain import DataRequest, DownloadAndProcess

#~ list of tiles to be processed (Reunion Island example)

tiles = ["40KCB"]

#~ Times Period (here the request cover August 2016)

req = DataRequest(start_date="2019-06-01", end_date="2019-06-19").from_tiles(tiles)

#~ The images can be downloaded in parallel from the Scihub and PEPS servers. 
#~ The number of concurrent downloads can't exceed 2 on Scihub and 8 on PEPS.
#~ If process_products is True you could calculate different product :
#~ a list of indices (available : NDVI/NDWIGAO/NDWIMCF/BIGR/BIRNIR/BIBG),
#~ And clouds_nodata (True/False) and quicklook (True/False)

DownloadAndProcess(req, hubs_limit={"peps":1, "scihub":0}, process_products=True, indices_list=["NDVI"], nodata_clouds=False, quicklook=False)
