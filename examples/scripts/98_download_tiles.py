# -*- coding:utf-8 -*-
"""
 Télécharge les nouvelles tuiles identifiées ds le fichier tiles_file.csv
 Le delta_t permet d'alléger les requètes au serveur en n'interrogeant que les n derniers jours
 Ce but de ce script est d'être lancé très régulièrement (cron)
"""

import logging
import pandas as pd
from sen2chain import DataRequest, DownloadAndProcess, Tile
import datetime
import os, shutil
import time
import glob
import math
import telegram

logger = logging.getLogger("Telechargement L1C")
logging.basicConfig(level=logging.INFO)

#cwd = os.getcwd()
fwd = os.path.dirname(os.path.realpath(__file__))

# liste de tuiles à traiter
tiles_file = fwd + "/98_download_tiles.csv"

# default nombre de jours à requeter à partir de today
delta_t = 15

# recuperation de la liste des tuiles a telecharger
tiles_list = pd.read_csv(tiles_file, sep = ';', na_values="", comment='#')

total = 0
logger.info("Total tiles to download = {}".format(total))
for index, row in tiles_list.iterrows():
    try:
        if math.isnan(row.start_time):
            row.start_time = (datetime.datetime.now()-datetime.timedelta(days=delta_t)).strftime('%Y-%m-%d')
    except Exception:
        pass
    try:
        if math.isnan(row.end_time):
            row.end_time = (datetime.datetime.now()+datetime.timedelta(days=1)).strftime('%Y-%m-%d')
    except Exception:
        pass
    
    # Pour ponctuellement forcer les dates de début et fin
    #~ row.start_time = "2018-01-01"
    #~ end_time = "2018-08-06"
        
    tuile = Tile(row.tile)
    req = DataRequest(start_date=row.start_time, end_date=row.end_time, cloud_cover_percentage=[0, row.max_clouds]).from_tiles([row.tile])
    #~ logger.info("req = {}".format(req))
    total += len(req['hubs'])
    logger.info("Total tiles to download = {}".format(total))
    DownloadAndProcess(req, hubs_limit={"peps":8, "scihub":0}, process_products=False, indices_list=[], nodata_clouds=False, quicklook=False, max_processes=8)
    time.sleep(1)
    tuile = Tile(row.tile)

