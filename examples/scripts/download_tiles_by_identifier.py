# -*- coding:utf-8 -*-

""" 
    Download L1C product by identifier provided by a csv file
    
    peps and scihub hub limits can be adjusted (line 31)
    - usually PEPS is faster for recent data (limit max 8)
    - scihub is better for a few months old products (limit max 2)
    
"""

import logging
import pandas as pd
from sen2chain import DataRequest, DownloadAndProcess, Tile
import datetime
import os
import time

logger = logging.getLogger("L1C Downloading")
logging.basicConfig(level=logging.INFO)

fwd = os.path.dirname(os.path.realpath(__file__))

# Tile process list
tiles_file = fwd + "/download_tiles_by_identifier.csv"
tiles_list = pd.read_csv(tiles_file, sep = '_', na_values="", comment='#')

for index, row in tiles_list.iterrows():
    row.starttime = datetime.datetime.strptime(row.starttime, '%Y%m%dT%H%M%S').strftime('%Y-%m-%d')
    row.endtime = (datetime.datetime.strptime(row.starttime, '%Y-%m-%d') + datetime.timedelta(days=1)).strftime('%Y-%m-%d')
    row.tile = row.tile[1:]

    tuile = Tile(row.tile)
    req = DataRequest(start_date=row.starttime, end_date=row.endtime).from_tiles([row.tile])
    DownloadAndProcess(req, hubs_limit={"peps":0, "scihub":2}, process_products=False, indices_list=[], nodata_clouds=False, quicklook=False, max_processes=8)
    time.sleep(1)
    tuile = Tile(row.tile)

