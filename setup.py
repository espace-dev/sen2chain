# !#-*-coding:utf-8-*-
from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

with open("sen2chain/__init__.py", encoding="utf-8") as f:
    for line in f:
        if line.startswith("__version__"):
            version = line.split("=")[1].strip().strip('"')

with open(path.join(here, "README.rst"), encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="sen2chain",
    version=version,
    description="Download and process Sentinel-2 images",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Jérémy Commins",
    author_email="jebins@openmailbox.org",
    license="GPLv3+",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: Education",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: GIS",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: Unix",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    keywords="GIS sentinel copernicus",  # Optional
    packages=find_packages(exclude=["dev", "example", "docs", "tests"]),
    package_data={  # Optional
        "sen2chain": [
            "data/tiles_index.gpkg",
            "data/tiles_index_dict.p",
            "data/sen2chain_info.xml",
            "data/job_ini.cfg",
        ],
    },
    install_requires=open("requirements.txt").read().splitlines(),
)
